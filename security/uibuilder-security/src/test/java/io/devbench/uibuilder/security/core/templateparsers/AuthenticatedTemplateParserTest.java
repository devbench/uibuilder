/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.security.core.templateparsers;

import io.devbench.uibuilder.security.core.templateparsers.authenticated.AuthenticatedTemplateParser;
import io.devbench.uibuilder.test.annotations.LoadElement;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


class AuthenticatedTemplateParserTest extends TemplateParserTestBase {

    @LoadElement(value = "/security-template-tests.html", id = "authenticatedTest")
    private Element testElement;

    private AuthenticatedTemplateParser testObj = new AuthenticatedTemplateParser();

    @Test
    @DisplayName("Should replace authenticated nodes with a div containing the same elements as the authenticated mode, if the user is authenticated")
    public void should_replace_authenticated_nodes_with_a_div_containing_the_same_elements_as_the_authenticated_mode_if_the_user_is_authenticated() {
        when(securityService.isAuthenticated()).thenReturn(true);

        walkElementWithTemplateParser(testElement, testObj);

        assertEquals(
            normalizeHtml("<uibuilder-page id=\"authenticatedTest\">" +
                "    <div> Hello logged in user! </div>" +
                "    <div> Other content </div>" +
                "</uibuilder-page>"),
            normalizeHtml(testElement.outerHtml()));
    }

    @Test
    @DisplayName("Should remove authenticated nodes if the user is not logged in")
    void should_remove_authenticated_nodes_if_the_user_is_not_logged_in() {
        when(securityService.isAuthenticated()).thenReturn(false);

        walkElementWithTemplateParser(testElement, testObj);

        assertEquals(
            normalizeHtml("<uibuilder-page id=\"authenticatedTest\">\n" +
                "    <div>Other content</div>\n" +
                "</uibuilder-page>"),
            normalizeHtml(testElement.outerHtml())
        );
    }

}
