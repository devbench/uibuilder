/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.security.login;

import io.devbench.uibuilder.security.api.AfterLoginFunction;
import io.devbench.uibuilder.security.api.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DefaultLoginServiceImpl implements LoginService {

    public void login(String username, String password, AfterLoginFunction afterLogin) {
        Subject subject = getSubject();
        try {
            subject.login(createTokenWithUsernameAndPassword(username, password));
            afterLogin.isSucceedLogin(true);
        } catch (AuthenticationException e) {
            log.error("Login panel authentication failed", e);
            afterLogin.isSucceedLogin(false);
        }
    }
}
