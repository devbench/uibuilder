/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.security.shiro;

import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.security.core.flow.xml.AuthenticatedFlowElement;
import io.devbench.uibuilder.security.core.flow.xml.HasPermissionFlowElement;
import io.devbench.uibuilder.security.core.flow.xml.NotAuthenticatedFlowElement;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;


@Service
public class FlowElementsConfig implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        FlowManager.registerClassesToContext(
            HasPermissionFlowElement.class,
            AuthenticatedFlowElement.class,
            NotAuthenticatedFlowElement.class
        );
    }
}
