/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.core;

import io.devbench.uibuilder.i18n.core.dto.TranslationCatalogId;
import io.devbench.uibuilder.i18n.core.interfaces.LanguageProvider;
import io.devbench.uibuilder.i18n.core.interfaces.TimeZoneProvider;
import io.devbench.uibuilder.i18n.core.text.Java8DateFormatWrapper;
import io.devbench.uibuilder.i18n.core.util.TranslationLoader;
import io.devbench.uibuilder.i18n.core.wrappers.UnmodifiableCatalog;
import org.fedorahosted.tennera.jgettext.Catalog;
import org.fedorahosted.tennera.jgettext.Message;
import org.fedorahosted.tennera.jgettext.MessageHashKey;
import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A class which helps to translate strings in any application.
 */
public final class I {

    @NotNull
    private static LanguageProvider languageProvider = () -> null;

    @NotNull
    private static TimeZoneProvider timeZoneProvider = () -> null;

    @NotNull
    private static Map<TranslationCatalogId, Catalog> translationsByComponent = Collections.emptyMap();
    private static Map<Locale, Catalog> mergedTranslations = Collections.emptyMap();

    /**
     * Translates a message to the active locale.
     * <p>
     * If the message is not translated or unknown the original message will be returned.
     *
     * @param msgid the message to translate.
     * @return the message in the active locale.
     */
    public static String tr(String msgid) {
        return getTranslationFor(null, msgid).orElse(msgid);
    }

    /**
     * Translates a message to the active locale.
     * <p>
     * If the message is not translated or unknown the original message will be returned.
     *
     * @param msgctxt the context of the message.
     *                For more details see <a href="https://www.gnu.org/software/gettext/manual/html_node/Contexts.html">the gettext documentation</a>.
     * @param msgid   the message to translate.
     * @return the message in the active locale.
     */
    public static String trc(String msgctxt, String msgid) {
        return getTranslationFor(msgctxt, msgid).orElse(msgid);
    }

    /**
     * Translates a format string to the active locale and inserts the given parameters into the translated format string.
     * <p>
     * For the format string syntax see the JavaDoc of the {@link MessageFormat} class.
     *
     * @param msgid      the format string to translate.
     * @param parameters the parameters to include in the format string
     * @return the formatted message in the active locale
     */
    public static String trf(String msgid, Object... parameters) {
        String pattern = tr(msgid);
        return formatMessage(pattern, parameters);
    }

    /**
     * Translates a format string to the active locale and inserts the given parameters into the translated format string.
     * <p>
     * For the format string syntax see the JavaDoc of the {@link MessageFormat} class.
     *
     * @param msgctxt    the context of the message.
     *                   For more details see <a href="https://www.gnu.org/software/gettext/manual/html_node/Contexts.html">the gettext documentation</a>.
     * @param msgid      the format string to translate.
     * @param parameters the parameters to include in the format string
     * @return the formatted message in the active locale
     */
    public static String trfc(String msgctxt, String msgid, Object... parameters) {
        String pattern = trc(msgctxt, msgid);
        return formatMessage(pattern, parameters);
    }

    /**
     * Translates a format string to the active locale and inserts the given parameters into the translated format string.
     * <p>
     * This method cares about the plural forms. For more details see
     * <a href="https://www.gnu.org/software/gettext/manual/html_node/Plural-forms.html#Plural-forms">the gettext documentation</a>.
     * <p>
     * The parameter {@code}itemCount{@code} is not included into the parameters array. If you'd like to insert the value of
     * {@code}itemCount{@code} into the format parameters then you have to insert that value into the varargs parameter.
     * <p>
     * For the format string syntax see the JavaDoc of the {@link MessageFormat} class.
     *
     * @param singular   the singular format string to translate.
     * @param plural     the singular format string to translate.
     * @param itemCount  the count of items on which the function will define which format string to use
     * @param parameters the parameters to include in the format string
     * @return the formatted message in the active locale
     */
    public static String trfp(String singular, String plural, int itemCount, Object... parameters) {
        String pattern = getPluralTranslationFor(null, singular, itemCount)
            .orElseGet(() -> {
                if (itemCount == 1) {
                    return singular;
                } else {
                    return plural;
                }
            });

        return formatMessage(pattern, parameters);
    }

    /**
     * Translates a format string to the active locale and inserts the given parameters into the translated format string.
     * <p>
     * This method cares about the plural forms. For more details see
     * <a href="https://www.gnu.org/software/gettext/manual/html_node/Plural-forms.html#Plural-forms">the gettext documentation</a>.
     * <p>
     * The parameter {@code}itemCount{@code} is not included into the parameters array. If you'd like to insert the value of
     * {@code}itemCount{@code} into the format parameters then you have to insert that value into the varargs parameter.
     * <p>
     * For the format string syntax see the JavaDoc of the {@link MessageFormat} class.
     *
     * @param msgctxt    the context of the message.
     *                   For more details see <a href="https://www.gnu.org/software/gettext/manual/html_node/Contexts.html">the gettext documentation</a>.
     * @param singular   the singular format string to translate.
     * @param plural     the singular format string to translate.
     * @param itemCount  the count of items on which the function will define which format string to use
     * @param parameters the parameters to include in the format string
     * @return the formatted message in the active locale
     */
    public static String trfpc(String msgctxt, String singular, String plural, int itemCount, Object... parameters) {
        String pattern = getPluralTranslationFor(msgctxt, singular, itemCount)
            .orElseGet(() -> {
                if (itemCount == 1) {
                    return singular;
                } else {
                    return plural;
                }
            });

        return formatMessage(pattern, parameters);
    }

    private static Optional<String> getTranslationFor(String msgctxt, String msgid) {
        Locale locale = getActiveLocale();
        Optional<Message> message = findBestMatchingTranslationForMessageAndLocale(msgctxt, msgid, locale);
        return message
            .map(Message::getMsgstr)
            .filter(msg -> !msg.isEmpty());
    }

    private static Optional<Message> findBestMatchingTranslationForMessageAndLocale(String msgctxt, String msgid, Locale locale) {
        Set<Locale> possibleLocales = new HashSet<>(mergedTranslations.keySet());
        List<Locale.LanguageRange> languageRanges = Locale.LanguageRange.parse(locale.toLanguageTag() + ";q=1.0");
        Locale bestMatchingLocale = Locale.lookup(languageRanges, possibleLocales);
        Optional<Message> messageForLocale = Optional.empty();
        while (bestMatchingLocale != null && !messageForLocale.isPresent()) {
            Optional<Catalog> forLocale = Optional.ofNullable(mergedTranslations.get(bestMatchingLocale));
            messageForLocale = forLocale.map(it -> it.locateMessage(new MessageHashKey(msgctxt, msgid)));
            possibleLocales.remove(bestMatchingLocale);
            bestMatchingLocale = Locale.lookup(languageRanges, possibleLocales);
        }
        return messageForLocale;
    }

    private static Optional<String> getPluralTranslationFor(String msgctxt, String singular, int itemCount) {
        Locale locale = getActiveLocale();
        return findBestMatchingTranslationForMessageAndLocale(msgctxt, singular, locale)
            .map(message -> message.getMsgstrPlural().get(getCorrectMessageIdx(itemCount, message)))
            .filter(msg -> !msg.isEmpty());
    }

    private static int getCorrectMessageIdx(int itemCount, Message message) {
        int messageIdx = Math.min(itemCount, message.getMsgstrPlural().size()) - 1;
        if (messageIdx < 0) {
            messageIdx = message.getMsgstrPlural().size() - 1;
        }
        return messageIdx;
    }

    public static void loadTranslations(String pathToScan) {
        mergedTranslations = new HashMap<>();
        translationsByComponent = TranslationLoader.loadTranslations(pathToScan);
        translationsByComponent.forEach((catalogId, catalog) -> {
            mergedTranslations.putIfAbsent(catalogId.getLocale(), new Catalog());
            catalog.processMessages(message -> mergedTranslations.get(catalogId.getLocale()).addMessage(message));
        });
    }

    public static void setLanguageProvider(@NotNull LanguageProvider languageProvider) {
        I.languageProvider = Objects.requireNonNull(languageProvider);
    }

    public static void setTimeZoneProvider(@NotNull TimeZoneProvider timeZoneProvider) {
        I.timeZoneProvider = Objects.requireNonNull(timeZoneProvider);
    }

    public static Locale getActiveLocale() {
        return Optional.ofNullable(languageProvider.getActiveLocale()).orElseGet(Locale::getDefault);
    }

    public static ZoneId getActiveTimeZoneId() {
        return Optional.ofNullable(timeZoneProvider.getActiveTimeZoneId()).orElseGet(ZoneId::systemDefault);
    }

    private static String formatMessage(String pattern, Object[] parameters) {
        MessageFormat messageFormat = new MessageFormat(pattern, getActiveLocale());

        messageFormat.setFormats(
            Stream.of(messageFormat.getFormats())
                .map(I::wrapFormat)
                .toArray(Format[]::new)
        );

        return messageFormat.format(parameters);
    }

    private static Format wrapFormat(final Format format) {
        if (format instanceof DateFormat) {
            return new Java8DateFormatWrapper((DateFormat) format);
        } else {
            return format;
        }
    }

    public static Map<String, UnmodifiableCatalog> findBestCatalogsForComponentIds(Set<String> componentIds) {
        Map<String, UnmodifiableCatalog> retMap = new HashMap<>();
        for (String componentId : componentIds) {
            Map<Locale, Catalog> catalogMap = translationsByComponent
                .entrySet()
                .stream()
                .filter(entry -> Objects.equals(componentId, entry.getKey().getComponentId()))
                .collect(Collectors.toMap(entry -> entry.getKey().getLocale(), Map.Entry::getValue));

            Set<Locale> possibleLocales = new HashSet<>(catalogMap.keySet());
            List<Locale.LanguageRange> languageRanges = Locale.LanguageRange.parse(getActiveLocale().toLanguageTag() + ";q=1.0");
            Locale bestMatchingLocale = Locale.lookup(languageRanges, possibleLocales);
            Optional
                .ofNullable(catalogMap.get(bestMatchingLocale))
                .map(UnmodifiableCatalog::new)
                .ifPresent(it -> retMap.put(componentId, it));
        }

        return retMap;
    }
}
