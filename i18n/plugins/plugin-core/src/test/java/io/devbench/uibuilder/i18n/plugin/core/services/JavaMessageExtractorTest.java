/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.services;

import io.devbench.uibuilder.i18n.plugin.core.services.sample.clazz.*;
import io.devbench.uibuilder.i18n.testtools.MessageUtil;
import io.devbench.uibuilder.test.CollectionAssert;
import org.fedorahosted.tennera.jgettext.Message;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class JavaMessageExtractorTest {

    private JavaMessageExtractor testObj = new JavaMessageExtractor();

    @Test
    public void should_find_simple_translation_call_in_simple_java_class() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldFindSimpleTranslationCallInSimpleJavaClass.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("Hello world!", ":24"),
            MessageUtil.createMessage("Hello {0}!", ":25"),
            MessageUtil.createMessage("{0, number} apple is red", "{0, number} apples are red", ":26")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_correct_message_when_other_method_has_other_constants_nearby() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldNotFailWhenOtherConstantsAreNearby.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("Hello world!", ":28")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_correct_message_when_the_same_method_has_other_constants_nearby() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldNotFailWhenTheSameMethodHasSomeConstants.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("Hello world!", ":25")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_correct_message_when_the_same_method_has_other_weird_int_constants_nearby() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldNotFailWhenTheSameMethodHasSomeWeirdConstants.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("Hello world!", ":24")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_correct_message_when_plural_with_small_number_is_used() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldNotFailWhenPluralWithSmallNumberIsUsed.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("One apple", "{0, number} apples", ":24")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_correct_message_when_plural_with_large_number_is_used() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldNotFailWhenPluralWithLargeNumberIsUsed.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("One apple", "{0, number} apples", ":27")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_correct_message_when_plural_with_various_primitives_are_used() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldNotFailWhenPluralWithVariousPrimitivesAreUsed.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("One apple", "{0, number} apples", ":27")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_messages_correctly_when_formatting_objects_came_from_method_call() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldNotFailWhenFormattingObjectsCameFromMethodCall.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("Hello world!", ":30")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_messages_with_context_correctly() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldExtractContextForTranslationsCorrectly.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessageWithContext("The same greeting Grievous used", "Hello there", ":24"),
            MessageUtil.createMessageWithContext("The same greeting Grievous used", "Hello there, General {0}", ":25"),
            MessageUtil.createMessageWithContext("Some context", "Once", "{0, number} times", ":26")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_messages_after_indy_correctly() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldExtractMessagesAfterInvokeDynamic.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("One apple", ":28")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_messages_after_private_method_call() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldExtractMessagesWithPrivateMethodCall.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("One apple", "{0, number} apples", ":25")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_messages_with_numbers_and_operations() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldExtractMessagesWithNumbersAndOperationsNearby.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Collections.singletonList(
            MessageUtil.createMessage("One apple", "{0, number} apples", Arrays.asList(":28", ":29"))
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    @Test
    public void should_extract_messages_from_field_initializers() throws IOException, URISyntaxException {
        Path basePath = getClassPath(ShouldExtractMessagesFromFieldInitializers.class);
        Collection<Message> actual = testObj.extractMessagesFromJavaFiles(basePath).values();
        Collection<Message> expected = Arrays.asList(
            MessageUtil.createMessage("Hello world!", ":23"),
            MessageUtil.createMessage("Apple", ":24")
        );
        CollectionAssert.assertContainsSame(expected, actual, MessageUtil::messageEquals);
    }

    private Path getClassPath(Class<?> clazz) throws URISyntaxException {
        String[] parts = clazz.getCanonicalName().split("\\.");
        parts[parts.length - 1] += ".class";
        Path path = Paths.get(
            clazz
                .getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .toURI()
        );
        for (String part : parts) {
            path = path.resolve(part);
        }
        return path;
    }

}
