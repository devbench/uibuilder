/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.services.sample.clazz;

import io.devbench.uibuilder.i18n.core.I;

public class ShouldExtractMessagesWithNumbersAndOperationsNearby {

    public void main() {
        int number = 4;
        long longNumber = 1L;
        double doubleNum = 4.0D;
        float f = (float) (2.0F / 1.2D);
        String sample01 = I.trfp("One apple", "{0, number} apples", number, longNumber);
        String sample02 = I.trfp("One apple", "{0, number} apples", (int) (doubleNum * 2.0F), (int) (2L * getLongZero()));
    }

    private long getLongZero() {
        return 0L;
    }

}
