/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.enums;

import org.fedorahosted.tennera.jgettext.Message;

import java.util.Deque;
import java.util.Objects;

public enum TranslationMethod {

    TR {
        @Override
        public Message toMessage(Deque<Object> parameters) {
            Message message = new Message();
            message.setMsgid((String) parameters.removeLast());
            return message;
        }
    },

    TRC {
        @Override
        public Message toMessage(Deque<Object> parameters) {
            Message message = new Message();
            message.setMsgid((String) parameters.removeLast());
            message.setMsgctxt((String) parameters.removeLast());
            return message;
        }
    },

    TRF {
        @Override
        public Message toMessage(Deque<Object> parameters) {
            Message message = new Message();
            parameters.removeLast(); // remove the array from the stack
            message.setMsgid((String) parameters.removeLast());
            return message;
        }
    },

    TRFC {
        @Override
        public Message toMessage(Deque<Object> parameters) {
            Message message = new Message();
            parameters.removeLast(); // remove the array from the stack
            message.setMsgid((String) parameters.removeLast());
            message.setMsgctxt((String) parameters.removeLast());
            return message;
        }
    },

    TRFP {
        @Override
        public Message toMessage(Deque<Object> parameters) {
            Message message = new Message();
            parameters.removeLast(); // remove the array from the stack
            parameters.removeLast(); // remove the itemCount from the stack
            String plural = (String) parameters.removeLast();
            String singular = (String) parameters.removeLast();
            message.setMsgid(singular);
            message.setMsgidPlural(plural);
            return message;
        }
    },

    TRFPC {
        @Override
        public Message toMessage(Deque<Object> parameters) {
            Message message = new Message();
            parameters.removeLast(); // remove the array from the stack
            parameters.removeLast(); // remove the itemCount from the stack
            String plural = (String) parameters.removeLast();
            String singular = (String) parameters.removeLast();
            String context = (String) parameters.removeLast();
            message.setMsgid(singular);
            message.setMsgidPlural(plural);
            message.setMsgctxt(context);
            return message;
        }
    };

    public boolean matches(String methodName) {
        return Objects.equals(name().toLowerCase(), methodName);
    }

    public abstract Message toMessage(Deque<Object> parameters);

}
