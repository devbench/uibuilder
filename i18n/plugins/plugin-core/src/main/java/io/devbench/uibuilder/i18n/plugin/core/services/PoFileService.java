/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.services;

import io.devbench.uibuilder.i18n.core.util.TranslationLoader;
import io.devbench.uibuilder.i18n.plugin.core.exceptions.RuntimeIOException;
import org.fedorahosted.tennera.jgettext.Catalog;
import org.fedorahosted.tennera.jgettext.Message;
import org.fedorahosted.tennera.jgettext.PoWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;

public class PoFileService {

    private static final Logger logger = LoggerFactory.getLogger(PoFileService.class);

    private final PoWriter poWriter;
    private final Path baseMessagePath;
    private final Catalog messageTemplate;
    private final Map<Locale, Catalog> messageFilesToLanguages;

    @Inject
    public PoFileService(
        PoWriter poWriter,
        @Named("baseMessagePath") Path baseMessagePath
    ) {
        try {
            this.poWriter = poWriter;
            this.baseMessagePath = baseMessagePath;
            this.messageTemplate = readMessageTemplate();
            this.messageFilesToLanguages = Collections.unmodifiableMap(readAllPoFilesToLanguages());
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    private Map<Locale, Catalog> readAllPoFilesToLanguages() throws IOException {
        Path localesDir = baseMessagePath.getParent();
        if (!Files.exists(localesDir)) {
            Files.createDirectories(localesDir);
        }
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**/*.{po}");
        return Files.walk(localesDir, 1)
            .filter(matcher::matches)
            .map(this::createReaderFromPath)
            .map(reader -> TranslationLoader.loadTranslation(reader, false))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Reader createReaderFromPath(Path path) {
        try {
            return Files.newBufferedReader(path);
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    private Catalog readMessageTemplate() throws IOException {
        if (Files.exists(baseMessagePath)) {
            BufferedReader reader = Files.newBufferedReader(baseMessagePath);
            return TranslationLoader.loadTranslation(reader, true).getValue();
        } else {
            return new Catalog(true);
        }
    }

    public void writeCatalogTo(Catalog catalog, Path messageFile) {
        Catalog sameCatalogButOrdered = sortCatalog(catalog);
        try {
            Files.deleteIfExists(messageFile);
            try (
                BufferedWriter outputWriter = Files.newBufferedWriter(messageFile, StandardOpenOption.CREATE)
            ) {
                StringWriter sw = new StringWriter();
                poWriter.write(sameCatalogButOrdered, sw);
                String catalogAsString = sw.toString();
                logger.debug(
                    "I18N plugin would like to write the following catalog to {} : \n {}",
                    messageFile.toAbsolutePath().toString(),
                    catalogAsString
                );
                outputWriter.write(catalogAsString);
            }
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    private Catalog sortCatalog(Catalog catalog) {
        Set<Message> messages = new HashSet<>();
        catalog.forEach(messages::add);
        Catalog retCatalog = new Catalog();

        if (catalog.locateHeader() != null) {
            retCatalog.addMessage(catalog.locateHeader());
        }

        messages
            .stream()
            .filter(m -> !m.isHeader())
            .peek(this::sortSourceReferences)
            .sorted(Comparator.comparing(this::getFirstSourceRef).thenComparing(Message::getMsgid).thenComparing(Message::getMsgctxt))
            .forEachOrdered(retCatalog::addMessage);
        return retCatalog;
    }

    private void sortSourceReferences(Message message) {
        message.getSourceReferences().sort(String::compareTo);
    }

    private String getFirstSourceRef(Message message) {
        return message.getSourceReferences().stream().sorted().findFirst().orElse("");
    }

    public Catalog readPoFileForLanguage(Locale locale) {
        Catalog catalog = messageFilesToLanguages.get(locale);
        if (catalog != null) {
            return catalog;
        }
        return createEmptyCatalogWithHeader(locale);
    }

    private Catalog createEmptyCatalogWithHeader(Locale locale) {
        Catalog catalog = new Catalog(false);
        Message header = new Message();
        header.setMsgid("");
        header.setMsgstr("\n\"Language: " + locale.toLanguageTag() + "\n");
        catalog.addMessage(header);
        return catalog;
    }

    public Catalog getMessageTemplate() {
        return messageTemplate;
    }

    public void writeCatalogTo(Catalog catalog) {
        Locale locale = TranslationLoader.extractLocaleFromHeader(catalog);
        if (locale == null) {
            throw new UnsupportedOperationException("Locale is not found in Translation Catalog");
        }
        String fileName = locale.toLanguageTag() + ".po";
        Path outputPath = baseMessagePath.resolveSibling(fileName);
        writeCatalogTo(catalog, outputPath);
    }
}
