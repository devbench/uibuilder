/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.services;

import org.fedorahosted.tennera.jgettext.Message;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexMessageExtractor {
    private static final String STRING_LITERAL = "(?:'(?:.*?[^\\\\])?'|\"(?:.*?[^\\\\])?\"|`(?:.*?[^\\\\])?`)";
    private static final String LITERAL = "(" + STRING_LITERAL + "|[0-9]+)";
    private static final String FUNCTION_ARGS = "\\(\\s*(" + LITERAL + "(?:\\s*,\\s*" + LITERAL + ")*)\\s*\\)";
    private static final String FUNCTION_NAME = "(tr|trc|trfp|trfpc)";
    private static final String FUNCTION_CALL = "\\b" + FUNCTION_NAME + FUNCTION_ARGS;
    private final Pattern functionCallPattern = Pattern.compile(FUNCTION_CALL, Pattern.DOTALL);
    private final Pattern literalPattern = Pattern.compile(LITERAL, Pattern.DOTALL);

    public List<Message> extractMessages(Path path, String pathString) throws IOException {
        String text = new String(Files.readAllBytes(path));
        List<Message> messages = new ArrayList<>();
        Matcher matcher = functionCallPattern.matcher(text);
        while (matcher.find()) {
            int lineCount = getLineCount(text, matcher.start());
            String functionName = matcher.group(1);
            List<String> args = parseArgs(matcher.group(2));

            messages.add(createMessage(pathString, lineCount, functionName, args));
        }
        return messages;
    }

    List<String> parseArgs(String text) {
        Matcher matcher = literalPattern.matcher(text);
        List<String> args = new ArrayList<>();
        while (matcher.find()) {
            String arg = matcher.group(1);
            for (String quote : new String[]{"'", "\"", "`"}) {
                if (arg.startsWith(quote) && arg.endsWith(quote)) {
                    arg = arg.replaceAll("\\\\" + quote, quote);
                    arg = arg.substring(1, arg.length() - 1);
                    break;
                }
            }
            args.add(arg);
        }
        return args;
    }

    private int getLineCount(String text, int offset) {
        return (int) (text.substring(0, offset)
            .chars()
            .filter(value -> value == '\n')
            .count() + 1);
    }

    private Message createMessage(String path, int line, String functionName, List<String> args) {
        Message message = new Message();
        message.addSourceReference(path, line);

        int paramIndex = 0;
        if (functionName.endsWith("c")) {
            message.setMsgctxt(args.get(0));
            paramIndex++;
        }
        message.setMsgid(args.get(paramIndex++));

        if (functionName.startsWith("trfp")) {
            message.setMsgidPlural(args.get(paramIndex));
        }

        return message;
    }
}
