/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty;

import com.vaadin.flow.function.SerializableSupplier;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import lombok.Data;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class FunctionalConverters {


    @Test
    public void serializableSuppliserShouldBeConvertable() {
        VaadinSession originalVaadinSession = VaadinSession.getCurrent();

        final Map<String, Object> stringObjectHashMap = new HashMap<>();
        final VaadinSession vaadinSession = new VaadinSession(null) {
            @Override
            public Object getAttribute(String name) {
                return stringObjectHashMap.get(name);
            }

            @Override
            public void setAttribute(String type, Object value) {
                stringObjectHashMap.put(type, value);
            }
        };
        VaadinSession.setCurrent(vaadinSession);

        final SerializableSupplier testSerializableSupplier = new TestClass().serializableSupplier;
        final ClassMetadata<TestClass> classMetadata = ClassMetadata.ofClass(TestClass.class);
        final AbstractPropertyMetadata<?> serializableSupplier =
            classMetadata.property("serializableSupplier").orElse(null);
        final StringPropertyConverter<SerializableSupplier> converter =
            (StringPropertyConverter<SerializableSupplier>) PropertyConverters.getConverterFor(serializableSupplier);

        assertNotNull(converter);
        final String id = converter.convertTo(testSerializableSupplier);
        assertNotNull(id);
        final SerializableSupplier fromConvert = converter.convertFrom(id);
        assertNotNull(fromConvert);
        assertEquals(testSerializableSupplier.get(), fromConvert.get());

        VaadinSession.setCurrent(originalVaadinSession);
    }

    @Data
    public static final class TestClass {
        final SerializableSupplier serializableSupplier = (SerializableSupplier) () -> "Hello World!";
    }
}
