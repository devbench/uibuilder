/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.ModelList;
import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CollectionNodeTest {

    @Test
    @SuppressWarnings({"rawtypes", "unchecked"})
    @DisplayName("Should not add statenode into model list if the node is BeanNode and is leaf")
    void test_should_not_add_statenode_into_model_list_if_the_node_is_bean_node_and_is_leaf() {
        StateNode stateNode = mock(StateNode.class);
        List<BindingNode> bindingNodes = new ArrayList<>();
        AbstractPropertyMetadata propertyMetadata = mock(AbstractPropertyMetadata.class);
        ParameterizedType parameterizedType = mock(ParameterizedType.class);
        Type[] types = new Type[]{String.class};
        when(propertyMetadata.getParameterizedType()).thenReturn(parameterizedType);
        when(parameterizedType.getActualTypeArguments()).thenReturn(types);

        TestCollectionNode collectionNode =
            new TestCollectionNode("test", propertyMetadata, mock(ClassMetadata.class), mock(StateNodeManager.class), stateNode, bindingNodes);

        SimpleNode simpleNode = mock(SimpleNode.class);
        BeanNode beanNodeLeaf = mock(BeanNode.class);
        BeanNode beanNodeNotLeaf = mock(BeanNode.class);
        bindingNodes.add(simpleNode);
        bindingNodes.add(beanNodeLeaf);
        bindingNodes.add(beanNodeNotLeaf);

        StateNode simpleStateNode = mock(StateNode.class);
        StateNode beanNodeNotLeafStateNode = mock(StateNode.class);

        when(beanNodeLeaf.isLeaf()).thenReturn(true);
        when(beanNodeNotLeaf.isLeaf()).thenReturn(false);

        when(simpleNode.populateCollectionValues()).thenReturn(simpleStateNode);
        when(beanNodeLeaf.populateCollectionValues()).thenThrow(new IllegalStateException());
        when(beanNodeNotLeaf.populateCollectionValues()).thenReturn(beanNodeNotLeafStateNode);

        ModelList modelList = new ModelList(new StateNode());
        when(stateNode.getFeature(ModelList.class)).thenReturn(modelList);

        assertDoesNotThrow(collectionNode::setupModelListInStateNode);
        assertEquals(2, modelList.size());
    }

    private static class TestCollectionNode extends CollectionNode {

        TestCollectionNode(String name, AbstractPropertyMetadata<? super Object> property,
                                  ClassMetadata<?> classMetadata, StateNodeManager manager, StateNode stateNode, List<BindingNode> collectionNodes) {
            super(name, property, classMetadata, manager);
            this.stateNode = stateNode;
            this.collectionNodes = collectionNodes;
        }

        @Override
        public Serializable populateValues() {
            return null;
        }

        @Override
        public void synchronizeStateNode() {

        }

        @Override
        public BindingNode cloneWithProperty(AbstractPropertyMetadata<? super Object> property) {
            return null;
        }
    }

}
