/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.templateparser;

import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({JsoupExtension.class, MockitoExtension.class, SingletonProviderForTestsExtension.class})
class MethodReferenceParserTest {

    @Mock
    @SingletonInstance(ControllerBeanManager.class)
    private ControllerBeanManager controllerBeanManager;


    @LoadElement(value = "/methodReferenceParser.html", id = "test1")
    private Element testElement;

    private MethodReferenceParser testObj = new MethodReferenceParser();

    @Test
    void canProcess() {
        assertTrue(testObj.canProcess(null));
        assertTrue(testObj.canProcess(new Element("div")));
    }

    public static class TestControllerBean {
        public boolean called = false;
        public void method() {
            called = true;
        }
    }

    @Test
    public void should_process_template_and_look_up_all_method_handles_in_class() throws Throwable {
        TestControllerBean testControllerBeanInstance = new TestControllerBean();
        when(controllerBeanManager.getControllerBean("something")).thenReturn(testControllerBeanInstance);
        when(controllerBeanManager.getBeanMetadata("something")).thenReturn(ClassMetadata.ofValue(testControllerBeanInstance));

        testObj.processTemplate(testElement);

        UIBuilderMethodReference methodReference = (UIBuilderMethodReference) VaadinSession.getCurrent().getAttribute(testElement.attr("test"));

        assertEquals(1, methodReference.getMethodsWithName().size());

        methodReference.getMethodsWithName().values().iterator().next().invoke(testControllerBeanInstance);

        assertTrue(testControllerBeanInstance.called);
    }
}
