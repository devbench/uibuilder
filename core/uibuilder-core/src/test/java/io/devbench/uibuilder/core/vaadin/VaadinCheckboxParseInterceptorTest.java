/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.vaadin;

import com.vaadin.flow.component.checkbox.Checkbox;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(JsoupExtension.class)
class VaadinCheckboxParseInterceptorTest {

    private VaadinCheckboxParseInterceptor testObj;

    @LoadElement(value = "/vaadinCheckboxParseInterceptor.html", id = "cb1")
    private Element checkboxElement;

    @LoadElement(value = "/vaadinCheckboxParseInterceptor.html", id = "cb2")
    private Element checkboxElementChecked;

    @LoadElement(value = "/vaadinCheckboxParseInterceptor.html", id = "cb3")
    private Element checkboxElementCheckedTrue;

    @LoadElement(value = "/vaadinCheckboxParseInterceptor.html", id = "not-cb")
    private Element nonCheckboxElement;

    @BeforeEach
    void setUp() {
        testObj = new VaadinCheckboxParseInterceptor();
    }

    @Test
    @DisplayName("should be applicable only for vaadin-checkbox")
    void test_should_be_applicable_only_for_vaadin_checkbox() {
        assertTrue(testObj.isApplicable(checkboxElement));
        assertFalse(testObj.isApplicable(nonCheckboxElement));
    }

    @Test
    @DisplayName("should set value true when element has checked attribute")
    void test_should_set_value_true_when_element_has_checked_attribute() {
        Checkbox checkbox = new Checkbox();
        assertFalse(checkbox.getValue());

        testObj.intercept(checkbox, checkboxElement);
        assertFalse(checkbox.getValue());

        testObj.intercept(checkbox, checkboxElementChecked);
        assertTrue(checkbox.getValue());

        checkbox.setValue(false);
        assertFalse(checkbox.getValue());

        testObj.intercept(checkbox, checkboxElementCheckedTrue);
        assertTrue(checkbox.getValue());
    }

    @Test
    @DisplayName("should not be an instantiator")
    void test_should_not_be_an_instantiator() {
        assertFalse(testObj.isInstantiator(checkboxElement));
        assertNull(testObj.instantiateComponent());
    }

}
