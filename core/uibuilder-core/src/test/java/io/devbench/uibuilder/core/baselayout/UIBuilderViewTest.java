/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.baselayout;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.internal.UIInternals;
import com.vaadin.flow.component.page.History;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WebBrowser;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.flow.exceptions.FlowDefinitionNotFound;
import io.devbench.uibuilder.core.page.Page;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({BaseUIBuilderTestExtension.class, MockitoExtension.class})
class UIBuilderViewTest {

    @Mock
    @SuppressWarnings("unused")
    VaadinService vaadinService;

    @Mock
    VaadinSession vaadinSession;

    @Mock
    WebBrowser webBrowser;

    @Mock
    UIInternals uiInternals;

    @Mock
    UI ui;

    Page page;

    UIBuilderView testObj;

    @BeforeEach
    void setupTestEnvironment() {
        UIContext.getContext();
        ArgumentCaptor<UIContext> uiContextArgumentCaptor = ArgumentCaptor.forClass(UIContext.class);
        verify(vaadinSession).setAttribute(eq("UIBuilderSessionContext"), uiContextArgumentCaptor.capture());
        UIContext uiContext = uiContextArgumentCaptor.getValue();
        when(vaadinSession.getAttribute("UIBuilderSessionContext")).thenReturn(uiContext);

        when(ui.getInternals()).thenReturn(uiInternals);
        when(uiInternals.getSession()).thenReturn(vaadinSession);
        when(vaadinSession.getBrowser()).thenReturn(webBrowser);
        when(webBrowser.getLocale()).thenReturn(Locale.US);

        com.vaadin.flow.component.page.Page vaadinPage = mock(com.vaadin.flow.component.page.Page.class);
        when(ui.getPage()).thenReturn(vaadinPage);
        History history = mock(History.class);
        when(vaadinPage.getHistory()).thenReturn(history);

        page = spy(new Page());
        testObj = new UIBuilderView(page);

        doNothing().when(page).loadContent(any(), eq((InputStream) null));

        FlowManager.getCurrent().registerFlowTarget("mainTarget", page);
    }

    @Test
    @DisplayName("Should create UIBuilderView with default page")
    void test_should_create_ui_builder_view_with_default_page() {
        UIBuilderView view = new UIBuilderView();
        assertNotNull(view.getPage());
    }

    @Test
    @DisplayName("Should load page by relativeUrl")
    void test_should_load_page_by_relative_url() {
        BeforeEvent event = mock(BeforeEvent.class, Answers.RETURNS_DEEP_STUBS);
        when(event.getLocation().getQueryParameters().getParameters()).thenReturn(new HashMap<>());

        testObj.setParameter(event, "/static");
        testObj.onViewReady();

        verify(page).loadContent("/FlowManagerTest/simple/html/static.html");
    }

    @Test
    @DisplayName("Should reroute to error page when exception thrown")
    void test_should_reroute_to_error_page_when_exception_thrown() {
        BeforeEvent event = mock(BeforeEvent.class, Answers.RETURNS_DEEP_STUBS);
        when(event.getLocation().getQueryParameters().getParameters()).thenReturn(new HashMap<>());

        testObj.setParameter(event, "/non-existent-url");
        testObj.onViewReady();

        verify(event).rerouteToError(any(FlowDefinitionNotFound.class), anyString());
    }

    @Test
    @DisplayName("Should throw exception when relative url is null")
    void test_should_throw_exception_when_relative_url_is_null() {
        BeforeEvent event = mock(BeforeEvent.class, Answers.RETURNS_DEEP_STUBS);
        when(event.getLocation().getQueryParameters().getParameters()).thenReturn(new HashMap<>());

        assertThrows(IllegalStateException.class, () -> testObj.setParameter(event, null));
    }

    @Test
    @DisplayName("Should throw exception when query parameters are null")
    void test_should_throw_exception_when_query_parameters_are_null() {
        BeforeEvent event = mock(BeforeEvent.class, Answers.RETURNS_DEEP_STUBS);
        when(event.getLocation().getQueryParameters().getParameters()).thenReturn(null);
        assertThrows(IllegalStateException.class, () -> testObj.setParameter(event, "/any-url"));
    }

    @Test
    @DisplayName("Should throw exception when query parameters cannot be obtained")
    void test_should_throw_exception_when_query_parameters_cannot_be_obtained() {
        BeforeEvent event = mock(BeforeEvent.class, Answers.RETURNS_DEEP_STUBS);
        when(event.getLocation()).thenThrow(new IllegalStateException("test"));
        assertThrows(IllegalStateException.class, () -> testObj.setParameter(event, "/any-url"));
    }

    @Test
    @DisplayName("Should unload page")
    void test_should_unload_page() {
        testObj.unloadContent();
        verify(page).unloadContent();
    }

    @Test
    @DisplayName("Should load given page")
    void test_should_load_given_page() {
        doNothing().when(page).loadContent(any(), any(InputStream.class));
        ByteArrayInputStream inputStream = new ByteArrayInputStream("content".getBytes());

        testObj.loadContent("/path/to/content.html", inputStream);

        verify(page, never()).loadContent("/path/to/content.html", inputStream);
        testObj.onViewReady();
        verify(page).loadContent("/path/to/content.html", inputStream);
    }

    @Test
    @DisplayName("Should load given page immediately after view ready event")
    void test_should_load_given_page_immediately_after_view_ready_event() {
        testObj.onViewReady();

        doNothing().when(page).loadContent(any(), any(InputStream.class));
        ByteArrayInputStream inputStream = new ByteArrayInputStream("content".getBytes());

        testObj.loadContent("/path/to/content.html", inputStream);

        verify(page).loadContent("/path/to/content.html", inputStream);
    }

}
