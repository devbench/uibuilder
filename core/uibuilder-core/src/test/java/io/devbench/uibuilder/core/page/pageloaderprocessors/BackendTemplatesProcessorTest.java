/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.parse.backendtemplate.BackendTemplateManager;
import io.devbench.uibuilder.api.parse.backendtemplate.TemplateParser;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class BackendTemplatesProcessorTest {

    @Mock
    private TemplateParser mockParser1;

    @Mock
    private TemplateParser mockParser2;

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner mockScanner;

    private BackendTemplatesProcessor testObj;

    @BeforeEach
    void setup() {
        when(mockScanner.findClassesBySuperType(TemplateParser.class)).thenReturn(Collections.emptySet());
        testObj = new BackendTemplatesProcessor();
        BackendTemplateManager templateManager = BackendTemplateManager.registerToActiveContext();
        templateManager.registerTemplateParser(mockParser1);
        templateManager.registerTemplateParser(mockParser2);
    }

    @Test
    @DisplayName("Should call template parsers registered for the template manager, with the contexts page element")
    void should_call_template_parsers_registered_for_the_template_manager_with_the_contexts_page_element() {
        Element element = new Element("uibuilder-page");
        PageLoaderContext context = new PageLoaderContext();
        context.setPageElement(element);

        when(mockParser1.canProcess(element)).thenReturn(true);
        when(mockParser2.canProcess(element)).thenReturn(true);

        when(mockParser1.processTemplate(element)).thenReturn(new Elements(element));
        when(mockParser2.processTemplate(element)).thenReturn(new Elements(element));

        testObj.process(context);

        verify(mockParser1).canProcess(element);
        verify(mockParser1).processTemplate(element);
        verify(mockParser2).canProcess(element);
        verify(mockParser2).processTemplate(element);
    }

    @Test
    @DisplayName("Should return when parser modified the element")
    void should_return_when_parser_modified_the_element() {
        Element parent = new Element("uibuilder-page");
        Element firstElement = new Element("first-element");
        Element elementToReplace = new Element("div");
        Element lastElement = new Element("last-element");

        parent.appendChild(firstElement);
        parent.appendChild(elementToReplace);
        parent.appendChild(lastElement);

        Element anotherElement = new Element("new-elem-one");
        Element anotherSecondElement = new Element("new-elem-two");
        PageLoaderContext context = new PageLoaderContext();
        context.setPageElement(elementToReplace);

        when(mockParser1.canProcess(elementToReplace)).thenReturn(true);
        when(mockParser2.canProcess(elementToReplace)).thenReturn(true);

        when(mockParser1.processTemplate(elementToReplace)).thenReturn(new Elements(anotherElement, anotherSecondElement));
        when(mockParser2.processTemplate(elementToReplace)).thenReturn(new Elements(elementToReplace));

        testObj.process(context);

        verify(mockParser1).canProcess(elementToReplace);
        verify(mockParser1).processTemplate(elementToReplace);

        assertSame(firstElement, parent.childNode(0));
        assertSame(anotherElement, parent.childNode(1));
        assertSame(anotherSecondElement, parent.childNode(2));
        assertSame(lastElement, parent.childNode(3));
    }

}
