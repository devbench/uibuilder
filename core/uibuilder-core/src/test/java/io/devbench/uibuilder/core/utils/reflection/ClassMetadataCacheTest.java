/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils.reflection;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({SingletonProviderForTestsExtension.class, MockitoExtension.class})
class ClassMetadataCacheTest {

    @Data
    public static class BaseTestClass {
        private String base;
    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    public static class TestClass extends BaseTestClass {
        private String data;
    }

    @Mock
    @SingletonInstance(MemberScanner.class)
    MemberScanner memberScanner;

    @Test
    @DisplayName("Should not scan properties if class metadata created for the second time")
    void test_should_not_scan_properties_if_class_metadata_created_for_the_second_time() {
        doReturn(new HashSet<>(Arrays.asList(TestClass.class, BaseTestClass.class))).when(memberScanner).findClassesBySuperType(TestClass.class);

        ClassMetadata<TestClass> withInheritance1 = new ClassMetadata<>(TestClass.class, true);
        ClassMetadata<TestClass> withInheritance2 = new ClassMetadata<>(TestClass.class, true);

        assertSame(withInheritance1.getProperties(), withInheritance2.getProperties());
        assertSame(withInheritance1.getMethods(), withInheritance2.getMethods());

        ClassMetadata<TestClass> withoutInheritance1 = new ClassMetadata<>(TestClass.class, false);
        ClassMetadata<TestClass> withoutInheritance2 = new ClassMetadata<>(TestClass.class, false);

        assertSame(withoutInheritance1.getProperties(), withoutInheritance2.getProperties());
        assertSame(withoutInheritance1.getMethods(), withoutInheritance2.getMethods());

        assertNotSame(withInheritance1.getProperties(), withoutInheritance1.getProperties());
        assertNotSame(withInheritance1.getMethods(), withoutInheritance1.getMethods());
    }

}
