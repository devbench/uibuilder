/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.startup;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class ComponentTagRegistryTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @BeforeEach
    void setUp() {
        Set<Class<?>> classes = new HashSet<>(Arrays.asList(TestTag.class, TestTag2.class));
        Mockito.doReturn(classes).when(memberScanner).findClassesByAnnotation(Tag.class);
        ComponentTagRegistry.registerToActiveContext();
    }

    @Test
    @DisplayName("should return a valid instance")
    void test_should_return_a_valid_instance() {
        ComponentTagRegistry instance = ComponentTagRegistry.getInstance();
        assertNotNull(instance);
    }

    @Test
    @DisplayName("should return proper class by tag name")
    void test_should_return_proper_class_by_tag_name() {
        Optional<Class<Component>> clz = ComponentTagRegistry.getInstance().getComponentClassByTag("test-tag-2");

        assertNotNull(clz);
        assertTrue(clz.isPresent());
        assertTrue(clz.get().isAssignableFrom(TestTag2.class), "Class should be assignable from " + TestTag2.class.getSimpleName());
    }

    @Test
    @DisplayName("should not return class if the class is not a component")
    void test_should_not_return_class_if_the_class_is_not_a_component() {
        Optional<Class<Component>> clz = ComponentTagRegistry.getInstance().getComponentClassByTag("test-tag");

        assertNotNull(clz);
        assertFalse(clz.isPresent());
    }

    @Tag("test-tag")
    private static class TestTag {

    }

    @Tag("test-tag-2")
    private static class TestTag2 extends Component {

    }

}
