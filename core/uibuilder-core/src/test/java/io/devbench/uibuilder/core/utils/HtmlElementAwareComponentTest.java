/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import io.devbench.uibuilder.api.components.HasRawElementComponent;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HtmlElementAwareComponentTest {

    @Test
    @DisplayName("should not change anything if both the component and the element has the id set")
    void test_should_not_change_anything_if_both_the_component_and_the_element_has_the_id_set() {
        TestComponent component = new TestComponent();
        Element element = new Element("test-component");

        component.setId("test-id");
        element.attr(ElementCollector.ID, "test-id");

        HtmlElementAwareComponent heac = new HtmlElementAwareComponent(component, element);

        assertEquals(component, heac.getComponent());
        assertEquals("test-id", heac.getComponent().getId().get());
        assertEquals(element, heac.getElement());
    }

    @Test
    @DisplayName("should set raw element if component supports")
    void test_should_set_raw_element_if_component_supports() {
        TestRawElementComponent component = new TestRawElementComponent();
        Element element = new Element("test-raw-element-component");

        component.setId("test-id");
        element.attr(ElementCollector.ID, "test-id");

        assertNull(component.getRawElement());

        HtmlElementAwareComponent heac = new HtmlElementAwareComponent(component, element);

        assertEquals(component, heac.getComponent());
        assertEquals("test-id", heac.getComponent().getId().get());
        assertEquals(element, component.getRawElement());
        assertEquals(element, heac.getElement());
    }

    @Test
    @DisplayName("should set id if element has but component misses")
    void test_should_set_id_if_element_has_but_component_misses() {
        TestComponent component = new TestComponent();
        Element element = new Element("test-component");
        element.attr(ElementCollector.ID, "test-id");

        HtmlElementAwareComponent heac = new HtmlElementAwareComponent(component, element);

        assertAll(
            () -> assertTrue(heac.getComponent().getId().isPresent()),
            () -> assertEquals("test-id", heac.getComponent().getId().get())
        );
    }

    @Test
    @DisplayName("should not set id when element doesn't have either")
    void test_should_not_set_id_when_element_doesn_t_have_either() {
        TestComponent component = new TestComponent();
        Element element = new Element("test-component");

        HtmlElementAwareComponent heac = new HtmlElementAwareComponent(component, element);

        assertFalse(heac.getComponent().getId().isPresent());
    }

    @Tag("test-component")
    public static class TestComponent extends Component {

    }

    @Tag("test-raw-element-component")
    public static class TestRawElementComponent extends HasRawElementComponent {

    }

}
