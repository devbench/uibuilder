/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.components;

import com.vaadin.flow.component.Tag;
import io.devbench.uibuilder.api.components.form.UIBuilderDetailCapable;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class TagGroupHelperTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @BeforeEach
    void setUp() {
        doReturn(new HashSet<>(Arrays.asList(TestForm.class, TestDetailPanel.class, TestEditorWindow.class)))
            .when(memberScanner)
            .findClassesBySuperType(UIBuilderDetailCapable.class);
    }

    @Test
    @DisplayName("Should find all tags whose backend class implements the given interface")
    void test_should_find_all_tags_whose_backend_class_implements_the_given_interface() throws Exception {
        Element body = Jsoup.parse("<div>" +
            "   <uibuilder-form id='my-form-one'></uibuilder-form>" +
            "   <div>" +
            "       <detail-panel id='my-detail-panel'></detail-panel>" +
            "       <span></span>" +
            "       <uibuilder-form id='my-form-two'></uibuilder-form>\" +" +
            "   </div>" +
            "   <uibuilder-editor-window id='my-editor-window'></uibuilder-editor-window>" +
            "</div>").body();

        Elements foundElement = TagGroupHelper.findElementsByGroupComponentClass(body, UIBuilderDetailCapable.class);

        assertEquals(4, foundElement.size());

        assertTrue(foundElement.stream()
            .anyMatch(element -> element.tagName().equals("uibuilder-form") && element.attr("id").equals("my-form-one")));
        assertTrue(foundElement.stream()
            .anyMatch(element -> element.tagName().equals("uibuilder-form") && element.attr("id").equals("my-form-two")));
        assertTrue(foundElement.stream()
            .anyMatch(element -> element.tagName().equals("detail-panel") && element.attr("id").equals("my-detail-panel")));
        assertTrue(foundElement.stream()
            .anyMatch(element -> element.tagName().equals("uibuilder-editor-window") && element.attr("id").equals("my-editor-window")));
    }

    @Tag("uibuilder-form")
    public static class TestForm implements UIBuilderDetailCapable<String> {
        @Override
        public String getItem() {
            return null;
        }

        @Override
        public void setItem(String item) {

        }
    }

    @Tag("uibuilder-editor-window")
    public static class TestEditorWindow extends TestForm {
    }

    @Tag("detail-panel")
    public static class TestDetailPanel extends TestForm {
    }

}
