/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page;

import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
class PageLoaderContextTest {

    private PageLoaderContext testObj;

    @Test
    @DisplayName("Read html content from internal resource file by htmlPath")
    void test_read_html_content_from_internal_resource_file_by_html_path() {
        testObj = new PageLoaderContext("/page-test-element.html", null, null, null);
        InputStream htmlContentInputStream = testObj.getHtmlContentInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(htmlContentInputStream));
        String htmlContent = reader.lines().collect(Collectors.joining("\n"));

        assertNotNull(htmlContent);
        assertEquals("<uibuilder-page>\n" +
            "    <!-- comment1 -->\n" +
            "    <div>content</div>\n" +
            "    <!-- comment2 -->\n" +
            "</uibuilder-page>", htmlContent);
    }

    @Test
    @DisplayName("Read html content from the provided input stream")
    void test_read_html_content_from_the_provided_input_stream() {
        String sourceHtml = "<uibuilder-page>\n" +
            "  <pre>test html</pre>\n" +
            "</uibuilder-page>";

        testObj = new PageLoaderContext("/page-test-element.html", new ByteArrayInputStream(sourceHtml.getBytes(StandardCharsets.UTF_8)), null, null);
        InputStream htmlContentInputStream = testObj.getHtmlContentInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(htmlContentInputStream));
        String htmlContent = reader.lines().collect(Collectors.joining("\n"));

        assertNotNull(htmlContent);
        assertEquals(sourceHtml, htmlContent);
    }

}
