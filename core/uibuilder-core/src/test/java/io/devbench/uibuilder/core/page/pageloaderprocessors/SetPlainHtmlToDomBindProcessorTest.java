/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.core.page.DomBind;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SetPlainHtmlToDomBindProcessorTest {

    private SetPlainHtmlToDomBindProcessor testObj = new SetPlainHtmlToDomBindProcessor();

    @Test
    @DisplayName("Should set the plain html version of the page element's innerHTML to the dom-bind as string")
    public void should_set_the_plain_html_version_of_the_page_elements_innerHTML_to_the_dom_bind_as_string() {
        Element pageElement = new Element("uibuilder-page");
        pageElement.appendElement("div");
        PageLoaderContext context = new PageLoaderContext(null, new DomBind());
        context.setPageElement(pageElement);

        testObj.process(context);

        assertEquals("<div></div>", context.getDomBind().getPlainHtml());
    }

}
