import { html, css, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

@customElement("uibuilder-view")
export class UIBuilderView extends LitElement {

    static styles = css`
        :host {
            display: block;
            width: 100%;
            height: 100vh;
        }
    `;

    private _mutationObserver?: MutationObserver;

    render() {
        return html`
            <slot></slot>
        `;
    }

    connectedCallback() {
        super.connectedCallback();
        const outlet = document.getElementById("outlet");
        if (outlet) {
            this._mutationObserver = new MutationObserver(() => {
                if (outlet.hasChildNodes()) {
                    this.dispatchEvent(new CustomEvent("view-ready"));
                    this._mutationObserver!.disconnect();
                }
            });
            this._mutationObserver.observe(outlet, {childList: true});
        }
    }
}
