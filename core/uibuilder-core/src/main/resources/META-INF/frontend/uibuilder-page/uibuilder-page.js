import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';

export class UIBuilderPage extends PolymerElement {

    static get template() {
        return html`
            <slot></slot>`;
    }

    static get is() {
        return 'uibuilder-page';
    }

    _uibuilderReady() {
        const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        this.dispatchEvent(new CustomEvent('user-timezone-upload', {detail: {value: timeZone}}));
    }
}

customElements.define(UIBuilderPage.is, UIBuilderPage);
