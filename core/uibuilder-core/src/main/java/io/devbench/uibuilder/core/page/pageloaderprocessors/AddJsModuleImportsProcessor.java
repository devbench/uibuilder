/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.internal.ReflectTools;
import com.vaadin.flow.server.VaadinServlet;
import com.vaadin.flow.theme.AbstractTheme;
import io.devbench.uibuilder.core.dynamictheme.ConfigurationBasedTheme;
import io.devbench.uibuilder.core.page.PageLoaderContext;

import java.util.stream.Stream;

public class AddJsModuleImportsProcessor extends PageLoaderProcessor {

    @Override
    public void process(PageLoaderContext context) {
        String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
        AbstractTheme theme = ReflectTools.createInstance(ConfigurationBasedTheme.class);

        collectJsImports(context)
            .distinct()
            .map(theme::translateUrl)
            .map(url -> url.startsWith("/") ? contextPath + url : url)
            .forEach(url -> UI.getCurrent().getPage().addJsModule(url));
    }

    private void removeDeprecatedHtmlImports(PageLoaderContext context) {
        context.getDocument().getElementsByTag("link").removeIf(element -> "import".equals(element.attr("rel")));
    }

    private Stream<String> collectJsImports(PageLoaderContext context) {
        removeDeprecatedHtmlImports(context);
        return context.getDocument().getElementsByTag("script").stream()
            .filter(e -> "module".equals(e.attr("type")))
            .map(e -> e.attr("src"));
    }

}
