/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean;

import com.vaadin.flow.dom.DomEvent;
import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.*;
import io.devbench.uibuilder.api.exceptions.ProgressInterruptException;
import io.devbench.uibuilder.core.controllerbean.injection.request.ItemBasedInjectionRequest;
import io.devbench.uibuilder.core.controllerbean.injection.request.StateNodeBaseInjectionRequest;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.exceptions.ControllerBeanParameterInjectionException;
import io.devbench.uibuilder.core.exceptions.UIEventHandlerException;
import io.devbench.uibuilder.core.flow.FlowParameter;
import io.devbench.uibuilder.core.flow.QueryParameter;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Stream;

import static io.devbench.uibuilder.core.controllerbean.injection.InjectionUtils.*;


@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UIEventHandlerContext {
    @Getter
    private String controllerBeanName;

    @Getter
    private String eventName;

    @Getter
    @NotNull
    private Collection<EventQualifier> eventQualifiers;

    @Getter
    @NotNull
    private Collection<String> customEventQualifiers;

    @Getter
    private Method method;

    @Getter
    private List<String> requestedClientParameters = Collections.synchronizedList(new ArrayList<>());

    @Getter
    private List<String> eventDataExpressions = Collections.synchronizedList(new ArrayList<>());

    @Getter
    private int debounceTimeout;

    public String getEventHandlerQualifiedName() {
        return controllerBeanName + "::" + eventName;
    }

    public void callEventHandler(StateNodeManager stateNodeManager, DomEvent event) {
        final StateNodeBaseInjectionRequest stateNodeBaseInjectionRequest = new StateNodeBaseInjectionRequest(stateNodeManager, event, method);
        if (stateNodeBaseInjectionRequest.isValid()) {
            final Object controllerBean = ControllerBeanManager.getInstance().getControllerBean(controllerBeanName);
            try {
                method.invoke(controllerBean, stateNodeBaseInjectionRequest.getParameters());
            } catch (IllegalAccessException e) {
                throw new UIEventHandlerException(e);
            } catch (InvocationTargetException e) {
                throw new UIEventHandlerException(e.getTargetException());
            }
        }
    }

    public String createJsObjectFromRequestedParameters() {
        final JsonObject res = Json.createObject();
        final JsonObject detail = Json.createObject();
        requestedClientParameters.forEach(parameterName -> {
            if ("item".equals(parameterName)) {
                detail.put(parameterName, "event.model." + parameterName);
                eventDataExpressions.add(EVENT_DETAIL_PREFIX + parameterName);
            } else {
                String parameterNameWithUnderscoreSeparator = parameterName.replace('.', '_');
                detail.put(parameterNameWithUnderscoreSeparator, "event." + parameterName);
                eventDataExpressions.add(EVENT_DETAIL_PREFIX + parameterNameWithUnderscoreSeparator);
            }

        });
        res.put("detail", detail);
        return res.toJson().replaceAll("\"", "");
    }

    public void callEventHandlerWithItem(Object item) {
        callEventHandlerWithItem(item, null);
    }

    public void callEventHandlerWithItem(Object item, Object source) {
        callEventHandlerWithItem(item, source, Collections.emptyMap());
    }

    protected ControllerBeanManager getControllerBeanManager() {
        return ControllerBeanManager.getInstance();
    }

    public void callEventHandlerWithItem(Object item, Object source, Map<String, Object> values) {
        Object controllerBean = getControllerBeanManager().getControllerBean(controllerBeanName);
        try {
            final ItemBasedInjectionRequest injectionRequest = new ItemBasedInjectionRequest(item, source, method, values);
            if (injectionRequest.isValid()) {
                method.invoke(controllerBean, injectionRequest.getParameters());
            } else {
                log.debug("Method {} cannot be invoked, because it is not valid", method.getName());
            }
        } catch (IllegalAccessException e) {
            throw new UIEventHandlerException(e);
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof ProgressInterruptException) {
                throw (ProgressInterruptException) e.getCause();
            } else {
                throw new UIEventHandlerException(e.getTargetException());
            }
        }
    }

    public static UIEventHandlerContext buildFromControllerBeanMethod(String controllerBeanName, Method method) {
        final UIEventHandler uiEventHandler = method.getAnnotation(UIEventHandler.class);
        ensureEventHandlerMethodModifiers(controllerBeanName, method, uiEventHandler);

        final UIEventHandlerContext res = new UIEventHandlerContext();
        res.controllerBeanName = controllerBeanName;
        res.method = method;
        res.eventName = uiEventHandler.value();
        res.eventQualifiers = Arrays.asList(uiEventHandler.qualifiers());
        res.customEventQualifiers = Arrays.asList(uiEventHandler.customQualifiers());
        res.debounceTimeout = uiEventHandler.debounce();

        for (Parameter parameter : method.getParameters()) {
            if (parameter.isAnnotationPresent(Item.class)) {
                res.requestedClientParameters.add("item");
            } else if (parameter.isAnnotationPresent(Value.class)) {
                res.requestedClientParameters.add(parameter.getAnnotation(Value.class).value());
            } else if (parameter.isAnnotationPresent(Data.class)) {
                res.requestedClientParameters.add(EVENT_DATASET_PREFIX + parameter.getAnnotation(Data.class).value());
            } else if (isMethodParameterNotSupported(parameter)) {
                throw new ControllerBeanParameterInjectionException(
                    String.format("UI event handler methods only allowed to have annotated parameters. Method: %s Parameter: %s",
                        method.getName(), parameter.getName()));
            }
        }

        return res;
    }

    private static void ensureEventHandlerMethodModifiers(String controllerBeanName, Method method, UIEventHandler uiEventHandler) {
        int methodModifiers = method.getModifiers();
        if ((methodModifiers & Modifier.PUBLIC) == 0) {
            throw new UIEventHandlerException(
                "UI event (" + controllerBeanName + "::" + uiEventHandler.value() + ") handler method (" + method.getName() + ") must be public");
        }
        if ((methodModifiers & Modifier.STATIC) != 0) {
            throw new UIEventHandlerException(
                "UI event (" + controllerBeanName + "::" + uiEventHandler.value() + ") handler method (" + method.getName() + ") cannot be static");
        }
    }

    private static boolean isMethodParameterNotSupported(Parameter parameter) {
        return Stream
            .of(UIComponent.class, Source.class, FlowParameter.class, QueryParameter.class)
            .noneMatch(parameter::isAnnotationPresent);
    }
}
