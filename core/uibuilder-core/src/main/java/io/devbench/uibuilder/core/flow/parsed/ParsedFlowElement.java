/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow.parsed;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;


public class ParsedFlowElement {

    @NotNull
    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    @Nullable
    private ParsedFlowElement parentElement;

    @NotNull
    @Getter(AccessLevel.PROTECTED)
    private final Supplier<Optional<String>> alternativeFlowId;

    private final Map<Integer, String> parameters;

    public ParsedFlowElement(@NotNull String id, @NotNull Supplier<Optional<String>> alternativeFlowId) {
        this.id = Objects.requireNonNull(id);
        this.alternativeFlowId = Objects.requireNonNull(alternativeFlowId);
        this.parameters = new HashMap<>();
    }

    public ParsedFlowElement(@NotNull String id, @Nullable ParsedFlowElement parentElement, @NotNull Supplier<Optional<String>> alternativeFlowId) {
        this.id = id;
        this.parentElement = parentElement;
        this.alternativeFlowId = alternativeFlowId;
        this.parameters = new HashMap<>();
    }

    public Optional<String> calcAlternativeFlowId() {
        if (parentElement != null) {
            Optional<String> parentAlternativeFlowId = parentElement.calcAlternativeFlowId();
            if (parentAlternativeFlowId.isPresent()) {
                return parentAlternativeFlowId;
            }
        }
        return alternativeFlowId.get();
    }

    public void defineParameter(String parameterName, int parameterIndex) {
        parameters.put(parameterIndex, Objects.requireNonNull(parameterName));
    }

    public String getParameterForIndex(int parameterIndex) {
        return Objects.requireNonNull(parameters.get(parameterIndex));
    }

    public Stream<String> getParameterNames() {
        return parameters.values().stream();
    }
}

