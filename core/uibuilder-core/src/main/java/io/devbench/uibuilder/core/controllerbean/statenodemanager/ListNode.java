/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class ListNode extends CollectionNode {

    private List<? super Object> referencedList;

    ListNode(String name, AbstractPropertyMetadata<? super Object> property, ClassMetadata<?> classMetadata, StateNodeManager manager) {
        super(name, property, classMetadata, manager);
    }

    @Override
    public Serializable populateValues() {
        if (property.getInstance() != null && property.getValue() != null) {
            referencedList = property.getValue();
        } else {
            referencedList = Collections.emptyList();
        }

        buildCollectionNodesByReferencedList();

        return createStateNodeFromCollectionNodes();
    }

    private void buildCollectionNodesByReferencedList() {
        collectionNodes = IntStream.range(0, referencedList.size())
            .boxed()
            .map(index -> syntheticPropertyPrototype.clone(
                () -> referencedList.get(index),
                value -> referencedList.set(index, value)
            ))
            .map(prototypeNode::cloneWithProperty)
            .collect(Collectors.toList());
    }

    @Override
    public void synchronizeStateNode() {
        List<? super Object> newValue = null;
        if (property.getInstance() != null) {
            newValue = property.getValue();
        }

        if (newValue != null && !newValue.isEmpty()) {
            if (newValue != referencedList) {
                referencedList = newValue;
                buildCollectionNodesByReferencedList();
            } else {
                while (newValue.size() > collectionNodes.size()) {
                    int index = collectionNodes.size();
                    collectionNodes.add(index,
                        prototypeNode.cloneWithProperty(syntheticPropertyPrototype.clone(
                            () -> referencedList.get(index),
                            value -> referencedList.set(index, value)
                        )));
                }

                while (newValue.size() < collectionNodes.size()) {
                    int index = collectionNodes.size() - 1;
                    collectionNodes.remove(index);
                }

                collectionNodes.stream()
                    .filter(BeanNode.class::isInstance)
                    .map(BeanNode.class::cast)
                    .forEach(BeanNode::refreshExtractedPropertiesInstances);
            }
        } else {
            collectionNodes.clear();
        }

        setupModelListInStateNode();
    }

    @Override
    public BindingNode cloneWithProperty(AbstractPropertyMetadata<? super Object> property) {
        CollectionNode result = new ListNode(name, property, classMetadata, manager);
        result.prototypeNode = this.prototypeNode;
        return result;
    }
}
