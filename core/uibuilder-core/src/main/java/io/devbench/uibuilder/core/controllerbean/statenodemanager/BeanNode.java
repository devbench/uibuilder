/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.ElementPropertyMap;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.core.exceptions.StateNodeBeanPopulateException;
import io.devbench.uibuilder.core.exceptions.StateNodeBeanSynchronizationException;
import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


public class BeanNode extends BindingNode {

    BeanNode(String name, AbstractPropertyMetadata<? super Object> property, ClassMetadata<?> classMetadata, StateNodeManager manager) {
        super(name, property, classMetadata, manager);
    }

    @Override
    public Serializable populateValues() {
        if (!isLeaf()) {
            stateNode = new StateNode(Collections.singletonList(ElementPropertyMap.class));
            ElementPropertyMap propertyMap = stateNode.getFeature(ElementPropertyMap.class);
            subTree.forEach((key, treeNode) -> {
                try {
                    propertyMap.setProperty(key, treeNode.populateValues());
                } catch (IllegalStateException e) {
                    throw new FieldConversionException(key, e);
                }
            });
            stateNode.setEnabled(true);
            manager.registerValueProvider(node -> (node != null && node.getId() == stateNode.getId())
                ? property.getValue()
                : null);
            return stateNode;
        }

        throw new StateNodeBeanPopulateException("Cannot populate bean as a client value");
    }

    @Override
    protected Serializable populateCollectionValues() {
        return populateValues();
    }

    @Override
    public List<BindingNodeSyncError> synchronizeProperty(@NotNull String parentPropertyPath) {
        List<BindingNodeSyncError> syncErrors = new ArrayList<>();
        String thisPropertyPath = calcPropertyPath(parentPropertyPath, this);
        subTree.forEach((key, node) -> {
            if (node instanceof SimpleNode) {
                Serializable value = stateNode.getFeature(ElementPropertyMap.class).getProperty(key);
                if (isNotTopLevelBean()
                    && (property.getInstance() == null || property.getValue() == null)
                    && value != null) {

                    Optional<Object> beanInstanceHolder = tryToCreateBeanInstance();
                    if (beanInstanceHolder.isPresent()) {
                        Object beanInstance = beanInstanceHolder.get();
                        property.setValue(beanInstance);
                        subTree.forEach((s, bindingNode) -> bindingNode.property.setInstance(beanInstance));
                        ((SimpleNode) node).synchronizePropertyWithValue(value, thisPropertyPath).ifPresent(syncErrors::add);
                    } else {
                        syncErrors.add(new BindingNodeSyncError(node,
                            new StateNodeBeanSynchronizationException("Cannot set property on null bean: " + name),
                            calcPropertyPath(thisPropertyPath, node)));
                    }
                } else {
                    ((SimpleNode) node).synchronizePropertyWithValue(value, thisPropertyPath).ifPresent(syncErrors::add);
                }
            } else {
                syncErrors.addAll(node.synchronizeProperty(thisPropertyPath));
            }
        });

        return syncErrors;
    }

    @SuppressWarnings("unchecked")
    private Optional<Object> tryToCreateBeanInstance() {
        return findBeanNodeInstanceFactory(property).map(factory -> factory.create(property));
    }

    @SuppressWarnings("unchecked")
    public static Optional<BeanNodeInstanceFactory> findBeanNodeInstanceFactory(Class<?> clz) {
        return MemberScanner.getInstance()
            .findInstancesBySuperType(BeanNodeInstanceFactory.class).stream()
            .filter(factory -> factory.isApplicable(clz))
            .findFirst();
    }

    @SuppressWarnings("unchecked")
    private static Optional<BeanNodeInstanceFactory> findBeanNodeInstanceFactory(AbstractPropertyMetadata<?> propertyMetadata) {
        return MemberScanner.getInstance()
            .findInstancesBySuperType(BeanNodeInstanceFactory.class).stream()
            .filter(factory -> factory.isApplicable(propertyMetadata))
            .findFirst();
    }

    @Override
    public void synchronizeStateNode() {
        subTree.forEach((key, node) -> {
            handleNullityOfReferredBean(node);
            if (isNotTopLevelBean() && (property.getInstance() == null || property.getValue() == null)) {
                node.property.setInstance(null);
            }
            if (node instanceof SimpleNode && stateNode != null) {
                stateNode.getFeature(ElementPropertyMap.class).setProperty(key, ((SimpleNode) node).getConvertedPropertyValue());
            } else {
                node.synchronizeStateNode();
            }
        });
    }

    /**
     * Handles the occasions of nullity, and value change of the underlying bean.
     * <p>
     * Nullity of the bean means all leaf elements of the subtree should become nulls as well, or empty
     * collections if the type is collection.
     */
    private void handleNullityOfReferredBean(BindingNode node) {
        if (isNotTopLevelBean()) {
            if (property.getInstance() == null) {
                node.property.setInstance(null);
            } else {
                if (property.getValue() == null) {
                    node.property.setInstance(null);
                } else if (node.property.getInstance() != property.getValue()) {
                    node.property.setInstance(property.getValue());
                }
            }
        }
    }

    /**
     * Top-level beans (those which reside on the top level of the tree and the root of the path)
     * contains a top-level synthetic property, because there's no container class for them
     */
    private boolean isNotTopLevelBean() {
        return !(property instanceof TopLevelSyntheticProperty);
    }

    void refreshExtractedPropertiesInstances() {
        subTree.forEach((key, node) -> node.property.setInstance(property.getValue()));
    }

    @Override
    public BindingNode cloneWithProperty(AbstractPropertyMetadata<? super Object> property) {
        BeanNode result = new BeanNode(name, property, classMetadata, manager);
        subTree.forEach((key, node) ->
            result.subTree.put(key, node.cloneWithProperty(node.property.clone(property.getValue()))));
        return result;
    }
}
