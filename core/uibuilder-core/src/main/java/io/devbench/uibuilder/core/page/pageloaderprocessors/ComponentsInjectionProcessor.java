/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.page.ElementParserContext;
import io.devbench.uibuilder.core.page.ElementParsingComponentManager;
import io.devbench.uibuilder.core.page.PageLoaderContext;

public class ComponentsInjectionProcessor extends PageLoaderProcessor {

    @Override
    public void process(PageLoaderContext context) {
        context.getPageElement().children().stream()
            .map(element -> new ElementParsingComponentManager(
                ElementParserContext.builder()
                    .element(element)
                    .componentInjectionPoints(ControllerBeanManager.getInstance().findInjectionPointsById())
                    .stateNodeManager(context.getStateNodeManager())
                    .bindings(context.getBindings())
                    .mainContainerElement(context.getDomBind().getElement())
                    .parent(null)
                    .availableFlowTargetEventConsumer(context.getFlowNavigationMethods()::add)
                    .htmlPath(context.getHtmlPath())
                    .build()
            ))
            .forEach(ElementParsingComponentManager::manageComponent);
    }

}
