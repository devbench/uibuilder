/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.JsonSerializable;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.internal.JsonSerializer;
import com.vaadin.flow.internal.StateNode;
import elemental.json.JsonArray;
import elemental.json.JsonNull;
import elemental.json.JsonObject;
import elemental.json.JsonValue;
import io.devbench.uibuilder.api.components.form.UIBuilderDetailCapable;
import io.devbench.uibuilder.api.controllerbean.injectionpoint.ComponentInjectionPoint;
import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.exceptions.ControllerBeanParameterInjectionException;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.core.utils.ElementCollector;
import io.devbench.uibuilder.core.utils.HtmlElementAwareComponent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

public class InjectionUtils {
    public static final String EVENT_DETAIL_PREFIX = "event.detail.";
    public static final String EVENT_DATASET_PREFIX = "target.dataset.";

    public static Object getCollectionAwareItem(Object item, Class<?> parameterType) {
        if (item != null) {
            if (Collection.class.isAssignableFrom(parameterType)) {
                if (parameterType.isAssignableFrom(item.getClass())) {
                    if (List.class.isAssignableFrom(parameterType)) {
                        return Collections.unmodifiableList((List<?>) item);
                    } else if (Set.class.isAssignableFrom(parameterType)) {
                        return Collections.unmodifiableSet((Set<?>) item);
                    } else {
                        return Collections.unmodifiableCollection((Collection<?>) item);
                    }
                } else {
                    if (!(item instanceof Collection)) {
                        return Collections.singletonList(item);
                    } else {
                        throw new InternalResolverException(
                            "Method parameter type (" + parameterType.getName() + ") is not assignable from item type: " + item.getClass().getName());
                    }
                }
            } else {
                return item instanceof Collection ? ((Collection<?>) item).size() == 1 ? ((Collection<?>) item).iterator().next() : null : item;
            }
        }
        return null;
    }


    public static Component getInjectionPointValueForParameter(Method method, Parameter parameter, List<ComponentInjectionPoint> injectionPoints) {
        return injectionPoints.stream()
            .filter(injectionPoint -> injectionPoint.isParameterMatching(method, parameter))
            .findAny()
            .map(matchingInjectionPoint -> matchingInjectionPoint.getValueForParameter(parameter))
            .orElse(null);
    }

    public static Object getClientParameterValue(String rowParameterName, StateNodeManager stateNodeManager, DomEvent event, Class<?> parameterType) {
        final JsonObject eventData = event.getEventData();
        if (eventData == null) {
            return null;
        }
        return findValueByKey(rowParameterName, stateNodeManager, eventData, parameterType);
    }

    @Nullable
    public static Object findValueByKey(String rowParameterName, StateNodeManager stateNodeManager, JsonObject eventData, Class<?> parameterType) {
        final String keyPath = parseParameterName(rowParameterName);
        if (!isContainEventDetailKey(eventData, keyPath)) {
            return null;
        }
        return findParameterByEventData(stateNodeManager, getEventDetailValue(eventData, keyPath), parameterType);
    }

    @Nullable
    public static Object findParameterByEventData(StateNodeManager stateNodeManager, JsonValue jsonValue, Class<?> parameterType) {
        if (jsonValue instanceof JsonObject || jsonValue instanceof JsonArray) {
            return findParameterInServer(stateNodeManager, jsonValue, parameterType);
        } else if (jsonValue instanceof JsonNull) {
            return null;
        } else {
            return jsonValue.asString();
        }
    }

    @Nullable
    public static Object findParameterInServer(StateNodeManager stateNodeManager, JsonValue jsonValue, Class<?> parameterType) {
        if (jsonValue instanceof JsonObject && isContainEventDetailKey((JsonObject) jsonValue, "nodeId")) {
            return stateNodeManager.getActualValueFromStateNode(findStateNodeByJson((JsonObject) jsonValue));
        } else if (jsonValue instanceof JsonObject && JsonObject.class.isAssignableFrom(parameterType)) {
            return jsonValue;
        } else if (jsonValue instanceof JsonArray && JsonArray.class.isAssignableFrom(parameterType)) {
            return jsonValue;
        } else if (JsonSerializable.class.isAssignableFrom(parameterType)) {
            return JsonSerializer.toObject(parameterType, jsonValue);
        } else {
            if (jsonValue instanceof JsonArray && Collection.class.isAssignableFrom(parameterType)) {
                Collection<Object> resolvedList = createCollectionByParameterType(parameterType);
                JsonArray jsonArray = ((JsonArray) jsonValue);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JsonValue val = jsonArray.get(i);
                    if (val instanceof JsonObject) {
                        resolvedList.add(
                            resolveItemByKey((JsonObject) val)
                                .orElseThrow(() -> new ControllerBeanParameterInjectionException("Could not resolve item in an array")));
                    } else {
                        resolvedList.add(JsonSerializer.toObject(String.class, val));
                    }
                }
                return resolvedList;
            } else if (jsonValue instanceof JsonObject) {
                return resolveItemByKey((JsonObject) jsonValue).orElse(null);
            } else {
                return null;
            }
        }
    }

    private static Collection<Object> createCollectionByParameterType(Class<?> parameterType) {
        if (Set.class.isAssignableFrom(parameterType)) {
            return new HashSet<>();
        } else {
            return new ArrayList<>();
        }
    }

    public static JsonValue getEventDetailValue(JsonObject eventData, String key) {
        return eventData.get(EVENT_DETAIL_PREFIX + key);
    }

    public static boolean isContainEventDetailKey(JsonObject eventData, String key) {
        return eventData.hasKey(EVENT_DETAIL_PREFIX + key);
    }

    @NotNull
    public static String parseParameterName(String parameterName) {
        return parameterName.replace('.', '_');
    }

    private static Optional<Object> resolveItemByKey(JsonObject jsonObject) {
        if (jsonObject.hasKey("___formId")) {
            return ElementCollector.getById(jsonObject.getString("___formId"))
                .map(HtmlElementAwareComponent::getComponent)
                .filter(UIBuilderDetailCapable.class::isInstance)
                .map(UIBuilderDetailCapable.class::cast)
                .map(UIBuilderDetailCapable::getItem);
        } else {
            return UIContext.getContext().get(ItemResolver.class).getByItemKey(jsonObject);
        }
    }

    public static StateNode findStateNodeByJson(JsonObject object) {
        return UI.getCurrent().getInternals().getStateTree().getNodeById((int) object.getNumber("nodeId"));
    }

}
