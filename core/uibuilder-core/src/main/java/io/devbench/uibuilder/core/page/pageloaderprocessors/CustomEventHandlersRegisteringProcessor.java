/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.vaadin.flow.dom.DebouncePhase;
import com.vaadin.flow.dom.DomListenerRegistration;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.internal.StateNode;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.controllerbean.UIEventHandlerContext;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.core.utils.PropertyUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
public class CustomEventHandlersRegisteringProcessor extends PageLoaderProcessor {

    public static final String EVENT_PREFIX = "uibuilder-custom-event-";

    private static final Config CONFIG = ConfigFactory.load();
    private static final String EVENT_HANDLERS_DEBOUNCE = "eventHandlers.debounce";

    Config getConfig() {
        return CONFIG;
    }

    @Override
    public void process(PageLoaderContext context) {
        Set<String> qualifiedNames = ControllerBeanManager.getInstance().getEventHandlerQualifiedNames();
        StateNode node = context.getDomBind().getElement().getNode();

        for (String qualifiedName : qualifiedNames) {
            registerListenerMethodToSendEventHandlerSpecificEventToBackend(context, node, qualifiedName);
            DomListenerRegistration listenerRegistration =
                registerEventListenerThatSynchronizesStateNodesAndCallsTheSpecificEventHandler(context, qualifiedName);

            if (getConfig().hasPath(EVENT_HANDLERS_DEBOUNCE)) {
                int debounce = getConfig().getInt(EVENT_HANDLERS_DEBOUNCE);
                listenerRegistration.debounce(debounce, DebouncePhase.TRAILING);
            }

            ControllerBeanManager.getInstance().getEventHandlerContext(qualifiedName)
                .map(UIEventHandlerContext::getDebounceTimeout)
                .filter(it -> it != -1)
                .ifPresent(it -> listenerRegistration.debounce(it, DebouncePhase.TRAILING));

            registerEventDataExpressionsSpecifiedInValueAnnotationsToEventListeners(qualifiedName, listenerRegistration);
            registerEventHandlerSpecificEventAsAPropertySynchronizerEvent(context, qualifiedName);
        }
    }

    private void registerEventHandlerSpecificEventAsAPropertySynchronizerEvent(PageLoaderContext context, String qualifiedName) {
        Element element = context.getDomBind().getElement();
        PropertyUtils.addSynchronizedProperty(element, EVENT_PREFIX + qualifiedName);
    }

    private void registerEventDataExpressionsSpecifiedInValueAnnotationsToEventListeners(String qualifiedName, DomListenerRegistration listenerRegistration) {
        ControllerBeanManager.getInstance().getEventHandlerContext(qualifiedName)
            .ifPresent(eh -> eh.getEventDataExpressions()
                .forEach(listenerRegistration::addEventData));
    }

    private DomListenerRegistration registerEventListenerThatSynchronizesStateNodesAndCallsTheSpecificEventHandler(
        PageLoaderContext context, String qualifiedName) {

        return context.getDomBind().getElement().addEventListener(EVENT_PREFIX + qualifiedName, event -> {
            StateNodeManager stateNodeManager = context.getStateNodeManager();

            stateNodeManager.synchronizeProperties();
            ControllerBeanManager.getInstance().callEventHandlers(qualifiedName, stateNodeManager, event);
            stateNodeManager.synchronizeStateNodes();
        });
    }

    private void registerListenerMethodToSendEventHandlerSpecificEventToBackend(PageLoaderContext context, StateNode node, String qualifiedName) {
        String normalizedQualifiedName = qualifiedName.replaceAll("::", "_");
        String eventDetailObject = ControllerBeanManager.getInstance().getEventHandlerContext(qualifiedName)
            .map(UIEventHandlerContext::createJsObjectFromRequestedParameters).orElse("{}");

        node.runWhenAttached(ui -> ui.getInternals().getStateTree().beforeClientResponse(node,
            ctx -> {
                String formattedJavascriptCodeToRun = String.format(
                    "document.getElementById('%s').%s = function(event) { " +
                        "this.dispatchEvent(new CustomEvent('%s%s', %s)) " +
                        "}",
                    context.getDomBind().getId().orElse("page"),
                    normalizedQualifiedName,
                    EVENT_PREFIX,
                    qualifiedName,
                    eventDetailObject
                );
                ctx.getUI().getPage().executeJs(formattedJavascriptCodeToRun);
            }));
    }

}
