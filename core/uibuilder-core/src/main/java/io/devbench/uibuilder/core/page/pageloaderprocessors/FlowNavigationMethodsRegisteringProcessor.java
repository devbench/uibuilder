/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import com.vaadin.flow.internal.StateNode;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.page.ElementParsingComponentManager;
import io.devbench.uibuilder.core.page.PageLoaderContext;

public class FlowNavigationMethodsRegisteringProcessor extends PageLoaderProcessor {

    @Override
    public void process(PageLoaderContext context) {
        StateNode node = context.getDomBind().getElement().getNode();

        for (String flowNavigationMethod : context.getFlowNavigationMethods()) {
            node.runWhenAttached(ui -> ui.getInternals().getStateTree().beforeClientResponse(node,
                ctx -> {
                    ctx.getUI().getPage().executeJs(
                        String.format(
                            "document.getElementById('%s').%s = function(event) { " +
                                "this.dispatchEvent(new CustomEvent('%s%s', {})) " +
                                "}",
                            context.getDomBind().getId().orElse("page"),
                            flowNavigationMethod,
                            CustomEventHandlersRegisteringProcessor.EVENT_PREFIX,
                            flowNavigationMethod
                        ));
                }));
            context.getDomBind().getElement().addEventListener(CustomEventHandlersRegisteringProcessor.EVENT_PREFIX + flowNavigationMethod, event -> {
                FlowManager.getCurrent().navigate(flowNavigationMethod.substring(ElementParsingComponentManager.FLOW_FUNCTION_PREFIX.length()), true);
                // TODO implement parameter handling
            });
        }
    }

}
