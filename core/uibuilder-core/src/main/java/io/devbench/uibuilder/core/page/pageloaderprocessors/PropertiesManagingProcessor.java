/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.ElementPropertyMap;
import com.vaadin.flow.shared.Registration;
import elemental.json.Json;
import elemental.json.JsonArray;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class PropertiesManagingProcessor extends PageLoaderProcessor {

    @Override
    public void process(PageLoaderContext context) {
        final AtomicReference<Runnable> managePropertiesAfterDomChangedReference = new AtomicReference<>(null);

        Registration domListenerRegistration = context.getDomBind().getElement().addEventListener("innerHTMLSet", event -> {
            if (managePropertiesAfterDomChangedReference.get() != null) {
                managePropertiesAfterDomChangedReference.get().run();
            }
        });

        managePropertiesAfterDomChangedReference.set(() -> {
            managePropertiesAfterDomChanged(context);
            managePropertiesAfterDomChangedReference.set(null);
            domListenerRegistration.remove();
        });
    }

    private void managePropertiesAfterDomChanged(PageLoaderContext context) {
        try {
            Set<String> allProperties = Stream.concat(context.getStateNodeManager().getPropertyNames().stream(),
                context.getBindings().stream()).collect(Collectors.toSet());
            ElementPropertyMap.getModel(context.getDomBind().getElement().getNode()).setUpdateFromClientFilter(allProperties::contains);
            registerPropertiesToSynchronize(context);
            populateProperties(context);
        } catch (Exception e) {
            log.error("", e);
        }
    }

    private void registerPropertiesToSynchronize(PageLoaderContext context) {
        JsonArray finalPropertyNames = Json.createArray();
        int index = 0;
        for (String binding : context.getBindings()) {
            finalPropertyNames.set(index++, binding);
        }

        StateNode node = context.getDomBind().getElement().getNode();
        node.runWhenAttached(ui -> ui.getInternals().getStateTree()
            .beforeClientResponse(node,
                ctx -> ctx.getUI().getPage().executeJs(
                    "this.registerUpdatableModelProperties($0, $1)",
                    context.getDomBind().getElement(),
                    finalPropertyNames)));

        node.runWhenAttached(ui -> ui.getInternals().getStateTree()
            .beforeClientResponse(node,
                ctx -> ctx.getUI().getPage().executeJs(
                    "this.populateModelProperties($0, $1)",
                    context.getDomBind().getElement(),
                    Json.createArray()))); //for us it will be always empty, because there are no top level properties that aren't statenodes
    }

    private void populateProperties(PageLoaderContext context) {
        Map<String, Serializable> propertyValues = context.getStateNodeManager().populatePropertyValues();
        propertyValues.forEach((name, value) -> {
            ElementPropertyMap propertyMap = context.getDomBind().getElement().getNode().getFeature(ElementPropertyMap.class);
            propertyMap.setProperty(name, value);
        });
    }

}
