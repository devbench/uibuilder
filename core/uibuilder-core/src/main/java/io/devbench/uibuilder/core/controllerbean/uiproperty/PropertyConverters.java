/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty;

import io.devbench.uibuilder.core.controllerbean.uiproperty.converters.FunctionPropertyConverter;
import io.devbench.uibuilder.core.controllerbean.uiproperty.converters.LocalDateTimePropertyConverter;
import io.devbench.uibuilder.core.controllerbean.uiproperty.converters.SupplierPropertyConverter;
import io.devbench.uibuilder.core.exceptions.PropertyConverterException;
import io.devbench.uibuilder.core.exceptions.PropertyConverterNotFoundException;
import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.*;

@Slf4j
public final class PropertyConverters {

    private PropertyConverters() {
    }

    public static final Map<Class<?>, PropertyConverter<?, ?>> IMPLICIT_CONVERTERS;
    private static final Map<Class<? extends Enum>, PropertyConverter<?, ?>> ENUM_CACHE = new HashMap<>();
    private static final Map<Class<?>, PropertyConverter<?, ?>> FUNCTIONAL_CONVERTER;

    private static Optional<PropertyConverter<?, ?>> getFunctionalConverterByType(Class type) {
        Set<Class> functionalClasses = new HashSet<>();
        functionalClasses.add(type);
        functionalClasses.addAll(Arrays.asList(type.getInterfaces()));
        functionalClasses.remove(Object.class);

        for (Class functionalClass : functionalClasses) {
            PropertyConverter<?, ?> propertyConverter = FUNCTIONAL_CONVERTER.get(functionalClass);
            if (propertyConverter != null) {
                return Optional.of(propertyConverter);
            }
        }

        return Optional.empty();
    }

    @SuppressWarnings("unchecked")
    public static String convertToString(Object object) {
        if (object == null) {
            return null;
        }

        PropertyConverter<Object, ?> converter = (PropertyConverter<Object, ?>) getConverterByType(object.getClass());
        return converter == null ? object.toString() : converter.convertTo(object).toString();
    }

    public static List<String> convertToStringList(List<?> objects) {
        return objects.stream()
            .map(PropertyConverters::convertToString)
            .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public static <T> T convertToObject(Class<T> objectType, String serializedValue) {
        if (serializedValue == null || objectType == null) {
            return null;
        }

        PropertyConverter<T, String> converter = (PropertyConverter<T, String>) getConverterByType(objectType);
        return converter == null ? (T) serializedValue : converter.convertFrom(serializedValue);
    }

    public static <T> List<T> convertToObjectList(Class<T> objectType, List<String> serializedObjects) {
        try {
            return serializedObjects.stream()
                .map(serializedObject -> convertToObject(objectType, serializedObject))
                .collect(Collectors.toList());
        } catch (Exception e) {
            log.warn("Could not convert serialized objects to object list");
            if (log.isDebugEnabled()) {
                log.debug("Could not convert serialized objects to object list", e);
            }
            return Collections.emptyList();
        }
    }

    public static boolean isConvertibleProperty(AbstractPropertyMetadata<?> property) {
        return property.isAnnotationPresent(Converter.class)
            || IMPLICIT_CONVERTERS.containsKey(property.getType())
            || getFunctionalConverterByType(property.getType()).isPresent();
    }

    @NotNull
    public static PropertyConverter<?, ?> getConverterFor(AbstractPropertyMetadata<?> property) {
        if (property.isAnnotationPresent(Converter.class)) {
            return getConverterByAnnotation(property);
        } else if (property.getType().isEnum()) {
            return createConverterForEnum(property.getType());
        } else if (IMPLICIT_CONVERTERS.containsKey(property.getType())) {
            return createImplicitPropertyConverterFor(property);
        } else {
            return getFunctionalConverterByType(property.getType()).orElseThrow(
                () -> new PropertyConverterNotFoundException("Property converter for property not found, Class: " + property.typeMeta().getTargetClass() +
                    " property name: " + property.getName()));
        }
    }

    private static PropertyConverter<?, ?> createImplicitPropertyConverterFor(AbstractPropertyMetadata<?> property) {
        return IMPLICIT_CONVERTERS.get(property.getType());
    }

    @SuppressWarnings("unchecked")
    private static PropertyConverter<?, ?> createConverterForEnum(Class<?> type) {
        return ENUM_CACHE.computeIfAbsent((Class<? extends Enum>) type, EnumPropertyConverter::new);
    }

    private static PropertyConverter<?, ?> getConverterByAnnotation(AbstractPropertyMetadata<?> property) {
        Class<? extends PropertyConverter<?, ?>> converterClass = property.getAnnotation(Converter.class).value();
        try {
            return converterClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new PropertyConverterException("Cannot instantiate property converter: " + converterClass.getSimpleName(), e);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> PropertyConverter<T, ?> getConverterByType(Class<T> type) {
        return (PropertyConverter<T, ?>) IMPLICIT_CONVERTERS.getOrDefault(type, getFunctionalConverterByType(type).orElse(null));
    }

    static {
        Map<Class<?>, PropertyConverter<?, ?>> converters = new HashMap<>();

        converters.put(String.class, (StringPropertyConverter<String>) value -> value);

        converters.put(boolean.class, (StringPropertyConverter<Boolean>) Boolean::parseBoolean);
        converters.put(Boolean.class, (StringPropertyConverter<Boolean>) Boolean::parseBoolean);
        converters.put(double.class, (StringPropertyConverter<Double>) Double::parseDouble);
        converters.put(Double.class, (StringPropertyConverter<Double>) Double::parseDouble);
        converters.put(Integer.class, (StringPropertyConverter<Integer>) Integer::parseInt);
        converters.put(int.class, (StringPropertyConverter<Integer>) Integer::parseInt);
        converters.put(Long.class, (StringPropertyConverter<Long>) Long::parseLong);
        converters.put(long.class, (StringPropertyConverter<Long>) Long::parseLong);
        converters.put(BigInteger.class, (StringPropertyConverter<BigInteger>) BigInteger::new);
        converters.put(BigDecimal.class, (StringPropertyConverter<BigDecimal>) BigDecimal::new);

        converters.put(Date.class, new DatePropertyConverter());
        converters.put(LocalDate.class, (StringPropertyConverter<LocalDate>) LocalDate::parse);
        converters.put(LocalTime.class, (StringPropertyConverter<LocalTime>) LocalTime::parse);
        converters.put(LocalDateTime.class, new LocalDateTimePropertyConverter());

        //Commons
        converters.put(UUID.class, (StringPropertyConverter<UUID>) UUID::fromString);
        converters.put(Byte.class, (StringPropertyConverter<Byte>) Byte::parseByte);
        converters.put(Short.class, (StringPropertyConverter<Short>) Short::parseShort);
        converters.put(Float.class, (StringPropertyConverter<Float>) Float::parseFloat);

        IMPLICIT_CONVERTERS = Collections.unmodifiableMap(converters);

        Map<Class<?>, PropertyConverter<?, ?>> functionalInterfaceConverters = new HashMap<>();
        functionalInterfaceConverters.put(Supplier.class, new SupplierPropertyConverter());
        functionalInterfaceConverters.put(Function.class, new FunctionPropertyConverter());
        FUNCTIONAL_CONVERTER = Collections.unmodifiableMap(functionalInterfaceConverters);
    }


    /**
     * @deprecated use the new DateTime API instead of Date
     */
    @Deprecated
    private static class DatePropertyConverter implements StringPropertyConverter<Date> {

        private DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        @Override
        public String convertTo(Date value) {
            if (value != null) {
                return df.format(value);
            }
            return null;
        }

        @Override
        public Date apply(@NotNull String value) {
            try {
                return df.parse(value);
            } catch (ParseException e) {
                return null;
            }
        }
    }

    private static class EnumPropertyConverter<T extends Enum<T>> implements StringPropertyConverter<Enum<T>> {
        private final Class<T> type;
        private final Map<String, T> collect;

        EnumPropertyConverter(@NotNull Class<T> type) {
            this.type = type;
            collect = Arrays.stream(type.getEnumConstants()).collect(toMap(Object::toString, Function.identity()));
        }

        @Override
        public Enum<T> apply(@NotNull String value) {
            return collect.get(value);
        }
    }
}
