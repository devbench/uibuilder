/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.api.components.TagGroupHelper;
import io.devbench.uibuilder.core.page.Page;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.core.page.exceptions.PageDefinitionMissingException;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import java.util.Optional;

public class FindPageElementProcessor extends PageLoaderProcessor {


    @Override
    public void process(PageLoaderContext context) {
        Optional<Element> optionalPageElement = TagGroupHelper.findElementsByGroupComponentClass(context.getDocument(), Page.class).stream().findFirst();

        context.setPageElement(optionalPageElement.orElseThrow(() -> new PageDefinitionMissingException(context.getHtmlPath())));

        removeCommentsRecursively(context.getPageElement());
    }

    private void removeCommentsRecursively(Node node) {
        int i = 0;
        while (i < node.childNodes().size()) {
            Node child = node.childNode(i);
            if (child instanceof Comment) {
                child.remove();
            } else {
                removeCommentsRecursively(child);
                i++;
            }
        }
    }
}
