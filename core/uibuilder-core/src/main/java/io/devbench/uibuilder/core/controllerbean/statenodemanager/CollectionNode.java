/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.ModelList;
import io.devbench.uibuilder.core.exceptions.StateNodeBeanSynchronizationException;
import io.devbench.uibuilder.core.exceptions.StateNodeInvalidReferenceException;
import io.devbench.uibuilder.core.exceptions.StateNodeUnsupportedPropertytypeException;
import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;


public abstract class CollectionNode extends BindingNode {

    public static final String ITEM_REFERENCE_STRING = "item";

    @Getter
    protected BindingNode prototypeNode;

    protected List<BindingNode> collectionNodes;

    private List<? super Object> valueHolderList;

    protected CollectionSyntheticProperty syntheticPropertyPrototype;

    CollectionNode(String name, AbstractPropertyMetadata<? super Object> property, ClassMetadata<?> classMetadata, StateNodeManager manager) {
        super(name, property, classMetadata, manager);
        collectionNodes = new ArrayList<>();
        createPrototypeNode();
    }

    public static CollectionNode createNode(
        String name, AbstractPropertyMetadata<? super Object> referredProperty, ClassMetadata<?> classMetadata, StateNodeManager manager) {

        if (List.class.isAssignableFrom(referredProperty.getType())) {
            return new ListNode(name, referredProperty, classMetadata, manager);
        } else if (Set.class.isAssignableFrom(referredProperty.getType())) {
            return new SetNode(name, referredProperty, classMetadata, manager);
        } else {
            throw new StateNodeUnsupportedPropertytypeException("Not supported collection type: " + referredProperty.getType());
        }
    }

    private void createPrototypeNode() {
        syntheticPropertyPrototype = new CollectionSyntheticProperty(property);
        prototypeNode = createNodeFromProperty(syntheticPropertyPrototype, syntheticPropertyPrototype.typeMeta());
    }

    @Override
    protected Serializable populateCollectionValues() {
        return populateValues();
    }

    protected void setupModelListInStateNode() {
        ModelList modelList = stateNode.getFeature(ModelList.class);
        modelList.clear();
        collectionNodes.stream()
            .filter(node -> !(node instanceof BeanNode) || !node.isLeaf())
            .forEach(node -> modelList.add((StateNode) node.populateCollectionValues()));
    }

    @Override
    public List<BindingNodeSyncError> synchronizeProperty(@NotNull String parentPropertyPath) {
        List<BindingNodeSyncError> retList = new ArrayList<>();
        ModelList modelList = stateNode.getFeature(ModelList.class);
        if (modelList.size() == collectionNodes.size()) {
            for (BindingNode bindingNode : collectionNodes) {
                List<BindingNodeSyncError> bindingNodeSyncErrors = bindingNode.synchronizeProperty(calcPropertyPath(parentPropertyPath, this));
                retList.addAll(bindingNodeSyncErrors);
            }
        } else {
            retList.add(
                new BindingNodeSyncError(
                    this,
                    new StateNodeBeanSynchronizationException("Bound collections not allowed to be modified directly on the client side"),
                    calcPropertyPath(parentPropertyPath, this)
                )
            );
        }
        return retList;
    }

    @Override
    public BindingNode getNodeAt(String[] path) {
        if (path.length == 1 && ITEM_REFERENCE_STRING.equals(path[0])) {
            return prototypeNode;
        } else if (path.length > 1 && ITEM_REFERENCE_STRING.equals(path[0])) {
            return prototypeNode.getNodeAt(path);
        } else {
            throw new StateNodeInvalidReferenceException("Invalid reference in collection: " + Arrays.toString(path));
        }
    }

    public void addCollectionPath(String binding) {
        if (prototypeNode instanceof BeanNode) {
            String[] path = binding.split("\\.");
            if (path.length > 1) {
                prototypeNode.addPath(path, 0);
            }
        } else if (binding.contains(".")) {
            throw new StateNodeInvalidReferenceException("Collection containing non-bean items was referenced with path: " + binding);
        }
    }

    protected StateNode createStateNodeFromCollectionNodes() {
        stateNode = new StateNode(ModelList.class);
        setupModelListInStateNode();
        manager.registerValueProvider(node -> (node != null && node.getId() == stateNode.getId()) ? property.getValue() : null);
        return stateNode;
    }

}
