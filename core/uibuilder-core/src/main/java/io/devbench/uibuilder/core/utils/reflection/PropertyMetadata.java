/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils.reflection;

import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Supplier;

@Slf4j
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PropertyMetadata<CONTAINER_TYPE> implements AbstractPropertyMetadata<CONTAINER_TYPE> {

    @Getter
    private String name;

    @Getter
    private Class<?> type;

    private ParameterizedType parameterizedType;

    private ClassMetadata<?> typeMetadataCache;

    @Getter
    private Field field;

    @Getter
    private Method getter;

    @Getter
    private Method setter;

    @Getter
    private CONTAINER_TYPE instance;

    @Getter
    private Class<CONTAINER_TYPE> containerClass;

    private boolean inheritanceEnabled;


    PropertyMetadata(CONTAINER_TYPE instance, Class<CONTAINER_TYPE> containerClass, Field field) {
        this(instance, containerClass, field, false);
    }

    PropertyMetadata(CONTAINER_TYPE instance, Class<CONTAINER_TYPE> containerClass, Field field, boolean inheritanceEnabled) {
        this.instance = instance;
        this.containerClass = containerClass;
        this.inheritanceEnabled = inheritanceEnabled;
        this.field = field;
        buildByField();
    }

    PropertyMetadata(CONTAINER_TYPE instance, Class<CONTAINER_TYPE> containerClass, Method getter) {
        this(instance, containerClass, getter, false);
    }

    PropertyMetadata(CONTAINER_TYPE instance, Class<CONTAINER_TYPE> containerClass, Method getter, boolean inheritanceEnabled) {
        this.instance = instance;
        this.containerClass = containerClass;
        this.inheritanceEnabled = inheritanceEnabled;
        this.getter = getter;
        buildByGetter();
    }

    public static Optional<Class<?>> findGenericType(Field field) {
        if (field.getGenericType() instanceof ParameterizedType) {
            Type paramType = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
            Class<?> objectProviderTypeParameter;
            if (paramType instanceof ParameterizedType) {
                objectProviderTypeParameter = (Class<?>) ((ParameterizedType) paramType).getRawType();
            } else {
                objectProviderTypeParameter = (Class<?>) paramType;
            }
            return Optional.ofNullable(objectProviderTypeParameter);
        }
        return Optional.empty();
    }

    private void checkInstance() {
        if (instance == null) {
            throw new InternalResolverException(String.format("Instance is not set on metadata, to handle request the type is: %s name is: %s " +
                "containerClass is: %s getter is: %s filed is: %s", type.getName(), name, containerClass.getName(), getter, field));
        }
    }

    private void buildByGetter() {
        type = getter.getReturnType();
        if (getter.getGenericReturnType() instanceof ParameterizedType) {
            parameterizedType = (ParameterizedType) getter.getGenericReturnType();
        }

        if (getter.getReturnType().equals(boolean.class)) {
            name = getter.getName().substring(2);
        } else {
            name = getter.getName().substring(3);
        }
        try {
            setter = containerClass.getMethod("set" + name, getter.getReturnType());
        } catch (NoSuchMethodException ignore) {
        }

        name = StringUtils.uncapitalize(name);

        try {
            field = MetadataReflectionUtils.getField(containerClass, name);
            type = field.getType(); //if there is a field we should prefer the field type
        } catch (NoSuchFieldException ignore) {
        }
    }

    private void buildByField() {
        name = field.getName();
        type = field.getType();
        Type genericType = field.getGenericType();
        if (genericType instanceof ParameterizedType) {
            parameterizedType = (ParameterizedType) genericType;
        }

        String getterPrefix = (field.getType().equals(boolean.class)) ? "is" : "get";
        String capitalizedFieldName = StringUtils.capitalize(field.getName());
        try {
            getter = containerClass.getMethod(getterPrefix + capitalizedFieldName);
        } catch (NoSuchMethodException ignore) {
        }
        try {
            setter = containerClass.getMethod("set" + capitalizedFieldName, field.getType());
        } catch (NoSuchMethodException ignore) {
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <VALUE> VALUE getValue() {
        checkInstance();
        if (getter != null) {
            try {
                return (VALUE) getter.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.error("Cannot fetch value of property, with getter: " + getter.getName(), e);
                return null;
            }
        } else {
            return (VALUE) handleAccessibilityAround(() -> {
                try {
                    return field.get(instance);
                } catch (IllegalAccessException e) {
                    log.error("Cannot fetch value of property, with field: " + field.getName(), e);
                    return null;
                }
            });
        }
    }

    @Override
    public <VALUE> void setValue(VALUE value) {
        checkInstance();
        if (setter != null) {
            try {
                setter.invoke(instance, value);
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.error("Cannot set value with setter: " + setter.getName(), e);
            }
        } else {
            handleAccessibilityAround(() -> {
                try {
                    field.set(instance, value);
                    return null;
                } catch (IllegalAccessException e) {
                    log.error("Cannot set value with field: " + field.getName(), e);
                    return null;
                }
            });
        }
    }

    private <VALUE> VALUE handleAccessibilityAround(Supplier<VALUE> doStuff) {
        VALUE res;
        boolean wasNotAccessible = false;
        if (!field.isAccessible()) {
            wasNotAccessible = true;
            field.setAccessible(true);
        }
        res = doStuff.get();
        if (wasNotAccessible) {
            field.setAccessible(false);
        }
        return res;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> ClassMetadata<T> typeMeta() {
        if (typeMetadataCache == null) {
            T value = null;
            if (instance != null) {
                value = getValue();
            }
            if (!type.isPrimitive() && !type.isEnum() && value != null) {
                typeMetadataCache = new ClassMetadata<>((Class<T>) type, inheritanceEnabled).withInstance(value);
            } else if (value == null) {
                typeMetadataCache = new ClassMetadata<>((Class<T>) type, inheritanceEnabled);
            }
        }
        return (ClassMetadata<T>) typeMetadataCache;
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
        if (field != null && field.isAnnotationPresent(annotationClass)) {
            return true;
        } else if (getter != null) {
            return getter.isAnnotationPresent(annotationClass);
        }
        return false;
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotationClass) {
        if (field != null && field.isAnnotationPresent(annotationClass)) {
            return field.getAnnotation(annotationClass);
        } else if (getter != null) {
            return getter.getAnnotation(annotationClass);
        }
        return null;
    }

    @Override
    public AbstractPropertyMetadata<CONTAINER_TYPE> clone(CONTAINER_TYPE instance) {
        PropertyMetadata<CONTAINER_TYPE> res;
        if (field != null) {
            res = new PropertyMetadata<>(instance, containerClass, field, inheritanceEnabled);
        } else {
            res = new PropertyMetadata<>(instance, containerClass, getter, inheritanceEnabled);
        }
        return res;
    }

    @Override
    public void setInstance(CONTAINER_TYPE instance) {
        this.instance = instance;
        if (typeMetadataCache != null) {
            typeMetadataCache = null;
        }
    }

    public PropertyMetadata<CONTAINER_TYPE> withInstance(CONTAINER_TYPE instance) {
        return new PropertyMetadata<>(
            name, type, parameterizedType, null, field, getter, setter, instance, containerClass, inheritanceEnabled
        );
    }

    @Override
    public ParameterizedType getParameterizedType() {
        if (parameterizedType == null) {
            parameterizedType = tryToFindParametrizedTypeBySuperClass();
        }
        return parameterizedType;
    }

    public Optional<Class<?>> getFirstParameterizedType() {
        ParameterizedType parameterizedType = getParameterizedType();
        if (parameterizedType != null
            && parameterizedType.getActualTypeArguments() != null
            && parameterizedType.getActualTypeArguments().length == 1) {
            return Optional.of((Class<?>) parameterizedType.getActualTypeArguments()[0]);
        }
        return Optional.empty();
    }

    public boolean isCollection() {
        return Collection.class.isAssignableFrom(getType());
    }

    private ParameterizedType tryToFindParametrizedTypeBySuperClass() {
        final Class<?> superclass = instance.getClass().getSuperclass();
        if (superclass != Object.class) {
            return tryToGetParameterizedTypeBySuperClassField(superclass);
        }
        return null;
    }

    private ParameterizedType tryToGetParameterizedTypeBySuperClassField(Class<?> superclass) {
        if (field != null) {
            return getParameterizedTypeByField(superclass);
        } else if (getter != null) {
            return tryToGetParameterizedTypeBySupperClassGetter(superclass);
        }
        return null;
    }

    private ParameterizedType tryToGetParameterizedTypeBySupperClassGetter(Class<?> superclass) {
        try {
            final Type genericReturnType = superclass.getMethod(getter.getName()).getGenericReturnType();
            if (genericReturnType instanceof ParameterizedType) {
                return (ParameterizedType) genericReturnType;
            }
        } catch (NoSuchMethodException ignored) {
        }
        return null;
    }

    private ParameterizedType getParameterizedTypeByField(Class<?> superclass) {
        try {
            final Type genericType = superclass.getField(field.getName()).getGenericType();
            if (genericType instanceof ParameterizedType) {
                return (ParameterizedType) genericType;
            }
        } catch (NoSuchFieldException ignored) {
        }
        return null;
    }
}
