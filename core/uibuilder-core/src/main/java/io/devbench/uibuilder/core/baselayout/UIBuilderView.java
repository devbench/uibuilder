/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.baselayout;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.WildcardParameter;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.flow.FlowTarget;
import io.devbench.uibuilder.core.page.Page;
import io.devbench.uibuilder.core.page.View;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Slf4j
@Route("")
public class UIBuilderView extends View implements HasUrlParameter<String>, FlowTarget {

    private static final Config CONFIG = ConfigFactory.load();

    @Getter(AccessLevel.PACKAGE)
    private final Page page;
    private final FlowManager flowManager;
    private final List<Runnable> runOnViewReady;

    private Map<String, List<String>> parameters;
    private boolean viewReady;

    public UIBuilderView() {
        this(new Page());
    }

    public UIBuilderView(@NotNull Page page) {
        viewReady = false;
        runOnViewReady = new ArrayList<>();
        flowManager = new FlowManager(CONFIG.getString("flowDefinition"));
        registerSelf();
        this.page = page;
        add(page);
        setId("root-context");
        useBrowserLocale();
    }

    private void runOnViewReady(Runnable runnable) {
        if (!viewReady) {
            runOnViewReady.add(runnable);
        } else {
            runnable.run();
        }
    }

    @Override
    public void onViewReady() {
        viewReady = true;
        runOnViewReady.forEach(Runnable::run);
    }

    @Override
    public void setParameter(BeforeEvent event, @WildcardParameter String relativeUrl) {
        try {
            this.parameters = event
                .getLocation()
                .getQueryParameters()
                .getParameters();
        } catch (IllegalArgumentException | IllegalStateException e) {
            log.error("Could not extract query parameters", e);
        }

        if (relativeUrl == null || parameters == null) {
            throw new IllegalStateException("Cannot load page due to missing relative URL and query parameters");
        }

        try {
            flowManager.checkUrlRequest(relativeUrl);
            runOnViewReady(() -> flowManager.handleUrlRequest(relativeUrl, parameters));
        } catch (Exception exception) {
            event.rerouteToError(exception, "Flow URL request check failed");
        }
    }

    @Override
    public String getComponentId() {
        return "UI";
    }

    @Override
    public void loadContent(@NotNull String htmlPath, @Nullable InputStream htmlContent) {
        runOnViewReady(() -> page.loadContent(htmlPath, htmlContent));
    }

    @Override
    public void unloadContent() {
        page.unloadContent();
    }

    private void useBrowserLocale() {
        UI ui = UI.getCurrent();
        ui.setLocale(
            ui.getInternals()
                .getSession()
                .getBrowser()
                .getLocale()
        );
    }
}
