/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import com.vaadin.flow.internal.StateNode;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverters;
import io.devbench.uibuilder.core.exceptions.StateNodeInvalidReferenceException;
import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.core.utils.reflection.PropertyMetadata;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.lang3.ClassUtils;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@EqualsAndHashCode(of = {"name", "property", "stateNode", "classMetadata"})
public abstract class BindingNode {

    public static final String UNACCESSABLE_PROPERTY_REFERRED_WITH_BINDING_PATH = "Unaccessable property referred with binding path: %s";

    @Getter
    protected String name;

    protected Map<String, BindingNode> subTree;

    @NotNull
    protected AbstractPropertyMetadata<? super Object> property;

    protected StateNode stateNode;

    protected ClassMetadata<?> classMetadata;

    protected StateNodeManager manager;

    BindingNode(String name, @NotNull AbstractPropertyMetadata<? super Object> property, ClassMetadata<?> classMetadata, StateNodeManager manager) {
        this.name = name;
        this.property = property;
        this.classMetadata = classMetadata;
        this.manager = manager;
        subTree = new HashMap<>();
    }

    public boolean isLeaf() {
        return subTree.isEmpty();
    }

    protected boolean isBean(Class<?> type) {
        return !isBasic(type)
            && !(Class.class.isAssignableFrom(type))
            && !(Iterable.class.isAssignableFrom(type))
            && !(Map.class.isAssignableFrom(type));
    }

    protected boolean isBasic(Class<?> type) {
        return (String.class.isAssignableFrom(type))
            || ClassUtils.isPrimitiveOrWrapper(type)
            || type.isEnum();
    }

    protected boolean isCollection(Class<?> type) {
        return Collection.class.isAssignableFrom(type);
    }

    @SuppressWarnings("unchecked")
    void addPath(@NotNull String[] path, int index) {
        if (name.equals(path[index])) {
            index++;
            if (index < path.length) {
                String nodeName = path[index];
                if (!subTree.containsKey(nodeName)) {
                    classMetadata = property.typeMeta();
                    checkContainerClass(path);
                    PropertyMetadata<? super Object> referredProperty = (PropertyMetadata<? super Object>) classMetadata.property(nodeName).orElse(null);
                    checkReferredProperty(referredProperty, path);
                    subTree.put(nodeName, createNodeFromProperty(referredProperty, classMetadata));
                }
                subTree.get(nodeName).addPath(path, index);
            }
        }
    }

    private void checkReferredProperty(PropertyMetadata<?> referredProperty, String[] path) {
        if (referredProperty == null) {
            throw new StateNodeInvalidReferenceException(String.format(UNACCESSABLE_PROPERTY_REFERRED_WITH_BINDING_PATH, String.join(".", path)));
        }
    }

    private void checkContainerClass(String[] path) {
        if (classMetadata == null) {
            throw new StateNodeInvalidReferenceException(String.format(UNACCESSABLE_PROPERTY_REFERRED_WITH_BINDING_PATH, String.join(".", path)));
        }
    }

    protected BindingNode createNodeFromProperty(AbstractPropertyMetadata<? super Object> referredProperty, ClassMetadata<?> classMetadata) {
        if (isBean(referredProperty.getType()) && !isConvertible(referredProperty)) {
            return new BeanNode(referredProperty.getName(), referredProperty, classMetadata, manager);

        } else if (isCollection(referredProperty.getType()) && !isConvertible(referredProperty)) {
            return CollectionNode.createNode(referredProperty.getName(), referredProperty, classMetadata, manager);

        } else if (isBasic(referredProperty.getType()) || isConvertible(referredProperty)) {
            return new SimpleNode(referredProperty.getName(), referredProperty, classMetadata, manager);

        } else {
            throw new StateNodeInvalidReferenceException("Referred property cannot be used in bindings, property: " + referredProperty);
        }
    }

    private boolean isConvertible(AbstractPropertyMetadata<? super Object> referredProperty) {
        return PropertyConverters.isConvertibleProperty(referredProperty);
    }

    public BindingNode getNodeAt(String[] path) {
        BindingNode node = this;
        for (int i = 1; i < path.length; i++) {
            node = node.subTree.get(path[i]);
        }
        return node;
    }

    public abstract Serializable populateValues();

    protected abstract Serializable populateCollectionValues();

    public abstract List<BindingNodeSyncError> synchronizeProperty(@NotNull String parentPropertyPath);

    public abstract void synchronizeStateNode();

    public abstract BindingNode cloneWithProperty(AbstractPropertyMetadata<? super Object> property);

    protected final String calcPropertyPath(@NotNull String parentPropertyPath, BindingNode beanNode) {
        String thisPropertyPath;
        if (parentPropertyPath.isEmpty()) {
            thisPropertyPath = beanNode.property.getName();
        } else {
            thisPropertyPath = parentPropertyPath + "." + beanNode.property.getName();
        }
        return thisPropertyPath;
    }


}
