/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow.errorpages;

import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.flow.exceptions.FlowUnauthorized;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Slf4j
@SuppressWarnings("unused") // Vaadin error handling uses this class
public class FlowUnauthorizedErrorPage extends AbstractFlowErrorPage<FlowUnauthorized> {

    protected FlowUnauthorizedErrorPage() {
        super(HttpServletResponse.SC_UNAUTHORIZED, FlowUnauthorizedErrorPage::errorPagePathSupplier);
    }

    private static String errorPagePathSupplier(FlowManager flowManager) {
        return Optional
            .ofNullable(flowManager.getFlowDefinitions().getUnauthorizedPage())
            .orElse("page_unauthorized.html");
    }
}
