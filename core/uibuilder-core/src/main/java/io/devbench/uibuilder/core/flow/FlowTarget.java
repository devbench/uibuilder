/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public interface FlowTarget {

    String getComponentId();

    default void loadContent(@NotNull String htmlPath) {
        loadContent(htmlPath, (InputStream) null);
    }

    default void loadContent(@NotNull String htmlPath, @Nullable String htmlContent) {
        loadContent(htmlPath, htmlContent == null ? null : new ByteArrayInputStream(htmlContent.getBytes(StandardCharsets.UTF_8)));
    }

    void loadContent(@NotNull String htmlPath, @Nullable InputStream htmlContent);

    default void registerSelf() {
        FlowManager.getCurrent().registerFlowTarget(getComponentId(), this);
    }

    void unloadContent();

}
