/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.parse;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.parse.PageTransformer;
import io.devbench.uibuilder.api.singleton.SingletonManager;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class PageTransformersRegistry {

    private Set<PageTransformer> pageTransformers;

    private PageTransformersRegistry() {
        initialize();
    }

    private void initialize() {
        pageTransformers = Collections.unmodifiableSet(
            MemberScanner.getInstance().findClassesBySuperType(PageTransformer.class).stream()
                .filter(clazz -> !Modifier.isAbstract(clazz.getModifiers()) && !Modifier.isInterface(clazz.getModifiers()))
                .map(this::tryToInstantiatePageTransformerClass)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet())
        );
    }

    private PageTransformer tryToInstantiatePageTransformerClass(Class<? extends PageTransformer> clazz) {
        try {
            Constructor<? extends PageTransformer> constructor = clazz.getDeclaredConstructor();
            return constructor.newInstance();
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            log.warn("Cannot initialize page transformer class: " + clazz.getSimpleName(), e);
            return null;
        }
    }

    public Set<PageTransformer> getPageTransformerByElement(Element element) {
        return pageTransformers.stream().filter(pageTransformer -> pageTransformer.isApplicable(element)).collect(Collectors.toSet());
    }

    public static PageTransformersRegistry getInstance() {
        return SingletonManager.getInstanceOf(PageTransformersRegistry.class);
    }

    public static PageTransformersRegistry registerToActiveContext() {
        PageTransformersRegistry instance = new PageTransformersRegistry();
        SingletonManager.registerSingleton(instance);
        return instance;
    }

}
