/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.test;

import io.devbench.uibuilder.api.exceptions.UnsupportedElementException;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;

public final class ReflectionUtil {

    private ReflectionUtil() {
        throw new UnsupportedElementException();
    }

    public static void setInternalField(@NotNull Object targetObject, @NotNull Field field, @NotNull Object value) {
        boolean accessible = field.isAccessible();
        try {
            field.setAccessible(true);
            field.set(targetObject, value);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(e);
        } finally {
            field.setAccessible(accessible);
        }
    }

}
