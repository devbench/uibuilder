/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.test.singleton;

import io.devbench.uibuilder.api.singleton.SingletonManager;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.stream.Stream;


public class SingletonProviderForTestsExtension implements BeforeAllCallback, BeforeEachCallback {

    @Override
    public void beforeAll(ExtensionContext context) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        StaticSingletonHolderForTests.useStaticSingletons = true;
        Method method = SingletonManager.class.getDeclaredMethod("resetManager");
        method.setAccessible(true);
        method.invoke(null);
    }

    @Override
    public void beforeEach(ExtensionContext context) {
        if (context.getTestInstance().isPresent()) {
            context.getTestClass().ifPresent(clazz -> {
                getDeclaredFieldsRecursive(clazz)
                    .filter(field -> field.isAnnotationPresent(SingletonInstance.class))
                    .peek(field -> field.setAccessible(true))
                    .map(field -> {
                        try {
                            Object value = field.get(context.getTestInstance().get());
                            if (value != null) {
                                return Pair.of(field.getAnnotation(SingletonInstance.class).value(), value);
                            }
                        } catch (Throwable ignore) {
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .forEach(value -> StaticSingletonHolderForTests.staticSingletonInstances.put(value.getKey(), value.getValue()));
            });
        }
    }

    private Stream<Field> getDeclaredFieldsRecursive(Class<?> clazz) {
        if (clazz.getSuperclass() == null) {
            return Stream.of(clazz.getDeclaredFields());
        } else {
            return Stream.concat(
                Stream.of(clazz.getDeclaredFields()),
                getDeclaredFieldsRecursive(clazz.getSuperclass())
            );
        }
    }
}
