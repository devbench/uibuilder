/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.test.extensions;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.internal.UIInternalUpdater;
import com.vaadin.flow.component.internal.UIInternals;
import com.vaadin.flow.component.littemplate.LitTemplateParser;
import com.vaadin.flow.di.Instantiator;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.function.DeploymentConfiguration;
import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.test.annotations.DisableMockedSession;
import io.devbench.uibuilder.test.annotations.DisableMockedUI;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.mockito.Mockito;
import org.mockito.internal.util.MockUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BaseUIBuilderTestExtension implements BeforeEachCallback, AfterEachCallback {

    private UI vaadinUi;
    private VaadinSession vaadinSession;


    @Override
    public void afterEach(ExtensionContext context) {
        verifyVaadinUI();
        verifyVaadinSession();
    }

    private void verifyVaadinSession() {
        assertSame(vaadinSession, VaadinSession.getCurrent());
        VaadinSession.setCurrent(null);
    }

    private void verifyVaadinUI() {
        assertSame(vaadinUi, UI.getCurrent());
        UI.setCurrent(null);
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        mockVaadinService(context);
        mockVaadinSession(context);
        mockVaadinUI(context);
        mockVaadinInternals();
    }

    private void mockVaadinService(ExtensionContext context) throws IllegalAccessException {
        getFieldOfType(VaadinService.class, context)
            .filter(MockUtil::isMock)
            .ifPresent(service -> {
                VaadinService.setCurrent(service);

                Instantiator instantiator = mock(Instantiator.class);
                LitTemplateParser.LitTemplateParserFactory parserFactory = mock(LitTemplateParser.LitTemplateParserFactory.class);
                LitTemplateParser parser = mock(LitTemplateParser.class);
                DeploymentConfiguration deploymentConfiguration = mock(DeploymentConfiguration.class);

                doReturn(instantiator).when(service).getInstantiator();
                doReturn(parserFactory).when(instantiator).getOrCreate(LitTemplateParser.LitTemplateParserFactory.class);
                doReturn(parser).when(parserFactory).createParser();
                doReturn(deploymentConfiguration).when(service).getDeploymentConfiguration();
                doReturn(true).when(deploymentConfiguration).isProductionMode();
            });
    }

    private void mockVaadinSession(ExtensionContext context) throws NoSuchFieldException, IllegalAccessException {
        Optional<VaadinSession> vaadinSession = getFieldOfType(VaadinSession.class, context);
        Optional<VaadinService> vaadinService = getFieldOfType(VaadinService.class, context);
        if (testMethodHasAnnotation(DisableMockedSession.class, context)) {
            this.vaadinSession = null;
        } else {
            this.vaadinSession = vaadinSession.orElseGet(() -> new VaadinSession(vaadinService.orElse(null)));
            Field lockField = VaadinSession.class.getDeclaredField("lock");
            lockField.setAccessible(true);
            ReentrantLock lock = new ReentrantLock();
            lockField.set(this.vaadinSession, lock);
            lock.lock();
        }
        VaadinSession.setCurrent(this.vaadinSession);
    }

    private void mockVaadinInternals() {
        if (vaadinSession != null && vaadinUi != null) {
            UIInternals internals = vaadinUi.getInternals();
            internals.setSession(vaadinSession);
        }
    }

    private <T> Optional<T> getFieldOfType(Class<T> clazz, ExtensionContext context) throws IllegalAccessException {
        Class<?> testClass = context.getTestClass().orElseThrow(NoSuchElementException::new);
        Object testInstance = context.getTestInstance().orElseThrow(NoSuchElementException::new);
        List<Field> fieldList = Stream.of(testClass.getDeclaredFields())
            .filter(field -> clazz.isAssignableFrom(field.getType()))
            .peek(field -> field.setAccessible(true))
            .collect(Collectors.toList());

        if (fieldList.size() == 0) {
            return Optional.empty();
        } else if (fieldList.size() == 1) {
            @SuppressWarnings("unchecked")
            T field = (T) fieldList.get(0).get(testInstance);
            return Optional.of(field);
        } else {
            throw new IllegalStateException("");
        }
    }

    private void mockVaadinUI(ExtensionContext context) throws IllegalAccessException {
        if (testMethodHasAnnotation(DisableMockedUI.class, context)) {
            vaadinUi = null;
        } else {
            Optional<UI> ui = getFieldOfType(UI.class, context);
            if (ui.isPresent()) {
                vaadinUi = ui.get();
                if (vaadinUi.getInternals() == null) {
                    UIInternals uiInternals = new UIInternals(vaadinUi, new UIInternalUpdater() {
                    });
                    doReturn(uiInternals).when(vaadinUi).getInternals();
                }
                if (vaadinUi.getElement() == null) {
                    Element element = mock(Element.class);
                    doReturn(element).when(vaadinUi).getElement();
                }
                if (vaadinUi.getElement().getNode() == null) {
                    Element element = vaadinUi.getElement();
                    StateNode stateNode = mock(StateNode.class);
                    doReturn(stateNode).when(element).getNode();
                }
            } else {
                vaadinUi = spy(new UI());
                when(vaadinUi.getSession()).thenReturn(vaadinSession);
            }
        }
        UI.setCurrent(vaadinUi);
    }

    private boolean testMethodHasAnnotation(Class<? extends Annotation> annotationClass, ExtensionContext context) {
        return context
            .getTestMethod()
            .map(method -> method.getAnnotation(annotationClass))
            .isPresent();
    }
}
