/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.test.extensions;

import io.devbench.uibuilder.test.annotations.ReadResource;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;

public class ResourceReaderExtension implements ParameterResolver {

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return getAnnotation(parameterContext).isPresent();
    }

    private Optional<ReadResource> getAnnotation(ParameterContext parameterContext) {
        final ReadResource annotation = parameterContext
            .getParameter()
            .getAnnotation(ReadResource.class);
        return Optional.ofNullable(annotation);
    }

    @Override
    public String resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        final ReadResource annotation = getAnnotation(parameterContext).get(); // only called when `supports` returns true
        final Class<?> testClass = extensionContext.getTestClass().get();
        return readResource(annotation.value(), extensionContext);
    }

    private String readResource(String resourceName, ExtensionContext extensionContext) {
        final Class<?> testClass = extensionContext.getTestClass().get();
        final InputStream stream = testClass.getResourceAsStream(resourceName);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {
            final StringBuilder sb = new StringBuilder();
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                sb.append(line).append('\n');
            }
            return sb.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
