/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.components;

import com.vaadin.flow.component.Tag;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Collection;
import java.util.stream.Collectors;

public interface TagGroupHelper {

    @NotNull
    static Collection<String> findTagNamesByGroupComponentClass(@NotNull Class<?> groupClass) {
        return MemberScanner.getInstance().findClassesBySuperType(groupClass).stream()
            .filter(clz -> clz.isAnnotationPresent(Tag.class))
            .map(clz -> clz.getAnnotation(Tag.class).value())
            .collect(Collectors.toList());
    }

    @NotNull
    static Elements findElementsByGroupComponentClass(@NotNull Element sourceElement, @NotNull Class<?> groupClass) {
        return new Elements(
            findTagNamesByGroupComponentClass(groupClass)
                .stream()
                .map(sourceElement::getElementsByTag)
                .flatMap(Collection::stream)
                .collect(Collectors.toList())
        );
    }

}
