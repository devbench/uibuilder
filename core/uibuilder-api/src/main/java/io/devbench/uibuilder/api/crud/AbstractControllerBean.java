/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.crud;

import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Source;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;

import java.util.function.Consumer;
import java.util.function.Supplier;

public abstract class AbstractControllerBean<TYPE> implements CrudControllerBean<TYPE> {
    protected final Supplier<TYPE> baseConstructor;
    protected final Consumer<TYPE> save;
    protected final Consumer<TYPE> delete;

    public AbstractControllerBean(Supplier<TYPE> baseConstructor, Consumer<TYPE> save, Consumer<TYPE> delete) {
        this.baseConstructor = baseConstructor;
        this.save = save;
        this.delete = delete;
    }

    @UIEventHandler("onSave")
    public void onSave(@Item TYPE subject, @Source Refreshable refreshable) {
        save.accept(subject);
        refresh(refreshable);
    }

    @UIEventHandler("onDelete")
    public void onDelete(@Item TYPE subject, @Source Refreshable refreshable) {
        delete.accept(subject);
        refresh(refreshable);
    }

    @UIEventHandler("onRefresh")
    public void refresh(@Source Refreshable refreshable) {
        if (refreshable != null) {
            refreshable.refresh();
        }
    }

    public TYPE create() {
        return baseConstructor.get();
    }
}
