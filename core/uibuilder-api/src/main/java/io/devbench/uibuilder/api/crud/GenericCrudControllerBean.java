/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.crud;

import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Source;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

public interface GenericCrudControllerBean<TYPE> extends CrudControllerBean<TYPE> {

    String BUILT_IN_GENERIC_CRUD_PANEL_CONTROLLER_BEAN_NAME = "builtInGenericCrudPanelControllerBean";

    void registerGenericCrudData(@NotNull String mdcId, @NotNull String datasourceName);

    void registerNestedInlineItemSaveHandler(@NotNull Consumer<String> inlineItemSaveCrudPanelIdConsumer);

    void onSave(@Item TYPE subject, @Source Refreshable refreshable);

    void onDelete(@Item TYPE subject, @Source Refreshable refreshable);

    void refresh(@Source Refreshable refreshable);

    void onCreate(@Source Refreshable refreshable);

    default TYPE create() {
        return null;
    }

}
