/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.utils;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.Function;

public abstract class CollectionUtil {

    private CollectionUtil() {
    }

    public static <T, R> Function<T, R> tryTo(ThrowingFunction<T, R> throwingFunction) {
        return t -> {
            try {
                return throwingFunction.apply(t);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    public interface ThrowingFunction<T, R> {
        R apply(T t) throws Exception;
    }

    public static <T> boolean isCollectionsEqual(@NotNull Collection<T> collection1, @NotNull Collection<T> collection2) {
        List<T> diff1 = new ArrayList<>(collection1);
        List<T> diff2 = new ArrayList<>(collection2);
        diff1.removeAll(collection2);
        diff2.removeAll(collection1);
        return diff1.isEmpty() && diff2.isEmpty();
    }

    public static <T> Collection<T> instantiateCollection(Class<?> collectionType) {
        try {
            @SuppressWarnings("unchecked")
            Collection<T> collection = (Collection<T>) collectionType.getConstructor().newInstance();
            return collection;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            // ignored
        }

        return Set.class.isAssignableFrom(collectionType) ? new HashSet<>() : new ArrayList<>();
    }

}
