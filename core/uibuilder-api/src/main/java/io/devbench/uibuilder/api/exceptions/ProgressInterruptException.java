/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.exceptions;

import lombok.Getter;

import java.util.Objects;

public class ProgressInterruptException extends ComponentException {

    @Getter
    private final boolean showErrorMessageOnFrontend;

    public ProgressInterruptException(String message) {
        super(Objects.requireNonNull(message));
        this.showErrorMessageOnFrontend = true;
    }

    public ProgressInterruptException(String message, boolean showErrorMessageOnFrontend) {
        super(Objects.requireNonNull(message));
        this.showErrorMessageOnFrontend = showErrorMessageOnFrontend;
    }

    public ProgressInterruptException(String message, Throwable cause) {
        super(Objects.requireNonNull(message), cause);
        this.showErrorMessageOnFrontend = true;
    }

    public ProgressInterruptException(String message, Throwable cause, boolean showErrorMessageOnFrontend) {
        super(Objects.requireNonNull(message), cause);
        this.showErrorMessageOnFrontend = showErrorMessageOnFrontend;
    }
}
