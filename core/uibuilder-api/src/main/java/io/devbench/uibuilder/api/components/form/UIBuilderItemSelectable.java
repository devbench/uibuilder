/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.components.form;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.shared.Registration;

import java.util.ArrayList;
import java.util.Collection;

public interface UIBuilderItemSelectable<T> {

    Collection<T> getSelectedItems();

    void setSelectedItems(Collection<T> items);

    default T getSelectedItem() {
        Collection<T> selectedItems = getSelectedItems();
        if (selectedItems != null && selectedItems.size() == 1) {
            return selectedItems.stream().findFirst().get();
        } else {
            return null;
        }
    }

    default void setSelectedItem(T item) {
        Collection<T> items = new ArrayList<>();
        if (item != null) {
            items.add(item);
        }
        setSelectedItems(items);
    }

    Registration addSelectionChangedListener(ComponentEventListener<? extends SelectionChangedEvent<T>> listener);

    interface SelectionChangedEvent<T> {
        Collection<T> getSelectedItems();

        T getSelectedItem();
    }

}
