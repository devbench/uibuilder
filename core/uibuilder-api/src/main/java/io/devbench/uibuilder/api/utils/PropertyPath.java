/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.utils;

import lombok.experimental.Delegate;
import org.jetbrains.annotations.NotNull;
import java.io.Serializable;
import java.util.Arrays;

public final class PropertyPath implements Serializable, Comparable<String>, CharSequence {

    @Delegate
    private final String propertyPath;

    public PropertyPath(String propertyPath) {
        if (propertyPath == null) {
            this.propertyPath = "";
        } else {
            this.propertyPath = normalize(propertyPath.trim());
        }
    }

    private String normalize(String path) {
        String normalizedPath = path;
        if (normalizedPath.endsWith(".")) {
            normalizedPath = normalize(normalizedPath.substring(0, normalizedPath.length() - 1));
        }
        if (normalizedPath.startsWith(".")) {
            normalizedPath = normalize(normalizedPath.substring(1));
        }
        return normalizedPath;
    }

    public boolean isSingleLevel() {
        return levelSize() == 1;
    }

    public int levelSize() {
        return (int) propertyPath.chars().filter(value -> value == '.').count() + 1;
    }

    public PropertyPath level(int level) {
        int levelSize = levelSize();
        if (level < levelSize) {
            return new PropertyPath(propertyPath.split("\\.")[level]);
        } else {
            throw new IndexOutOfBoundsException("Level size: " + levelSize + ", level: " + level);
        }
    }

    public PropertyPath subPath(int fromLevel) {
        int levelSize = levelSize();
        if (fromLevel < levelSize) {
            return new PropertyPath(
                String.join(".",
                    Arrays.asList(propertyPath.split("\\.")).subList(fromLevel, levelSize)));
        } else {
            throw new IndexOutOfBoundsException("Level size: " + levelSize + ", level: " + fromLevel);
        }
    }

    public PropertyPath getLeaf() {
        return isSingleLevel() ? this : level(levelSize() - 1);
    }

    public PropertyPath getFirstLevel() {
        return level(0);
    }

    @NotNull
    @Override
    public String toString() {
        return propertyPath;
    }
}
