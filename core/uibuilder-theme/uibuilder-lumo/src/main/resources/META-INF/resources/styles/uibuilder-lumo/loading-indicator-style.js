(() => {
    const styleContainer = document.createElement('template');
    styleContainer.innerHTML = `
    <custom-style>
        <style>
        .v-loading-indicator {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            pointer-events: auto;
            z-index: 2147483647;
        }

        .v-loading-indicator:before {
            width: 76px;
            height: 76px;

            position: absolute;
            top: 50%;
            left: 50%;

            margin: -38px 0 0 -38px;

            border: 8px solid transparent;
            border-radius: 100%;
            animation: rotation 1.2s infinite linear;
            content: "";
        }

        .v-loading-indicator.first:before {
            background: linear-gradient(45deg,skyblue,skyblue,transparent,transparent,transparent) border-box;
            -webkit-mask: linear-gradient(#fff 0 0) padding-box, linear-gradient(#fff 0 0);
            -webkit-mask-composite: xor;
            mask-composite: exclude;
        }

        .v-loading-indicator.second:before {
            background: linear-gradient(45deg,salmon,salmon,transparent,transparent,transparent) border-box;
            -webkit-mask: linear-gradient(#fff 0 0) padding-box, linear-gradient(#fff 0 0);
            -webkit-mask-composite: xor;
            mask-composite: exclude;
        }

        .v-loading-indicator.third:before {
            background: linear-gradient(45deg,red,red,transparent,transparent,transparent) border-box;
            -webkit-mask: linear-gradient(#fff 0 0) padding-box, linear-gradient(#fff 0 0);
            -webkit-mask-composite: xor;
            mask-composite: exclude;
        }

        @keyframes rotation {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(359deg);
            }
        }

        .v-status-message {
            display: none;
        }
        </style>
    </custom-style>`;
    document.head.appendChild(styleContainer.content);
})();
