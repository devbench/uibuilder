import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-lumo-styles/vaadin-iconset.js';


export class UibuilderLumoIcons extends PolymerElement {

    static get is() {
        return 'uibuilder-lumo-icons'
    }

}

customElements.define(UibuilderLumoIcons.is, UibuilderLumoIcons);
