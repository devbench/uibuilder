/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.vaadin;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.LoadingIndicatorConfiguration;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.server.*;
import com.vaadin.flow.shared.Registration;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalMatchers;

import static org.mockito.Mockito.*;

class VaadinAppShellConfigurationTest {

    private Page testPage;
    private LoadingIndicatorConfiguration testLoadingIndicatorConfiguration;

    @Test
    @DisplayName("Should not set favicon if it is not present in config file")
    void test_should_not_set_favicon_if_it_is_not_present_in_config_file() {
        VaadinAppShellConfiguration testObj = withConfig("application-empty");

        AppShellSettings settings = mock(AppShellSettings.class);
        testObj.configurePage(settings);

        verify(settings, never()).addFavIcon("icon", "/path/to/favicon.ico", null);
    }

    @Test
    @DisplayName("Should set favicon if present in config")
    void test_should_set_favicon_if_present_in_config() {
        VaadinAppShellConfiguration testObj = withConfig("application-favicon");

        AppShellSettings settings = mock(AppShellSettings.class);
        testObj.configurePage(settings);

        verify(settings).addFavIcon("icon", "/path/to/favicon.ico", null);
    }

    @Test
    @DisplayName("Should disable default theme")
    void test_should_disable_default_theme() {
        VaadinAppShellConfiguration testObj = withConfig("application-loading-indicator-disable-default");

        ServiceInitEvent serviceInitEvent = mockServiceInitEvent();
        testObj.serviceInit(serviceInitEvent);

        verify(testLoadingIndicatorConfiguration).setApplyDefaultTheme(false);
    }

    @Test
    @DisplayName("Should not disabled default theme")
    void test_should_not_disabled_default_theme() {
        VaadinAppShellConfiguration testObj = withConfig("application-empty");

        ServiceInitEvent serviceInitEvent = mockServiceInitEvent();
        testObj.serviceInit(serviceInitEvent);

        verify(testLoadingIndicatorConfiguration, never()).setApplyDefaultTheme(anyBoolean());
    }

    @Test
    @DisplayName("Should set delays")
    void test_should_set_delays() {
        VaadinAppShellConfiguration testObj = withConfig("application-loading-indicator-delay");

        ServiceInitEvent serviceInitEvent = mockServiceInitEvent();
        testObj.serviceInit(serviceInitEvent);

        verify(testLoadingIndicatorConfiguration).setFirstDelay(3000);
        verify(testLoadingIndicatorConfiguration).setSecondDelay(5000);
        verify(testLoadingIndicatorConfiguration).setThirdDelay(8000);
    }

    @Test
    @DisplayName("Should not set delays")
    void test_should_not_set_delays() {
        VaadinAppShellConfiguration testObj = withConfig("application-empty");

        ServiceInitEvent serviceInitEvent = mockServiceInitEvent();
        testObj.serviceInit(serviceInitEvent);

        verify(testLoadingIndicatorConfiguration, never()).setFirstDelay(anyInt());
        verify(testLoadingIndicatorConfiguration, never()).setSecondDelay(anyInt());
        verify(testLoadingIndicatorConfiguration, never()).setThirdDelay(anyInt());
    }

    @Test
    @DisplayName("Should set only second delay")
    void test_should_set_only_second_delay() {
        VaadinAppShellConfiguration testObj = withConfig("application-loading-indicator-delay-only-second");

        ServiceInitEvent serviceInitEvent = mockServiceInitEvent();
        testObj.serviceInit(serviceInitEvent);

        verify(testLoadingIndicatorConfiguration, never()).setFirstDelay(anyInt());
        verify(testLoadingIndicatorConfiguration).setSecondDelay(5000);
        verify(testLoadingIndicatorConfiguration, never()).setThirdDelay(anyInt());
    }

    @Test
    @DisplayName("Should enable uibuilder lumo style")
    void test_should_enable_uibuilder_lumo_style() {
        VaadinAppShellConfiguration testObj = withConfig("application-loading-indicator-style-enable");

        ServiceInitEvent serviceInitEvent = mockServiceInitEvent();
        testObj.serviceInit(serviceInitEvent);

        verify(testPage).addDynamicImport(AdditionalMatchers.and(
            contains("import"),
            contains(VaadinAppShellConfiguration.UIBUILDER_LUMO_LOADING_INDICATOR_STYLE_PATH)));
    }

    @Test
    @DisplayName("Should not enable any style")
    void test_should_not_enable_any_style() {
        VaadinAppShellConfiguration testObj = withConfig("application-empty");

        ServiceInitEvent serviceInitEvent = mockServiceInitEvent();
        testObj.serviceInit(serviceInitEvent);

        verify(testPage, never()).addDynamicImport(anyString());
    }

    @Test
    @DisplayName("Should enable specific style js to load")
    void test_should_enable_specific_style_js_to_load() {
        VaadinAppShellConfiguration testObj = withConfig("application-loading-indicator-style-specify");

        ServiceInitEvent serviceInitEvent = mockServiceInitEvent();
        testObj.serviceInit(serviceInitEvent);

        verify(testPage).addDynamicImport(AdditionalMatchers.and(
            contains("import"),
            contains("/styles/uibuilder-lumo/loading-indicator-style.js")));
    }


    private ServiceInitEvent mockServiceInitEvent() {
        ServiceInitEvent serviceInitEvent = mock(ServiceInitEvent.class);
        VaadinService vaadinService = mock(VaadinService.class);
        Registration registration = mock(Registration.class);
        UIInitEvent event = mock(UIInitEvent.class);
        UI ui = mock(UI.class);
        testLoadingIndicatorConfiguration = mock(LoadingIndicatorConfiguration.class);
        testPage = mock(Page.class);

        doReturn(ui).when(event).getUI();
        doReturn(testPage).when(ui).getPage();
        doReturn(testLoadingIndicatorConfiguration).when(ui).getLoadingIndicatorConfiguration();
        doReturn(vaadinService).when(serviceInitEvent).getSource();

        doAnswer(invocationOnMock -> {
            UIInitListener listener = invocationOnMock.getArgument(0);
            listener.uiInit(event);
            return registration;
        }).when(vaadinService).addUIInitListener(any(UIInitListener.class));

        return serviceInitEvent;
    }

    private VaadinAppShellConfiguration withConfig(String configName) {
        Config testConfig = ConfigFactory.load("test-configs/" + configName + ".conf");
        return new VaadinAppShellConfiguration(testConfig);
    }

}
