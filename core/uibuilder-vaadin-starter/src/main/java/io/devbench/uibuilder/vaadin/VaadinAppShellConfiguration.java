/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.vaadin;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.LoadingIndicatorConfiguration;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.theme.Theme;
import io.devbench.uibuilder.core.dynamictheme.ConfigurationBasedTheme;

import java.util.function.Consumer;

@Theme(themeClass = ConfigurationBasedTheme.class)
public class VaadinAppShellConfiguration implements AppShellConfigurator, VaadinServiceInitListener {

    private final Config config;
    static final String UIBUILDER_LUMO_LOADING_INDICATOR_STYLE_PATH = "/styles/uibuilder-lumo/loading-indicator-style-with-fade.js";

    public VaadinAppShellConfiguration() {
        this(ConfigFactory.load());
    }

    public VaadinAppShellConfiguration(Config config) {
        this.config = config;
    }

    @Override
    public void configurePage(AppShellSettings settings) {
        ifConfigPathPresentApply("favicon", (String faviconPath) -> settings.addFavIcon("icon", faviconPath, null));
    }

    @Override
    public void serviceInit(ServiceInitEvent event) {
        event.getSource().addUIInitListener(uiEvent -> {
            UI ui = uiEvent.getUI();
            LoadingIndicatorConfiguration loadingIndicatorConfiguration = ui.getLoadingIndicatorConfiguration();
            ifConfigPathPresentApply("loadingIndicator.disable-default", (Boolean disable) -> loadingIndicatorConfiguration.setApplyDefaultTheme(!disable));
            ifConfigPathPresentApply("loadingIndicator.delay.first", loadingIndicatorConfiguration::setFirstDelay);
            ifConfigPathPresentApply("loadingIndicator.delay.second", loadingIndicatorConfiguration::setSecondDelay);
            ifConfigPathPresentApply("loadingIndicator.delay.third", loadingIndicatorConfiguration::setThirdDelay);
            ifConfigPathPresentApply("loadingIndicator.style-loader", (Object style) -> {
                String stylePath = null;
                if (style instanceof Boolean && (Boolean) style) {
                    stylePath = UIBUILDER_LUMO_LOADING_INDICATOR_STYLE_PATH;
                } else if (style instanceof String) {
                    stylePath = (String) style;
                }
                if (stylePath != null) {
                    ui.getPage().addDynamicImport("return Promise.resolve(0).then(() => import('" + stylePath + "'));");
                }
            });
        });
    }

    private <T> void ifConfigPathPresentApply(String path, Consumer<T> configValueConsumer) {
        if (config.hasPath(path)) {
            ConfigValue configValue = config.getValue(path);
            @SuppressWarnings("unchecked") T unwrapped = (T) configValue.unwrapped();
            configValueConsumer.accept(unwrapped);
        }
    }

}
