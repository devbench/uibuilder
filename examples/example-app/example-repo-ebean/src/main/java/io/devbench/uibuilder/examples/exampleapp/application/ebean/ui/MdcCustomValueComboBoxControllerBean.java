/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.ebean.ui;

// CODE BLOCK DEFINITION - MdcCustomValueComboBoxControllerBean.java - START

import com.vaadin.flow.spring.annotation.UIScope;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.Label;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import io.devbench.uibuilder.examples.exampleapp.application.ebean.repositories.TodoItemRepository;
import io.devbench.uibuilder.spring.crud.AbstractSpringControllerBean;

@UIScope
@ControllerBean("mdcCustomValueComboBox")
public class MdcCustomValueComboBoxControllerBean extends AbstractSpringControllerBean<TodoItem, TodoItemRepository> {

    public MdcCustomValueComboBoxControllerBean(TodoItemRepository repository) {
        super(() -> new TodoItem("created todo item", null), repository);
    }

    public Label createLabel(String value) {
        return new Label(value);
    }

}
// CODE BLOCK DEFINITION END
