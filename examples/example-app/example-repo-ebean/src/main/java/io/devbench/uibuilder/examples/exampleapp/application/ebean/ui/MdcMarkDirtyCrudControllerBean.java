/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.ebean.ui;

// CODE BLOCK DEFINITION - MdcMarkDirtyCrudControllerBean.java - START
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.detailpanel.UIBuilderDetailPanel;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.ReminderItem;
import io.devbench.uibuilder.examples.exampleapp.application.ebean.repositories.TodoItemRepository;
import org.springframework.context.annotation.Scope;

@Scope("session")
@ControllerBean("mdcMarkDirtyCrudBean")
public class MdcMarkDirtyCrudControllerBean extends MdcSimpleCrudControllerBean {

    public MdcMarkDirtyCrudControllerBean(TodoItemRepository repository) {
        super(repository);
    }

    @UIEventHandler("markDetailDirty")
    public void markDetailDirty(@UIComponent("mdc") UIBuilderMasterDetailController<ReminderItem> mdc) {
        mdc.getDetailComponent()
            .filter(UIBuilderDetailPanel.class::isInstance)
            .map(UIBuilderDetailPanel.class::cast)
            .ifPresent(UIBuilderDetailPanel::setInternalFormDirty);
    }

}
// CODE BLOCK DEFINITION END
