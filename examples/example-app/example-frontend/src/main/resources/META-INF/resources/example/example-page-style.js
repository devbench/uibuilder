(() => {
    const templateElement = document.createElement('template');
    templateElement.innerHTML = `
<custom-style>
    <style>
        .content {
            margin: 0;
            padding: 16px;
            background-color: var(--uibuilder-lumo-menu-bg-color);
        }

        .description {
            margin-top: 4px;
            margin-bottom: 16px;
            padding: 8px;
            background-color: #FEFDF8;
            border: 1px solid #F0E9EA;
            border-radius: 5px;
            color: #505040;
            font-family: sans-serif;
        }

        .descriptionToggle {
            font-size: 10px;
            user-select: none;
            color: #909090;
        }

        .descriptionToggleCheckbox {
            width: 0;
            height: 0;
        }

        .descriptionToggle:has(~ .description[hidden]) {
            color: #000000;
        }

        code, .code {
            font-family: monospace;
            font-weight: bold;
            font-size: 1.1rem;
            padding: 3px;
            background-color: rgba(0, 0, 0, 0.05);
            border-radius: 3px;
        }

        .component {
            margin: 16px 0;
            padding: 16px;
            border: 1px solid #CBCBCB;
            border-radius: 5px;
            background-color: #FFFFFF;
            color: #101010;
        }

        page {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            position: relative;
        }

        h2[part="header"] {
            margin-top: 4px;
            color: #606060;
            font-size: 20px;
        }

        uibuilder-code-block {
            font-size: 14px;
        }
    </style>
</custom-style>
`;

    document.head.appendChild(templateElement.content);

})();
