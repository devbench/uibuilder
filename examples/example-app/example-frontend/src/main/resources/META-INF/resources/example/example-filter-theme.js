// CODE BLOCK DEFINITION - example-filter-theme.js - START
(() => {
    const domModule = document.createElement('dom-module');
    domModule.setAttribute('id', 'example-filter-theme');
    domModule.setAttribute('theme-for', 'uibuilder-common-filter');

    const templateElement = document.createElement('template');
    templateElement.innerHTML = `
<style>
    :host{
        --lumo-primary-text-color: green;
    }

    [part="search-button"] {
        float: left;
        margin-left: 10px;
        color: green;
    }

    [part="add-button"]{
        float: left;
        color: green;
    }

    #search [part="search-button-label"]::before {
        font-family: "lumo-icons";
        content: var(--lumo-icons-search);
        font-size: var(--lumo-icon-size-s);
    }

    #add [part="add-button-label"]::before {
        font-family: "lumo-icons";
        content: var(--lumo-icons-plus);
        font-size: var(--lumo-icon-size-s);
    }
</style>
`;

    domModule.appendChild(templateElement);
    document.head.appendChild(domModule);
})();
// CODE BLOCK DEFINITION END
