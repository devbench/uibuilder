// CODE BLOCK DEFINITION - simple_menu_theme.js - START
document.head.insertAdjacentHTML('beforeend', `
    <dom-module id="simple_menu_example_menu_item_theme" theme-for="menu-item">
        <template>
            <style>
                :host([theme~='fancy']) [part='button'] {
                    background-color: hsl(270, 50%, 50%);
                    color: #fff;
                }

                :host([theme~='fancy']) [part='button']:hover {
                    background-color: hsl(270, 50%, 70%);
                }
            </style>
        </template>
    </dom-module>
`);
Uibuilder.applyCurrentThemeModule(document.head.lastElementChild);
// CODE BLOCK DEFINITION END
