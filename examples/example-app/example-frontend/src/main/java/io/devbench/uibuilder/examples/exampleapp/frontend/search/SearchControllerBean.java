/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.search;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.dom.Style;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.core.flow.FlowManager;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Provider;
import java.util.List;
import java.util.stream.Collectors;

@ControllerBean("exampleSearch")
public class SearchControllerBean {

    @UIComponent("exampleSearchTextField")
    private Provider<TextField> exampleSearchTextFieldProvider;

    @UIComponent("exampleSearchResults")
    private Provider<Div> exampleSearchResultsProvider;

    private final SearchService searchService;

    public SearchControllerBean(SearchService searchService) {
        this.searchService = searchService;
    }

    @UIEventHandler("afterPageLoaded")
    public void onAfterPageLoaded() {
        exampleSearchResultsProvider.get().getElement().setAttribute("hidden", true);
        TextField exampleSearchTextField = exampleSearchTextFieldProvider.get();
        exampleSearchTextField.setValueChangeMode(ValueChangeMode.EAGER);
        exampleSearchTextField.addValueChangeListener(this::onExampleSearchTextFieldValueChanged);
    }

    private void onExampleSearchTextFieldValueChanged(AbstractField.ComponentValueChangeEvent<TextField, String> event) {
        String searchText = event.getValue();
        if (StringUtils.isBlank(searchText) || searchText.length() < 3) {
            hideSearchResults();
        } else {
            search(searchText);
        }
    }

    private void search(String searchText) {
        Div exampleSearchResults = exampleSearchResultsProvider.get();
        exampleSearchResults.removeAll();

        SearchResults searchResults = searchService.search(searchText);

        if (searchResults.isIndexing()) {
            exampleSearchResults.add(new Text("Indexing, please wait..."));
        } else {
            List<SearchResult> searchResultList = searchResults.getResults();
            if (searchResultList.isEmpty()) {
                exampleSearchResults.add(new Text("NOTHING FOUND :("));
            } else {
                List<Component> resultComponents = searchResultList.stream()
                    .map(SearchControllerBean::createResultLine)
                    .collect(Collectors.toList());
                applyResultLineStripes(resultComponents);

                exampleSearchResults.add(resultComponents);
            }
        }

        exampleSearchResults.getElement().removeAttribute("hidden");
    }

    private static void applyResultLineStripes(List<Component> resultComponents) {
        for (int i = 0; i < resultComponents.size(); i = i + 2) {
            HasStyle component = (HasStyle) resultComponents.get(i);
            component.getStyle().set("background-color", "#F6F6F6");
        }
    }

    private static Component createResultLine(SearchResult result) {
        HorizontalLayout line = new HorizontalLayout();
        line.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        line.setAlignItems(FlexComponent.Alignment.CENTER);

        Button flowNavigationButton = new Button(result.getTitle());
        flowNavigationButton.addThemeNames("small", "tertiary");
        flowNavigationButton.addClickListener(event -> FlowManager.getCurrent().navigate(result.getFlowId()));

        Div snippetDiv = new Div(new Text(result.getSnippet()));
        Style snippetDivStyle = snippetDiv.getStyle();
        snippetDivStyle.set("text-align", "right");
        snippetDivStyle.set("color", "#606060");
        snippetDivStyle.set("font-size", "0.7rem");

        line.add(flowNavigationButton);
        line.add(snippetDiv);
        return line;
    }

    private void hideSearchResults() {
        Div exampleSearchResults = exampleSearchResultsProvider.get();
        exampleSearchResults.removeAll();
        exampleSearchResults.getElement().setAttribute("hidden", true);
    }

}
