/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

import com.vaadin.flow.component.UI;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Data;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.examples.exampleapp.frontend.service.UIBuilderVersionService;
import lombok.RequiredArgsConstructor;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@ControllerBean("uibuilderVersion")
public class UIBuilderVersionControllerBean {

    private final UIBuilderVersionService uiBuilderVersionService;

    public String getCurrentVersion() {
        return uiBuilderVersionService.getCurrentVersion();
    }

    public String getUiBuilderVersionMenuItemTitle() {
        return "Current UIBuilder version: " + uiBuilderVersionService.getCurrentVersion();
    }

    public List<String> getOtherVersions() {
        return uiBuilderVersionService.getOtherVersions().stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }

    @UIEventHandler("versionSwitchMenuItemActivated")
    public void onVersionSwitchMenuItemActivated(@Data("selectedVersion") String selectedVersion) {
        String url = uiBuilderVersionService.getVersionUrl(selectedVersion);
        UI.getCurrent().getPage().executeJs("window.open(\"" + url + "\", \"_self\");");
    }

}
