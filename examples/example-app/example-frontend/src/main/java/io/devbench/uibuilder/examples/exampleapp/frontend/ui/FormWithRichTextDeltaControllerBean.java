/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - FormWithRichTextDeltaControllerBean.java - START

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Pre;
import io.devbench.quilldelta.Op;
import io.devbench.quilldelta.Ops;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.components.richtext.UIBuilderRichTextEditor;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;

import javax.inject.Provider;
import java.time.LocalDate;

@ControllerBean("formRichTextDeltaCb")
public class FormWithRichTextDeltaControllerBean {

    @UIComponent("form")
    private Provider<UIBuilderForm<TodoItem>> formProvider;

    @UIComponent("richText")
    private Provider<UIBuilderRichTextEditor> richTextFieldProvider;

    @UIComponent("lastSavedOps")
    private Provider<Pre> lastSavedOps;

    @UIComponent("lastSavedHtml")
    private Provider<Div> lastSavedHtml;

    @UIEventHandler("initForm")
    public void initForm() {
        Ops ops = Ops.from(Op.insert("Initial text"));
        String initialTodoValue = ops.toJson().toJson();
        TodoItem todoItem = new TodoItem(initialTodoValue, LocalDate.now());
        formProvider.get().setFormItem(todoItem);
    }

    @UIEventHandler("onSave")
    public void onSave() {
        String todoOps = formProvider.get().getFormItem().getTodo()
            .replace("},{", "},\n  {")
            .replace("\"ops\":[{", "\"ops\":[\n  {");
        String todoHtml = richTextFieldProvider.get().getValueAsHtmlText();

        Div lastSavedHtmlDiv = lastSavedHtml.get();
        lastSavedHtmlDiv.removeAll();
        lastSavedHtmlDiv.add(new Html("<div class=\"ql-editor\" style=\"white-space: normal\">" + todoHtml + "</div>"));

        Pre lastSavedOpsPre = lastSavedOps.get();
        lastSavedOpsPre.removeAll();
        lastSavedOpsPre.setText(todoOps);
    }

}
// CODE BLOCK DEFINITION END
