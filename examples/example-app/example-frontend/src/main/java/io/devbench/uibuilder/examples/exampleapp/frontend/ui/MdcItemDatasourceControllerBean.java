/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - MdcItemDatasourceControllerBean.java - START

import com.vaadin.flow.function.SerializableSupplier;
import com.vaadin.flow.spring.annotation.UIScope;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@UIScope
@ControllerBean("itemDsMdc")
public class MdcItemDatasourceControllerBean {

    @Data
    @Builder
    public static class SimpleItem implements Serializable {
        private String name;
        private Integer age;
    }

    @Getter
    private List<SimpleItem> simpleItems;

    @Getter
    private SerializableSupplier<SimpleItem> create = () -> SimpleItem.builder().name("John Doe").age(33).build();

    @PostConstruct
    public void createItems() {
        simpleItems = new ArrayList<>();
        simpleItems.add(SimpleItem.builder().name("Robert Smith").age(20).build());
        simpleItems.add(SimpleItem.builder().name("Axl Rose").age(40).build());
        simpleItems.add(SimpleItem.builder().name("Freddy Mercury").age(30).build());
    }

}
// CODE BLOCK DEFINITION END
