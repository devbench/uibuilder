/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.combobox;

// CODE BLOCK DEFINITION - CustomValueComboboxControllerBean.java - START

import com.vaadin.flow.spring.annotation.UIScope;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Value;
import io.devbench.uibuilder.components.combobox.UIBuilderComboBox;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@UIScope
@ControllerBean("customValueCb")
public class CustomValueComboboxControllerBean {

    private static final List<String> DEFAULT_LIST = Arrays.asList("The", "default", "list");
    private static final List<String> MATCHED_LIST = Arrays.asList("The", "custom value", "is", "show");

    @Getter
    private List<String> items = DEFAULT_LIST;

    @UIEventHandler("customValueChanged")
    public void onCustomValueChanged(@UIComponent("combobox") UIBuilderComboBox<String> comboBox,
                                     @Value("detail.customValue") String customValue) {

        if ("show".equals(customValue)) {
            items = MATCHED_LIST;
            comboBox.setOpened(true);
        } else {
            items = DEFAULT_LIST;
            comboBox.setOpened("default".equals(customValue));
        }
    }
}
// CODE BLOCK DEFINITION END
