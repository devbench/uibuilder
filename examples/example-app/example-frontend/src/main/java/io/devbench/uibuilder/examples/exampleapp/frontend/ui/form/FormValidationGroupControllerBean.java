/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.form;
// CODE BLOCK DEFINITION - FormValidationGroupControllerBean.java - START

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.dialog.Dialog;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.Address;
import io.devbench.uibuilder.examples.exampleapp.application.api.validation.AddressWarningGroup;

import javax.inject.Provider;
import javax.validation.groups.Default;

@ControllerBean("formValidationGroupController")
public class FormValidationGroupControllerBean {

    @UIComponent("form")
    private Provider<UIBuilderForm<Address>> formProvider;

    @UIEventHandler("afterLoad")
    public void onAfterLoad() {
        UIBuilderForm<Address> form = formProvider.get();
        form.setValidationGroup(Default.class, true);
        form.setValidationGroup(AddressWarningGroup.class, false);
        form.setItem(new Address());
    }

    @UIEventHandler("clearFormItem")
    public void onClearFormItem() {
        formProvider.get().setItem(new Address());
    }

    @UIEventHandler("save")
    public void onSave(@Item Address item) {
        new Dialog(new Text("Saved address: " + item)).open();
    }
}
// CODE BLOCK DEFINITION END
