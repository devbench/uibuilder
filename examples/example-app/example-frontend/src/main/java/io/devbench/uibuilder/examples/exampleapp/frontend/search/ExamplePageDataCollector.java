/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.search;

import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.flow.xml.*;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static io.devbench.uibuilder.core.flow.FlowManager.*;

@Slf4j
@Service
public class ExamplePageDataCollector {

    @Data
    public static class ExamplePage {
        private String menuId;
        private String menuTitle;
        private String title;
        private String description;
        private Map<String, String> codes = new HashMap<>();
        private String flowId;
    }

    private Map<String, String> flowIdMenuItemName;

    @Getter
    private List<ExamplePage> examplePages;

    @Getter
    private boolean indexing;

    public ExamplePageDataCollector() {
        this.indexing = true;
    }

    public void requireIndexedData() {
        if (isIndexing()) {
            synchronized (this) {
                if (isIndexing()) {
                    collectData();
                    indexing = false;
                }
            }
        }
    }

    private void collectData() {
        FlowDefinitions flowDefinitions = FlowManager.getCurrent().getFlowDefinitions();
        collectFlowIdMenuItemNames();
        examplePages = allFlowDefinitionsWithHtml(flowDefinitions.getSubFlows().get(0))
            .map(this::readExamplePage)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.toList());
        examplePages.forEach(examplePage -> examplePage.setMenuTitle(flowIdMenuItemName.get(examplePage.getFlowId())));
    }

    private void collectFlowIdMenuItemNames() {
        flowIdMenuItemName = new HashMap<>();
        loadResourceFileContent("/webapp/base.html").ifPresent(menuHtml -> {
            for (Element menuItemElement : Jsoup.parse(menuHtml).getElementsByAttribute("on-activate")) {
                String onActivate = menuItemElement.attr("on-activate");
                if (onActivate.startsWith("flow:")) {
                    String flowId = onActivate.substring(5);
                    String name = menuItemElement.attr("name");
                    flowIdMenuItemName.put(flowId, name);
                }
            }
        });
    }

    private Optional<String> loadResourceFileContent(String resourceFile) {
        String content = null;
        try (InputStream inputStream = getClass().getResourceAsStream(resourceFile)) {
            content = StreamUtils.copyToString(inputStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.error("Could not load resource file: {}", resourceFile);
        }
        return Optional.ofNullable(content);
    }

    private Optional<ExamplePage> readExamplePage(FlowDefinition flowDefinition) {
        String flowId = flowDefinition.getId();
        String htmlFilePath = flowDefinition.getHtml();

        return loadResourceFileContent(htmlFilePath)
            .flatMap(htmlContent -> {
                Document htmlDocument = Jsoup.parse(htmlContent);
                return parseExamplePage(htmlDocument, flowId);
            });
    }

    private Optional<ExamplePage> parseExamplePage(Document html, String flowId) {
        ExamplePage examplePage = new ExamplePage();
        examplePage.setFlowId(flowId);

        Element descriptionElement = html.getElementsByClass("description").first();
        if (descriptionElement != null && descriptionElement.tagName().equalsIgnoreCase("div")) {
            examplePage.setDescription(descriptionElement.text());
        }

        Element headerElement = html.getElementsByAttributeValue("part", "header").first();
        if (headerElement != null && headerElement.tagName().equalsIgnoreCase("h2")) {
            examplePage.setTitle(headerElement.text());
        } else {
            // if no h2 part header, this is not an example page
            return Optional.empty();
        }

        return parseExamplePageCodes(html, examplePage);
    }

    @NotNull
    private String getFileNameFromPath(@NotNull String pathWithFilename) {
        int lastSlashIndex = pathWithFilename.lastIndexOf('/');
        if (lastSlashIndex != -1) {
            return pathWithFilename.substring(lastSlashIndex + 1);
        }
        return pathWithFilename;
    }

    private Optional<ExamplePage> parseExamplePageCodes(Document html, ExamplePage examplePage) {
        html.getElementsByTag("uibuilder-code-block").forEach(element -> {
            String href = element.attr("href");
            loadResourceFileContent(href)
                .ifPresent(content -> examplePage.getCodes().put(getFileNameFromPath(href), content));
        });
        return Optional.ofNullable(examplePage);
    }

    private Stream<FlowDefinition> allFlowDefinitionsWithHtml(FlowElement flowElement) {
        return flattenFlowElements(flowElement).stream()
            .filter(FlowDefinition.class::isInstance)
            .map(FlowDefinition.class::cast)
            .filter(flowDefinition -> StringUtils.isNotBlank(flowDefinition.getHtml()));
    }

    private List<FlowElement> flattenFlowElements(FlowElement element) {
        List<FlowElement> elements = new ArrayList<>();
        elements.add(element);
        if (element instanceof HasSubFlow) {
            List<FlowElement> childElements = ((HasSubFlow) element).getSubFlows();
            if (!childElements.isEmpty()) {
                for (FlowElement childElement : childElements) {
                    elements.addAll(flattenFlowElements(childElement));
                }
            }
        }
        if (element instanceof FlowInclude) {
            FlowDefinitions includedFlowDefinitions = loadFlowDefinition(((FlowInclude) element).getPath());
            List<FlowElement> childElements = includedFlowDefinitions.getSubFlows();
            if (!childElements.isEmpty()) {
                for (FlowElement childElement : childElements) {
                    elements.addAll(flattenFlowElements(childElement));
                }
            }
        }
        return elements;
    }

}
