/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - BackendCommunicatorControllerBean.java - START
import com.vaadin.flow.spring.annotation.UIScope;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.backendcommunicator.BackendCommunicator;
import lombok.AllArgsConstructor;
import lombok.Data;
import javax.inject.Provider;

@UIScope
@ControllerBean("backendCommunicatorBean")
public class BackendCommunicatorControllerBean {

    @UIComponent("communicator")
    private Provider<BackendCommunicator> communicator;

    @Data
    public static class MyParameter {
        private String name;
        private Integer value;
    }

    @Data
    @AllArgsConstructor
    public static class MyResult {
        private String value;
        private Double valueDouble;
    }


    @UIEventHandler("registerMethod")
    public void onRegisterMethod() {
        communicator.get().registerMethod("myCallableMethod", MyParameter.class, MyResult.class, this::someMethod);
    }

    private MyResult someMethod(MyParameter param) {
        switch(param.name) {
            case "div": return new MyResult("value / 100", param.value / 100.0);
            case "times": return new MyResult("value * 100", param.value * 100.0);
            default: return new MyResult("invalid request", -1.0);
        }
    }

}
// CODE BLOCK DEFINITION END
