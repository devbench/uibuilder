/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.dynamic;

// CODE BLOCK DEFINITION - DynamicPageControllerBean.java - START
import com.vaadin.flow.component.html.Div;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.core.page.Page;

@ControllerBean("dynamicPageCb")
public class DynamicPageControllerBean {

    @UIEventHandler("menuSelected")
    public void onMenuSelected(@UIComponent("flowNavigationExampleTarget") Page page) {
        String html = "<uibuilder-page>\n" +
            "    <style include=\"example-page-style\"></style>\n" +
            "    <div class=\"content\">" +
            "        <h2 part=\"header\">Dynamic content</h2>\n" +
            "        <div id=\"dynamicDiv\">Not clicked yet :(</div>\n" +
            "        <vaadin-button on-click=\"dynamicPageCb::clicked\">Click me</vaadin-button>\n" +
            "    </div>\n" +
            "</uibuilder-page>";

        page.loadContent("dynamic-page-1", html);
    }

    @UIEventHandler("clicked")
    public void onClicked(@UIComponent("dynamicDiv") Div div) {
        div.setText("Clicked :)");
    }
}
// CODE BLOCK DEFINITION END
