/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.search;

import io.devbench.uibuilder.examples.exampleapp.frontend.search.ExamplePageDataCollector.ExamplePage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SearchService {

    private static final Pattern TOKENIZER_PATTERN = Pattern.compile("\"([^\"]*)\"|'([^']*)'|(\\S+)");

    @Getter
    @AllArgsConstructor
    public static class SearchToken {
        private String text;
        private boolean caseSensitive;
    }

    private final ExamplePageDataCollector dataCollector;

    public SearchService(ExamplePageDataCollector dataCollector) {
        this.dataCollector = dataCollector;
    }

    @NotNull
    public SearchResults search(@NotNull String searchText) {
        dataCollector.requireIndexedData();

        Map<ExamplePage, Integer> foundPages = dataCollector.getExamplePages().stream().collect(Collectors.toMap(examplePage -> examplePage, examplePage -> 0));
        for (SearchToken searchToken : tokenizeSearchText(searchText)) {
            foundPages = searchPages(searchToken.getText(), searchToken.isCaseSensitive(), foundPages);
        }
        List<ExamplePage> sortedFoundPages = foundPages.entrySet()
            .stream()
            .sorted(Map.Entry.comparingByValue())
            .map(Map.Entry::getKey)
            .collect(Collectors.toList());

        List<SearchResult> results = new ArrayList<>();
        for (ExamplePage examplePage : sortedFoundPages) {
            results.add(SearchResult.builder()
                .snippet(examplePage.getTitle())
                .title(examplePage.getMenuTitle())
                .flowId(examplePage.getFlowId())
                .build());
        }

        return new SearchResults(results, dataCollector.isIndexing());
    }

    private List<SearchToken> tokenizeSearchText(String searchText) {
        List<SearchToken> tokens = new ArrayList<>();
        Matcher m = TOKENIZER_PATTERN.matcher(searchText);
        while (m.find()) {
            if (m.group(1) != null) {
                tokens.add(new SearchToken(m.group(1), false));
            } else if (m.group(2) != null) {
                tokens.add(new SearchToken(m.group(2), true));
            } else {
                tokens.add(new SearchToken(m.group(3), false));
            }
        }
        return tokens;
    }

    private boolean contains(boolean caseSensitive, String text, @NotNull String searchText) {
        if (text == null) {
            return false;
        }
        if (caseSensitive) {
            return text.contains(searchText);
        } else {
            return text.toLowerCase().contains(searchText.toLowerCase());
        }
    }

    private Map<ExamplePage, Integer> searchPages(String searchText, boolean caseSensitive, Map<ExamplePage, Integer> examplePages) {

        Map<ExamplePage, Integer> foundPages = new HashMap<>();

        for (ExamplePage examplePage : examplePages.keySet()) {
            if (contains(caseSensitive, examplePage.getMenuTitle(), searchText)) {
                foundPages.put(examplePage, foundPages.computeIfAbsent(examplePage, page -> 1));
                continue;
            }
            if (contains(caseSensitive, examplePage.getTitle(), searchText)) {
                foundPages.put(examplePage, foundPages.computeIfAbsent(examplePage, page -> 2));
                continue;
            }
            if (contains(caseSensitive, examplePage.getDescription(), searchText)) {
                foundPages.put(examplePage, foundPages.computeIfAbsent(examplePage, page -> 4));
                continue;
            }
            for (Map.Entry<String, String> codeEntry : examplePage.getCodes().entrySet()) {
                String code = codeEntry.getValue();
                if (code.contains(searchText)) {
                    foundPages.put(examplePage, foundPages.computeIfAbsent(examplePage, page -> 8));
                    break;
                }
            }
        }

        return foundPages;
    }

}
