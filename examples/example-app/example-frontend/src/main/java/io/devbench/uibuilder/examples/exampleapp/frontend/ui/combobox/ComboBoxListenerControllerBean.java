/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.combobox;

// CODE BLOCK DEFINITION - ComboBoxListenerControllerBean.java - START
import com.vaadin.flow.component.notification.Notification;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.combobox.UIBuilderComboBox;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import io.devbench.uibuilder.spring.page.PageScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;

@PageScope("combobox-backend-listener")
@ControllerBean("comboboxBackendListenerController")
public class ComboBoxListenerControllerBean {

    @Autowired
    @Qualifier("todoItemRepository")
    private CrudRepository<TodoItem, String> todoItemRepository;

    @UIEventHandler("afterLoad")
    public void afterLoad(@UIComponent("uibuilder-combobox") UIBuilderComboBox<TodoItem> comboBox) {
        comboBox.addValueChangeListener(event -> {
            if (event.getValue() != null) {
                Notification.show("The new element is: " + event.getValue().getTodo() + ".");
            } else {
                Notification.show("Cleared");
            }
        });
    }

    @UIEventHandler("showSelectedValue")
    public void showSelectedValue(@UIComponent("uibuilder-combobox") UIBuilderComboBox<TodoItem> comboBox) {
        if (comboBox.getValue() != null) {
            Notification.show("The selected element is: " + comboBox.getValue().getTodo() + ".");
        } else {
            Notification.show("Nothing is selected.");
        }
    }

    @UIEventHandler("toggleReadOnly")
    public void toggleReadOnly(@UIComponent("uibuilder-combobox") UIBuilderComboBox<TodoItem> comboBox) {
        comboBox.setReadOnly(!comboBox.isReadOnly());
    }

    @UIEventHandler("setValueToFirst")
    public void setValueToFirst(@UIComponent("uibuilder-combobox") UIBuilderComboBox<TodoItem> comboBox) {
        TodoItem todoItem = todoItemRepository.findAll().iterator().next();
        comboBox.setValue(todoItem);
    }

}
// CODE BLOCK DEFINITION END
