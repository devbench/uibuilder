/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.service;

import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonFactory;
import elemental.json.JsonObject;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import static java.util.function.Predicate.*;

@Slf4j
@Service
@PropertySource(value = "classpath:uibuilder-version.properties")
public class UIBuilderVersionService {

    private static final int FETCH_INTERVAL_SECONDS = 300; // 5 minutes

    @Getter
    private final String currentVersion;
    private final String versionsUrl;
    private final HttpClient httpClient;
    private final Map<String, String> versionUrlMap = new HashMap<>();

    private LocalDateTime lastFetchTime;

    public UIBuilderVersionService(@Value("${uibuilder.version}") String currentVersion,
                                   @Value("${uibuilder.versionsUrl:http://example-app-versions:8081}") String versionsUrl) {
        this.currentVersion = currentVersion;
        this.versionsUrl = versionsUrl;
        this.httpClient = HttpClient.newBuilder().build();
    }

    @PostConstruct
    public void initialize() {
        fetchVersions();
    }

    private Optional<String> fetchVersionsJson() {
        HttpRequest httpRequest = HttpRequest.newBuilder(URI.create(versionsUrl)).GET().build();
        try {
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            int responseStatusCode = response.statusCode();
            if (responseStatusCode == HttpURLConnection.HTTP_OK) {
                return Optional.ofNullable(response.body());
            }
            log.error("Could not fetch current uibuilder versions, response status code: {}", responseStatusCode);
        } catch (IOException | InterruptedException e) {
            log.error("Could not fetch current uibuilder versions", e);
        }
        return Optional.empty();
    }

    private void fetchVersions() {
        versionUrlMap.clear();
        fetchVersionsJson().ifPresent(versionsJson -> {
            JsonArray versions = Json.instance().parse(versionsJson);
            for (int versionIndex = 0; versionIndex < versions.length(); versionIndex++) {
                JsonObject versionObject = versions.getObject(versionIndex);
                versionUrlMap.put(versionObject.getString("version"), versionObject.getString("url"));
            }
            lastFetchTime = LocalDateTime.now();
        });
    }

    private void fetchVersionsIfNecessary() {
        if (lastFetchTime == null || lastFetchTime.isBefore(LocalDateTime.now().minusSeconds(FETCH_INTERVAL_SECONDS))) {
            fetchVersions();
        }
    }

    public Collection<String> getOtherVersions() {
        String currentVersion = getCurrentVersion();
        return getAllAvailableVersions().stream().filter(not(currentVersion::equals)).collect(Collectors.toSet());
    }

    public String getVersionUrl(@NotNull String version) {
        fetchVersionsIfNecessary();
        return Objects.requireNonNull(versionUrlMap.get(version), "UIBuilder version is null");
    }

    private Collection<String> getAllAvailableVersions() {
        fetchVersionsIfNecessary();
        return versionUrlMap.keySet();
    }
}
