/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.filter;

// CODE BLOCK DEFINITION - FilterSettingsController.java - START
import com.vaadin.flow.spring.annotation.UIScope;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.commonfilter.CommonFilter;
import io.devbench.uibuilder.components.commonfilter.filterrow.FilterDescriptor;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;
import javax.inject.Provider;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@UIScope
@ControllerBean("filterSettingsController")
public class FilterSettingsController {

    @UIComponent("commonFilter")
    private Provider<CommonFilter> commonFilter;

    private List<FilterDescriptor> filterDescriptors = new ArrayList<>();

    @UIEventHandler("setDeadlineFilter")
    public void onSetDeadlineFilter() {
        commonFilter.get().applyFilterDescriptors(Collections.singletonList(
            FilterDescriptor.builder()
                .filterPath("deadline")
                .operator(OperatorType.BETWEEN)
                .values(Arrays.asList(LocalDate.now().plusDays(1).toString(), LocalDate.now().plusDays(9).toString()))
                .build()
        ));
    }

    @UIEventHandler("setTextFilter")
    public void onSetTextFilter() {
        commonFilter.get().applyFilterDescriptors(Collections.singletonList(
            FilterDescriptor.builder()
                .filterPath("todo")
                .operator(OperatorType.LIKE)
                .value("%cat%")
                .build()
        ));
    }

    @UIEventHandler("saveCurrentFilterSettings")
    public void onSaveCurrentFilterSettings() {
        filterDescriptors = commonFilter.get().getCurrentFilterDescriptors();
    }

    @UIEventHandler("loadFilterSettings")
    public void onLoadFilterSettings() {
        commonFilter.get().applyFilterDescriptors(filterDescriptors);
    }
}
// CODE BLOCK DEFINITION END
