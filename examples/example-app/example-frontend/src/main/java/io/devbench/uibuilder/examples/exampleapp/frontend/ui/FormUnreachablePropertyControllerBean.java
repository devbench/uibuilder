/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - FormUnreachablePropertyControllerBean.java - START
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.Label;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import org.springframework.context.annotation.Scope;

import java.time.LocalDate;

@Scope("session")
@ControllerBean("unreachablePropertyControllerBean")
public class FormUnreachablePropertyControllerBean {

    @UIEventHandler("activateTodoItem1")
    public void activateTodoItem1(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        TodoItem todoItem = new TodoItem("First todo item", LocalDate.now(), null, null, new Label("Custom label"));
        simpleForm.setFormItem(todoItem);
    }

    @UIEventHandler("activateTodoItem2")
    public void activateTodoItem2(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        TodoItem todoItem = new TodoItem("Second todo item", LocalDate.now().minusDays(2), null, null, new Label(""));
        simpleForm.setFormItem(todoItem);
    }

    @UIEventHandler("activateTodoItem3")
    public void activateTodoItem3(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        TodoItem todoItem = new TodoItem("Third todo item", LocalDate.now().plusDays(2), null);
        simpleForm.setFormItem(todoItem);
    }

    @UIEventHandler("clearFormItem")
    public void clearFormItem(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        simpleForm.setFormItem(null);
    }

    @UIEventHandler("createLabel")
    public void createLabel(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        TodoItem formItem = simpleForm.getFormItem();
        if (formItem != null) {
            formItem.setLabel(new Label("created label"));
            simpleForm.setFormItem(formItem);
        }
    }

}
// CODE BLOCK DEFINITION END
