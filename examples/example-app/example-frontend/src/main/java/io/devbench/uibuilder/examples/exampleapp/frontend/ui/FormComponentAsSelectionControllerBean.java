/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - FormComponentAsSelectionControllerBean.java - START

import com.vaadin.flow.component.notification.Notification;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.ReminderItem;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@ControllerBean("formComponentAsSelectionControllerBean")
public class FormComponentAsSelectionControllerBean {

    private List<ReminderItem> reminderItems;

    @UIEventHandler("initializeData")
    public void onInitializeData() {
        reminderItems = new ArrayList<>();
        reminderItems.add(new ReminderItem("Reminder one", LocalDate.now(), 10));
        reminderItems.add(new ReminderItem("Reminder two", LocalDate.now(), 20));
        reminderItems.add(new ReminderItem("Reminder three", LocalDate.now(), 30));
        reminderItems.add(new ReminderItem("Reminder four", LocalDate.now(), 40));
        reminderItems.add(new ReminderItem("Reminder five", LocalDate.now(), 50));
    }

    @UIEventHandler("initializeForm")
    public void onInitializeForm(@UIComponent("form") UIBuilderForm<TodoItem> form) {
        Set<ReminderItem> reminders = new HashSet<>();
        reminders.add(reminderItems.get(1));

        TodoItem todoItem = new TodoItem("Test todo", LocalDate.now(), null, reminders, null);

        form.setItem(todoItem);
    }

    @UIEventHandler("save")
    public void onFormSave(@Item TodoItem item) {

        String notificationMessage = item.getReminders()
            .stream()
            .map(ReminderItem::getText)
            .collect(joiningNbsp());
        Notification.show(notificationMessage, 5000, Notification.Position.MIDDLE);

    }

    // CODE BLOCK DEFINITION END
    private static Collector<CharSequence, ?, String> joiningNbsp() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 30; i++) {
            sb.append("\u00A0");
        }
        sb.append("\n");
        return Collectors.joining(sb.toString());
    }
// CODE BLOCK DEFINITION - FormComponentAsSelectionControllerBean.java - START
}
// CODE BLOCK DEFINITION END
