/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - FormWithRichTextControllerBean.java - START

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Pre;
import io.devbench.quilldelta.*;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.components.richtext.UIBuilderRichTextEditor;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;

import javax.inject.Provider;
import java.time.LocalDate;

@ControllerBean("formRichTextCb")
public class FormWithRichTextControllerBean {

    @UIComponent("form")
    private Provider<UIBuilderForm<TodoItem>> formProvider;

    @UIComponent("richText")
    private Provider<UIBuilderRichTextEditor> richTextFieldProvider;

    @UIComponent("lastSavedHtml")
    private Provider<Div> lastSavedHtml;

    @UIComponent("lastSavedHtmlRaw")
    private Provider<Pre> lastSavedHtmlRaw;

    @UIEventHandler("initForm")
    public void initForm() {
        TodoItem todoItem = new TodoItem("First todo item", LocalDate.now());
        formProvider.get().setFormItem(todoItem);
    }

    @UIEventHandler("onSave")
    public void onSave() {
        TodoItem item = formProvider.get().getFormItem();
        Div lastSavedHtmlDiv = lastSavedHtml.get();
        lastSavedHtmlDiv.removeAll();
        lastSavedHtmlDiv.add(new Html("<div class=\"ql-editor\" style=\"white-space: normal\">" + item.getTodo() + "</div>"));

        Pre lastSavedHtmlRawDiv = lastSavedHtmlRaw.get();
        lastSavedHtmlRawDiv.removeAll();
        lastSavedHtmlRawDiv.setText(item.getTodo());
    }

    @UIEventHandler("resetWithHtml")
    public void resetWithHtml() {
        String html = "<p>paragraph one</p><p>second <strong>line</strong> :)</p><p>last line</p>";
        richTextFieldProvider.get().setValueAsHtmlText(html);
    }

    @UIEventHandler("resetWithPlain")
    public void resetWithPlain() {
        richTextFieldProvider.get().setValueAsPlainText("paragraph one\nsecond line :)\nlast line");
    }

    @UIEventHandler("resetWithOps")
    public void resetWithOps() {
        Delta delta = new Delta("This is a text\nAnd this is another");
        delta.apply(Ops.from(Op.retain(5), Op.retain(2, Attribute.create(AttributeType.BOLD, true))));
        Ops ops = delta.getOps();
        richTextFieldProvider.get().setValueAsOps(ops);
    }

    @UIEventHandler("resetWithInsertOps")
    public void resetWithInsertOps() {
        Ops ops = Ops.from(
            Op.insert("This "),
            Op.insert("is", Attribute.create(AttributeType.BOLD, true)),
            Op.insert(" a text\nAnd this is "),
            Op.insert("another",
                Attribute.create(AttributeType.BOLD, true),
                Attribute.create(AttributeType.ITALIC, true)),
            Op.insert(" line."));
        richTextFieldProvider.get().setValueAsOps(ops);
    }

}
// CODE BLOCK DEFINITION END
