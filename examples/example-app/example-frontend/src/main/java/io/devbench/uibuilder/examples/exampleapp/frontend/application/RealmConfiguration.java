/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.application;

import org.apache.shiro.authz.permission.WildcardPermission;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

@Configuration
public class RealmConfiguration {

    public static final String ADMIN_ROLE = "ADMIN";
    public static final String USER_ROLE = "USER";

    @Bean
    public SimpleAccountRealm simpleAccountRealm() {
        SimpleAccountRealm simpleAccountRealm = new SimpleAccountRealm();
        simpleAccountRealm.addAccount("admin", "admin", ADMIN_ROLE, USER_ROLE);
        simpleAccountRealm.addAccount("user", "user", USER_ROLE);
        simpleAccountRealm.setRolePermissionResolver(roleString -> Collections.singleton(new WildcardPermission(roleString)));
        return simpleAccountRealm;
    }

}
