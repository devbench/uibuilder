/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - MdcControllerBean.java - START
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.EventQualifier;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.api.exceptions.ProgressInterruptException;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;

import java.util.List;

@ControllerBean("simpleMdc")
public class MdcControllerBean {

    @UIEventHandler(value = "selected", qualifiers = EventQualifier.REQUESTED)
    public void throwExceptionConditionsAreMetOnSpecifiedItem(@Item List<TodoItem> selectedItems) {
        if (selectedItems.stream().anyMatch(i -> i.getTodo().contains("test"))) {
            throw new ProgressInterruptException("Item with 'test' cannot be selected");
        }
    }

}
// CODE BLOCK DEFINITION END
