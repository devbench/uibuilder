/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.list;

// CODE BLOCK DEFINITION - ListBoxControllerBean.java - START

import com.vaadin.flow.component.notification.Notification;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Value;
import io.devbench.uibuilder.components.listbox.UIBuilderListBox;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;

import javax.inject.Provider;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@ControllerBean("listBoxCb")
public class ListBoxControllerBean {

    @UIComponent("myListBox")
    private Provider<UIBuilderListBox<TodoItem>> listBoxProvider;

    @Autowired
    @Qualifier("todoItemRepository")
    private CrudRepository<TodoItem, String> todoItemRepository;

    @UIEventHandler("selection")
    public void onSelection(@Item List<TodoItem> items, @Value("detail.newSelectedIndexes") List<String> newSelectedIndexes) {
        String selectedItemsNames = items
            .stream()
            .map(TodoItem::getTodo)
            .collect(Collectors.joining(", "));

        Notification.show("Selected indexes: " + newSelectedIndexes + ", Selected items: " + selectedItemsNames);
    }

    @UIEventHandler("selectSecondItem")
    public void onSelectSecondItemClick() {
        StreamSupport.stream(todoItemRepository.findAll().spliterator(), false)
            .filter(this::isTodoTextStartsWithWrite)
            .findFirst()
            .ifPresent(item -> listBoxProvider.get().setSelectedItem(item));
    }

    @UIEventHandler("selectSecondAndThirdItem")
    public void onSelectSecondAndThirdItemClick() {
        List<TodoItem> items = StreamSupport.stream(todoItemRepository.findAll().spliterator(), false)
            .filter(this::isTodoTextStartsWithWriteOrBrew)
            .collect(Collectors.toList());

        listBoxProvider.get().setSelectedItems(items);
    }

    private boolean isTodoTextStartsWithWriteOrBrew(TodoItem item) {
        String todo = item.getTodo();
        return todo.startsWith("Write") || todo.startsWith("Brew");
    }

    private boolean isTodoTextStartsWithWrite(TodoItem item) {
        return item.getTodo().startsWith("Write");
    }

}
// CODE BLOCK DEFINITION END
