/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - MultiValueFormBeanItemsControllerBean.java - START

import com.vaadin.flow.component.notification.Notification;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.detailpanel.UIBuilderDetailPanel;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.MultiItem;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.MultiItemHolder;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.MultiItemLabel;
import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ControllerBean("multiValueFormBeanItemsCb")
public class MultiValueFormBeanItemsControllerBean {

    private List<MultiItemLabel> labels;
    private List<MultiItemHolder> holders;

    @PostConstruct
    public void initControllerBean() {
        labels = new ArrayList<>();
        for (int i = 0; i < 300; i++) {
            labels.add(new MultiItemLabel("Label " + i, 1000 + i));
        }

        MultiItemHolder holder = new MultiItemHolder();

        holder.setHolderName("First holder");
        holder.setMultiItems(new ArrayList<>(Arrays.asList(
            new MultiItem("One", 100, LocalDateTime.now().withSecond(0).withNano(0)),
            new MultiItem("Two", 200, LocalDateTime.now().withSecond(0).withNano(0).plusDays(1), labels.get(2)),
            new MultiItem("120", 10, LocalDateTime.now().withSecond(0).withNano(0).plusDays(2), labels.get(120)))));

        holders = new ArrayList<>();
        holders.add(holder);

        holder = new MultiItemHolder();
        holder.setHolderName("Empty holder");

        holders.add(holder);
    }

    private void showMultiItems(List<MultiItem> multiItems) {
        Notification.show(
            "Items:\n" + multiItems.stream()
                .map(MultiItem::toString)
                .collect(Collectors.joining("\n"))
                .replaceAll(" ", "\u00A0")
                .replaceAll("-", "\u2011"),
            5000, Notification.Position.TOP_CENTER);
    }

    @UIEventHandler("save")
    public void onSave(@Item MultiItemHolder holder, @UIComponent("multiDetail") UIBuilderDetailPanel<MultiItemHolder> detailPanel) {
        showMultiItems(holder.getMultiItems());
    }

}
// CODE BLOCK DEFINITION END
