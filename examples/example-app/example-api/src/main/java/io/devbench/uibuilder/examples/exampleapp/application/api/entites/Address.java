/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.api.entites;

// CODE BLOCK DEFINITION - Address.java - START

import io.devbench.uibuilder.examples.exampleapp.application.api.validation.AddressWarningGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@NoArgsConstructor
@Table(name = "addresses")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Address extends BaseEntity {

    @NotNull(message = "Country cannot be null")
    private String country;

    @NotNull(message = "City cannot be null")
    private String city;

    @Size(groups = AddressWarningGroup.class, min = 5, message = "Zip should be at least 5 characters")
    @NotNull(message = "Zip code must be specified")
    private String zip;

    private String street;

}
// CODE BLOCK DEFINITION END
