/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.api.entites;

import lombok.Data;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

// CODE BLOCK DEFINITION - MultiItemHolder.java - START
@Data
public class MultiItemHolder implements Serializable {

    @Size(min = 3, message = "Minimum three characters required")
    private String holderName;

    private List<@Min(value = 5, message = "Minimum priority is 5") Integer> priorities;

    private List<@Valid MultiItem> multiItems;

}
// CODE BLOCK DEFINITION END
