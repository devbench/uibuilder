/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.seleniumtest;

import io.devbench.uibuilder.test.componenttest.UIbuilderMenuItem;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.jupiter.api.Assertions.*;

public class ThemeIT extends AbstractSeleniumTest {

    @Test
    void should_apply_theme_if_component_loaded_first() {
        driver.get(getBaseUrl());
        wait.until(driver -> driver.findElement(By.tagName("menu-bar")));

        WebElement mainMenuItem = driver.findElement(By.cssSelector("menu-item#main\\/menu"));
        mainMenuItem.click();

        new UIbuilderMenuItem(driver, By.cssSelector("menu-item#main\\/menu\\/simple_themed")).getButton().click();

        assertThemeApplied();
    }

    @Test
    void should_apply_theme_if_theme_loaded_first() {
        driver.get(getBaseUrl() + "/menu/simple_themed");

        assertThemeApplied();
    }

    private void assertThemeApplied() {
        WebElement fancyMenuItem = wait.until(driver -> driver.findElement(By.cssSelector("#mainTarget menu-bar")))
            .findElement(By.cssSelector("menu-item[theme='fancy']"));

        String fancyMenuItemBackgroundColor = new UIbuilderMenuItem(driver, fancyMenuItem)
            .getButton()
            .getCssValue("background-color");

        assertTrue(
            "rgba(128, 64, 191, 1)".equals(fancyMenuItemBackgroundColor) ||
                "rgb(128, 64, 191)".equals(fancyMenuItemBackgroundColor)
        );
    }
}
