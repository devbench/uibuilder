/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.seleniumtest;

import io.devbench.uibuilder.test.componenttest.UIbuilderWindow;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import static org.junit.jupiter.api.Assertions.*;


public class SimpleWindowIT extends AbstractSeleniumTest {

    @Test
    void close_button_should_close_window() {
        driver.get(getBaseUrl() + "/window/simple");
        wait.until(driver -> driver.findElement(By.tagName("vaadin-button")))
            .click();

        UIbuilderWindow window = new UIbuilderWindow(driver, By.cssSelector("uibuilder-window#example_window_001"));

        assertTrue(window.getElement().isDisplayed());

        window.getCloseButton().click();

        assertFalse(window.getElement().isDisplayed());
    }

}
