/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.jpa.ui;

// CODE BLOCK DEFINITION - CrudPanelChangedOnlyValidationControllerBean.java - START

import com.vaadin.flow.component.checkbox.Checkbox;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.detailpanel.UIBuilderDetailPanel;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.ReminderItem;
import io.devbench.uibuilder.examples.exampleapp.application.jpa.repositories.ReminderItemRepository;
import io.devbench.uibuilder.spring.crud.AbstractSpringControllerBean;

import java.time.LocalDate;

@ControllerBean("crudPanelChangedOnlyValidationControllerBean")
public class CrudPanelChangedOnlyValidationControllerBean extends AbstractSpringControllerBean<ReminderItem, ReminderItemRepository> {

    public CrudPanelChangedOnlyValidationControllerBean(ReminderItemRepository repository) {
        super(() -> new ReminderItem(null, LocalDate.now(), 110), repository);
    }

    @UIEventHandler("changedOnlyValidationClicked")
    public void onChangedOnlyValidationClicked(@UIComponent("covCb") Checkbox changedOnlyValidationCheckbox,
                                               @UIComponent("cp-detail-1") UIBuilderDetailPanel<ReminderItem> detailPanel) {
        detailPanel.setChangedOnlyValidation(changedOnlyValidationCheckbox.getValue());
    }
}
// CODE BLOCK DEFINITION END
