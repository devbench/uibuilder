/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.jpa.repositories;

// CODE BLOCK DEFINITION - TodoItemDynamicCustomQueryImpl.java - START
import com.querydsl.core.support.QueryBase;
import com.querydsl.jpa.impl.JPAQuery;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.QTodoItem;
import io.devbench.uibuilder.examples.exampleapp.application.repository.dynamic.DynamicControllerBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Optional;

public class TodoItemDynamicCustomQueryImpl implements TodoItemDynamicCustomQuery {

    @Autowired
    private DynamicControllerBean dynamicControllerBean;

    @Override
    public QueryBase findByDeadLine() {
        final Optional<LocalDate> deadLine = dynamicControllerBean.getDeadLine();
        if (deadLine.isPresent()) {
            return new JPAQuery<>().from(QTodoItem.todoItem).where(QTodoItem.todoItem.deadline.eq(deadLine.get()));
        } else {
            return new JPAQuery<>().from(QTodoItem.todoItem);
        }
    }

}
// CODE BLOCK DEFINITION END
