/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.utils.copycode.services;

import io.devbench.uibuilder.examples.utils.copycode.pojos.CodeBlock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class CodeBlockExtractor {

    private static final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:**/*.{java,html,xml,js}");

    private static Collection<Pattern> codeBlockStartPatterns = Collections.unmodifiableCollection(Arrays.asList(
        Pattern.compile("// CODE BLOCK DEFINITION - (.+?) - START"),
        Pattern.compile("<!-- CODE BLOCK DEFINITION - (.+?) - START -->")
    ));

    private static Collection<String> codeBlockEndPatterns = Collections.unmodifiableCollection(Arrays.asList(
        "// CODE BLOCK DEFINITION END",
        "<!-- CODE BLOCK DEFINITION END -->"
    ));

    public Collection<CodeBlock> extractCodeBlocksFromPath(Path path) throws IOException {
        return Files
            .walk(path)
            .filter(pathMatcher::matches)
            .filter(Files::isRegularFile)
            .map(this::readPathToString)
            .flatMap(this::extractCodeBlocksFromFile)
            .collect(Collectors.toList());
    }

    private Stream<CodeBlock> extractCodeBlocksFromFile(List<String> fileContents) {
        Iterator<String> lines = fileContents.iterator();
        List<CodeBlock> codeBlocks = new ArrayList<>();
        while (lines.hasNext()) {
            String line = lines.next();
            Optional<String> codeBlockName = getCodeBlockNameFromLine(line);
            if (codeBlockName.isPresent()) {
                StringBuilder codeBlock = extractCodeBlockFromPosition(codeBlockName.get(), lines);
                codeBlocks.add(new CodeBlock(codeBlockName.get(), align(codeBlock.toString())));
            }
        }
        return codeBlocks.stream();
    }

    private String align(String codeBlockToAlign) {
        Supplier<Stream<String>> linesSupplier = () -> new BufferedReader(new StringReader(codeBlockToAlign)).lines();
        int minIndent = linesSupplier.get()
            .mapToInt(this::firstNonWhiteSpaceChar)
            .min()
            .orElse(0);
        return linesSupplier.get()
            .map(line -> line.substring(minIndent))
            .collect(Collectors.joining("\n", "", "\n"));
    }

    private int firstNonWhiteSpaceChar(String string) {
        for (int i = 0; i < string.length(); i++) {
            if (!Character.isWhitespace(string.charAt(i))) {
                return i;
            }
        }
        return string.length();
    }

    private StringBuilder extractCodeBlockFromPosition(String codeBlockName, Iterator<String> lines) {
        if (lines.hasNext()) {
            String line = lines.next();
            StringBuilder codeBlock = new StringBuilder();
            do {
                codeBlock.append(line).append('\n');
                if (lines.hasNext()) {
                    line = lines.next();
                } else {
                    throw new IllegalStateException("Code block (" + codeBlockName + ") is not correctly formatted.");
                }
            } while (!isCurrentLineTheCodeBlocksEnd(line));
            return codeBlock;
        } else {
            throw new IllegalStateException("Code block (" + codeBlockName + ") is not correctly formatted.");
        }
    }

    private boolean isCurrentLineTheCodeBlocksEnd(String line) {
        String trimmedLine = line.trim();
        return codeBlockEndPatterns.stream().anyMatch(trimmedLine::equals);
    }

    private Optional<String> getCodeBlockNameFromLine(String line) {
        return codeBlockStartPatterns
            .stream()
            .map(pattern -> pattern.matcher(line))
            .filter(Matcher::find)
            .map(matcher -> matcher.group(1))
            .findFirst();
    }

    private List<String> readPathToString(Path path) {
        try {
            return Files.readAllLines(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
