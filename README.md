# DevBench UIBuilder

UIBuilder is web framework that mainly focuses on simplicity and ease of use. Provides a clean
and polished way to define the UI (web browser) and link the tasks and events to the backend
(java).

The web presentation is as simple as creating a simple HTML web page with web components, like
if you would create a polymer application. Mainly this is because the relevant part of the
components are simple web components, with some custom code needed for the integration to the
framework.

The communication with the backend is mainly driven by Vaadin and GWT.

### Build

Requirements:

 - Apache Maven 3.3.9+
 - Java 1.8

The module versions are all the same, and the whole project can be built by a simple: 

```
mvn clean package
```

However, to run the frontend tests, you will need to add the `-Pfrontend-test` parameter to
activate the profile.

```
mvn clean package -Pfrontend-test
``` 

Also, it is safe to build the modules in parallel:

```
mvn clean package -T2C -Pfrontend-test
```

### Example App

The Example App is standalone spring-boot application, which demonstrates the basic workflow
of the framework along with the presentation of the main components with running examples and
showing the required HTML and java source at the same time.

To run the example app, you have two options. The Example App contains a part that is separated into two alternative implementations.
This part is the persistence. Currently supported persistence providers are JPA (EclipseLink)
and Ebean. You can choose which to use by defining a maven profile:  

#### Spring JPA

To use the JPA implementation with the example app, run the app with the following command:

```
mvn clean package -Pjpa-example-app -Dmaven.test.skip
```

or if the UIBuilder is already installed in your local repository, simple run:

```
mvn -f examples clean package -Pjpa-example-app
```

#### Spring Ebean

To use the Ebean implementation with the example app, run the app with the following command:

```
mvn clean package -ebean-example-app -Dmaven.test.skip
```

or if the UIBuilder is already installed in your local repository, simple run:

```
mvn -f examples clean package -Pebean-example-app
```


<sub>Financed by Webvalto Ltd. and European Union European Regional Development Fund as part of GINOP-2.1.7-15-2016-00836 project</sub>
