/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuildertest.data;

import io.devbench.uibuilder.api.parse.BindingContext;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Element;
import java.util.Set;

@Getter
@AllArgsConstructor
public class SimpleTestBindingContext implements BindingContext {
    private Set<String> bindings;
    private String dataSourceName;

    public @NotNull String getDataSourceId() {
        return null;
    }

    @Override
    public @NotNull Element getParsedElement() {
        return null;
    }
}
