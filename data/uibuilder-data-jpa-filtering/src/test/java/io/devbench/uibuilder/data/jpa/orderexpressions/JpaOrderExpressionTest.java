/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.jpa.orderexpressions;

import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.PathBuilder;
import io.devbench.uibuilder.data.api.order.SortOrder;
import lombok.Data;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class JpaOrderExpressionTest {

    private JpaOrderExpression testObj;

    @BeforeEach
    public void setup() {
        testObj = new JpaOrderExpression();
    }

    @Test
    @DisplayName("Should create one order expression")
    public void should_create_one_order_expression() {
        PathBuilder<TestEntity> testEntityPathBuilder = new PathBuilder<>(TestEntity.class, TestEntity.class.getSimpleName());
        testObj.setQueryPath(testEntityPathBuilder);
        SortOrder sortOrder = createSortOrder("nev", SortOrder.Direction.ASCENDING);
        testObj.addOrder(sortOrder);
        List<OrderSpecifier> orderSpecifierList = testObj.toOrder().collect(Collectors.toList());
        assertAll(
            () -> assertEquals(1, orderSpecifierList.size()),
            () -> assertEquals(Order.ASC, orderSpecifierList.get(0).getOrder()),
            () -> assertEquals("TestEntity.nev", orderSpecifierList.get(0).getTarget().toString())
        );
    }

    @Test
    @DisplayName("Should create composite order expression")
    public void should_create_composite_order_expression() {
        PathBuilder<TestEntity> testEntityPathBuilder = new PathBuilder<>(TestEntity.class, TestEntity.class.getSimpleName());
        testObj.setQueryPath(testEntityPathBuilder);

        SortOrder counterSortOrder = createSortOrder("counter", SortOrder.Direction.DESCENDING);
        SortOrder portSortOrder = createSortOrder("port", SortOrder.Direction.ASCENDING);
        SortOrder nevSortOrder = createSortOrder("nev", SortOrder.Direction.DESCENDING);
        testObj.addOrders(counterSortOrder, portSortOrder, nevSortOrder);

        List<OrderSpecifier> orderSpecifierList = testObj.toOrder().collect(Collectors.toList());
        assertEquals(3, orderSpecifierList.size());

        assertOrderSpecifier(orderSpecifierList.get(0), Order.DESC, "TestEntity.counter");
        assertOrderSpecifier(orderSpecifierList.get(1), Order.ASC, "TestEntity.port");
        assertOrderSpecifier(orderSpecifierList.get(2), Order.DESC, "TestEntity.nev");
    }

    @Test
    void should_filter_out_the_not_filed_orders() {
        PathBuilder<TestEntity> testEntityPathBuilder = new PathBuilder<>(TestEntity.class, TestEntity.class.getSimpleName());
        testObj.setQueryPath(testEntityPathBuilder);
        SortOrder counterSortOrder = createSortOrder("counter", SortOrder.Direction.DESCENDING);
        SortOrder portSortOrder = createSortOrder("port", null);
        SortOrder nevSortOrder = createSortOrder("nev", null);
        testObj.addOrders(counterSortOrder, portSortOrder, nevSortOrder);

        List<OrderSpecifier> orderSpecifierList = testObj.toOrder().collect(Collectors.toList());
        assertEquals(1, orderSpecifierList.size());
    }

    public void assertOrderSpecifier(OrderSpecifier orderSpecifier, Order expectedOrder, String expectedTarget) {
        assertEquals(expectedOrder, orderSpecifier.getOrder());
        assertEquals(expectedTarget, orderSpecifier.getTarget().toString());
    }

    public SortOrder createSortOrder(String nev, SortOrder.Direction direction) {
        SortOrder sortOrder = new SortOrder();
        sortOrder.setPath(nev);
        sortOrder.setDirection(direction);
        return sortOrder;
    }


    @Data
    private final class TestEntity {
        private String nev;
        private Long port;
        private Integer counter;
    }

}
