/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.filter.metadata;

import io.devbench.uibuilder.data.common.dataprovidersupport.TestElement;
import io.devbench.uibuilder.data.common.filter.metadata.CommonBindingMetadataProvider;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CommonBindingMetadataProviderTest {

    private CommonBindingMetadataProvider<TestElement> testObj;

    @BeforeEach
    void setup() {
        testObj = new CommonBindingMetadataProvider<>(() -> TestElement.class);
    }

    @Test
    @DisplayName("Should find property by path in class metadata and create converter for the property")
    public void should_find_property_by_path_in_class_metadata_and_create_converter_for_the_property() {
        BindingMetadata idBindMetadata = testObj.getMetadataForPath("id");
        assertAll(
            () -> assertEquals("id", idBindMetadata.getPath()),
            () -> assertEquals(100, idBindMetadata.getConverter().convertFrom("100")),
            () -> assertEquals("150", idBindMetadata.getConverter().convertTo(150)),
            () -> assertEquals(Integer.class, idBindMetadata.getPropertyType())
        );

        BindingMetadata subNameBindMetadata = testObj.getMetadataForPath("subElement.name");
        assertAll(
            () -> assertEquals("subElement.name", subNameBindMetadata.getPath()),
            () -> assertEquals("testName", subNameBindMetadata.getConverter().convertFrom("testName")),
            () -> assertEquals("testName2", subNameBindMetadata.getConverter().convertTo("testName2")),
            () -> assertEquals(String.class, subNameBindMetadata.getPropertyType())
        );
    }

}
