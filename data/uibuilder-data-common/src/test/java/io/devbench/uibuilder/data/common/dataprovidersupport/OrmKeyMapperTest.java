/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import elemental.json.Json;
import io.devbench.uibuilder.core.controllerbean.uiproperty.StringPropertyConverter;
import io.devbench.uibuilder.core.exceptions.PropertyConverterNotFoundException;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadata;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadataProvider;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.datasource.MockAndFE;
import io.devbench.uibuilder.data.common.datasource.MockEquals;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;
import io.devbench.uibuilder.data.common.filter.logicaloperators.AndFilterExpression;
import io.devbench.uibuilder.test.annotations.ReadResource;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.extensions.ResourceReaderExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({ResourceReaderExtension.class, MockitoExtension.class})
class OrmKeyMapperTest {

    @Mock
    private CommonDataSource<TestElement, ?, FilterExpression<?>, ?> dataSource;

    @Mock
    private FilterExpressionFactory expressionFactory;

    @Mock
    private BindingMetadataProvider metadataProvider;

    private OrmKeyMapper<TestElement, ?> testObj;

    @BeforeEach
    @SuppressWarnings("unused")
    void setup() {
        testObj = new OrmKeyMapper<>(dataSource, metadataProvider, Arrays.asList("id", "name", "subElement.name"));
    }

    @Test
    @DisplayName("Should build key json object from a single element based on the data source's key paths")
    public void should_build_key_json_object_from_a_single_element_based_on_the_data_source_s_key_paths(@ReadResource("/key-mapper-expected-key.json") String expectedKeyString) {
        TestElement testElement = createTestElement();

        BindingMetadata idMetadata = new BindingMetadata();
        idMetadata.setPath("id");
        StringPropertyConverter idPropertyConverter = mock(StringPropertyConverter.class);
        when(idPropertyConverter.convertTo(any(Integer.class))).then(invocation -> invocation.getArgument(0).toString());
        idMetadata.setConverter(idPropertyConverter);
        idMetadata.setPropertyType(Integer.class);
        when(metadataProvider.getMetadataForPath("id")).thenReturn(idMetadata);

        BindingMetadata nameMetadata = new BindingMetadata();
        nameMetadata.setPath("name");
        StringPropertyConverter namePropertyConverter = mock(StringPropertyConverter.class);
        when(namePropertyConverter.convertTo(any(String.class))).then(invocation -> invocation.getArgument(0));
        nameMetadata.setConverter(namePropertyConverter);
        nameMetadata.setPropertyType(String.class);
        when(metadataProvider.getMetadataForPath("name")).thenReturn(nameMetadata);

        BindingMetadata subElementNameMetadata = new BindingMetadata();
        subElementNameMetadata.setPath("subElement.name");
        StringPropertyConverter subNamePropertyConverter = mock(StringPropertyConverter.class);
        when(subNamePropertyConverter.convertTo(any(String.class))).then(invocation -> invocation.getArgument(0));
        subElementNameMetadata.setConverter(subNamePropertyConverter);
        subElementNameMetadata.setPropertyType(String.class);
        when(metadataProvider.getMetadataForPath("subElement.name")).thenReturn(subElementNameMetadata);

        String key = testObj.getKey(ClassMetadata.ofClass(TestElement.class).withInstance(testElement));

        assertAll(
            () -> assertNotNull(key),
            () -> assertEquals(key, Base64.getEncoder().encodeToString(Json.parse(expectedKeyString).toJson().getBytes(StandardCharsets.UTF_8)))
        );
    }


    @Test
    @DisplayName("Should query item from data source based on the json representation of its key")
    public void should_query_item_from_data_source_based_on_the_json_representation_of_the_key(@ReadResource("/key-mapper-expected-key.json") String expectedKeyString) {
        mockFilterExpressions();
        TestElement testElement = createTestElement();
        when(dataSource.findElementByKeyFilter(any(FilterExpression.class))).thenReturn(testElement);

        StringPropertyConverter converter = mock(StringPropertyConverter.class);
        when(converter.convertFrom("1")).thenReturn(1);
        when(converter.convertFrom("testName")).thenReturn("testName");
        when(converter.convertFrom("subName")).thenReturn("subName");


        BindingMetadata idMetadata = new BindingMetadata();
        idMetadata.setPath("id");
        idMetadata.setConverter(converter);
        idMetadata.setPropertyType(Integer.class);
        when(metadataProvider.getMetadataForPath("id")).thenReturn(idMetadata);

        BindingMetadata nameMetadata = new BindingMetadata();
        nameMetadata.setPath("name");
        nameMetadata.setConverter(converter);
        nameMetadata.setPropertyType(Integer.class);
        when(metadataProvider.getMetadataForPath("name")).thenReturn(nameMetadata);

        BindingMetadata subNameMetadata = new BindingMetadata();
        subNameMetadata.setPath("subElement.name");
        subNameMetadata.setConverter(converter);
        subNameMetadata.setPropertyType(Integer.class);
        when(metadataProvider.getMetadataForPath("subElement.name")).thenReturn(subNameMetadata);

        TestElement item = testObj.getItem(Base64.getEncoder().encodeToString(expectedKeyString.getBytes(StandardCharsets.UTF_8)));

        verify(converter, times(3)).convertFrom(matches("(1|testName|subName)"));

        ArgumentCaptor<FilterExpression> expressionCaptor = ArgumentCaptor.forClass(FilterExpression.class);
        verify(dataSource).findElementByKeyFilter(expressionCaptor.capture());
        FilterExpression value = expressionCaptor.getValue();

        assertAll(
            () -> assertSame(testElement, item),
            () -> assertTrue(value instanceof AndFilterExpression),
            () -> assertEquals(3, ((AndFilterExpression) value).getExpressions().size()),
            () -> assertEqualsExpressions(((AndFilterExpression) value).getExpressions())
        );
    }

    @Test
    void should_throw_exception_when_type_not_convert(@ReadResource("/key-mapper-expected-key.json") String expectedKeyString) {
        BindingMetadata idMetadata = new BindingMetadata();
        idMetadata.setPath("id");
        idMetadata.setConverter(null);
        idMetadata.setPropertyType(Integer.class);
        when(metadataProvider.getMetadataForPath("id")).thenReturn(idMetadata);
        mockFilterExpressions();
        when(dataSource.getElementType()).thenReturn((Class) TestElement.class);
        final PropertyConverterNotFoundException propertyConverterNotFoundException = assertThrows(PropertyConverterNotFoundException.class,
            () -> testObj.getItem(Base64.getEncoder().encodeToString(expectedKeyString.getBytes(StandardCharsets.UTF_8))));
        assertEquals("The elementType is: class io.devbench.uibuilder.data.common.dataprovidersupport.TestElement the path is: id",
            propertyConverterNotFoundException.getMessage());
    }

    private void assertEqualsExpressions(List<FilterExpression<?>> expressions) {
        expressions
            .forEach(expression -> {
                assertTrue(expression instanceof ExpressionTypes.Equals);
                ExpressionTypes.Equals<?> equalsExpression = ((ExpressionTypes.Equals<?>) expression);
                switch (equalsExpression.getPath()) {
                    case "id":
                        assertEquals(1, equalsExpression.getValue());
                        break;
                    case "name":
                        assertEquals("testName", equalsExpression.getValue());
                        break;
                    case "subElement.name":
                        assertEquals("subName", equalsExpression.getValue());
                        break;
                    default:
                        fail("Non matching path in expression: " + equalsExpression.getPath());
                }
            });
    }

    private TestElement createTestElement() {
        return TestElement.builder()
            .id(1)
            .name("testName")
            .subElement(TestElement.builder().name("subName").build())
            .build();
    }

    private void mockFilterExpressions() {
        when(dataSource.getFilterExpressionFactory()).thenReturn(expressionFactory);
        when(expressionFactory.create(AndFilterExpression.class)).thenReturn((FilterExpression) new MockAndFE());
        when(expressionFactory.create(ExpressionTypes.Equals.class)).thenReturn((FilterExpression) new MockEquals());
    }

}
