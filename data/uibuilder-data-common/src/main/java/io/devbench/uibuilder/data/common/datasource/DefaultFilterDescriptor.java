/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.datasource;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import java.util.HashMap;
import java.util.Map;

public class DefaultFilterDescriptor {

    static final String ALL_PATH = "";
    static final FilterType STRING_TYPE_DEFAULT_PROPERTY = new FilterType("like", "contains");
    static final FilterType ALL_TYPE_DEFAULT_PROPERTY = new FilterType("=", "none");

    private final Map<String, FilterType> pathFilterTypes;

    public DefaultFilterDescriptor() {
        pathFilterTypes = new HashMap<>();
    }

    private FilterType createDefaultFilterType(boolean stringTypeProperty) {
        return stringTypeProperty ? STRING_TYPE_DEFAULT_PROPERTY : ALL_TYPE_DEFAULT_PROPERTY;
    }

    public FilterType getDefaultFilterType(boolean stringTypeProperty) {
        return pathFilterTypes.getOrDefault(ALL_PATH, createDefaultFilterType(stringTypeProperty));
    }

    public FilterType getFilterType(@NotNull String path) {
        return getFilterType(path, false);
    }

    public FilterType getFilterType(@NotNull String path, boolean stringTypeProperty) {
        return pathFilterTypes.getOrDefault(path, getDefaultFilterType(stringTypeProperty));
    }

    public void addFilterType(@NotNull String path, @NotNull String filterTypeName, String filterMode) {
        pathFilterTypes.put(
            StringUtils.isBlank(path) ? ALL_PATH : path,
            new FilterType(filterTypeName,
                StringUtils.isBlank(filterMode) ? "none" : filterMode));
    }

    public void setDefaultFilterType(@NotNull String filterTypeName, String filterMode) {
        addFilterType(ALL_PATH, filterTypeName, filterMode);
    }

    @Getter
    public static class FilterType {
        private final String name;
        private final String mode;

        FilterType(String name, String mode) {
            this.name = name;
            this.mode = mode;
        }
    }

}
