/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverter;
import io.devbench.uibuilder.core.exceptions.PropertyConverterNotFoundException;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadataProvider;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;
import io.devbench.uibuilder.data.common.filter.logicaloperators.AndFilterExpression;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.util.*;

public class OrmKeyMapper<ELEMENT, FILTER_EXPRESSION_TYPE extends FilterExpression<?>> implements KeyMapper<ELEMENT> {

    public static final String DOT = "__DOT__";
    private final CommonDataSource<ELEMENT, ?, FILTER_EXPRESSION_TYPE, ?> dataSource;
    private final BindingMetadataProvider metadataProvider;
    private final List<String> keyPaths;

    public OrmKeyMapper(CommonDataSource<ELEMENT, ?, FILTER_EXPRESSION_TYPE, ?> dataSource, BindingMetadataProvider metadataProvider, List<String> keyPaths) {
        this.dataSource = dataSource;
        this.metadataProvider = metadataProvider;
        this.keyPaths = keyPaths;
    }

    @Override
    public String getKey(ClassMetadata<ELEMENT> itemClassMetadata) {
        JsonObject result = Json.createObject();
        keyPaths
            .forEach(path -> {
                String underscoredPath = path.replaceAll("\\.", DOT);
                String valueString = itemClassMetadata.getPropertyValue(path)
                    .map(value -> metadataProvider.getMetadataForPath(path).getConverter()
                        .convertTo(value))
                    .orElse("");
                result.put(underscoredPath, valueString);
            });
        return Base64.getEncoder().encodeToString(result.toJson().getBytes(StandardCharsets.UTF_8));
    }

    @Override
    @SuppressWarnings("unchecked")
    public ELEMENT getItem(@NotNull JsonObject key) {
        FilterExpressionFactory<FilterExpression<?>> filterExpressionFactory =
            (FilterExpressionFactory<FilterExpression<?>>) dataSource.getFilterExpressionFactory();
        final AndFilterExpression keyFilterExpression = filterExpressionFactory.create(AndFilterExpression.class);

        Arrays.stream(key.keys())
            .map(it -> Pair.of(it.replaceAll(DOT, "."), key.getString(it)))
            .map(this::convertPairToEqualsExpression)
            .forEach(keyFilterExpression::add);

        return dataSource.findElementByKeyFilter((FILTER_EXPRESSION_TYPE) keyFilterExpression);
    }

    @Override
    public ELEMENT getItem(String key) {
        JsonObject keyObject = KeyMapper.decodeKey(key).orElseThrow(IllegalStateException::new);
        return getItem(keyObject);
    }

    private ExpressionTypes.Equals<?> convertPairToEqualsExpression(Pair<String, String> pair) {
        FilterExpressionFactory filterExpressionFactory = dataSource.getFilterExpressionFactory();
        ExpressionTypes.Equals equals = (ExpressionTypes.Equals) filterExpressionFactory.create(ExpressionTypes.Equals.class);
        final PropertyConverter<Object, String> converter = metadataProvider.getMetadataForPath(pair.getKey()).getConverter();
        if (converter == null) {
            throw new PropertyConverterNotFoundException("The elementType is: " + dataSource.getElementType() + " the path is: " + pair.getKey());
        }
        final Object value = converter.convertFrom(pair.getValue());
        equals.setPath(pair.getKey());
        equals.setValue(value);
        return equals;
    }

}
