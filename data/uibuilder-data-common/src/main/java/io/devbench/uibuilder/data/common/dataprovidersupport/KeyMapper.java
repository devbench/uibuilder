/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import org.jetbrains.annotations.NotNull;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;

public interface KeyMapper<ELEMENT> {

    String getKey(ClassMetadata<ELEMENT> itemClassMetadata);

    ELEMENT getItem(@NotNull JsonObject key);

    ELEMENT getItem(String key);

    static Optional<JsonObject> decodeKey(String key) {
        if (key == null) {
            LoggerFactory.getLogger(KeyMapper.class).debug("Key is null");
            return Optional.empty();
        }
        try {
            byte[] decoded = Base64.getDecoder().decode(key.getBytes(StandardCharsets.UTF_8));
            JsonObject keyObject = Json.parse(new String(decoded, StandardCharsets.UTF_8));
            return Optional.ofNullable(keyObject);
        } catch (Exception e) {
            LoggerFactory.getLogger(KeyMapper.class).debug("Not a valid key: {}", key);
            return Optional.empty();
        }
    }

}
