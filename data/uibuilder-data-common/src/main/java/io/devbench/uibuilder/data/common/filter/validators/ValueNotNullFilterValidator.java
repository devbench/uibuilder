/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.filter.validators;

import io.devbench.uibuilder.data.api.filter.FilterExpressionDescriptor;
import io.devbench.uibuilder.data.api.filter.validation.CustomFilterValidator;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationContext;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidator;
import io.devbench.uibuilder.data.common.filter.filterdescriptors.AnyOperandFilterDescriptor;
import io.devbench.uibuilder.data.common.filter.filterdescriptors.BinaryOperandFilterDescriptor;
import io.devbench.uibuilder.i18n.core.I;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@CustomFilterValidator(ValueNotNullFilterValidator.NAME)
public class ValueNotNullFilterValidator implements FilterValidator<FilterExpressionDescriptor> {

    public static final String NAME = "null-value-validator";

    @Override
    public void validate(FilterExpressionDescriptor descriptor, FilterValidationContext context) {
        if (descriptor instanceof BinaryOperandFilterDescriptor) {
            Object value = ((BinaryOperandFilterDescriptor) descriptor).getValue();
            if (value == null) {
                context.error(I.tr("Value is null"));
            }
        } else if (descriptor instanceof AnyOperandFilterDescriptor) {
            Collection<Object> values = ((AnyOperandFilterDescriptor) descriptor).getValues();
            List<String> errors = new ArrayList<>();
            values.forEach(value -> errors.add(value == null ? I.tr("Value is null") : null));
            if (errors.stream().anyMatch(Objects::nonNull)) {
                errors.forEach(context::error);
            }
        }
    }
}
