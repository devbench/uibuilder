/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import com.vaadin.flow.component.Component;
import lombok.extern.slf4j.Slf4j;

import java.lang.ref.ReferenceQueue;
import java.util.Map;

@Slf4j
public class DataProviderEndpointRegistrationCleanUpDaemon extends Thread {

    public static final String THREAD_NAME = "ds-endpoint-manager-ref-cleanup-thread";

    DataProviderEndpointRegistrationCleanUpDaemon(ReferenceQueue<Component> referenceQueue, Map<String, DataProviderEndpointWeakReference> endpointMap) {
        super(() -> {
            while (true) {
                try {
                    DataProviderEndpointWeakReference ref = (DataProviderEndpointWeakReference) referenceQueue.remove();
                    DataProviderEndpointWeakReference reference = endpointMap.remove(ref.getEndpoint().getEndpointId());
                    reference.getEndpoint().close();
                } catch (InterruptedException e) {
                    log.error("CleanUp Thread interrupted", e);
                    Thread.currentThread().interrupt();
                }
            }
        });
        setName(THREAD_NAME);
        setPriority(Thread.MAX_PRIORITY);
        setDaemon(true);
    }

}
