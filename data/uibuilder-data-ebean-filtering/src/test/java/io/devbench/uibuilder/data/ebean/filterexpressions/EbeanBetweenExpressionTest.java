/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.ebean.filterexpressions;

import io.devbench.uibuilder.data.api.exceptions.FilterException;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.ebean.Query;
import io.ebeaninternal.server.expression.DefaultExpressionFactory;
import io.ebeaninternal.server.expression.DefaultExpressionList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EbeanBetweenExpressionTest {
    @Mock
    Query query;
    private DefaultExpressionList defaultExpressionList;
    private EbeanBetweenExpression testObject;

    @BeforeEach
    public void setUp() {
        when(query.getExpressionFactory()).thenReturn(new DefaultExpressionFactory(true, true));
        defaultExpressionList = new DefaultExpressionList(query);
        testObject = getNew();
    }

    @Test
    public void shouldThrowExceptionWhenParameterSizeAreNot2() {
        testObject.setValues(Arrays.asList(10));
        assertEquals(1, testObject.getValues().size());
        assertThrows(FilterException.class, testObject::toPredicate);
        testObject.setValues(Arrays.asList(10, 20, 30));
        assertEquals(3, testObject.getValues().size());
        assertThrows(FilterException.class, testObject::toPredicate);
    }

    @Test
    public void predicateShouldNotNullWhenSizeIsTwo() {
        testObject.setValues(Arrays.asList(10, 20));
        assertEquals(2, testObject.getValues().size());
        final DefaultExpressionList<?> actual = (DefaultExpressionList) testObject.toPredicate();
        assertNotNull(actual);
    }

    private EbeanBetweenExpression getNew() {
        final EbeanBetweenExpression ebeanBetweenExpression = new EbeanBetweenExpression();
        ebeanBetweenExpression.setContainerExpressionList(defaultExpressionList);
        return ebeanBetweenExpression;
    }

}
