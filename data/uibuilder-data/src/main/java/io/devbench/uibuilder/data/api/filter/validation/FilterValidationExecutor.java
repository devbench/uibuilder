/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.filter.validation;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.singleton.SingletonManager;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionDescriptor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class FilterValidationExecutor {

    private FilterValidationExecutor() {
    }

    @SuppressWarnings("unchecked")
    public <EXPRESSION extends FilterExpression> FilterValidationReport validate(@NotNull String validatorName, @NotNull EXPRESSION expression) {
        Objects.requireNonNull(validatorName);
        Objects.requireNonNull(expression);
        FilterExpressionDescriptor immutableDescriptor = expression.getImmutableDescriptor();

        List<FilterValidator<FilterExpressionDescriptor>> validators = MemberScanner.getInstance().findClassesByAnnotation(CustomFilterValidator.class).stream()
            .filter(clazz -> clazz.getDeclaredAnnotation(CustomFilterValidator.class).value().equals(validatorName))
            .filter(FilterValidator.class::isAssignableFrom)
            .map(clazz -> ((Class<? extends FilterValidator>) clazz))
            .map(this::crateFilterValidator)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

        if (validators.isEmpty()) {
            throw new FilterValidatorNotFoundException(validatorName);
        }

        Optional<FilterValidator<FilterExpressionDescriptor>> optionalFilterValidator = validators.stream()
            .filter(validator -> checkValidatorHasCorrectGenericType(validator, immutableDescriptor))
            .findFirst();

        FilterValidationContext context = new FilterValidationContext();
        optionalFilterValidator.ifPresent(validator -> validator.validate(immutableDescriptor, context));
        return context.getValidationReport();
    }

    private boolean checkValidatorHasCorrectGenericType(FilterValidator<?> validator, FilterExpressionDescriptor immutableDescriptor) {
        Type[] genericInterfaces = validator.getClass().getGenericInterfaces();
        if (genericInterfaces.length > 0) {
            return Arrays.stream(genericInterfaces)
                .filter(ParameterizedType.class::isInstance)
                .map(ParameterizedType.class::cast)
                .filter(parameterizedType -> parameterizedType.getRawType() instanceof Class)
                .filter(parameterizedType -> FilterValidator.class.isAssignableFrom(((Class<?>) parameterizedType.getRawType())))
                .filter(parameterizedType -> parameterizedType.getActualTypeArguments().length == 1)
                .filter(parameterizedType -> parameterizedType.getActualTypeArguments()[0] instanceof Class)
                .map(parameterizedType -> ((Class<?>) parameterizedType.getActualTypeArguments()[0]))
                .anyMatch(declaredDescriptorClass -> declaredDescriptorClass.isAssignableFrom(immutableDescriptor.getClass()));
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private FilterValidator<FilterExpressionDescriptor> crateFilterValidator(Class<? extends FilterValidator> validatorClass) {
        try {
            return (FilterValidator<FilterExpressionDescriptor>) validatorClass.newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassCastException e) {
            log.debug("FilterValidator cannot be instantiated", e);
            return null;
        }
    }


    public static FilterValidationExecutor getInstance() {
        return SingletonManager.getInstanceOf(FilterValidationExecutor.class);
    }

    public static void registerToActiveContext() {
        SingletonManager.registerSingleton(new FilterValidationExecutor());
    }

    public static void registerToContext(Object context) {
        SingletonManager.registerSingletonToContext(context, new FilterValidationExecutor());
    }

}
