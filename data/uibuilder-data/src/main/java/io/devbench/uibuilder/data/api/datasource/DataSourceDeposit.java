/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.datasource;

import io.devbench.uibuilder.api.parse.BindingContext;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.core.session.context.UIContextStored;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * When a data source is defined for a component inside a template, the component will not be instantiated at the parse
 * time, so the collected and created binding context cannot be applied into a datasource object and set for the
 * component which would use the datasource. In this case, the datasource parse interceptor will store the collected
 * binding context into this deposit discriminated by the datasource name. Later, when the template is being
 * instantiated and the created html has been inserted into the current UI, the html fragment will be parsed and the
 * parse interceptors should find the previously collected binding contexts for the appropriate datasource descriptors.
 */
@UIContextStored
public final class DataSourceDeposit implements Serializable {

    private final Map<String, BindingContext> bindingContextMap;

    private DataSourceDeposit() {
        bindingContextMap = new HashMap<>();
    }

    public static DataSourceDeposit getInstance() {
        DataSourceDeposit dataSourceDeposit = UIContext.getContext().get(DataSourceDeposit.class);
        if (dataSourceDeposit == null) {
            synchronized (DataSourceDeposit.class) {
                dataSourceDeposit = UIContext.getContext().get(DataSourceDeposit.class);
                if (dataSourceDeposit == null) {
                    dataSourceDeposit = new DataSourceDeposit();

                    UIContext.getContext().addToActiveUIContext(dataSourceDeposit);
                }
            }
        }
        return dataSourceDeposit;
    }

    public void add(String dataSourceName, BindingContext bindingContext) {
        bindingContextMap.put(dataSourceName, bindingContext);
    }

    public BindingContext get(String dataSourceName) {
        return bindingContextMap.get(dataSourceName);
    }

}
