/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.parse;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.function.Function;

import static io.devbench.uibuilder.data.api.parse.DatePickerParseInterceptor.*;

@Slf4j
@SuppressWarnings("unused")
public class DateTimePickerParseInterceptor implements ParseInterceptor {

    private static final long NANO_SCALE = 1000000000L;

    @Override
    public void intercept(Component component, Element element) {
        if (component instanceof DateTimePicker) {
            DateTimePicker dateTimePicker = (DateTimePicker) component;

            processAttribute(element, "step", Double::parseDouble,
                value -> dateTimePicker.setStep(Duration.ofNanos(Double.valueOf(value * NANO_SCALE).longValue())));

            processAttribute(element, "min", DateTimeFormatter.ISO_DATE_TIME::parse,
                value -> dateTimePicker.setMin(LocalDateTime.from(value)));

            processAttribute(element, "max", DateTimeFormatter.ISO_DATE_TIME::parse,
                value -> dateTimePicker.setMax(LocalDateTime.from(value)));

            processAttribute(element, "date-placeholder", Function.identity(), dateTimePicker::setDatePlaceholder);
            processAttribute(element, "time-placeholder", Function.identity(), dateTimePicker::setTimePlaceholder);
        }
    }

    @Override
    public boolean isInstantiator(Element element) {
        return true;
    }

    @Override
    public Component instantiateComponent() {
        Locale locale = UI.getCurrent().getLocale();
        DateTimePicker dateTimePicker = new DateTimePickerForceBackend();
        dateTimePicker.setDatePickerI18n(createI18n(locale));
        return dateTimePicker;
    }

    @Override
    public boolean isApplicable(Element element) {
        return "vaadin-date-time-picker".equals(element.tagName());
    }

}
