/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.parse;

import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import java.time.LocalDateTime;

/**
 * DateTimePicker with a forced "false" for fromClient to ensure the form modified values to take effect
 */
public class DateTimePickerForceBackend extends DateTimePicker {

    @Override
    protected void setModelValue(LocalDateTime newModelValue, boolean fromClient) {
        super.setModelValue(newModelValue, false);
    }

}
