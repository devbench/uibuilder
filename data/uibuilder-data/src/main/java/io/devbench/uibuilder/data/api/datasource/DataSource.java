/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.datasource;

import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.order.OrderExpression;
import io.devbench.uibuilder.data.api.order.OrderExpressionFactory;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface DataSource<
    RESULT_TYPE,
    ORDER_EXPRESSION_TYPE extends OrderExpression,
    FILTER_EXPRESSION_TYPE extends FilterExpression<?>,
    FETCH_REQUEST_TYPE extends FetchRequest
    > {

    FilterExpressionFactory<FILTER_EXPRESSION_TYPE> getFilterExpressionFactory();

    OrderExpressionFactory<ORDER_EXPRESSION_TYPE> getOrderExpressionFactory();

    void registerFilter(FILTER_EXPRESSION_TYPE expression);

    void registerOrder(ORDER_EXPRESSION_TYPE expression);

    /**
     * @param request the fetch request
     * @return the size of the given page. when the {@code request} is null it returns the size of the whole underlying dataset.
     */
    long fetchSize(@Nullable FETCH_REQUEST_TYPE request);

    boolean hasChildren(FILTER_EXPRESSION_TYPE request);

    /**
     * @param request the fetch request
     * @return the size of the given page. when the {@code request} is null it returns the whole underlying dataset.
     */
    RESULT_TYPE fetchData(@Nullable FETCH_REQUEST_TYPE request);

    Collection<String> getBindings();
}
