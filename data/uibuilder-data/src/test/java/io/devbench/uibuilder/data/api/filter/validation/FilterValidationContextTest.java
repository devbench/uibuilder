/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.filter.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FilterValidationContextTest {

    private FilterValidationContext testObj;

    @BeforeEach
    void setup() {
        testObj = new FilterValidationContext();
    }

    @Test
    @DisplayName("Should add error messages to the reports error list")
    public void should_add_error_messages_to_the_reports_error_list() {
        testObj.error("test message1");
        testObj.error("test message2");

        assertAll(
            () -> assertEquals(2, testObj.getValidationReport().getErrors().size()),
            () -> assertEquals("test message1", testObj.getValidationReport().getErrors().get(0)),
            () -> assertEquals("test message2", testObj.getValidationReport().getErrors().get(1))
        );
    }

    @Test
    @DisplayName("Should add warn messages to the reports warnings list")
    public void should_add_warn_messages_to_the_reports_warnings_list() {
        testObj.warn("test message1");
        testObj.warn("test message2");

        assertAll(
            () -> assertEquals(2, testObj.getValidationReport().getWarnings().size()),
            () -> assertEquals("test message1", testObj.getValidationReport().getWarnings().get(0)),
            () -> assertEquals("test message2", testObj.getValidationReport().getWarnings().get(1))
        );
    }

}
