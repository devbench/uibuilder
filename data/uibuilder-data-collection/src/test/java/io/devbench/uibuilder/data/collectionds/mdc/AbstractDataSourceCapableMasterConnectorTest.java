/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.mdc;

import io.devbench.uibuilder.api.exceptions.MasterConnectorNotDirectlyModifiableException;
import io.devbench.uibuilder.api.exceptions.MasterConnectorUnsupportedOperationException;
import io.devbench.uibuilder.data.collectionds.CollectionDataSource;
import io.devbench.uibuilder.data.collectionds.datasource.component.AbstractDataSourceComponent;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AbstractDataSourceCapableMasterConnectorTest {

    @Test
    public void should_proxy_the_trivial_master_component_methods() {
        TestConnector testObj = new TestConnector();
        AbstractDataSourceComponent<Object> masterComponent = mock(AbstractDataSourceComponent.class);
        testObj.connect(masterComponent);


        doReturn(mock(CollectionDataSource.class)).when(masterComponent).getDataSource();
        assertTrue(testObj.isDirectModifiable());
        verify(masterComponent, times(1)).getDataSource();
        reset(masterComponent);

        doReturn(mock(CommonDataSource.class)).when(masterComponent).getDataSource();
        assertFalse(testObj.isDirectModifiable());
        verify(masterComponent, times(1)).getDataSource();
        reset(masterComponent);

        testObj.refresh();
        verify(masterComponent, times(1)).refresh();
        reset(masterComponent);

        assertThrows(MasterConnectorUnsupportedOperationException.class, () -> testObj.refresh(new Object()));
        reset(masterComponent);

        testObj.setEnabled(true);
        verify(masterComponent, times(1)).setEnabled(true);
        reset(masterComponent);

        boolean randomBoolean = new Random().nextBoolean();
        when(masterComponent.isEnabled()).thenReturn(randomBoolean);
        assertEquals(randomBoolean, testObj.isEnabled());
    }

    @Test
    public void item_modifications_should_work_on_collection_datasource() {
        TestConnector testObj = new TestConnector();
        AbstractDataSourceComponent<Object> masterComponent = mock(AbstractDataSourceComponent.class);
        CollectionDataSource collectionDataSource = mock(CollectionDataSource.class, RETURNS_DEEP_STUBS);
        testObj.connect(masterComponent);
        doReturn(collectionDataSource).when(masterComponent).getDataSource();

        Object item = new Object();
        testObj.addItem(item);
        verify(collectionDataSource.getItems()).add(item);
        reset(collectionDataSource);

        item = new Object();
        testObj.removeItem(item);
        verify(collectionDataSource.getItems()).remove(item);
    }

    @Test
    public void item_modifications_should_throw_exception_on_non_collection_datasource() {
        TestConnector testObj = new TestConnector();
        AbstractDataSourceComponent<Object> masterComponent = mock(AbstractDataSourceComponent.class);
        CommonDataSource collectionDataSource = mock(CommonDataSource.class);
        testObj.connect(masterComponent);
        doReturn(collectionDataSource).when(masterComponent).getDataSource();

        assertThrows(MasterConnectorNotDirectlyModifiableException.class, () -> testObj.addItem(new Object()));
        assertThrows(MasterConnectorNotDirectlyModifiableException.class, () -> testObj.removeItem(new Object()));
    }

    private static class TestConnector extends AbstractDataSourceCapableMasterConnector<AbstractDataSourceComponent<Object>, Object> {

        protected TestConnector() {
            super((Class) AbstractDataSourceComponent.class);
        }

        @Override
        public void disconnect() {

        }

        @Override
        public Collection<Object> getSelectedItems() {
            return null;
        }

        @Override
        public void setSelectedItems(Collection<Object> items) {

        }
    }

}
