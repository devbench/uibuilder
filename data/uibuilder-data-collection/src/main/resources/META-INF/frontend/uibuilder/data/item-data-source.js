import { PolymerElement } from '@polymer/polymer/polymer-element.js';

export class ItemDataSource extends PolymerElement {
    static get is() {
        return "item-data-source";
    }

    static get properties() {
        return {
            name: String,
            items: String
        };
    }
}

customElements.define(ItemDataSource.is, ItemDataSource);
