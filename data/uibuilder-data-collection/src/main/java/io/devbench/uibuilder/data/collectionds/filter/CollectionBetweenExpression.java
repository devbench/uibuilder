/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.filter;

import io.devbench.uibuilder.data.api.exceptions.FilterException;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;

import java.util.Iterator;
import java.util.function.Predicate;

public class CollectionBetweenExpression<T>
    extends ExpressionTypes.Between<Predicate<T>>
    implements CollectionPathFilterExpression<T> {

    @Override
    public Predicate<T> toPredicate() {
        final int size = getValues().size();
        if (size == 2) {
            final Iterator<Object> iterator = getValues().iterator();
            Comparable lower = (Comparable) iterator.next();
            Comparable upper = (Comparable) iterator.next();
            return createBetweenPredicate(lower, upper);
        } else {
            throw new FilterException(
                "The Between FilterExpression requires exactly 2 parameters, but got " + size
                    + ", the exact values were: " + getValues());
        }
    }

    private Predicate<T> createBetweenPredicate(Comparable lower, Comparable upper) {
        return item -> valueBetween((Comparable) getPropertyValue(item), lower, upper);
    }

    @SuppressWarnings("unchecked")
    private boolean valueBetween(Comparable value, Comparable lower, Comparable upper) {
        if (value == null) {
            return false;
        }

        return value.compareTo(lower) >= 0 && value.compareTo(upper) <= 0;
    }
}
