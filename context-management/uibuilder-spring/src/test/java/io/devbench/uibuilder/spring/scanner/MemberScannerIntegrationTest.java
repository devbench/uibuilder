/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.scanner;

import com.testpkg.annotations.TestAnnotation;
import com.testpkg.classes.*;
import com.testpkg.interfaces.AnnotatedInterface;
import com.testpkg.interfaces.SubInterface;
import com.testpkg.interfaces.SuperInterface;
import io.devbench.uibuilder.annotations.EnableUIBuilder;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ContextConfiguration
@ComponentScan("io.devbench.uibuilder")
@EnableUIBuilder("com.testpkg")
@ExtendWith(SpringExtension.class)
public class MemberScannerIntegrationTest {

    private MemberScanner testObj;

    @BeforeEach
    void setup() {
        testObj = MemberScanner.getInstance();
    }

    @Test
    @DisplayName("Should find real class annoteted with custom annotation")
    public void should_find_real_class_annoteted_with_custom_annotation() {
        Set<Class<?>> annotatedClasses = testObj.findClassesByAnnotation(TestAnnotation.class);

        assertAll(
            () -> assertEquals(1, annotatedClasses.size()),
            () -> assertEquals(AnnotatedClass.class, annotatedClasses.iterator().next())
        );
    }

    @Test
    @DisplayName("Should find real classes annotated with custom annotation")
    public void should_find_real_classes_annotated_with_custom_annotation() {
        Set<Class<?>> annotatedClassesAndInterfaces = testObj.findClassesOrInterfacesByAnnotation(TestAnnotation.class);

        assertAll(
            () -> assertEquals(2, annotatedClassesAndInterfaces.size()),
            () -> assertTrue(annotatedClassesAndInterfaces.contains(AnnotatedClass.class)),
            () -> assertTrue(annotatedClassesAndInterfaces.contains(AnnotatedInterface.class))
        );
    }

    @Test
    @DisplayName("Should find real sub classes of superclass")
    public void should_find_real_sub_classes_of_superclass() {
        Set<Class<? extends SuperClass>> classes = testObj.findClassesBySuperType(SuperClass.class);

        assertAll(
            () -> assertEquals(1, classes.size()),
            () -> assertEquals(SubClass.class, classes.iterator().next())
        );
    }

    @Test
    @DisplayName("Should find real sub interfaces, and implementing classes of superinterface")
    public void should_find_real_sub_interfaces_and_implementing_classes_of_superinterface() {
        Set<Class<? extends SuperInterface>> classes = testObj.findClassesOrInterfacesBySuperType(SuperInterface.class);

        assertAll(
            () -> assertEquals(3, classes.size()),
            () -> assertTrue(classes.contains(SubInterface.class)),
            () -> assertTrue(classes.contains(SubInterfaceImpl.class)),
            () -> assertTrue(classes.contains(SuperInterface.class))
        );
    }

    @Test
    @DisplayName("Should find real instances by interface")
    void test_should_find_real_instances_by_interface() {
        Set<SuperInterface> instances = testObj.findInstancesBySuperType(SuperInterface.class);

        assertAll(
            () -> assertNotNull(instances),
            () -> assertEquals(1, instances.size()),
            () -> assertEquals(SubInterfaceImpl.class, instances.iterator().next().getClass())
        );
    }

    @Test
    @DisplayName("Should not return noninstantiable instances")
    void test_should_not_return_noninstantiable_instances() {
        Set<NoninstantiableClass> instances = testObj.findInstancesBySuperType(NoninstantiableClass.class);

        assertAll(
            () -> assertNotNull(instances),
            () -> assertTrue(instances.isEmpty(), "Should not return any instance")
        );
    }
}
