/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.page;

import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BeanStoreTest {

    @Mock
    private VaadinSession vaadinSession;

    @InjectMocks
    private BeanStore<String> testObj;

    @Test
    public void should_return_the_same_object_for_the_same_id_multiple_times() {
        Object o = testObj.get("obj-01", Object::new);
        assertEquals(o, testObj.get("obj-01", Object::new));
        assertEquals(o, testObj.get("obj-01", Object::new));
        assertNotEquals(o, testObj.get("obj-02", Object::new));
    }

    @Test
    public void should_call_destruction_callback_for_bean_if_registered() {
        Runnable obj01Callback = mock(Runnable.class);

        testObj.get("obj-01", Object::new);
        testObj.get("obj-02", Object::new);
        testObj.registerDestructionCallback("obj-01", obj01Callback);
        testObj.destroyBeans(i -> true);

        verify(obj01Callback, times(1)).run();
    }
}
