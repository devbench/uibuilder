/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean;

import com.vaadin.flow.component.button.Button;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import lombok.Getter;
import org.springframework.context.annotation.Scope;

import javax.inject.Provider;


@Scope("singleton")
@ControllerBean("testBean")
public class TestControllerBean implements BaseTestControllerBean {

    @Getter
    private Object item;

    @UIComponent("testComponent")
    private Provider<Button> button;

    @UIEventHandler("testEventHandlerWithItem")
    public void testEventHandlerWithItem(@Item Object item) {
        this.item = item;
    }

    public Button getButton() {
        return button.get();
    }

}
