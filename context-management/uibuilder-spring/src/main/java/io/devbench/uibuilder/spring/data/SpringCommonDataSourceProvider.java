/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.data;

import io.devbench.uibuilder.api.parse.BindingContext;
import io.devbench.uibuilder.data.api.annotations.TargetDataSource;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceProvider;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public abstract class SpringCommonDataSourceProvider<DATA_SOURCE extends CommonDataSource<?, ?, ?, ?>, REPOSITORY_TYPE extends PagingAndSortingRepository>
    extends SpringDataSourceProvider<DATA_SOURCE, CommonDataSourceSelector>
    implements CommonDataSourceProvider<DATA_SOURCE> {

    protected final ApplicationContext applicationContext;

    protected Map<String, BindingContext> registeredBindingContexts;

    @Getter
    protected Map<String, Pair<TargetDataSource, Class<? extends REPOSITORY_TYPE>>> dataSourceRepositories;

    protected Map<String, String> idToDataSourceName;

    public SpringCommonDataSourceProvider(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        this.registeredBindingContexts = new HashMap<>();
        this.idToDataSourceName = new HashMap<>();
    }

    @Override
    public final void registerBindingContextForDataSource(BindingContext bindingContext, @Nullable CommonDataSourceSelector dataSourceSelector) {
        idToDataSourceName.put(bindingContext.getDataSourceId(), bindingContext.getDataSourceName());
        if (!bindingContext.getBindings().isEmpty()) {
            final String bindingsKey = createBindingsKey(bindingContext.getDataSourceId(), getOptionalDefaultQueryName(dataSourceSelector));
            registeredBindingContexts.put(bindingsKey, bindingContext);
        }
    }

    @Nullable
    @Override
    public final BindingContext getBindingContextForName(String dataSourceName, @Nullable CommonDataSourceSelector dataSourceSelector) {
        return registeredBindingContexts.get(createBindingsKey(dataSourceName, getOptionalDefaultQueryName(dataSourceSelector)));
    }

    protected final Optional<Pair<TargetDataSource, Class<? extends REPOSITORY_TYPE>>> findDataSourceNameById(String dataSourceId) {
        return Optional.ofNullable(getDataSourceRepositories().get(idToDataSourceName.get(dataSourceId)));
    }

    public Optional<Class<?>> tryToFindEntityClassByRepositoryClass(Class<? extends PagingAndSortingRepository<?, ?>> repoClass) {
        return Arrays.stream(repoClass.getGenericInterfaces())
            .filter(ParameterizedType.class::isInstance)
            .map(ParameterizedType.class::cast)
            .map(type -> type.getActualTypeArguments()[0])
            .filter(Class.class::isInstance)
            .<Class<?>>map(entityType -> (Class<?>) entityType)
            .findAny();
    }

}
