/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.data;

import io.devbench.uibuilder.data.api.datasource.DataSource;
import io.devbench.uibuilder.data.api.datasource.DataSourceProvider;
import io.devbench.uibuilder.data.api.datasource.DataSourceSelector;
import lombok.AccessLevel;
import lombok.Getter;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

public abstract class SpringDataSourceProvider<DATA_SOURCE extends DataSource<?, ?, ?, ?>, DATA_SOURCE_SELECTOR extends DataSourceSelector>
    implements DataSourceProvider<DATA_SOURCE, DATA_SOURCE_SELECTOR> {

    @Getter(AccessLevel.PROTECTED)
    private Map<Class<? extends Annotation>, Set<Class<?>>> annotatedClasses;

    public abstract Set<Class<? extends Annotation>> getInterestedAnnotationTypes();

    public void initialize(Map<Class<? extends Annotation>, Set<Class<?>>> annotatedClasses) {
        if (this.annotatedClasses == null) {
            this.annotatedClasses = Collections.unmodifiableMap(annotatedClasses);
            this.lateInit();
        }
    }

    protected abstract void lateInit();
}
