/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.configuration.staticcontent;

import io.devbench.uibuilder.annotations.StaticContent;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;
import java.util.Objects;

public class StaticContentSpringConfiguration implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry registry) {
        String annotationName = StaticContent.class.getName();
        if (annotationMetadata.hasAnnotation(annotationName)) {
            Map<String, Object> annotationAttributes = Objects.requireNonNull(annotationMetadata.getAnnotationAttributes(annotationName));
            String[] path = (String[]) annotationAttributes.get("lookupPaths");
            String url = (String) annotationAttributes.get("url");

            GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
            beanDefinition.setBeanClass(StaticContentDescription.class);
            beanDefinition.getConstructorArgumentValues().addIndexedArgumentValue(0, url);
            beanDefinition.getConstructorArgumentValues().addIndexedArgumentValue(1, path);
            beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
            registry.registerBeanDefinition(annotationName, beanDefinition);
        }
    }
}
