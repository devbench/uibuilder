/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.configuration.basepackages;

import io.devbench.uibuilder.annotations.EnableUIBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UIBuilderScanPackagesRegistrar implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry registry) {
        String annotationName = getAnnotationName();
        String[] basePackages;
        if (annotationMetadata.hasAnnotation(annotationName)) {
            basePackages = (String[]) annotationMetadata.getAnnotationAttributes(annotationName).get("value");
        } else {
            basePackages = new String[]{""};
        }
        List<String> packagesFromAnnotation = new ArrayList<>(Arrays.asList(basePackages));
        packagesFromAnnotation.add(0, getUIBuilderBasePackageToScan());
        basePackages = packagesFromAnnotation.toArray(new String[]{});
        registerScanPackagesBean(basePackages, registry);
    }

    private String getUIBuilderBasePackageToScan() {
        return "io.devbench.uibuilder";
    }

    private void registerScanPackagesBean(String[] basePackages, BeanDefinitionRegistry registry) {
        GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
        beanDefinition.setBeanClass(UIBuilderScanPackages.class);
        beanDefinition.getConstructorArgumentValues().addIndexedArgumentValue(0, basePackages);
        beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
        registry.registerBeanDefinition(getScanPackagesBeanName(), beanDefinition);
    }

    private String getAnnotationName() {
        return EnableUIBuilder.class.getName();
    }

    private String getScanPackagesBeanName() {
        return UIBuilderScanPackages.class.getSimpleName();
    }

}
