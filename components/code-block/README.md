# UIBuilder code block component

This is a client side component, which can display a code block with a copy button.
The code block is syntax highlighted by the Prism.

Supported languages:

 * **xml** - (default)
 * **html**
 * **javascript**
 * **java**
 
The code block has to be in a `<scipt>` tag with a required type attribute with a
value that cannot be `text/javascript`.

Example:

```html
<uibuilder-code-block>
    <script type="text/code">
        <vaadin-button id="window_open" theme="primary">Click me!</vaadin-button>
        <uibuilder-window id="example_window_001" header-text="Hi, I'm a window"
                          width="530px" height="350px" resize="none" on-close="windowController::windowOnClose" closeable>
            <div>This is the body of the Window-Component</div>
        </uibuilder-window>
        <script>
            const button = document.querySelector('#window_open');
            const exampleWindow = document.querySelector('#example_window_001');
            button.addEventListener('click', () => exampleWindow.opened = true);
        <//script>
    </script>
</uibuilder-code-block>
```

If it is required to use the `<script>` tag, the closing tag has to be used as `<//script>` which
will be replaced with the correct `</script>`.

Also, the `<s-script>` and `</s-script>` tags are replaced to the `<script>` and `</script>` tags,
and this version doesn't confuse IDEs.

Another examples:

```html
<uibuilder-code-block>
    <script type="text/code">

        <menu-bar>
            <menu-item id="orderManagement" name="Order management">
                <menu-item id="listOrders" name="List orders"></menu-item>
                <menu-item id="createNewOrder" name="Create new Order"></menu-item>
            </menu-item>
            <menu-item id="shipmentManagement" name="Shipments">
                <menu-item id="scheduleShipment" name="Schedule a shipment"></menu-item>
                <menu-item id="cancelShipment" name="Cancel Shipment" disabled="{{userPermissions}}"></menu-item>
            </menu-item>
            <menu-item id="administration" name="Administration">
                <menu-item id="adminPermissions" name="Permissions">
                    <menu-item
                        id="getAdminPermissions"
                        name="Get Admin Permissions"
                        on-activate="getAdminPermissions"
                        disabled="{{adminPermissions}}"></menu-item>
                    <menu-item
                        id="getUserPermissions"
                        name="Get User Permissions"
                        on-activate="getUserPermissions"
                        disabled="{{userPermissions}}"></menu-item>
                </menu-item>
            </menu-item>
        </menu-bar>
        <s-script>
            var domBind = document.querySelector('uibuilder-dom-bind');
            domBind.userPermissions = true;
            domBind.adminPermissions = false;
            domBind.getAdminPermissions = () => {
                domBind.userPermissions = false;
                domBind.adminPermissions = true;
            };

            domBind.getUserPermissions = () => {
                domBind.userPermissions = true;
                domBind.adminPermissions = false;
            };
        </s-script>
    </script>
</uibuilder-code-block>
```

```html
<uibuilder-code-block lang="java">
    <script type="text/code">

        @Scope("session")
        @ControllerBean("windowController")
        public class WindowControllerBean {

            @UIEventHandler("windowOnClose")
            public void windowOnClose(@NotNull @UIComponent("notification") Notification notification) {
                notification.setDuration(1000);
                notification.open();
            }

        }

    </script>
</uibuilder-code-block>
```
