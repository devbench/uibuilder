/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.interceptor;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailControllerEventHandlerMethod;
import org.jsoup.nodes.Element;

import java.util.Optional;

import static io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailControllerEventHandlerMethod.*;
import static io.devbench.uibuilder.core.utils.ElementCollector.*;

public class MasterDetailControllerParseInterceptor implements ParseInterceptor {

    private static final String MASTER = "master";
    private static final String DETAIL = "detail";
    private static final String DETAIL_PROPERTY = "detail-property";
    private static final String MASTER_CONNECTOR_SELECTOR = "master-connector-selector";

    @Override
    public boolean isApplicable(Element element) {
        return UIBuilderMasterDetailController.TAG_NAME.equals(element.tagName());
    }

    @Override
    public void intercept(Component component, Element element) {
        UIBuilderMasterDetailController<?> controller = (UIBuilderMasterDetailController<?>) component;
        if (!controller.getId().isPresent() && element.hasAttr(ID)) {
            controller.setId(element.attr(ID));
        }
        controller.registerIntoGenericItemControllerBean();
        controller.setRawElement(element);
        controller.setMasterId(element.attr(MASTER));
        controller.setDetailIds(element.attr(DETAIL).split(","));
        controller.setParentMdcId(findParentMdcId(element));
        if (element.hasAttr(DETAIL_PROPERTY)) {
            controller.setDetailProperty(element.attr(DETAIL_PROPERTY));
        }
        if (element.hasAttr(MASTER_CONNECTOR_SELECTOR)) {
            controller.setMasterConnectorSelector(element.attr(MASTER_CONNECTOR_SELECTOR));
        }

        parseMethod(controller, element, ITEM_SELECTED, "on-item-selected");
        parseMethod(controller, element, EDIT, "on-edit");
        parseMethod(controller, element, CREATE, "on-create");
        parseMethod(controller, element, DELETE, "on-delete");
        parseMethod(controller, element, REFRESH, "on-refresh");
        parseMethod(controller, element, SAVE, "on-save");
        parseMethod(controller, element, CANCEL, "on-cancel");
        parseMethod(controller, element, RESET, "on-reset");
    }

    private Optional<Element> getDirectChildMdc(Element element) {
        return element.children().stream()
            .filter(child -> UIBuilderMasterDetailController.TAG_NAME.equals(child.tagName()))
            .findFirst();
    }

    private String findParentMdcId(Element element) {
        return element.parents().stream()
            .map(this::getDirectChildMdc)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .filter(parent -> parent != element)
            .filter(parent -> parent.hasAttr(ID))
            .map(parent -> parent.attr(ID))
            .findFirst()
            .orElse(null);
    }

    @Override
    public boolean isInstantiator(Element element) {
        return true;
    }

    @Override
    public Component instantiateComponent() {
        com.vaadin.flow.dom.Element vaadinElement = new com.vaadin.flow.dom.Element(UIBuilderMasterDetailController.TAG_NAME);
        return Component.from(vaadinElement, UIBuilderMasterDetailController.class);
    }

    private void parseMethod(UIBuilderMasterDetailController<?> controller, Element element,
                             UIBuilderMasterDetailControllerEventHandlerMethod method, String attribute) {
        if (element.hasAttr(attribute)) {
            String methodName = element.attr(attribute);
            controller.registerEventHandlerMethodName(method, methodName);
            element.removeAttr(attribute);
        }
    }
}
