/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail;

import io.devbench.uibuilder.api.controllerbean.uieventhandler.EventQualifier;
import io.devbench.uibuilder.api.exceptions.ComponentInternalException;
import io.devbench.uibuilder.components.masterdetail.event.*;

import java.lang.reflect.Constructor;

public enum UIBuilderMasterDetailControllerEventHandlerMethod {
    ITEM_SELECTED(ItemSelectedEvent.class),
    EDIT(EditEvent.class),
    CREATE(CreateEvent.class),
    DELETE(DeleteEvent.class),
    REFRESH(RefreshEvent.class),
    SAVE(SaveEvent.class),
    CANCEL(CancelEvent.class),
    RESET(ResetEvent.class);

    private Class<? extends QualifierAwareComponentEvent> componentEventClass;

    UIBuilderMasterDetailControllerEventHandlerMethod(Class<? extends QualifierAwareComponentEvent> componentEventClass) {
        this.componentEventClass = componentEventClass;
    }

    public Class<? extends QualifierAwareComponentEvent> componentEventClass() {
        return componentEventClass;
    }

    @SuppressWarnings("unchecked")
    public <E extends QualifierAwareComponentEvent> E createEvent(UIBuilderMasterDetailController controller, EventQualifier eventQualifier) {
        Class<? extends QualifierAwareComponentEvent> eventClass = componentEventClass();
        try {
            Constructor<? extends QualifierAwareComponentEvent> constructor =
                eventClass.getConstructor(UIBuilderMasterDetailController.class, EventQualifier[].class);
            EventQualifier[] eventQualifiers = new EventQualifier[1];
            eventQualifiers[0] = eventQualifier;

            return (E) constructor.newInstance(controller, eventQualifiers);
        } catch (Exception e) {
            throw new ComponentInternalException("Could not create event: " + eventClass.getName(), e);
        }
    }
}
