/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';

class UibuilderMasterDetailController extends PolymerElement {

    static get template() {
        return html`<slot></slot>`;
    }

    static get is() {
        return 'master-detail-controller'
    }

    static get properties() {
        return {
            disableMasterEnabledControl: {
                type: Boolean,
                notify: true,
                value: false
            },
            master: {
                type: String,
                value: ''
            },
            detail: {
                type: String,
                value: ''
            },
            confirmDelete: {
                type: Boolean,
                value: false
            },
            confirmReset: {
                type: Boolean,
                value: false
            },
            itemSupplier: {
                type: String
            },
            syncDetailsFormControls: {
                type: Boolean,
                value: false
            }
        };
    }

    constructor() {
        super();
        this._ackSave = null;
        this.detailComponents = [];
        this.details = [];
    }

    _extractDetailsFromDetail() {
        if (this.detail) {
            this.details = this.detail.split(',').map(detail => detail.trim());
        }
    }

    _uibuilderReady() {
        this._extractDetailsFromDetail();
        if (this.details.length > 0) {
            this.detailComponents = this.details.map(detail => this.getRootNode().getElementById(detail));
            this.detailComponents.forEach(detailComponent => {
                detailComponent.interruptibleReset = true;
                detailComponent.interruptibleCancel = true;
                detailComponent.interruptibleSave = true;
                detailComponent.saveAckRequired = true;
                detailComponent.addEventListener("reset", evt => {
                    if (this._isDirectParentManagedCrudPanel()) {
                        this._resetNestedMdc();
                    }
                    this._fireMdcEvent('reset', evt);
                });
                detailComponent.addEventListener("cancel", evt => {
                    this._resetNestedMdc();
                    this._fireMdcEvent('cancel', evt);
                });
                detailComponent.addEventListener("save", evt => {
                    this._dispatchSaveEventByNestedMdc();
                    if (evt && evt.detail && evt.detail.ackSave) {
                        this._ackSave = evt.detail.ackSave;
                    }
                    this._fireMdcEvent('save', evt);
                });

                if (this.syncDetailsFormControls) {
                    this._setupFormControlsSynchronization(detailComponent);
                }
            });
        }

        if (this.master) {
            this.masterComponent = this.getRootNode().getElementById(this.master);
        }

        this._initMdcEventListeners();

        this.dispatchEvent(new CustomEvent("ready"));
    }

    _getDetailInternalForm(detailComponent) {
        return detailComponent.$.__internalForm;
    }

    _setupFormControlsSynchronization(detailComponent) {
        ['SAVE', 'RESET', 'CANCEL'].forEach(formControlType => {
            let formControl = this._getDetailInternalForm(detailComponent).formControls[formControlType];
            if (formControl != null) {
                this.detailComponents.forEach(anotherDetailComponent => {
                    if (anotherDetailComponent !== detailComponent) {
                        let anotherFormControl = this._getDetailInternalForm(anotherDetailComponent).formControls[formControlType];
                        if (anotherFormControl) {
                            anotherFormControl.nodes.forEach(node => {
                                formControl.addNode(node, null, true);
                            });
                        }
                    }
                });
            }
        });

        detailComponent.addEventListener("reset", () => {
            this.detailComponents.forEach(anotherDetailComponent => {
                if (anotherDetailComponent !== detailComponent) {
                    this._getDetailInternalForm(anotherDetailComponent).reset();
                }
            });
        });
    }

    _isDirectParentManagedCrudPanel() {
        let currentElement = this.parentElement;
        while (currentElement && 'crud-panel' !== currentElement.tagName.toLowerCase()) {
            currentElement = currentElement.parentElement;
        }
        return currentElement && currentElement.hasAttribute('manage-nested');
    }

    _findManagerCrudPanel() {
        return this.parentElement ? this.parentElement.closest('crud-panel[manage-nested]') : null;
    }

    _dispatchSaveEventByNestedMdc() {
        if (this._isDirectParentManagedCrudPanel()) {
            this.detailComponents.forEach(detailComponent => {
                if (detailComponent) {
                    let innerMdc = detailComponent.querySelector('master-detail-controller');
                    if (innerMdc) {
                        innerMdc.detailComponents.forEach(innerDetailComponent => {
                            if (innerDetailComponent && this._getDetailInternalForm(innerDetailComponent)) {
                                this._getDetailInternalForm(innerDetailComponent).save();
                            } else {
                                innerMdc.dispatchEvent(new CustomEvent('save'));
                            }
                        });
                    }
                } else {
                    console.warn('Cannot find nested master-detail-controller to dispatch save event')
                }
            });
        }
    }

    _findDirectMdc(crudPanel) {
        return document.evaluate('master-detail-controller', crudPanel).iterateNext();
    }

    _markDirtyManagerMdc() {
        const managerCrudPanel = this._findManagerCrudPanel();
        if (managerCrudPanel) {
            const managerMdc = this._findDirectMdc(managerCrudPanel);
            if (managerMdc) {
                managerMdc.detailMarkDirty();
            }
        }
    }

    _resetNestedMdc() {
        this.detailComponents.forEach(detailComponent => {
            if (detailComponent) {
                const managedMdc = detailComponent.querySelector('master-detail-controller');
                if (managedMdc) {
                    managedMdc._onCancel();
                }
            }
        });
    }

    _initMdcEventListeners() {
        this.mdcEventListeners = {};
        for (const child of this.children) {
            const event = child.getAttribute('observed-event');
            if (event)
                this.mdcEventListeners[event] = child;
        }
    }

    _onAttached() {
        this.dispatchEvent(new CustomEvent("attached"));
    }

    _fireSelectedItemChanged(hasSelectedItem, created = false) {
        this.dispatchEvent(new CustomEvent('selected-item-changed', {
            detail: {
                hasSelectedItem: hasSelectedItem,
                created: created
            }
        }));
    }

    _onMasterEnabledChange(enabled) {
        this.dispatchEvent(new CustomEvent('master-enabled-changed', {
            detail: {
                enabled: enabled
            }
        }));
    }

    _onSave() {
        if (this._ackSave) {
            this._ackSave();
            this._ackSave = null;
        }
    }

    _onCancel() {
        this.dispatchEvent(new CustomEvent('cancel'));
    }

    _onReset() {
        this.dispatchEvent(new CustomEvent('reset'));
    }

    create() {
        this.dispatchEvent(new CustomEvent('create'));
        this._markDirtyManagerMdc();
    }

    refresh() {
        this.dispatchEvent(new CustomEvent('refresh'));
    }

    edit() {
        this.dispatchEvent(new CustomEvent('edit'));
    }

    delete() {
        this._fireMdcEvent('delete');
    }

    _onError(errorObj) {
        this.showError(errorObj.message);
    }

    _fireMdcEvent(eventName, sourceEvent) {
        const component = this.mdcEventListeners[eventName];
        if (component && (!sourceEvent || !sourceEvent.detail || !sourceEvent.detail.internal)) {
            component[component.getAttribute('listener') || 'open'](
                () => this._proceedProcessing(eventName, sourceEvent)
            );
        } else {
            this._proceedProcessing(eventName, sourceEvent);
        }
    }

    _proceedProcessing(eventName, sourceEvent) {
        if (sourceEvent && sourceEvent.detail && sourceEvent.detail.proceedProcessing)
            sourceEvent.detail.proceedProcessing();
        this.dispatchEvent(new CustomEvent(eventName));
    }

    showError(error) {
        const component = this.mdcEventListeners['error'];
        if (component) {
            component[component.getAttribute('listener') || 'handleError'](error);
        } else {
            this.dispatchEvent(new CustomEvent('handle-error', {detail: {errorMessage: error}}));
        }
    }

    detailMarkNewItem() {
        this.detailComponents.forEach(detailComponent => {
            if (detailComponent && detailComponent.markNewItem) {
                detailComponent.markNewItem();
            }
        });
    }

    detailMarkDirty() {
        this.detailComponents.forEach(detailComponent => {
            if (detailComponent && detailComponent.markDirty) {
                detailComponent.markDirty();
            }
        });
    }

    enableDetail() {
        this.dispatchEvent(new CustomEvent('detail-enabled'));
    }

    disableDetail() {
        this.dispatchEvent(new CustomEvent('detail-disabled'));
    }

    _onValueChanged(propertyName, oldValue, newValue) {
        this.dispatchEvent(new CustomEvent('value-changed', {
            detail: {
                propertyName: propertyName,
                oldValue: oldValue,
                newValue: newValue
            }
        }));
    }
}

customElements.define(UibuilderMasterDetailController.is, UibuilderMasterDetailController);

window.Uibuilder = window.Uibuilder || {};
window.Uibuilder.mdcTags = window.Uibuilder.mdcTags || new Set();
window.Uibuilder.mdcTags.add(UibuilderMasterDetailController.is);
