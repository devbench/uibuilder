/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';

export class UIBuilderBackendCommunicator extends ThemableMixin(PolymerElement) {

    static get is() {
        return 'uibuilder-backend-communicator';
    }

    static get version() {
        return '0.1';
    }

    ready() {
        super.ready();
        this.methodCallId = 0;
        this.methodCallTable = [];
    }

    _registerMethod(name, requiresArgument, hasReturnValue) {
        if (hasReturnValue) {
            if (requiresArgument) {
                this[name] = (arg) => this.__callMethodWithReturnValue(name, arg);
            } else {
                this[name] = () => this.__callMethodWithReturnValue(name, null);
            }
        } else {
            if (requiresArgument) {
                this[name] = (arg) => this.__callMethodWithoutReturnValue(name, arg);
            } else {
                this[name] = () => this.__callMethodWithoutReturnValue(name, null);
            }
        }
    }

    __callMethodWithReturnValue(name, argument) {
        return new Promise((resolve, reject) => {
            const methodIdForCurrentCall = this.methodCallId++;
            this.methodCallTable[methodIdForCurrentCall] = resolve;
            this.dispatchEvent(new CustomEvent('method-call', {
                detail: {
                    methodName: name,
                    methodId: methodIdForCurrentCall,
                    methodArgument: argument ? JSON.stringify(argument) : null
                }
            }));
        });
    }

    __callMethodWithoutReturnValue(name, argument) {
        this.dispatchEvent(new CustomEvent('method-call', {
            detail: {
                methodName: name,
                methodId: null,
                methodArgument: argument ? JSON.stringify(argument) : null
            }
        }));
    }

    _onMethodReturn(methodId, returnValue) {
        const resolve = this.methodCallTable[methodId];
        if (resolve) {
            resolve(JSON.parse(returnValue));
            this.methodCallTable[methodId] = null;
        }
    }

}

customElements.define(UIBuilderBackendCommunicator.is, UIBuilderBackendCommunicator);
