/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.backendcommunicator;

import com.google.gson.Gson;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.dom.DomEvent;
import io.devbench.uibuilder.api.listeners.BackendAttachListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Tag("uibuilder-backend-communicator")
@JsModule("./uibuilder-backend-communicator/src/uibuilder-backend-communicator.js")
public class BackendCommunicator extends Component implements BackendAttachListener {

    static final String EVENT_DETAIL_METHOD_NAME = "event.detail.methodName";
    static final String EVENT_DETAIL_METHOD_ARGUMENT = "event.detail.methodArgument";
    static final String EVENT_DETAIL_METHOD_ID = "event.detail.methodId";
    static final String REGISTER_METHOD = "_registerMethod";

    private final Map<String, JsCallableMethod<?, ?>> registeredMethods = new HashMap<>();

    public <T, R> void registerMethod(String jsName, Class<T> argumentType, Class<R> returnType, Function<T, R> method) {
        getElement().callJsFunction(REGISTER_METHOD, jsName, true, true);
        registeredMethods.put(jsName, new JsCallableMethod<>(argumentType, returnType, method));
    }

    public <T> void registerMethod(String jsName, Class<T> argumentType, Consumer<T> method) {
        getElement().callJsFunction(REGISTER_METHOD, jsName, true, false);
        registeredMethods.put(jsName, new JsCallableMethod<>(argumentType, null, arg -> {
            method.accept(arg);
            return null;
        }));
    }

    public <R> void registerMethod(String jsName, Class<R> returnType, Supplier<R> method) {
        getElement().callJsFunction(REGISTER_METHOD, jsName, false, true);
        registeredMethods.put(jsName, new JsCallableMethod<>(null, returnType, arg -> method.get()));
    }

    public void registerMethod(String jsName, Runnable method) {
        getElement().callJsFunction(REGISTER_METHOD, jsName, false, false);
        registeredMethods.put(jsName, new JsCallableMethod<>(null, null, arg -> {
            method.run();
            return null;
        }));
    }

    @Override
    public void onAttached() {
        getElement().addEventListener("method-call", this::onMethodCalledEvent)
            .addEventData(EVENT_DETAIL_METHOD_NAME)
            .addEventData(EVENT_DETAIL_METHOD_ARGUMENT)
            .addEventData(EVENT_DETAIL_METHOD_ID);
    }

    @SuppressWarnings("unchecked")
    private void onMethodCalledEvent(DomEvent event) {
        final String methodName = event.getEventData().getString(EVENT_DETAIL_METHOD_NAME);
        final JsCallableMethod<Object, Object> jsCallableMethod = (JsCallableMethod<Object, Object>) registeredMethods.get(methodName);
        final Gson gson = new Gson();

        final Object argument;
        if (jsCallableMethod.getArgumentType() != null) {
            final String argumentString = event.getEventData().getString(EVENT_DETAIL_METHOD_ARGUMENT);
            argument = gson.fromJson(argumentString, jsCallableMethod.getArgumentType());
        } else {
            argument = null;
        }

        if (jsCallableMethod.getReturnType() != null) {
            final double methodId = event.getEventData().getNumber(EVENT_DETAIL_METHOD_ID);
            final Object returnValue = jsCallableMethod.getFunction().apply(argument);
            getElement().callJsFunction("_onMethodReturn", methodId, gson.toJson(returnValue));
        } else {
            jsCallableMethod.getFunction().apply(argument);
        }
    }

    @Getter
    @AllArgsConstructor
    private static class JsCallableMethod<T, R> {
        private Class<T> argumentType;
        private Class<R> returnType;
        private Function<T, R> function;
    }

}
