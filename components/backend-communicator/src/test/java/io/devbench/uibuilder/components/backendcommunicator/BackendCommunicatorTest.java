/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.backendcommunicator;

import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.DomEventListener;
import com.vaadin.flow.dom.DomListenerRegistration;
import com.vaadin.flow.dom.Element;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.function.Supplier;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BackendCommunicatorTest {

    @Mock
    private Element element;

    @Spy
    @InjectMocks
    private BackendCommunicator testObj;

    @BeforeEach
    void setup() {
        lenient().doReturn(element).when(testObj).getElement();
    }

    @Test
    @DisplayName("Should start listening to method call events on attach")
    public void should_start_listening_to_method_call_events_on_attach() {
        final DomListenerRegistration registration = mock(DomListenerRegistration.class);
        when(element.addEventListener(eq("method-call"), any())).thenReturn(registration);
        when(registration.addEventData(anyString())).thenReturn(registration);

        testObj.onAttached();

        assertAll(
            () -> verify(element).addEventListener(eq("method-call"), any()),
            () -> verify(registration).addEventData(BackendCommunicator.EVENT_DETAIL_METHOD_NAME),
            () -> verify(registration).addEventData(BackendCommunicator.EVENT_DETAIL_METHOD_ARGUMENT),
            () -> verify(registration).addEventData(BackendCommunicator.EVENT_DETAIL_METHOD_ID)
        );
    }

    @Test
    @DisplayName("Should register js callable methods for different method types")
    public void should_register_js_callable_methods_for_different_method_types() {
        testObj.registerMethod("test1", TestType.class, TestType.class, arg -> arg);
        verify(element).callJsFunction(BackendCommunicator.REGISTER_METHOD, "test1", true, true);

        testObj.registerMethod("test2", TestType.class, arg -> {
        });
        verify(element).callJsFunction(BackendCommunicator.REGISTER_METHOD, "test2", true, false);

        testObj.registerMethod("test3", TestType.class, TestType::new);
        verify(element).callJsFunction(BackendCommunicator.REGISTER_METHOD, "test3", false, true);

        testObj.registerMethod("test4", () -> {
        });
        verify(element).callJsFunction(BackendCommunicator.REGISTER_METHOD, "test4", false, false);
    }

    @Test
    @DisplayName("Should handle method call event for method with argument and return type")
    public void should_handle_method_call_event_for_method_with_argument_and_return_type() {
        final DomListenerRegistration registration = mock(DomListenerRegistration.class);
        when(element.addEventListener(eq("method-call"), any())).thenReturn(registration);
        when(registration.addEventData(anyString())).thenReturn(registration);

        testObj.onAttached();

        final ArgumentCaptor<DomEventListener> listenerCaptor = ArgumentCaptor.forClass(DomEventListener.class);
        verify(element).addEventListener(eq("method-call"), listenerCaptor.capture());

        final AtomicReference<TestType> argument = new AtomicReference<>();

        Function<TestType, TestType> testFunction = arg -> {
            argument.set(arg);
            return new TestType("test result", 2.2);
        };

        testObj.registerMethod("test1", TestType.class, TestType.class, testFunction);

        final DomEvent event = mock(DomEvent.class, RETURNS_DEEP_STUBS);
        when(event.getEventData().getString(BackendCommunicator.EVENT_DETAIL_METHOD_NAME)).thenReturn("test1");
        when(event.getEventData().getString(BackendCommunicator.EVENT_DETAIL_METHOD_ARGUMENT)).thenReturn("{\"name\":\"test name\", \"value\": 1.1}");
        when(event.getEventData().getNumber(BackendCommunicator.EVENT_DETAIL_METHOD_ID)).thenReturn(0.0);

        listenerCaptor.getValue().handleEvent(event);

        final ArgumentCaptor<String> resultCaptor = ArgumentCaptor.forClass(String.class);

        assertAll(
            () -> assertEquals(new TestType("test name", 1.1), argument.get()),
            () -> verify(element).callJsFunction(eq("_onMethodReturn"), eq(0.0), resultCaptor.capture()),
            () -> assertEquals("{\"name\":\"testresult\",\"value\":2.2}", resultCaptor.getValue().replaceAll("\\s", ""))
        );
    }

    @Test
    @DisplayName("Should handle method call event for method with return value without argument")
    public void should_handle_method_call_event_for_method_with_return_value_without_argument() {
        final DomListenerRegistration registration = mock(DomListenerRegistration.class);
        when(element.addEventListener(eq("method-call"), any())).thenReturn(registration);
        when(registration.addEventData(anyString())).thenReturn(registration);

        testObj.onAttached();

        final ArgumentCaptor<DomEventListener> listenerCaptor = ArgumentCaptor.forClass(DomEventListener.class);
        verify(element).addEventListener(eq("method-call"), listenerCaptor.capture());

        Supplier<TestType> testFunction = () -> new TestType("result", 0.1);

        testObj.registerMethod("test1", TestType.class, testFunction);

        final DomEvent event = mock(DomEvent.class, RETURNS_DEEP_STUBS);
        when(event.getEventData().getString(BackendCommunicator.EVENT_DETAIL_METHOD_NAME)).thenReturn("test1");
        when(event.getEventData().getString(BackendCommunicator.EVENT_DETAIL_METHOD_ARGUMENT)).thenThrow(ClassCastException.class);
        when(event.getEventData().getNumber(BackendCommunicator.EVENT_DETAIL_METHOD_ID)).thenReturn(0.0);

        listenerCaptor.getValue().handleEvent(event);

        final ArgumentCaptor<String> resultCaptor = ArgumentCaptor.forClass(String.class);

        assertAll(
            () -> verify(element).callJsFunction(eq("_onMethodReturn"), eq(0.0), resultCaptor.capture()),
            () -> assertEquals("{\"name\":\"result\",\"value\":0.1}", resultCaptor.getValue().replaceAll("\\s", ""))
        );
    }

    @Test
    @DisplayName("Should handle method call event for method without return value with argument")
    public void should_handle_method_call_event_for_method_without_return_value_with_argument() {
        final DomListenerRegistration registration = mock(DomListenerRegistration.class);
        when(element.addEventListener(eq("method-call"), any())).thenReturn(registration);
        when(registration.addEventData(anyString())).thenReturn(registration);

        testObj.onAttached();

        final ArgumentCaptor<DomEventListener> listenerCaptor = ArgumentCaptor.forClass(DomEventListener.class);
        verify(element).addEventListener(eq("method-call"), listenerCaptor.capture());

        final AtomicReference<TestType> argument = new AtomicReference<>();

        testObj.registerMethod("test1", TestType.class, argument::set);

        final DomEvent event = mock(DomEvent.class, RETURNS_DEEP_STUBS);
        when(event.getEventData().getString(BackendCommunicator.EVENT_DETAIL_METHOD_NAME)).thenReturn("test1");
        when(event.getEventData().getString(BackendCommunicator.EVENT_DETAIL_METHOD_ARGUMENT)).thenReturn("{\"name\":\"argument\", \"value\": 3.3}");
        when(event.getEventData().getNumber(BackendCommunicator.EVENT_DETAIL_METHOD_ID)).thenThrow(ClassCastException.class);

        listenerCaptor.getValue().handleEvent(event);

        assertAll(
            () -> assertEquals(new TestType("argument", 3.3), argument.get()),
            () -> verify(element, times(0)).callJsFunction(eq("_onMethodReturn"), any(), any())
        );
    }

    @Test
    @DisplayName("Should handle method call event for method without return value and argument")
    public void should_handle_method_call_event_for_method_without_return_value_and_argument() {
        final DomListenerRegistration registration = mock(DomListenerRegistration.class);
        when(element.addEventListener(eq("method-call"), any())).thenReturn(registration);
        when(registration.addEventData(anyString())).thenReturn(registration);

        testObj.onAttached();

        final ArgumentCaptor<DomEventListener> listenerCaptor = ArgumentCaptor.forClass(DomEventListener.class);
        verify(element).addEventListener(eq("method-call"), listenerCaptor.capture());

        AtomicBoolean methodRan = new AtomicBoolean();

        final Runnable testFunction = () -> methodRan.set(true);

        testObj.registerMethod("test1", testFunction);

        final DomEvent event = mock(DomEvent.class, RETURNS_DEEP_STUBS);
        when(event.getEventData().getString(BackendCommunicator.EVENT_DETAIL_METHOD_NAME)).thenReturn("test1");
        when(event.getEventData().getString(BackendCommunicator.EVENT_DETAIL_METHOD_ARGUMENT)).thenThrow(ClassCastException.class);
        when(event.getEventData().getNumber(BackendCommunicator.EVENT_DETAIL_METHOD_ID)).thenThrow(ClassCastException.class);

        listenerCaptor.getValue().handleEvent(event);

        assertAll(
            () -> verify(element, times(0)).callJsFunction(eq("_onMethodReturn"), any(), any()),
            () -> assertTrue(methodRan.get())
        );
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestType {
        private String name;
        private double value;
    }

}
