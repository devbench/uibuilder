/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { PolymerElement } from '@polymer/polymer/polymer-element.js';

export class UIBuilderCommonFilterTypesElement extends PolymerElement {

    static get is() {
        return 'filter-types';
    }

    static get version() {
        return '0.1';
    }

    static get properties() {
        return {};
    }
}

export class UIBuilderCommonFilterTypeElement extends PolymerElement {

    static get is() {
        return 'filter-type';
    }

    static get version() {
        return '0.1';
    }

    static get properties() {
        return {
            label: {
                type: String,
            },
            filter: {
                type: String,
            },
            type: {
                type: String
            },
            valueComponent: {
                type: String
            },
            validator: {
                type: String
            },
            customConfiguration: {
                type: Object
            },
            operators: {
                type: Array,
            }
        };
    }

    _getPropertiesObject() {
        return {
            label: this.label,
            filter: this.filter,
            type: this.type,
            valueComponent: this.valueComponent,
            validatorName: this.validator,
            customConfiguration: this.customConfiguration,
            operators: this.operators,
        };
    }
}

customElements.define(UIBuilderCommonFilterTypesElement.is, UIBuilderCommonFilterTypesElement);
customElements.define(UIBuilderCommonFilterTypeElement.is, UIBuilderCommonFilterTypeElement);
