/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.valuecomponents;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.data.provider.ListDataProvider;
import io.devbench.uibuilder.data.common.filter.operator.OperatorValueType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;

import static com.helger.commons.mock.CommonsAssert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class EnumFilterValueComponentTest {

    public enum TestEnum {
        ONE, TWO, THREE
    }

    @Test
    @DisplayName("should populate combobox with the enum values")
    void test_should_populate_combobox_with_the_enum_values() {
        EnumFilterValueComponent testObj = new EnumFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.SINGLE);
        testObj.setEnumType(TestEnum.class);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));

        assertEquals(1, testObj.getComponents().size());
        ComboBox comboBox = testObj.getComponents().get(0);
        assertNotNull(comboBox);
        assertTrue(comboBox.getDataProvider() instanceof ListDataProvider, "ComboBox's data provider should be a list data provider");
        Collection items = ((ListDataProvider) comboBox.getDataProvider()).getItems();

        assertNotNull(items);
        assertIterableEquals(Arrays.asList(TestEnum.ONE, TestEnum.TWO, TestEnum.THREE), items);
    }

}
