/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.filtertype;

import com.vaadin.flow.component.textfield.TextField;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterOperatorTypeNotFoundException;
import io.devbench.uibuilder.components.commonfilter.exceptions.InvalidFilterOperatorsAttributeValueException;
import io.devbench.uibuilder.components.commonfilter.filterrow.DefaultFilterRow;
import io.devbench.uibuilder.components.commonfilter.filterrow.FilterRow;
import io.devbench.uibuilder.components.commonfilter.valuecomponents.FilterValueComponent;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadata;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadataProvider;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import java.util.Collections;
import java.util.List;
import static io.devbench.uibuilder.api.utils.elemental.json.JsonBuilder.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
public class DefaultFilterTypeTest {

    private DefaultFilterType testObj;

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;


    @BeforeEach
    void setup() {
        JsonObject filterDescriptor = jsonObject()
            .put("label", "test filter")
            .put("filter", "test.filter")
            .build();
        testObj = new DefaultFilterType(filterDescriptor);
    }

    @Test
    @DisplayName("Should create the default filter row and pass the expression factory and metadata to it")
    public void should_create_the_default_filter_row_and_pass_the_expression_factory_and_metadata_to_it() {
        FilterExpressionFactory expressionFactory = mock(FilterExpressionFactory.class);
        BindingMetadataProvider metadataProvider = mock(BindingMetadataProvider.class);
        BindingMetadata metadata = mock(BindingMetadata.class);
        when(metadataProvider.getMetadataForPath("test.filter")).thenReturn(metadata);
        doReturn(String.class).when(metadata).getPropertyType();
        doReturn(Collections.singleton(MockFilterValueComponent.class)).when(memberScanner).findClassesBySuperType(FilterValueComponent.class);

        FilterRow defaultFilterRow = testObj.createFilterRow(expressionFactory, metadataProvider);

        assertAll(
            () -> assertTrue(defaultFilterRow instanceof DefaultFilterRow),
            () -> assertSame(expressionFactory, defaultFilterRow.getExpressionFactory()),
            () -> assertSame(metadata, defaultFilterRow.getMetadata()),
            () -> assertSame(testObj, ((DefaultFilterRow) defaultFilterRow).getFilterType())
        );
    }

    @Test
    @DisplayName("Should throw npe if one of the main filter descriptor attributes are missing")
    public void should_throw_npe_if_one_of_the_main_filter_descriptor_attributes_are_missing() {
        JsonObject filterDescriptor = jsonObject()
            .put("filter", "test.filter")
            .build();
        assertThrows(NullPointerException.class, () -> new DefaultFilterType(filterDescriptor));
    }

    @Test
    @DisplayName("Should throw exception if operator type cannot be found")
    void test_should_throw_exception_if_operator_type_cannot_be_found() {
        String filterTypeLabel = "test filter";
        String wrongOperatorType = "in__n";

        JsonArray operatorsJsonArray = jsonArray().add("like").add("=").add(wrongOperatorType).build();
        JsonObject filterDescriptor = jsonObject()
            .put("filter", "testProperty")
            .put("label", filterTypeLabel)
            .put("operators", operatorsJsonArray)
            .build();

        FilterOperatorTypeNotFoundException exception = assertThrows(FilterOperatorTypeNotFoundException.class,
            () -> new DefaultFilterType(filterDescriptor), "Should throw exception");

        String exceptionMessage = exception.getMessage();
        assertNotNull(exceptionMessage);
        assertTrue(exceptionMessage.contains(filterTypeLabel));
        assertTrue(exceptionMessage.contains(wrongOperatorType));
    }

    @Test
    @DisplayName("Should throw exception if operator attribute is an invalid json")
    void test_should_throw_exception_if_operator_attribute_is_an_invalid_json() {
        String filterTypeLabel = "test filter";

        JsonObject filterDescriptor = Json.parse("{" +
            "'filter': 'testProperty'," +
            "'label': '" + filterTypeLabel + "'," +
            "'operators': null," +
            "}");

        InvalidFilterOperatorsAttributeValueException exception = assertThrows(InvalidFilterOperatorsAttributeValueException.class,
            () -> new DefaultFilterType(filterDescriptor), "Should throw exception");

        String exceptionMessage = exception.getMessage();
        assertNotNull(exceptionMessage);
        assertTrue(exceptionMessage.contains(filterTypeLabel));
    }

    public static class MockFilterValueComponent extends FilterValueComponent<TextField, String> {

        public MockFilterValueComponent() {
            super(TextField.class);
        }

        @Override
        public List<OperatorType> getOperators() {
            return Collections.emptyList();
        }

        @Override
        public boolean isApplicableOnPropertyType(Class<?> propertyType) {
            return true;
        }
    }

}
