/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.filtertype;

import elemental.json.JsonObject;
import io.devbench.uibuilder.components.commonfilter.filterrow.FilterRow;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadataProvider;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public interface FilterType {

    /**
     * This is reserved for custom filter types that want to handle configuration based on the json object sent from the frontend side.
     * The default filter types are initialized by the constructor.
     * @param frontendValues The configuration values json object, sent by the frontend component
     */
    default void initialize(@NotNull JsonObject frontendValues) {
    }

    String getLabel();


    FilterRow createFilterRow(FilterExpressionFactory<FilterExpression<?>> expressionFactory, BindingMetadataProvider metadataProvider);

    default Optional<String> getValidatorName() {
        return Optional.empty();
    }

}
