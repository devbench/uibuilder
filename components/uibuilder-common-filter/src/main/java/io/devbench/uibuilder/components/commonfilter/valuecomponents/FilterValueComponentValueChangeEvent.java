/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.valuecomponents;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.HasValue;

public class FilterValueComponentValueChangeEvent<T>
    extends ComponentEvent<Component>
    implements HasValue.ValueChangeEvent<T> {

    private HasValue<?, T> hasValue;
    private T oldValue;
    private T value;

    public FilterValueComponentValueChangeEvent(Component source, boolean fromClient,
                                                HasValue<?, T> hasValue, T oldValue, T value) {
        super(source, fromClient);
        this.hasValue = hasValue;
        this.oldValue = oldValue;
        this.value = value;
    }

    @Override
    public HasValue<?, T> getHasValue() {
        return hasValue;
    }

    @Override
    public T getOldValue() {
        return oldValue;
    }

    @Override
    public T getValue() {
        return value;
    }
}
