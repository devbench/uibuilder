/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.valuecomponents;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterValueComponentException;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterValueComponentInternalComponentInstantiationException;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;
import io.devbench.uibuilder.data.common.filter.operator.OperatorValueType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * Extended class must have a no arg constructor
 *
 * @param <COMP> is the internal component type
 * @param <T> is the value type that the component returns
 */
public abstract class FilterValueComponent<COMP extends Component & HasValue, T>
    extends Div implements HasValueAndElement<FilterValueComponentValueChangeEvent<T>, T> {

    public static final int DEFAULT_PRIORITY = 1000;

    @Getter
    private List<COMP> components;

    private Class<COMP> internalComponentClass;

    @Getter(AccessLevel.PROTECTED)
    private Button addButton;

    @Getter(AccessLevel.PROTECTED)
    private List<Button> removeButtons;

    @Getter(AccessLevel.PROTECTED)
    private Div addButtonContainer;
    private Div componentContainer;

    @Getter
    private boolean dynamic;

    @Getter
    @Setter
    private int minimumSize;

    private boolean readOnly = false;

    public FilterValueComponent(Class<COMP> internalComponentClass) {
        this(internalComponentClass, 1, false);
    }

    public FilterValueComponent(Class<COMP> internalComponentClass, OperatorValueType operatorValueType) {
        this(internalComponentClass, operatorValueType.minimalComponentCount(), operatorValueType == OperatorValueType.ANY);
    }

    public FilterValueComponent(Class<COMP> internalComponentClass, int minimumSize, boolean dynamic) {
        this.internalComponentClass = internalComponentClass;
        this.minimumSize = minimumSize;

        components = new LinkedList<>();
        removeButtons = new LinkedList<>();

        setupContainers();

        setClassName("filter-component");
        setDynamic(dynamic);

        addAttachListener(event -> {
            if (event.isInitialAttach()) {
                IntStream.range(components.size(), this.minimumSize).forEach(i -> addComponent());
            }
        });
    }

    public abstract List<OperatorType> getOperators();

    public abstract boolean isApplicableOnPropertyType(Class<?> propertyType);

    /**
     * @return the component priority in case of overlapping (lowest value means higher priority)
     */
    public int getPriority() {
        return DEFAULT_PRIORITY;
    }

    public COMP afterComponentCreated(COMP component) {
        return component;
    }

    public void setDynamic(boolean dynamic) {
        this.dynamic = dynamic;
        addButtonContainer.setVisible(dynamic);
        removeButtons.forEach(button -> button.setVisible(dynamic));
    }

    public void setOperatorValueType(OperatorValueType operatorValueType) {
        setMinimumSize(operatorValueType.minimalComponentCount());
        setDynamic(operatorValueType == OperatorValueType.ANY);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T getValue() {
        if (!dynamic) {
            if (minimumSize == 0) {
                throw new FilterValueComponentException("Cannot return value of a none-value filter component");
            } else if (minimumSize == 1) {
                return (T) components.get(0).getValue();
            }
        }
        return (T) components.stream().map(comp -> (T) comp.getValue()).collect(Collectors.toList());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setValue(Object value) {
        if (value instanceof Collection) {
            setCollectionValue((Collection<T>) value);
        } else {
            setSingleValue((T) value);
        }
    }

    @SuppressWarnings("unchecked")
    private void setSingleValue(T value) {
        if (components.size() == 1) {
            components.get(0).setValue(value);
        } else if (components.isEmpty()) {
            addComponentWithValue(value);
        } else {
            throw new FilterValueComponentException("Cannot set single value on a no value or multi value component");
        }
    }

    private void setCollectionValue(Collection<T> collection) {
        if (minimumSize == 2 && !dynamic && collection.size() != 2) {
            throw new FilterValueComponentException(
                String.format("Cannot set collection with size %d on operator type with a constraint for 2 values", collection.size()));
        }
        componentContainer.removeAll();
        components.clear();
        removeButtons.clear();
        collection.forEach(this::addComponentWithValue);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Registration addValueChangeListener(ValueChangeListener<? super FilterValueComponentValueChangeEvent<T>> listener) {
        ComponentEventListener componentListener = event -> listener.valueChanged((FilterValueComponentValueChangeEvent<T>) event);
        return getEventBus().addListener(FilterValueComponentValueChangeEvent.class, componentListener);
    }

    private void setupContainers() {
        addButton = new Button(new Icon(VaadinIcon.PLUS_CIRCLE_O));
        addButton.addClickListener(event -> addComponent());

        addButtonContainer = new Div();
        addButtonContainer.setClassName("add-button");
        addButtonContainer.add(addButton);

        componentContainer = new Div();
        componentContainer.setClassName("component-container");

        add(addButtonContainer);
        add(componentContainer);
    }

    @SuppressWarnings("unchecked")
    private COMP createComponent() {
        try {
            COMP component = internalComponentClass.newInstance();
            component.addValueChangeListener(event -> {
                FilterValueComponentValueChangeEvent<T> valueChangeEvent = new FilterValueComponentValueChangeEvent<>(
                    component, event.isFromClient(), (HasValue<?, T>) event.getHasValue(),
                    (T) event.getOldValue(), (T) event.getValue());

                getEventBus().fireEvent(valueChangeEvent);
            });
            component.setReadOnly(readOnly);
            return afterComponentCreated(component);
        } catch (InstantiationException | IllegalAccessException e) {
            throw new FilterValueComponentInternalComponentInstantiationException(internalComponentClass, e);
        }
    }

    private void addComponent() {
        addComponentWithValue(null);
    }

    @SuppressWarnings("unchecked")
    private void addComponentWithValue(T initialValue) {
        COMP component = createComponent();
        if (initialValue != null) {
            component.setValue(initialValue);
        }

        Div removableComponent = createRemovableComponent();
        Button removeButton = createRemovableComponentRemoveButton(removableComponent, component);
        removeButton.setEnabled(!readOnly);
        removableComponent.add(component, removeButton);

        removeButtons.add(removeButton);
        components.add(component);
        componentContainer.add(removableComponent);
    }

    private Div createRemovableComponent() {
        Div removableComponent = new Div();
        removableComponent.addClassName("removable-component");

        if (!dynamic) {
            if (minimumSize == 1) {
                removableComponent.addClassName("removable-component-full");
            } else if (minimumSize == 2) {
                removableComponent.addClassName("removable-component-50");
            }
        }
        return removableComponent;
    }

    private Button createRemovableComponentRemoveButton(Div removableComponent, COMP component) {
        Button removeButton = new Button(new Icon(VaadinIcon.MINUS_CIRCLE_O));
        removeButton.getElement().setAttribute("theme", "error icon tertiary");
        removeButton.setVisible(isDynamic());
        removeButton.addClickListener(removeButtonEvent -> {
            if (minimumSize < components.size()) {
                componentContainer.remove(removableComponent);
                components.remove(component);
                removeButtons.remove(removeButton);
            } else {
                onRemoveComponentFailed(removeButtonEvent);
            }
        });
        return removeButton;
    }

    public void onRemoveComponentFailed(ClickEvent<Button> removeButtonClickEvent) {
        Notification.show("Could not remove component, minimum count is " + minimumSize, 4000, Notification.Position.TOP_END);
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
        addButton.setEnabled(!readOnly);
        removeButtons.forEach(button -> button.setEnabled(!readOnly));
        components.forEach(component -> component.setReadOnly(readOnly));
    }

    @Override
    public boolean isReadOnly() {
        return readOnly;
    }
}
