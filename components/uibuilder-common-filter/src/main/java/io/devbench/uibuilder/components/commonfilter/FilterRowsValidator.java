/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter;

import com.vaadin.flow.dom.Element;
import io.devbench.uibuilder.api.utils.elemental.json.JsonBuilder;
import io.devbench.uibuilder.components.commonfilter.filterrow.FilterRow;
import io.devbench.uibuilder.components.commonfilter.filterrow.HasFilterValidationReportSupport;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationExecutor;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationReport;
import io.devbench.uibuilder.data.common.filter.comperingfilters.AnyOperandFilterExpression;
import io.devbench.uibuilder.data.common.filter.comperingfilters.BinaryOperandFilterExpression;
import io.devbench.uibuilder.data.common.filter.comperingfilters.EmptyOperandFilterExpression;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;
import io.devbench.uibuilder.data.common.filter.logicaloperators.OrFilterExpression;
import io.devbench.uibuilder.data.common.filter.validators.ValueNotNullFilterValidator;
import io.devbench.uibuilder.i18n.core.I;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import static io.devbench.uibuilder.api.utils.elemental.json.JsonBuilder.*;


class FilterRowsValidator {

    private final List<String> validationErrors;

    private final List<String> validationWarnings;

    private final List<FilterRow<?>> filterRows;

    FilterRowsValidator(List<FilterRow<?>> filterRows) {
        this.filterRows = filterRows;
        validationWarnings = new ArrayList<>();
        validationErrors = new ArrayList<>();
    }

    static void clearValidationOnFrontend(Element element) {
        if (element.getPropertyRaw(CommonFilter.VALIDATION_RESULT_PROPERTY_NAME) != null) {
            new FilterRowsValidator(Collections.emptyList()).reportToFrontend(element);
        }
    }

    boolean isValid() {
        return validationErrors.isEmpty();
    }

    private void setValidationErrorsAndWarnings(FilterValidationReport report, FilterRow<?> filterRow) {
        if (filterRow instanceof HasFilterValidationReportSupport) {
            ((HasFilterValidationReportSupport) filterRow).reportValidationResult(report);
        } else {
            if (report.hasErrors()) {
                validationErrors.addAll(report.getErrors());
            }
            if (report.hasWarnings()) {
                validationWarnings.addAll(report.getWarnings());
            }
        }
    }

    private Pair<Optional<FilterExpression<?>>, Optional<FilterValidationReport>> createValidatedExpressionWithReport(FilterRow<?> filterRow) {
        Optional<FilterExpression<?>> filterExpression = filterRow.createFilterExpression();
        Optional<FilterValidationReport> filterValidationReport = filterExpression
            .map(expression -> FilterValidationExecutor.getInstance()
                .validate(filterRow.getFilterType().getValidatorName().orElse(ValueNotNullFilterValidator.NAME), expression));

        filterValidationReport
            .filter(FilterValidationReport::isInvalid)
            .ifPresent(report -> setValidationErrorsAndWarnings(report, filterRow));

        return Pair.of(filterExpression, filterValidationReport);
    }

    List<FilterExpression<?>> collectValidatedFilterExpressions() {
        AtomicBoolean hasErrors = new AtomicBoolean();

        List<FilterExpression<?>> filterExpressions = filterRows.stream()
            .map(this::createValidatedExpressionWithReport)
            .peek(pair -> pair.getValue()
                .ifPresent(report -> hasErrors.compareAndSet(false, report.hasErrors())))
            .map(Pair::getKey)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.toList());

        return hasErrors.get() ? Collections.emptyList() : filterExpressions;
    }

    boolean isCompositeExpressionValid(FilterExpression<?> expression, String validatorName) {
        if (StringUtils.isNotBlank(validatorName)) {
            FilterValidationReport report = FilterValidationExecutor.getInstance().validate(validatorName, expression);
            if (report.isInvalid()) {
                validationErrors.addAll(report.getErrors());
                validationWarnings.addAll(report.getWarnings());
                return !report.hasErrors();
            }
        }
        return true;
    }

    void reportToFrontend(Element element) {
        boolean hasErrors = !validationErrors.isEmpty();
        boolean hasWarnings = !validationWarnings.isEmpty();

        List<FilterExpression<?>> filterExpressions = filterRows.stream()
            .map(FilterRow::createFilterExpression)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.toList());

        JsonBuilder.JsonObjectBuilder validationResultBuilder = jsonObject()
            .put("hasErrors", hasErrors)
            .putIf(hasErrors, "errors", jsonArray().addAllStrings(validationErrors).build())
            .put("hasWarnings", hasWarnings)
            .putIf(hasWarnings, "warnings", jsonArray().addAllStrings(validationWarnings).build())
            .put("errorHeader", I.trc(CommonFilter.TAG_NAME, "Filtering resulted errors:"))
            .put("warnHeader", I.trc(CommonFilter.TAG_NAME, "Filtering resulted warnings:"))
            .put("description", expressionsToString(filterExpressions, " <i>" + I.tr("AND") + "</i> "));

        element.setPropertyJson(CommonFilter.VALIDATION_RESULT_PROPERTY_NAME, validationResultBuilder.build());
    }

    private String expressionsToString(List<FilterExpression<?>> expressions, String delimiter) {
        StringBuilder filterDescription = new StringBuilder();
        expressions.forEach(expression -> {
            String expressionDescription = expressionToString(expression);
            if (filterDescription.length() > 0) {
                filterDescription.append(delimiter);
            }
            filterDescription.append(expressionDescription);
        });
        return filterDescription.toString();
    }

    private String expressionToString(FilterExpression<?> expression) {
        if (expression instanceof OrFilterExpression) {
            @SuppressWarnings("unchecked")
            List<FilterExpression<?>> expressions =
                (List<FilterExpression<?>>) ((OrFilterExpression<?, ?>) expression).getExpressions();
            return "(" + expressionsToString(expressions, " <i>" + I.tr("OR") + "</i> ") + ")";
        } else if (expression instanceof BinaryOperandFilterExpression) {
            String path = "<b>" + ((BinaryOperandFilterExpression<?>) expression).getPath() + "</b>";
            Object value = "<strong>\"" + ((BinaryOperandFilterExpression<?>) expression).getValue() + "\"</strong>";
            if (expression instanceof ExpressionTypes.IgnoreCaseLike) {
                return path + " ~ " + value;
            } else if (expression instanceof ExpressionTypes.IgnoreCaseNotLike) {
                return path + " !~ " + value;
            } else if (expression instanceof ExpressionTypes.Equals) {
                return path + " = " + value;
            } else if (expression instanceof ExpressionTypes.NotEquals) {
                return path + " <> " + value;
            } else if (expression instanceof ExpressionTypes.GreaterThan) {
                return path + " > " + value;
            } else if (expression instanceof ExpressionTypes.GreaterThanOrEquals) {
                return path + " >= " + value;
            } else if (expression instanceof ExpressionTypes.LessThan) {
                return path + " < " + value;
            } else if (expression instanceof ExpressionTypes.LessThanOrEquals) {
                return path + " <= " + value;
            }
        } else if (expression instanceof AnyOperandFilterExpression) {
            String path = "<b>" + ((AnyOperandFilterExpression<?>) expression).getPath() + "</b>";
            Collection<Object> values = ((AnyOperandFilterExpression<?>) expression).getValues();
            List<String> stringValues = values.stream().map(String::valueOf).map(value -> "<strong>\""+value+"\"</strong>").collect(Collectors.toList());
            if (expression instanceof ExpressionTypes.Between) {
                return path + " = " + stringValues.get(0) + " " + I.tr("BETWEEN") + " " + stringValues.get(1);
            } else if (expression instanceof ExpressionTypes.In) {
                return path + " " + I.tr("IN") + " (" + String.join(", ", stringValues) + ")";
            } else if (expression instanceof ExpressionTypes.NotIn) {
                return path + " " + I.tr("NOT IN") + " (" + String.join(", ", stringValues) + ")";
            }
        } else if (expression instanceof EmptyOperandFilterExpression) {
            String path = "<b>" + ((EmptyOperandFilterExpression<?>) expression).getPath() + "</b>";
            if (expression instanceof ExpressionTypes.IsNull) {
                return path + " " + I.tr("IS NULL");
            } else if (expression instanceof ExpressionTypes.IsNotNull) {
                return path + " " + I.tr("IS NOT NULL");
            }
        }

        return "?";
    }

}
