/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.event;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import elemental.json.JsonValue;
import io.devbench.uibuilder.components.form.UIBuilderForm;

@DomEvent("form-field-value-change")
public class FormFieldValueChangeEvent extends ComponentEvent<UIBuilderForm<?>> {

    private final String propertyName;
    private final JsonValue oldValue;
    private final JsonValue newValue;
    private final boolean resetInProgress;
    private boolean formChildValid;

    public FormFieldValueChangeEvent(UIBuilderForm<?> source, boolean fromClient,
                                     @EventData("event.detail.propertyName") String propertyName,
                                     @EventData("event.detail.newPropertyValue") JsonValue newPropertyValue,
                                     @EventData("event.detail.oldPropertyValue") JsonValue oldPropertyValue,
                                     @EventData("event.detail.resetInProgress") Boolean resetInProgress) {
        super(source, fromClient);
        this.propertyName = propertyName;
        this.oldValue = oldPropertyValue;
        this.newValue = newPropertyValue;
        this.resetInProgress = resetInProgress;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public JsonValue getOldValue() {
        return oldValue;
    }

    public JsonValue getNewValue() {
        return newValue;
    }

    public boolean isResetInProgress() {
        return resetInProgress;
    }

    public boolean isFormChildValid() {
        return formChildValid;
    }

    public void setFormChildValid(boolean formChildValid) {
        this.formChildValid = formChildValid;
    }

    public static FormFieldValueChangeEvent nullEvent(UIBuilderForm<?> source) {
        return new FormFieldValueChangeEvent(source, false, null, null, null, false);
    }

    public static FormFieldValueChangeEvent nullEventWithFormChildValid(UIBuilderForm<?> source, boolean formChildValid) {
        FormFieldValueChangeEvent formFieldValueChangeEvent =
            new FormFieldValueChangeEvent(source, false, null, null, null, false);
        formFieldValueChangeEvent.setFormChildValid(formChildValid);
        return formFieldValueChangeEvent;
    }
}
