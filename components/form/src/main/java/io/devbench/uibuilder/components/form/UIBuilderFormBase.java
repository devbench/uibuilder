/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import com.vaadin.flow.component.*;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.api.components.HasRawElementComponent;
import io.devbench.uibuilder.api.listeners.BackendAttachListener;
import io.devbench.uibuilder.components.form.event.*;
import org.jsoup.nodes.Element;

import static com.vaadin.flow.dom.DisabledUpdateMode.*;

public class UIBuilderFormBase extends HasRawElementComponent implements HasComponents, HasElement, BackendAttachListener {

    public static final String TAG_NAME = "uibuilder-form";
    public static final String ATTR_ITEM_BIND = "item-bind";

    public static final String PROP_FORM_ITEM = "formItem";
    static final String PROP_TYPE_TIMEOUT = "typeTimeout";
    static final String PROP_VALID = "valid";
    static final String PROP_ITEMS = "items";
    static final String PROP_SELECTED = "selected";
    static final String PROP_BACKEND = "backend";
    static final String PROP_LEGEND = "legend";
    static final String PROP_HIDE_BORDER = "hideBorder";
    static final String PROP_READONLY = "readonly";
    static final String PROP_VALIDITY_DESCRIPTORS = "_validityDescriptors";
    static final String PROP_CHANGED_ONLY_VALIDATION = "changedOnlyValidation";
    static final String ATTR_FORM_CONTROL = "form-control";

    public enum Control {
        SAVE, RESET, CANCEL
    }

    public static Component[] asFormControl(Control formControlType, Component... components) {
        for (Component component : components) {
            component.getElement().setAttribute(ATTR_FORM_CONTROL, formControlType.name().toLowerCase());
        }
        return components;
    }

    private String itemBind;

    @Override
    public void setRawElement(Element rawElement) {
        super.setRawElement(rawElement);
        UIBuilderFormRegistry.updateFormTree();
    }

    @Override
    public void onAttached() {
        getElement().callJsFunction("_fireFormFieldChangeEvent");
    }

    @Synchronize(value = "legend-changed", allowUpdates = ALWAYS)
    public String getLegend() {
        return getElement().getProperty(PROP_LEGEND, "");
    }

    public void setLegend(String legend) {
        getElement().setProperty(PROP_LEGEND, legend);
    }

    @Synchronize("valid-changed")
    public boolean isValid() {
        return getElement().getProperty(PROP_VALID, true);
    }

    public void setValid(Boolean valid) {
        getElement().setProperty(PROP_VALID, valid);
    }

    @Synchronize("changed-only-validation-changed")
    public boolean isChangedOnlyValidation() {
        return getElement().getProperty(PROP_CHANGED_ONLY_VALIDATION, false);
    }

    public void setChangedOnlyValidation(boolean changedOnlyValidation) {
        getElement().setProperty(PROP_CHANGED_ONLY_VALIDATION, changedOnlyValidation);
    }

    @Synchronize(value = "hide-border-changed", property = "hideBorder", allowUpdates = ALWAYS)
    public boolean isBorderHidden() {
        return getElement().getProperty(PROP_HIDE_BORDER, false);
    }

    public void setBorderHidden(boolean borderHidden) {
        getElement().setProperty(PROP_HIDE_BORDER, borderHidden);
    }

    @Synchronize("type-timeout-changed")
    public int getTypeTimeout() {
        return getElement().getProperty(PROP_TYPE_TIMEOUT, 300);
    }

    public void setTypeTimeout(int typeTimeout) {
        getElement().setProperty(PROP_TYPE_TIMEOUT, typeTimeout);
    }

    @Synchronize(value = {"readonly-changed", "form-ready"}, allowUpdates = ALWAYS)
    public boolean isReadonly() {
        return getElement().getProperty(PROP_READONLY, false);
    }

    public void setReadonly(Boolean readonly) {
        getElement().setProperty(PROP_READONLY, readonly);
    }

    public void save() {
        getElement().callJsFunction(Control.SAVE.name().toLowerCase());
    }

    public void cancel() {
        getElement().callJsFunction(Control.CANCEL.name().toLowerCase());
    }

    public void reset() {
        getElement().callJsFunction(Control.RESET.name().toLowerCase());
    }

    public String getItemBind() {
        return itemBind;
    }

    public void setItemBind(String itemBind) {
        this.itemBind = itemBind;
    }

    String getItemBindValue() {
        return itemBind != null && itemBind.contains(":") ? itemBind.split(":")[1] : itemBind;
    }


    @SuppressWarnings("UnusedReturnValue")
    public Registration addFormFieldChangeListener(ComponentEventListener<FormFieldChangeEvent> formFieldChangeListener) {
        return addListener(FormFieldChangeEvent.class, formFieldChangeListener);
    }

    @SuppressWarnings("UnusedReturnValue")
    public Registration addFormFieldValueChangeListener(ComponentEventListener<FormFieldValueChangeEvent> formFieldValueChangeListener) {
        return addListener(FormFieldValueChangeEvent.class, formFieldValueChangeListener);
    }

    @SuppressWarnings("UnusedReturnValue")
    public Registration addFormItemAssignedListener(ComponentEventListener<FormItemAssignedEvent> formItemAssignedListener) {
        return addListener(FormItemAssignedEvent.class, formItemAssignedListener);
    }

    @SuppressWarnings("UnusedReturnValue")
    public Registration addFormSaveListener(ComponentEventListener<FormSaveEvent> formSaveListener) {
        return addListener(FormSaveEvent.class, formSaveListener);
    }

    @SuppressWarnings("UnusedReturnValue")
    public Registration addFormResetListener(ComponentEventListener<FormResetEvent> formResetListener) {
        return addListener(FormResetEvent.class, formResetListener);
    }

    @SuppressWarnings("UnusedReturnValue")
    public Registration addFormCancelListener(ComponentEventListener<FormCancelEvent> formCancelListener) {
        return addListener(FormCancelEvent.class, formCancelListener);
    }

    @SuppressWarnings("UnusedReturnValue")
    public Registration addFormReadyListener(ComponentEventListener<FormReadyEvent> formReadyListener) {
        return addListener(FormReadyEvent.class, formReadyListener);
    }

}
