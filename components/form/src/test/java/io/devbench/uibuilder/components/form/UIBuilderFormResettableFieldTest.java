/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.data.binder.HasItems;
import io.devbench.uibuilder.api.components.form.UIBuilderItemSelectable;
import io.devbench.uibuilder.api.exceptions.ComponentException;
import io.devbench.uibuilder.components.form.testutils.model.TestFormItem;
import io.devbench.uibuilder.components.form.testutils.model.TestFormSubItem;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.core.utils.reflection.PropertyMetadata;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.Serializable;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UIBuilderFormResettableFieldTest {

    @Mock
    private PropertyMetadata propertyMetadata;

    @Mock(extraInterfaces = {HasValue.class})
    private Component hasValueComponent;

    @Mock(extraInterfaces = {HasItems.class})
    private Component hasItemsComponent;

    @Mock(extraInterfaces = {UIBuilderItemSelectable.class})
    private Component itemSelectableComponent;

    @Mock(extraInterfaces = {HasItems.class, HasValue.class})
    private Component hasItemsHasValueComponent;

    @Mock
    private Component notSupportedComponent;

    @Mock
    private ClassMetadata<TestFormSubItem> classMetadata;

    @Mock
    private UIBuilderForm<TestFormSubItem> form;


    @Test
    @DisplayName("should clone original value by the property metadata")
    void test_should_clone_original_value_by_the_property_metadata() {
        TestFormSubItem item = new TestFormSubItem();
        item.setData("DATA");

        // mock
        Mockito.doReturn(item).when(propertyMetadata).getValue();
        UIBuilderFormResettableField<TestFormSubItem> resettableField = new UIBuilderFormResettableField<>(
            form, hasValueComponent, propertyMetadata, "data", true);

        // assert
        assertAll(
            () -> assertTrue(resettableField.isForceValue(), "ForceValue should be true"),
            () -> assertNotSame(item, resettableField.getOriginal(), "Original and Item should not be the same instance"),
            () -> assertEquals(item, resettableField.getOriginal(), "Original and Item should be equal")
        );
    }

    @Test
    @DisplayName("should set null when resetting to null")
    void test_should_set_null_when_resetting_to_null() {
        TestFormSubItem item = new TestFormSubItem();
        item.setData("DATA");

        UIBuilderFormResettableField<TestFormSubItem> resettableField = new UIBuilderFormResettableField<>(
            form, hasItemsComponent, propertyMetadata, "data", null);

        // mock

        Mockito.doReturn(classMetadata).when(form).getFormItemClassMetadata();
        Mockito.doReturn(Optional.of(propertyMetadata)).when(classMetadata).property("data");
        Mockito.doReturn(item).when(propertyMetadata).getValue();

        resettableField.reset();

        // assert
        Mockito.verify(propertyMetadata).setValue(Mockito.isNull());
        Mockito.verify((HasItems) hasItemsComponent).setItems((Collection) Mockito.isNull());
    }

    @Test
    @DisplayName("should call the setValue method when hasItems interface present by forceValue is true")
    void test_should_call_the_set_value_method_when_has_items_interface_present_by_force_value_is_true() {
        TestFormSubItem item = new TestFormSubItem();
        item.setData("DATA");

        UIBuilderFormResettableField<TestFormSubItem> resettableField =
            new UIBuilderFormResettableField<>(form, hasItemsHasValueComponent, propertyMetadata, "data", null);
        resettableField.setForceValue(true);

        // mock
        Mockito.doReturn(classMetadata).when(form).getFormItemClassMetadata();
        Mockito.doReturn(Optional.of(propertyMetadata)).when(classMetadata).property("data");
        Mockito.doReturn(item).when(propertyMetadata).getValue();

        resettableField.reset();

        // assert
        Mockito.verify(propertyMetadata).setValue(Mockito.isNull());
        Mockito.verify((HasValue) hasItemsHasValueComponent).setValue(Mockito.isNull());
    }

    @Test
    @DisplayName("should throw excpetion if property metadata is not found")
    void test_should_throw_excpetion_if_property_metadata_is_not_found() {
        UIBuilderFormResettableField<TestFormSubItem> resettableField = new UIBuilderFormResettableField<>(
            form, notSupportedComponent, propertyMetadata, "data", null);

        // mock
        Mockito.doReturn(Optional.empty()).when(classMetadata).property(Mockito.any());
        Mockito.doReturn(classMetadata).when(form).getFormItemClassMetadata();

        // assert
        assertThrows(ComponentException.class, resettableField::reset, "Should throw exception");
    }

    @Test
    @DisplayName("should throw exception if component is not supported")
    void test_should_throw_exception_if_component_is_not_supported() {
        TestFormSubItem item = new TestFormSubItem();
        item.setData("DATA");

        UIBuilderFormResettableField<TestFormSubItem> resettableField = new UIBuilderFormResettableField<>(
            form, notSupportedComponent, propertyMetadata, "data", null);

        // mock
        Mockito.doReturn(classMetadata).when(form).getFormItemClassMetadata();
        Mockito.doReturn(Optional.of(propertyMetadata)).when(classMetadata).property("data");
        Mockito.doReturn(item).when(propertyMetadata).getValue();

        // assert
        assertThrows(ComponentException.class, resettableField::reset, "Should throw exception");
        Mockito.verify(propertyMetadata).setValue(Mockito.isNull());
    }

    @Test
    @DisplayName("should not reset if the originial and the current item are equal")
    void test_should_not_reset_if_the_originial_and_the_current_item_are_equal() {
        TestFormSubItem item1 = new TestFormSubItem();
        item1.setData("SAME DATA");

        TestFormSubItem item2 = new TestFormSubItem();
        item2.setData("SAME DATA");

        UIBuilderFormResettableField<TestFormSubItem> resettableField = new UIBuilderFormResettableField<>(
            form, hasValueComponent, propertyMetadata, "data", item1);

        // mock
        Mockito.doReturn(classMetadata).when(form).getFormItemClassMetadata();
        Mockito.doReturn(Optional.of(propertyMetadata)).when(classMetadata).property("data");
        Mockito.doReturn(item2).when(propertyMetadata).getValue();

        resettableField.reset();
        // assert

        Mockito.verify(propertyMetadata, Mockito.never()).setValue(Mockito.any());
        Mockito.verify((HasValue) hasValueComponent, Mockito.never()).setValue(Mockito.any());
    }

    @Test
    @DisplayName("should reset and clone the original")
    void test_should_reset_and_clone_the_original() {
        TestFormSubItem item1 = new TestFormSubItem();
        item1.setData("ORIGINAL DATA");

        TestFormSubItem item2 = new TestFormSubItem();
        item2.setData("MODIFIED DATA");

        UIBuilderFormResettableField<TestFormSubItem> resettableField = new UIBuilderFormResettableField<>(
            form, hasValueComponent, propertyMetadata, "data", item1);

        // mock
        Mockito.doReturn(classMetadata).when(form).getFormItemClassMetadata();
        Mockito.doReturn(Optional.of(propertyMetadata)).when(classMetadata).property("data");
        Mockito.doReturn(item2).when(propertyMetadata).getValue();

        assertAll(
            () -> assertNotSame(item1, resettableField.getOriginal(), "Before reset, the original should not be the same as item1, should be a clone"),
            () -> assertEquals(item1, resettableField.getOriginal(), "Before reset, the original should be equal to item1, should be a clone")
        );

        resettableField.reset();
        // assert

        assertAll(
            () -> assertNotSame(item1, resettableField.getOriginal(), "After reset, the original should not be the same as item1"),
            () -> assertEquals(item1, resettableField.getOriginal(), "After reset, the original should be equal to item1"),
            () -> assertNotSame(item1, item2),
            () -> assertNotEquals(item1, item2)
        );

        Mockito.verify(propertyMetadata).setValue(Mockito.eq(item1));
        Mockito.verify((HasValue) hasValueComponent).setValue(Mockito.eq(item1));
    }

    @Test
    @DisplayName("should detect value change in collections too")
    void test_should_detect_value_change_in_collections_too() {
        UIBuilderForm<TestFormItem> form = mock(UIBuilderForm.class);

        List<TestFormSubItem> datas = new ArrayList<>(Arrays.asList(
            new TestFormSubItem("Data 1"),
            new TestFormSubItem("Data 2"),
            new TestFormSubItem("Data 3")));

        TestFormItem item = new TestFormItem();
        item.setAge(40);
        item.setDatas(datas);

        ClassMetadata<TestFormItem> formItemClassMetadata = ClassMetadata.ofValue(item);
        Mockito.doReturn(formItemClassMetadata).when(form).getFormItemClassMetadata();

        UIBuilderFormResettableField<TestFormItem> resettableFieldDatas = new UIBuilderFormResettableField<>(
            form, hasValueComponent, propertyMetadata, "datas", (Serializable) datas);

        UIBuilderFormResettableField<TestFormItem> resettableFieldAge = new UIBuilderFormResettableField<>(
            form, hasValueComponent, propertyMetadata, "age", 40);

        assertFalse(resettableFieldDatas.isValueChanged());
        assertFalse(resettableFieldAge.isValueChanged());

        item.getDatas().get(1).setData("Data 2 modified");
        assertTrue(resettableFieldDatas.isValueChanged());

        resettableFieldDatas.reset();
        assertFalse(resettableFieldDatas.isValueChanged());

        item.setAge(41);
        assertTrue(resettableFieldAge.isValueChanged());

        resettableFieldAge.reset();
        assertFalse(resettableFieldAge.isValueChanged());

        item.setDatas(null);
        assertTrue(resettableFieldDatas.isValueChanged());
        resettableFieldDatas.reset();

        resettableFieldDatas = new UIBuilderFormResettableField<>(
            form, hasValueComponent, propertyMetadata, "datas", null);
        assertTrue(resettableFieldDatas.isValueChanged());
        item.setDatas(null);
        assertFalse(resettableFieldDatas.isValueChanged());
    }

    @Test
    @SuppressWarnings({"unchecked", "rawtypes"})
    @DisplayName("Should set selected items when component is UIBuilderItemSelectable and selected flag is on")
    void test_should_set_selected_items_when_component_is_ui_builder_item_selectable_and_selected_flag_is_on() {
        TestFormItem item = new TestFormItem();
        TestFormSubItem subItem1 = new TestFormSubItem("data 1");
        TestFormSubItem subItem2 = new TestFormSubItem("data 2");
        TestFormSubItem subItem3 = new TestFormSubItem("data 3");

        item.setDatas(new ArrayList<>(Collections.singletonList(subItem2)));

        UIBuilderForm<TestFormItem> testForm = mock(UIBuilderForm.class);

        doReturn(classMetadata).when(testForm).getFormItemClassMetadata();
        doReturn(Optional.of(propertyMetadata)).when(classMetadata).property("datas");
        doReturn(item.getDatas()).when(propertyMetadata).getValue();
        doAnswer(invocation -> {
            item.setDatas(invocation.getArgument(0));
            return null;
        }).when(propertyMetadata).setValue(any());

        UIBuilderFormResettableField<TestFormItem> resettableField = new UIBuilderFormResettableField<>(
            testForm, itemSelectableComponent, propertyMetadata, "datas", true);
        resettableField.setSelection(true);

        item.getDatas().clear();
        item.getDatas().add(subItem1);
        item.getDatas().add(subItem3);

        resettableField.reset();

        List<TestFormSubItem> datas = item.getDatas();
        assertEquals(1, datas.size());
        assertEquals(subItem2, datas.get(0));

        ArgumentCaptor<Collection> subItemArgumentCaptor = ArgumentCaptor.forClass(Collection.class);
        verify((UIBuilderItemSelectable<?>) itemSelectableComponent).setSelectedItems(subItemArgumentCaptor.capture());

        assertSame(item.getDatas(), subItemArgumentCaptor.getValue());
    }

}
