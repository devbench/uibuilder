/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.testutils;

import org.mockito.Mockito;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.groups.Default;
import javax.validation.metadata.ConstraintDescriptor;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MockConstraintViolation<T> implements ConstraintViolation<T> {

    private String message;
    private Path propertyPath;
    private ConstraintDescriptor<?> constraintDescriptor;
    private Set<Class<?>> constraintViolationGroups;

    public MockConstraintViolation(String message, String propertyPath) {
        this.message = message;
        this.propertyPath = new MockPath(propertyPath);
        this.constraintDescriptor = Mockito.mock(ConstraintDescriptor.class);
        this.constraintViolationGroups = new HashSet<>(Collections.singleton(Default.class));
        Mockito.doAnswer(invocation -> this.constraintViolationGroups).when(constraintDescriptor).getGroups();
    }

    public void setConstraintViolationGroups(Set<Class<?>> constraintViolationGroups) {
        this.constraintViolationGroups = constraintViolationGroups;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getMessageTemplate() {
        return null;
    }

    @Override
    public T getRootBean() {
        return null;
    }

    @Override
    public Class<T> getRootBeanClass() {
        return null;
    }

    @Override
    public Object getLeafBean() {
        return null;
    }

    @Override
    public Object[] getExecutableParameters() {
        return new Object[0];
    }

    @Override
    public Object getExecutableReturnValue() {
        return null;
    }

    @Override
    public Path getPropertyPath() {
        return propertyPath;
    }

    @Override
    public Object getInvalidValue() {
        return null;
    }

    @Override
    public ConstraintDescriptor<?> getConstraintDescriptor() {
        return constraintDescriptor;
    }

    @Override
    public <U> U unwrap(Class<U> type) {
        return null;
    }
}
