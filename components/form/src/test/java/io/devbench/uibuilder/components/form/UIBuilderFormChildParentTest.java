/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import io.devbench.uibuilder.components.form.testutils.FormTestHelper;
import io.devbench.uibuilder.components.form.testutils.model.TestFormItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UIBuilderFormChildParentTest {

    @BeforeEach
    void setUp() {
        FormTestHelper.createForms(true);
    }

    @Test
    @DisplayName("should set childForms when setting parent form")
    void test_should_set_child_forms_when_setting_parent_form() {
        UIBuilderForm<TestFormItem> subsub = UIBuilderFormRegistry.getById("subsub", TestFormItem.class).get();
        UIBuilderForm<TestFormItem> anotherForm = UIBuilderFormRegistry.getById("another-form", TestFormItem.class).get();

        assertAll(
            () -> assertFalse(subsub.hasChildren()),
            () -> assertNull(anotherForm.getParentForm())
        );

        anotherForm.setParentForm(subsub);

        assertAll(
            () -> assertTrue(subsub.hasChildren()),
            () -> assertNotNull(anotherForm.getParentForm()),
            () -> assertSame(subsub, anotherForm.getParentForm()),
            () -> assertSame(anotherForm, subsub.getChildForms().iterator().next())
        );
    }

    @Test
    @DisplayName("should set parent form when added child form")
    void test_should_set_parent_form_when_added_child_form() {
        UIBuilderForm<TestFormItem> subsub = UIBuilderFormRegistry.getById("subsub", TestFormItem.class).get();
        UIBuilderForm<TestFormItem> anotherForm = UIBuilderFormRegistry.getById("another-form", TestFormItem.class).get();

        assertAll(
            () -> assertFalse(subsub.hasChildren()),
            () -> assertNull(anotherForm.getParentForm())
        );

        subsub.addChildForm(anotherForm);

        assertAll(
            () -> assertTrue(subsub.hasChildren()),
            () -> assertNotNull(anotherForm.getParentForm()),
            () -> assertSame(subsub, anotherForm.getParentForm()),
            () -> assertSame(anotherForm, subsub.getChildForms().iterator().next())
        );
    }

    @Test
    @DisplayName("should update parent form and child forms when changing parent")
    void test_should_update_parent_form_and_child_forms_when_changing_parent() {
        UIBuilderForm<TestFormItem> subsub = UIBuilderFormRegistry.getById("subsub", TestFormItem.class).get();
        UIBuilderForm<TestFormItem> subform1 = UIBuilderFormRegistry.getById("subform1", TestFormItem.class).get();
        UIBuilderForm<TestFormItem> subform2 = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();

        assertAll(
            () -> assertFalse(subsub.hasChildren()),
            () -> assertFalse(subform1.hasChildren()),
            () -> assertTrue(subform2.hasChildren()),
            () -> assertSame(subform2, subsub.getParentForm())
        );

        subsub.setParentForm(subform1);

        assertAll(
            () -> assertFalse(subsub.hasChildren()),
            () -> assertTrue(subform1.hasChildren()),
            () -> assertFalse(subform2.hasChildren()),
            () -> assertSame(subform1, subsub.getParentForm())
        );
    }

    @Test
    @DisplayName("should update child forms when removing child form")
    void test_should_update_child_forms_when_removing_child_form() {
        UIBuilderForm<TestFormItem> rootForm = UIBuilderFormRegistry.getById("parent-form", TestFormItem.class).get();
        UIBuilderForm<TestFormItem> subform3 = UIBuilderFormRegistry.getById("subform3", TestFormItem.class).get();

        rootForm.removeChildForm(subform3);

        assertAll(
            () -> assertEquals(2, rootForm.getChildForms().size()),
            () -> assertNull(subform3.getParentForm())
        );
    }

}
