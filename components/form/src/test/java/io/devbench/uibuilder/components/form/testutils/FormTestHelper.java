/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.testutils;

import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.components.form.UIBuilderFormRegistry;
import io.devbench.uibuilder.components.form.event.FormFieldBinding;
import io.devbench.uibuilder.components.form.testutils.model.TestFormItem;
import io.devbench.uibuilder.components.form.testutils.model.TestFormSubItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.util.*;

public final class FormTestHelper {

    public static final Element RAW_ELEMENT_ROOT = Jsoup.parse("<root>" +
        "<uibuilder-form id=\"parent-form\">" +
        "   <div id=\"header\"></div>" +
        "   <uibuilder-form id=\"subform1\"></uibuilder-form>" +
        "   <uibuilder-form id=\"subform2\">" +
        "       <span>Header</span>" +
        "       <uibuilder-form id=\"subsub\" item-bind=\"formItem:subItem\">" +
        "           <vaadin-text-field item-bind=\"data\"></vaadin-text-field>" +
        "       </uibuilder-form>" +
        "       <span>Footer</span>" +
        "   </uibuilder-form>" +
        "   <uibuilder-form id=\"subform3\"></uibuilder-form>" +
        "   <div id=\"footer\"></div>" +
        "</uibuilder-form>" +
        "<hr/>" +
        "<uibuilder-form id=\"another-form\">" +
        "</uibuilder-form>" +
        "</root>");

    public static void createForms() {
        createForms(false);
    }

    public static void createForms(boolean reset) {
        if (reset) {
            testFormSet.clear();
        }
        createForm("subform1");
        createForm("subform2");
        createForm("subform3");
        createForm("subsub");
        createForm("parent-form");
        createForm("another-form");
    }

    public static void createForm(String id) {
        UIBuilderForm<TestFormItem> form = new UIBuilderForm<>();
        form.setId(id);
        form.setRawElement(FormTestHelper.RAW_ELEMENT_ROOT.getElementById(id));
        form.onAttached();
    }

    public static TestFormItem createItem(String name, Integer age, String data) {
        TestFormItem item = new TestFormItem();
        item.setName(name);
        item.setAge(age);
        item.setSubItem(createSubItem(data));
        return item;
    }

    public static TestFormItem createItem(String name, Integer age, String... data) {
        TestFormItem item = new TestFormItem();
        item.setName(name);
        item.setAge(age);
        item.setSubItem(createSubItem(data[0]));
        List<TestFormSubItem> datas = new ArrayList<>();
        if (data.length > 1) {
            for (int i = 1; i < data.length; i++) {
                datas.add(createSubItem(data[i]));
            }
        }
        item.setDatas(datas);
        return item;
    }

    public static TestFormSubItem createSubItem(String data) {
        TestFormSubItem subItem = new TestFormSubItem();
        subItem.setData(data);
        return subItem;
    }

    public static FormFieldBinding createFormFieldBinding(String itemBind) {
        FormFieldBinding binding = new FormFieldBinding();
        if (itemBind.contains(":")) {
            String[] split = itemBind.split(":");
            binding.setValueSourcePropertyName(split[0]);
            binding.setItemBind(split[1]);
        } else {
            binding.setItemBind(itemBind);
        }
        return binding;
    }

    public static Set<FormFieldBinding> createBindings() {
        Set<FormFieldBinding> bindings = new HashSet<>();
        bindings.add(createFormFieldBinding("name"));
        bindings.add(createFormFieldBinding("age"));
        bindings.add(createFormFieldBinding("subItem.data"));
        return bindings;
    }

    public static Set<FormFieldBinding> createBindingsSubItemSelected() {
        Set<FormFieldBinding> bindings = new HashSet<>();
        bindings.add(createFormFieldBinding("name"));
        bindings.add(createFormFieldBinding("age"));
        bindings.add(createFormFieldBinding("selected:subItem"));
        return bindings;
    }

    public static Set<FormFieldBinding> createBindingsDatasSelected() {
        Set<FormFieldBinding> bindings = new HashSet<>();
        bindings.add(createFormFieldBinding("name"));
        bindings.add(createFormFieldBinding("age"));
        bindings.add(createFormFieldBinding("selected:datas"));
        return bindings;
    }

    public static Set<FormFieldBinding> createBindingsNestedForm() {
        Set<FormFieldBinding> bindings = new HashSet<>();
        bindings.add(createFormFieldBinding("name"));
        bindings.add(createFormFieldBinding("age"));
        return bindings;
    }

    public static Set<FormFieldBinding> createBindingsNestedFormSub() {
        Set<FormFieldBinding> bindings = new HashSet<>();
        bindings.add(createFormFieldBinding("data"));
        return bindings;
    }

    public static Set<FormFieldBinding> createBindingsWithAgeSelected() {
        Set<FormFieldBinding> bindings = new HashSet<>();
        bindings.add(createFormFieldBinding("name"));
        bindings.add(createFormFieldBinding("selected:age"));
        bindings.add(createFormFieldBinding("subItem.data"));
        return bindings;
    }

    public static Set<FormFieldBinding> createBindingsWithDatas() {
        Set<FormFieldBinding> bindings = new HashSet<>();
        bindings.add(createFormFieldBinding("name"));
        bindings.add(createFormFieldBinding("age"));
        bindings.add(createFormFieldBinding("subItem.data"));
        bindings.add(createFormFieldBinding("items:datas"));
        return bindings;
    }

    public static Set<FormFieldBinding> createBindingsWithNameAndDatasAndNums() {
        Set<FormFieldBinding> bindings = new HashSet<>();
        bindings.add(createFormFieldBinding("name"));
        bindings.add(createFormFieldBinding("items:datas"));
        bindings.add(createFormFieldBinding("nums"));
        return bindings;
    }

    public static List<String> createChildFormList(String... childFormIds) {
        return new ArrayList<>(Arrays.asList(childFormIds));
    }

    private static Set<UIBuilderForm<?>> testFormSet = new HashSet<>();

    static {
        UIBuilderFormRegistry.setFormSetProvider(() -> testFormSet);
    }

}
