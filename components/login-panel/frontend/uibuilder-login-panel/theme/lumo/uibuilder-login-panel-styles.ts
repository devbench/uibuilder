///
///
/// Copyright © 2018 Webvalto Ltd.
///
/// Licensed under the Apache License, Version 2.0 (the "License");
/// you may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
///     http://www.apache.org/licenses/LICENSE-2.0
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.
///


(() => {
    const templateElement = document.createElement("template");
    templateElement.innerHTML = `
        <style>

        </style>
    `;
    const domModuleElement = document.createElement("dom-module");
    domModuleElement.setAttribute("id", "uibuilder-login-panel-style");
    domModuleElement.setAttribute("theme-for", "uibuilder-login-panel");
    domModuleElement.appendChild(templateElement);
    document.head.appendChild(domModuleElement);
})();
