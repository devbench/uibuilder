///
///
/// Copyright © 2018 Webvalto Ltd.
///
/// Licensed under the Apache License, Version 2.0 (the "License");
/// you may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
///     http://www.apache.org/licenses/LICENSE-2.0
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.
///

import { html, css, LitElement, PropertyValues, nothing } from 'lit';
import { customElement, property, state, query, queryAssignedElements } from 'lit/decorators.js';
import { ElementMixin } from '@vaadin/component-base/src/element-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { TextField } from '@vaadin/text-field';
import { PasswordField } from '@vaadin/password-field';
import { Button } from '@vaadin/button';
import { Notification } from '@vaadin/notification';
import { I18NBaseMixin } from '@vaadin/flow-frontend/uibuilder/uibuilder-i18n/i18n-base-mixin.js';


@customElement('uibuilder-login-panel')
export class UiBuilderLoginPanel extends I18NBaseMixin('uibuilder-login-panel', ElementMixin(ThemableMixin(LitElement))) {

    static styles = css`
        :host {
            display: block;
        }

        .center {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .imgContainer {
            text-align: center;
            align-items: center;
            padding: 20px;
        }

        .imgContainer img {
            width: var(--login-image-width, 100%);
            height: var(--login-image-height, 100%);
            border-radius: var(--login-image-border-radius, 0%);
        }

        #defaultErrorMessage {
            color: red;
            padding: 1em;
        }

        #loginButton {
            margin: var(--lumo-space-s) 0;
        }
    `;

    render() {
        return html`
            <div part="uibuilder-login-panel-container" @keyup=${(event: KeyboardEvent) => event.code === 'Enter' && this.login()}>
                ${this.image ? html`
                    <div class="imgContainer">
                        <img src="${this.image}" alt="Login image">
                    </div>
                ` : nothing}
                <vaadin-vertical-layout class="center">
                    <vaadin-text-field id="usernameField" label="Username"></vaadin-text-field>
                    <vaadin-password-field id="passwordField" label="Password"></vaadin-password-field>
                    <vaadin-button id="loginButton" @click=${this.login} theme="primary">
                        Login
                    </vaadin-button>
                    ${this.hideDefaultLoginFailedMessage ? html`
                        <div id="customErrorMessage" ?hidden=${this._errorHidden}>
                            <slot></slot>
                        </div>
                    ` : html`
                        <div id="defaultErrorMessage" ?hidden=${this._errorHidden}>
                            ${this.errorMessageToShow}
                        </div>
                    `}
                </vaadin-vertical-layout>
            </div>
        `;
    }

    static get is() {
        return 'uibuilder-login-panel'
    }

    @property({type: String}) image?: string;
    @property({type: Boolean, attribute: 'hide-default-login-failed-message'}) hideDefaultLoginFailedMessage: boolean = false;
    @property({type: String, attribute: 'error-message'}) errorMessage: string = '';
    @state() private _errorHidden: boolean = true;

    @query("#usernameField") usernameField!: TextField;
    @query("#passwordField") passwordField!: PasswordField;
    @query("#loginButton") loginButton!: Button;
    @query("#customErrorMessage") customErrorMessageDiv?: HTMLDivElement;
    @query("#defaultErrorMessage") defaultErrorMessageDiv?: HTMLDivElement;

    @queryAssignedElements({selector: 'vaadin-notification'}) _vaadinNotificationCustomErrorElement!: Notification[];

    // called once after the first render
    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        this.doTranslation();
    }

    private doTranslation() {
        this.usernameField.label = this.tr('Username');
        this.passwordField.label = this.tr('Password');
        this.loginButton.textContent = this.tr('Login');
    }

    private get errorMessageToShow(): string {
        if (!this.hideDefaultLoginFailedMessage && !this.errorMessage) {
            return this.tr('Username or password doesn\'t match.');
        } else {
            return this.errorMessage;
        }
    }

    private get errorElement() {
        return this.hideDefaultLoginFailedMessage ? this.getCustomErrorElement() : this.defaultErrorMessageDiv;
    }

    private getCustomErrorElement() {
        if (this._vaadinNotificationCustomErrorElement.length > 0) {
            return this._vaadinNotificationCustomErrorElement[0];
        } else {
            return this.customErrorMessageDiv;
        }
    }

    private login() {
        this.dispatchEvent(new CustomEvent('login', {
            detail: {
                username: this.usernameField.value,
                password: this.passwordField.value
            }
        }));
        this._errorHidden = true;
    }

    // noinspection JSUnusedLocalSymbols // called from the backend
    private onSucceedLogin() {
        this.dispatchEvent(new CustomEvent('success'));
        this.usernameField.value = '';
        this.passwordField.value = '';
    }

    // noinspection JSUnusedLocalSymbols // called from the backend
    private onFailedLogin() {
        if (this.errorElement instanceof Notification) {
            (this.errorElement as Notification).open();
        } else {
            this._errorHidden = false;
        }
        this.dispatchEvent(new CustomEvent('failed'));
        this.passwordField.value = '';
    }

}
