/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.confirmdialog;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Synchronize;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.internal.nodefeature.VirtualChildrenList;
import io.devbench.uibuilder.api.listeners.BackendAttachListener;
import io.devbench.uibuilder.components.confirmdialog.event.ChangeEvent;
import io.devbench.uibuilder.components.window.UIBuilderWindow;

import static com.vaadin.flow.internal.nodefeature.NodeProperties.*;

@Tag(ConfirmDialog.TAG_NAME)
@JsModule("./uibuilder-confirm-dialog/src/uibuilder-confirm-dialog.js")
public class ConfirmDialog extends Component implements HasComponents, BackendAttachListener {
    public static final String TAG_NAME = "confirm-dialog";

    private UIBuilderWindow window;

    public ConfirmDialog() {
    }

    @Override
    public void onAttached() {
        window = new UIBuilderWindow();
        getVirtualChildrenList().append(window.getElement().getNode(), INJECT_BY_ID, "window");
    }

    @Synchronize("opened-changed")
    public String getOpened() {
        return getElement().getProperty("opened");
    }

    public void setOpened(Boolean open) {
        getElement().setProperty("opened", open);
        this.fireEvent(new ChangeEvent(this));
    }

    private VirtualChildrenList getVirtualChildrenList() {
        return getElement().getNode().getFeature(VirtualChildrenList.class);
    }
}
