/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid.interceptor;

import io.devbench.uibuilder.api.parse.ParseInterceptor;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VaadinGridTreeToggleParseInterceptorTest {


    ParseInterceptor testObject = new VaadinGridTreeToggleParseInterceptor();

    @Test
    void shouldRecognizeVaadinGridTreeToggle() {
        final Element testElementGrid = new Element(UIBuilderGrid.TAG_NAME);
        final Element fakeTestElement = new Element("div");

        assertTrue(testObject.isApplicable(testElementGrid));
        assertFalse(testObject.isApplicable(fakeTestElement));
    }

    @Test
    void shouldFileTheNecessaryAttributes() {
        final Element testWrapper = new Element("div");
        final Element testElementGridTreeToggle = new Element(VaadinGridTreeToggleParseInterceptor.TAG_NAME);
        testWrapper.appendChild(testElementGridTreeToggle);
        testObject.intercept(null, testWrapper);

        assertEquals(testElementGridTreeToggle.attr(VaadinGridTreeToggleParseInterceptor.LEAF), VaadinGridTreeToggleParseInterceptor.LEAF_VALUE);
        assertEquals(testElementGridTreeToggle.attr(VaadinGridTreeToggleParseInterceptor.LEVEL), VaadinGridTreeToggleParseInterceptor.LEVEL_VALUE);
        assertEquals(testElementGridTreeToggle.attr(VaadinGridTreeToggleParseInterceptor.EXPANDED), VaadinGridTreeToggleParseInterceptor.EXPANDED_VALUE);
    }

    @Test
    void shouldNotChageTheFiledAttributes() {
        final Element testWrapper = new Element("div");
        final Element testElementGridTreeToggle = new Element(VaadinGridTreeToggleParseInterceptor.TAG_NAME);
        testWrapper.appendChild(testElementGridTreeToggle);

        testElementGridTreeToggle.attr(VaadinGridTreeToggleParseInterceptor.LEAF, "something_else_leaf_path");
        testElementGridTreeToggle.attr(VaadinGridTreeToggleParseInterceptor.LEVEL, "something_else_level_path");
        testElementGridTreeToggle.attr(VaadinGridTreeToggleParseInterceptor.EXPANDED, "something_else_expanded_path");

        testObject.intercept(null, testWrapper);

        assertEquals(testElementGridTreeToggle.attr(VaadinGridTreeToggleParseInterceptor.LEAF), "something_else_leaf_path");
        assertEquals(testElementGridTreeToggle.attr(VaadinGridTreeToggleParseInterceptor.LEVEL), "something_else_level_path");
        assertEquals(testElementGridTreeToggle.attr(VaadinGridTreeToggleParseInterceptor.EXPANDED), "something_else_expanded_path");
    }
}
