/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid;

import com.vaadin.flow.component.ComponentEventBus;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.server.VaadinSession;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.crud.GenericCrudGridInlineEditorControllerBean;
import io.devbench.uibuilder.components.grid.event.SelectionIDsChangedEvent;
import io.devbench.uibuilder.components.grid.exception.GridMultiSelectModeException;
import io.devbench.uibuilder.components.grid.exception.GridNoneSelectionModeException;
import io.devbench.uibuilder.components.grid.exception.GridSingleSelectModeException;
import io.devbench.uibuilder.components.util.datasource.DataSourceReadyEvent;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.data.collectionds.CollectionDataSource;
import io.devbench.uibuilder.data.collectionds.datasource.component.AbstractDataSourceComponent;
import io.devbench.uibuilder.data.collectionds.interceptors.ItemDataSourceBindingContext;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProviderEndpointManager;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProviderEndpointRegistration;
import io.devbench.uibuilder.data.common.dataprovidersupport.inlineedit.InlineEditHandler;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceContext;
import io.devbench.uibuilder.data.common.datasource.PagingFetchRequest;
import io.devbench.uibuilder.test.ReflectionUtil;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import javax.inject.Provider;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class, BaseUIBuilderTestExtension.class})
class UIBuilderGridTest {

    public static final String SET_ENDPOINT_URL = "_setEndpointInfo";
    public static final String TEST_CSRF_ID = "test-csrf-id";

    @Mock
    private VaadinSession mockSession;

    @Mock
    private DataProviderEndpointManager endpointManager;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(DataSourceManager.class)
    private DataSourceManager dataSourceManager;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private CommonDataSource<?, ?, ?, ?> dataSource;

    @Mock
    private UIContext uiContext;

    @Mock
    private CommonDataSourceContext<?> commonDataSourceContext;

    @Mock
    private Provider<GenericCrudGridInlineEditorControllerBean<?>> genericCrudGridInlineEditorControllerBeanProvider;

    @Mock
    private GenericCrudGridInlineEditorControllerBean<?> genericCrudGridInlineEditorControllerBean;

    private UIBuilderGridForTest<Object> testObj;

    @BeforeEach
    void setup() throws Exception {
        testObj = spy(new UIBuilderGridForTest<>());
        Field genericCrudGridInlineEditorControllerBeanField = UIBuilderGrid.class.getDeclaredField("genericCrudGridInlineEditorControllerBean");
        genericCrudGridInlineEditorControllerBeanField.setAccessible(true);
        genericCrudGridInlineEditorControllerBeanField.set(testObj, this.genericCrudGridInlineEditorControllerBeanProvider);
        DataProviderEndpointRegistration endpoint = DataProviderEndpointRegistration.of("testEndpoint", dataSource, "/testEndpointUrl");
        when(endpointManager.register(any(), anyString(), anyString(), anyString())).thenReturn(endpoint);
        when(UI.getCurrent().getCsrfToken()).thenReturn(TEST_CSRF_ID);
        doReturn(uiContext).when(mockSession).getAttribute("UIBuilderSessionContext");
        doReturn(endpointManager).when(uiContext).computeIfAbsent(eq(DataProviderEndpointManager.class), any());
        doReturn(commonDataSourceContext).when(uiContext).computeIfAbsent(eq(CommonDataSourceContext.class), any());
        doReturn(this.genericCrudGridInlineEditorControllerBean).when(genericCrudGridInlineEditorControllerBeanProvider).get();
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should register datasource ready event listener on attach")
    public void should_register_datasource_ready_event_listener_on_attach() {
        ComponentEventBus mockEventBus = mock(ComponentEventBus.class);
        when(testObj.getEventBus()).thenReturn(mockEventBus);

        testObj.onAttached();

        verify(mockEventBus).addListener(eq(DataSourceReadyEvent.class), any(ComponentEventListener.class));
    }

    @Test
    @DisplayName("Should register component for selected datasource and custom query in endpoint manager, when datasource ready event fired")
    public void should_register_component_for_selected_datasource_and_custom_query_in_endpoint_manager_when_datasource_ready_event_fired() {
        testObj.onAttached();
        ComponentEventBus realEventBus = testObj.getEventBus();

        realEventBus.fireEvent(createDataSourceReadyEvent());

        verify(endpointManager).register(testObj, "testDataSourceId", "testDataSourceName", "testQueryName");
    }

    @Test
    @DisplayName("Should call the _setEndpointUrl function on the frontend component with the endpoints url")
    public void should_call_the_set_endpoint_url_function_on_the_frontend_component_with_the_endpoints_url() {
        Element mockElement = spy(new Element("uibuilder-grid"));
        when(testObj.getElement()).thenReturn(mockElement);
        testObj.onAttached();

        testObj.getEventBus().fireEvent(createDataSourceReadyEvent());

        verify(mockElement).callJsFunction(SET_ENDPOINT_URL, "/testEndpointUrl", TEST_CSRF_ID);
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should register selection changed event listener on attach")
    public void should_register_selection_changed_event_listener_on_attach() {
        ComponentEventBus mockEventBus = mock(ComponentEventBus.class);
        when(testObj.getEventBus()).thenReturn(mockEventBus);

        testObj.onAttached();

        verify(mockEventBus).addListener(eq(SelectionIDsChangedEvent.class), any(ComponentEventListener.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should create collection datasource from elements")
    public void should_create_collection_datasource_from_elements() {
        ItemDataSourceBindingContext itemDataSourceBindingContext = mock(ItemDataSourceBindingContext.class);
        ArgumentCaptor<CollectionDataSource<String>> argumentCaptor = ArgumentCaptor.forClass(CollectionDataSource.class);

        when(dataSourceManager.getDataSourceProvider().getBindingContextForName("testDs", null))
            .thenReturn(itemDataSourceBindingContext);
        when(itemDataSourceBindingContext.getKeyPaths()).thenReturn(Collections.singletonList("id"));
        when(endpointManager.register(any(), any())).thenReturn(mock(DataProviderEndpointRegistration.class, RETURNS_DEEP_STUBS));

        testObj.connectItemDataSource("testDs");
        testObj.setItems("Foo", "Bar");

        verify(endpointManager, atLeastOnce()).register(eq(testObj), argumentCaptor.capture());
        assertEquals(Arrays.asList("Foo", "Bar"), argumentCaptor.getValue().fetchData((PagingFetchRequest) null));
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should create collection datasource from elements")
    public void should_set_empty_collection_as_datasource_on_item_datasource_tag() {
        ItemDataSourceBindingContext itemDataSourceBindingContext = mock(ItemDataSourceBindingContext.class);
        ArgumentCaptor<CollectionDataSource<String>> argumentCaptor = ArgumentCaptor.forClass(CollectionDataSource.class);

        when(dataSourceManager.getDataSourceProvider().getBindingContextForName("testDs", null))
            .thenReturn(itemDataSourceBindingContext);
        when(itemDataSourceBindingContext.getKeyPaths()).thenReturn(Collections.singletonList("id"));
        when(endpointManager.register(any(), any())).thenReturn(mock(DataProviderEndpointRegistration.class, RETURNS_DEEP_STUBS));

        testObj.connectItemDataSource("testDs");
        verify(endpointManager, times(1)).register(eq(testObj), argumentCaptor.capture());
        assertEquals(Collections.emptyList(), argumentCaptor.getValue().fetchData((PagingFetchRequest) null));
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should create collection datasource when collection is empty")
    void test_should_create_collection_datasource_when_collection_is_empty() {
        ItemDataSourceBindingContext itemDataSourceBindingContext = mock(ItemDataSourceBindingContext.class);
        ArgumentCaptor<CollectionDataSource<String>> argumentCaptor = ArgumentCaptor.forClass(CollectionDataSource.class);

        when(dataSourceManager.getDataSourceProvider().getBindingContextForName("testDs", null))
            .thenReturn(itemDataSourceBindingContext);
        when(itemDataSourceBindingContext.getKeyPaths()).thenReturn(Collections.singletonList("id"));
        when(endpointManager.register(any(), any())).thenReturn(mock(DataProviderEndpointRegistration.class, RETURNS_DEEP_STUBS));

        testObj.connectItemDataSource("testDs");
        testObj.setItems(Collections.emptyList());

        verify(endpointManager, atLeastOnce()).register(eq(testObj), argumentCaptor.capture());
        CollectionDataSource<String> value = argumentCaptor.getValue();

        assertNotNull(value);

        try {
            Method getElementType = CollectionDataSource.class.getDeclaredMethod("getElementType");
            getElementType.setAccessible(true);
            Class<?> elementType = (Class<?>) getElementType.invoke(value);
            assertNotNull(elementType);
            assertEquals(Object.class, elementType);
        } catch (Exception e) {
            fail("Could not check element type of the collection datasource");
        }
    }

    @Test
    @SuppressWarnings({"unchecked", "rawtypes"})
    @DisplayName("Should find the backend versions of the items selected on the frontend by using the datasource")
    public void should_find_the_backend_versions_of_the_items_selected_on_the_frontend_by_using_the_datasource() {
        when(dataSource.findItemsByJson(any(JsonArray.class))).thenReturn(Collections.emptyList());
        testObj.onAttached();

        AtomicBoolean listenerCalled = new AtomicBoolean(false);
        testObj.addSelectionChangedListener(event -> {
            listenerCalled.set(true);
            assertTrue(event.isFromClient(), "from client should be true");
        });

        ComponentEventBus realEventBus = testObj.getEventBus();
        realEventBus.fireEvent(createDataSourceReadyEvent());

        realEventBus.fireEvent(createSelectionChangedEvent());

        verify(dataSource).findItemsByJson(any(JsonArray.class));

        assertEquals(Collections.emptyList(), testObj.getSelectedItems());
        assertFalse(listenerCalled.get(), "listener should NOT been called if selected items were not changed");

        when(dataSource.findItemsByJson(any(JsonArray.class))).thenReturn((List) Arrays.asList("Changed item1", "Changed item2"));
        realEventBus.fireEvent(createSelectionChangedEvent());

        assertEquals(2, testObj.getSelectedItems().size());
        assertTrue(listenerCalled.get(), "listener should been called");
    }

    @Test
    @DisplayName("Should call remote js function on refresh")
    void test_should_call_remote_js_function_on_refresh() {
        Element element = mock(Element.class);
        doReturn(element).when(testObj).getElement();

        testObj.refresh();

        verify(element).callJsFunction("_refresh");
    }

    @Test
    @DisplayName("Should select all provided items if selection mode is multi select")
    void test_should_select_all_provided_items_if_selection_mode_is_multi_select() {
        Element element = mock(Element.class);
        doReturn(element).when(testObj).getElement();
        doReturn("multi").when(testObj).getSelectionMode();

        mockDataSource();

        List<String> itemsToSelect = Arrays.asList("One", "Two");
        testObj.setSelectedItems(itemsToSelect);

        List<?> selectedItems = testObj.getSelectedItems();
        assertNotNull(selectedItems);
        assertIterableEquals(Arrays.asList("One", "Two"), itemsToSelect);
        verify(element).callJsFunction(eq("_onItemsSelected"), any());
    }

    @Test
    @DisplayName("Should select nothing when dataSource is null")
    void should_select_nothing_when_datasource_is_null() {
        Element element = mock(Element.class);
        doReturn(element).when(testObj).getElement();
        doReturn("multi").when(testObj).getSelectionMode();
        doReturn(null).when(testObj).getDataSource();

        List<String> itemsToSelect = Arrays.asList("One", "Two");
        testObj.setSelectedItems(itemsToSelect);

        verify(element, never()).callJsFunction(eq("_onItemsSelected"), any());
    }

    @Test
    @DisplayName("Should throw exception if calling selectAll when selection mode is not multi select")
    void test_should_throw_exception_if_calling_select_all_when_selection_mode_is_not_multi_select() {
        doReturn("single").when(testObj).getSelectionMode();
        Assertions.assertThrows(GridSingleSelectModeException.class, () -> testObj.setSelectedItems(Arrays.asList("One", "Two")), "Should throw exception");
    }

    @SuppressWarnings("UnusedReturnValue")
    private CommonDataSource<?, ?, ?, ?> mockDataSource() {
        CommonDataSource<?, ?, ?, ?> commonDataSource = mock(CommonDataSource.class, Answers.RETURNS_DEEP_STUBS);
        try {
            ReflectionUtil.setInternalField(testObj, AbstractDataSourceComponent.class.getDeclaredField("dataSource"), commonDataSource);
        } catch (NoSuchFieldException e) {
            fail(e);
        }
        return commonDataSource;
    }

    @Test
    @DisplayName("Should clear selection")
    void test_should_clear_selection() {
        Element element = mock(Element.class);
        doReturn(element).when(testObj).getElement();
        doReturn("multi").when(testObj).getSelectionMode();

        mockDataSource();

        List<String> itemsToSelect = Arrays.asList("One", "Two");
        testObj.setSelectedItems(itemsToSelect);

        List<?> selectedItems = testObj.getSelectedItems();
        assertNotNull(selectedItems);
        assertIterableEquals(Arrays.asList("One", "Two"), itemsToSelect);

        testObj.deselect();

        selectedItems = testObj.getSelectedItems();
        assertNotNull(selectedItems);
        assertTrue(selectedItems.isEmpty(), "Selected items should be empty");

        verify(element, times(2)).callJsFunction(eq("_onItemsSelected"), any());
    }

    @Test
    @DisplayName("Should clear selection when items has been reassigned")
    void test_should_clear_selection_when_items_has_been_reassigned() {
        Element element = mock(Element.class);
        doReturn(element).when(testObj).getElement();
        doReturn("multi").when(testObj).getSelectionMode();

        mockDataSource();

        List<String> itemsToSelect = Arrays.asList("One", "Two");
        testObj.setSelectedItems(itemsToSelect);

        List<?> selectedItems = testObj.getSelectedItems();
        assertNotNull(selectedItems);
        assertIterableEquals(Arrays.asList("One", "Two"), itemsToSelect);


        ItemDataSourceBindingContext itemDataSourceBindingContext = mock(ItemDataSourceBindingContext.class);
        when(dataSourceManager.getDataSourceProvider().getBindingContextForName("testDs", null))
            .thenReturn(itemDataSourceBindingContext);
        when(itemDataSourceBindingContext.getKeyPaths()).thenReturn(Collections.singletonList("id"));
        when(endpointManager.register(any(), any())).thenReturn(mock(DataProviderEndpointRegistration.class, RETURNS_DEEP_STUBS));
        testObj.connectItemDataSource("testDs");

        List<String> anotherItems = Arrays.asList("Four", "Five", "Six");
        testObj.setItems(anotherItems);

        selectedItems = testObj.getSelectedItems();
        assertNotNull(selectedItems);
        assertTrue(selectedItems.isEmpty(), "Selected items should be empty");

        verify(element).callJsFunction(eq("_onItemsSelected"), any());
    }

    @Test
    @DisplayName("Should throw exception if trying to select single item on multi select mode")
    void test_should_throw_exception_if_trying_to_select_single_item_on_multi_select_mode() {
        doReturn("multi").when(testObj).getSelectionMode();
        assertThrows(GridMultiSelectModeException.class, () -> testObj.setSelectedItem("Item"), "Should throw exception");
    }

    @Test
    @DisplayName("Should throw exception if trying to select single item on none select mode")
    void test_should_throw_exception_if_trying_to_select_single_item_on_none_select_mode() {
        doReturn("none").when(testObj).getSelectionMode();
        Assertions.assertThrows(GridNoneSelectionModeException.class, () -> testObj.setSelectedItem("Item"), "Should throw exception");
    }

    @Test
    @DisplayName("Should select item on single select mode")
    void test_should_select_item_on_single_select_mode() {
        Element element = mock(Element.class);
        doReturn(element).when(testObj).getElement();
        doReturn("single").when(testObj).getSelectionMode();
        mockDataSource();

        testObj.setSelectedItem("Item");

        List<?> selectedItems = testObj.getSelectedItems();
        assertNotNull(selectedItems);
        assertIterableEquals(Collections.singletonList("Item"), selectedItems);
        verify(element).callJsFunction(eq("_onItemsSelected"), any());
    }

    @Test
    @DisplayName("Should not select item on single select mode if item is null")
    void test_should_not_select_item_on_single_select_mode_if_item_is_null() {
        Element element = mock(Element.class);
        doReturn(element).when(testObj).getElement();
        doReturn("single").when(testObj).getSelectionMode();
        mockDataSource();

        testObj.setSelectedItem(null);

        List<?> selectedItems = testObj.getSelectedItems();
        assertNotNull(selectedItems);
        assertTrue(selectedItems.isEmpty(), "Selected items should be empty");
        verify(element).callJsFunction(eq("_onItemsSelected"), any());
    }

    @Test
    @DisplayName("Should throw exception when trying to get single selected item and none select mode is set")
    void test_should_throw_exception_when_trying_to_get_single_selected_item_and_none_select_mode_is_set() {
        doReturn("none").when(testObj).getSelectionMode();
        assertThrows(GridNoneSelectionModeException.class, () -> testObj.getSelectedItem(), "Should throw exception");
    }

    @Test
    @DisplayName("Should return null when getting single selected item and there is nothing selected")
    void test_should_return_null_when_getting_single_selected_item_and_there_is_nothing_selected() {
        doReturn("single").when(testObj).getSelectionMode();
        mockDataSource();
        testObj.deselect();
        Object selectedItem = testObj.getSelectedItem();
        assertNull(selectedItem);
    }

    @Test
    @DisplayName("Should throw exception when trying to get single selected item and there is multiple selected items and multi selection mode is set")
    void test_should_throw_exception_when_trying_to_get_single_selected_item_and_there_is_multiple_selected_items_and_multi_selection_mode_is_set() {
        doReturn("multi").when(testObj).getSelectionMode();
        mockDataSource();
        testObj.setSelectedItems(Arrays.asList("One", "Two"));
        assertThrows(GridMultiSelectModeException.class, () -> testObj.getSelectedItem(), "Should throw exception");
    }

    @Test
    @DisplayName("Should get first selected item the trying to get single selected item and multi selection mode is not set")
    void test_should_get_first_selected_item_the_trying_to_get_single_selected_item_and_multi_selection_mode_is_not_set() {
        doReturn("multi").when(testObj).getSelectionMode();
        mockDataSource();
        testObj.setSelectedItems(Arrays.asList("One", "Two"));
        doReturn("single").when(testObj).getSelectionMode();

        Object selectedItem = testObj.getSelectedItem();

        assertNotNull(selectedItem);
        assertEquals("One", selectedItem);
    }

    @Test
    @DisplayName("Should set selection mode")
    void test_should_set_selection_mode() {
        testObj.setSelectionMode(SelectionMode.MULTI);
        assertEquals("multi", testObj.getSelectionMode());
        testObj.setSelectionMode(SelectionMode.SINGLE);
        assertEquals("single", testObj.getSelectionMode());
    }

    @Test
    @DisplayName("Should get inline edit handler")
    void test_should_get_inline_edit_handler() {
        InlineEditHandler mockInlineEditHandler = mock(InlineEditHandler.class);
        CommonDataSource<?, ?, ?, ?> commonDataSource = mockDataSource();
        when(commonDataSource.getDataProcessor().getInlineEditHandler()).thenReturn(mockInlineEditHandler);

        InlineEditHandler inlineEditHandler = testObj.getInlineEditHandler();
        assertSame(mockInlineEditHandler, inlineEditHandler);
    }

    @Test
    @DisplayName("Should register datasource")
    void test_should_register_datasource() {
        doReturn(Optional.of("grid-id")).when(testObj).getId();

        testObj.registerDataSource("testDS");

        verify(genericCrudGridInlineEditorControllerBean).registerDataSourceName("grid-id", "testDS");
    }

    @Test
    @DisplayName("Should call refresh item")
    void test_should_call_refresh_item() {
        Object item = new Object();
        JsonObject jsonItem = Json.createObject();
        Element element = mock(Element.class);

        @SuppressWarnings("unchecked")
        CommonDataSource<Object, ?, ?, ?> commonDataSource = (CommonDataSource<Object, ?, ?, ?>) mockDataSource();
        doReturn(jsonItem).when(commonDataSource).convertToKey(item);
        doReturn(element).when(testObj).getElement();

        testObj.refreshItem(item);

        verify(element).callJsFunction(eq("_refreshItem"), same(jsonItem));
        verify(element).callJsFunction(eq("_onItemsSelected"), any());
    }

    @Test
    @DisplayName("Should set edit mode for item")
    void test_should_set_edit_mode_for_item() {
        Object item = new Object();
        JsonObject jsonItem = Json.createObject();
        Element element = mock(Element.class);

        @SuppressWarnings("unchecked")
        CommonDataSource<Object, ?, ?, ?> commonDataSource = (CommonDataSource<Object, ?, ?, ?>) mockDataSource();
        InlineEditHandler mockInlineEditHandler = mock(InlineEditHandler.class);
        when(commonDataSource.getDataProcessor().getInlineEditHandler()).thenReturn(mockInlineEditHandler);
        doReturn(jsonItem).when(commonDataSource).convertToKey(item);
        doReturn(element).when(testObj).getElement();

        testObj.setEditMode(item, true);

        verify(mockInlineEditHandler).setEditMode(same(jsonItem), eq(true));
        verify(element).callJsFunction(eq("_refreshItem"), same(jsonItem));
        verify(element).callJsFunction(eq("_onItemsSelected"), any());
    }

    @Test
    @DisplayName("Should set edit mode for all the provided items")
    void test_should_set_edit_mode_for_all_the_provided_items() {
        Object item1 = new Object();
        Object item2 = new Object();
        List<Object> items = Arrays.asList(item1, item2);

        JsonObject jsonItem1 = Json.createObject();
        JsonObject jsonItem2 = Json.createObject();
        Element element = mock(Element.class);

        @SuppressWarnings("unchecked")
        CommonDataSource<Object, ?, ?, ?> commonDataSource = (CommonDataSource<Object, ?, ?, ?>) mockDataSource();
        InlineEditHandler mockInlineEditHandler = mock(InlineEditHandler.class);
        when(commonDataSource.getDataProcessor().getInlineEditHandler()).thenReturn(mockInlineEditHandler);

        JsonArray jsonItems = Json.createArray();
        jsonItems.set(0, jsonItem1);
        jsonItems.set(1, jsonItem2);
        doReturn(jsonItems).when(commonDataSource).convertToKeysArray(items);
        doReturn(element).when(testObj).getElement();

        testObj.setEditMode(items, true);

        verify(mockInlineEditHandler).setEditMode(same(jsonItem1), eq(true));
        verify(mockInlineEditHandler).setEditMode(same(jsonItem2), eq(true));
        verify(element).callJsFunction(eq("_onItemsSelected"), any());
    }

    @Test
    @SuppressWarnings("SuspiciousMethodCalls")
    @DisplayName("Should set and get predicate supplier")
    void test_should_set_and_get_predicate_supplier() {
        Map<String, Object> sessionAttributeMap = new HashMap<>();
        doAnswer(invocation -> sessionAttributeMap.put(invocation.getArgument(0), invocation.getArgument(1)))
            .when(mockSession).setAttribute(anyString(), any());
        doAnswer(invocation -> sessionAttributeMap.get(invocation.getArgument(0))).when(mockSession).getAttribute(anyString());

        assertNull(testObj.getItemPredicate());

        Predicate<Object> predicate = o -> true;
        testObj.setItemPredicate(predicate);

        assertSame(predicate, testObj.getItemPredicate());
    }

    private DataSourceReadyEvent createDataSourceReadyEvent() {
        return new DataSourceReadyEvent(testObj, true,
            "testDataSourceId", "testDataSourceName", "testQueryName");
    }

    private SelectionIDsChangedEvent createSelectionChangedEvent() {
        return new SelectionIDsChangedEvent(testObj, true, Json.createArray());
    }

    private static class UIBuilderGridForTest<T> extends UIBuilderGrid<T> {
        @Override
        protected ComponentEventBus getEventBus() {
            return super.getEventBus();
        }
    }

}
