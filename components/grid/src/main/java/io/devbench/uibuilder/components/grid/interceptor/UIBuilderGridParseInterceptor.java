/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid.interceptor;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import io.devbench.uibuilder.components.grid.SelectionMode;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import org.jsoup.nodes.Element;

import java.util.UUID;

import static io.devbench.uibuilder.core.utils.ElementCollector.*;

public class UIBuilderGridParseInterceptor implements ParseInterceptor {

    protected static final String SELECTION_MODE_ATTR = "selection-mode";

    @Override
    public boolean isApplicable(Element element) {
        return UIBuilderGrid.TAG_NAME.equals(element.tagName());
    }

    @Override
    public void intercept(Component component, Element element) {
        UIBuilderGrid<?> grid = (UIBuilderGrid<?>) component;
        if (!grid.getId().isPresent() && element.hasAttr(ID)) {
            grid.setId(element.attr(ID));
        } else if (!element.hasAttr(ID)) {
            String generatedId = UUID.randomUUID().toString();
            element.attr(ID, generatedId);
            grid.setId(generatedId);
        }
        if (element.hasAttr(SELECTION_MODE_ATTR)) {
            grid.setSelectionMode(SelectionMode.byPropertyName(element.attr(SELECTION_MODE_ATTR)));
        }

        if (element.hasAttr("generic-datasource-name")) {
            String dataSourceName = element.attr("generic-datasource-name");
            grid.registerDataSource(dataSourceName);
        }

        grid.setRawElement(element);
    }

    @Override
    public boolean isInstantiator(Element element) {
        return true;
    }

    @Override
    public Component instantiateComponent() {
        com.vaadin.flow.dom.Element vaadinElement = new com.vaadin.flow.dom.Element(UIBuilderGrid.TAG_NAME);
        return Component.from(vaadinElement, UIBuilderGrid.class);
    }

}
