
/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid.interceptor;

import io.devbench.uibuilder.api.crud.GenericCrudGridInlineEditorControllerBean;
import io.devbench.uibuilder.api.parse.PageTransformer;
import io.devbench.uibuilder.components.grid.exception.ItemEditableUnsupportedComponentException;
import io.devbench.uibuilder.core.parse.elementwalker.ElementWalker;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class UIBuilderItemEditableTransformer implements PageTransformer {

    private static final String UIBUILDER_ITEM_EDITABLE_TAG_NAME = "uibuilder-item-editable";

    @Override
    public boolean isApplicable(Element element) {
        return UIBUILDER_ITEM_EDITABLE_TAG_NAME.equals(element.tagName());
    }

    @Override
    public void transform(Element element) {
        failOnUIBuilderDatasourceDependentComponents(element);

        ensureItemAttribute(element);
        ensurePath(element);

        String controllerBeanName = GenericCrudGridInlineEditorControllerBean.BUILT_IN_GENERIC_CRUD_GRID_INLINE_EDITOR_CONTROLLER_BEAN_NAME;

        if (element.hasAttr("controller-bean")) {
            controllerBeanName = element.attr("controller-bean");
            element.removeAttr("controller-bean");
        } else {
            controllerBeanName = element.parents().stream()
                .filter(parent -> parent.tagName().endsWith("-grid"))
                .filter(parent -> parent.hasAttr("controller-bean"))
                .findFirst()
                .map(parent -> parent.attr("controller-bean"))
                .orElse(controllerBeanName);
        }

        handleDefaultButtons(element, controllerBeanName);
        handleDefaultEditor(element, controllerBeanName);
        handleDefaultEditorEventHandlers(element, controllerBeanName);

        if (element.hasAttr("path")) {
            handleDefaultValidator(element, element.attr("path"));
        }
    }

    private void failOnUIBuilderDatasourceDependentComponents(Element element) {
        ElementWalker.of(element).withProcessor(childElement -> {
            if (isUIBuilderDatasourceDependentComponentTag(childElement.tagName())) {
                throw new ItemEditableUnsupportedComponentException(
                    "The component with tag: " + childElement.tagName() + " is not supported in <" + UIBUILDER_ITEM_EDITABLE_TAG_NAME + ">");
            }
            return new Elements(childElement);
        }).walk();
    }

    private boolean isUIBuilderDatasourceDependentComponentTag(String tagName) {
        return tagName != null &&
            (tagName.endsWith("uibuilder-grid") ||
                tagName.equals("uibuilder-combobox"));
    }

    private void handleDefaultEditorEventHandlers(Element element, String controllerBean) {
        ElementWalker.of(element).withProcessor(childElement -> {
            if (childElement.hasAttr("default-event-handler")) {
                childElement.removeAttr("default-event-handler");
                childElement.attr("on-value-changed", controllerBean + "::inlineItemValueChange");
            }
            return new Elements(childElement);
        }).walk();
    }

    private void handleDefaultValidator(Element element, String path) {
        ElementWalker.of(element).withProcessor(childElement -> {
            if (childElement.hasAttr("default-validator")) {
                childElement.removeAttr("default-validator");
                addPathValidatorAttributes(childElement, path);
            }
            return new Elements(childElement);
        }).walk();
    }

    private void handleDefaultButtons(Element element, String controllerBean) {
        if (element.hasAttr("default-buttons")) {
            Document document = element.ownerDocument();
            Element editButton = createButton(document, controllerBean, "inlineItemEdit", "edit", null, false);
            editButton.attr("disabled", "[[!item._state.editAllowed]]");
            Element saveButton = createButton(document, controllerBean, "inlineItemSave", "check", "success", true);
            saveButton.attr("disabled", "[[!item._state.saveAllowed]]");
            Element cancelButton = createButton(document, controllerBean, "inlineItemCancel", "close", "error", true);
            element.appendChild(editButton);
            element.appendChild(saveButton);
            element.appendChild(cancelButton);
        }
    }

    private void addPathValidatorAttributes(Element element, String path) {
        element.attr("error-message", "[[item._state.errors." + path + "]]");
        element.attr("invalid", "[[item._state.errors." + path + "]]");
    }

    private void handleDefaultEditor(Element element, String controllerBean) {
        if (element.hasAttr("default-editor") && element.hasAttr("path")) {
            getPathBindingByAttribute(element).ifPresent(binding -> {
                Document document = element.ownerDocument();

                Element textField = document.createElement("vaadin-text-field");
                textField.attr("slot", "edit");
                textField.attr("value", binding);
                textField.attr("on-value-changed", controllerBean + "::inlineItemValueChange");
                textField.attr("style", "width: 100%");
                addPathValidatorAttributes(textField, element.attr("path"));

                element.appendChild(textField);
            });
        }
    }

    private Element createButton(Document document, String controllerBean, String method, String icon, String theme, boolean editMode) {
        Element button = document.createElement("vaadin-button");
        if (editMode) {
            button.attr("slot", "edit");
        }
        button.attr("on-click", controllerBean + "::" + method);
        button.attr("theme", "small icon tertiary" + (theme != null ? " " + theme : ""));
        Element buttonIcon = document.createElement("vaadin-icon");
        buttonIcon.attr("icon", "vaadin:" + icon);
        button.appendChild(buttonIcon);
        return button;
    }

    private Optional<String> getPathBindingByAttribute(Element element) {
        if (element.hasAttr("path")) {
            String path = element.attr("path").trim();
            if (!path.isEmpty()) {
                return Optional.of("{{item." + path + "}}");
            }
        }
        return Optional.empty();
    }

    private void ensurePath(Element element) {
        getPathBindingByAttribute(element).ifPresent(binding -> {
            if (!containsBinding(element, binding)) {
                element.insertChildren(0, new TextNode(binding));
            }
        });
    }

    private boolean containsBinding(Element element, String binding) {
        AtomicBoolean hasBinding = new AtomicBoolean(false);
        ElementWalker.of(element).withProcessor(currentElement -> {
            if (!hasBinding.get()) {
                if (currentElement.ownText().contains(binding)) {
                    hasBinding.set(true);
                }
                if (!currentElement.hasAttr("slot")) {
                    currentElement.attributes().forEach(attribute -> {
                        if (attribute.getValue().contains(binding)) {
                            hasBinding.set(true);
                        }
                    });
                }
            }
            return new Elements(currentElement);
        }).walk();
        return hasBinding.get();
    }

    private void ensureItemAttribute(Element element) {
        if (!element.hasAttr("item")) {
            element.attr("item", "[[item]]");
        }
    }
}
