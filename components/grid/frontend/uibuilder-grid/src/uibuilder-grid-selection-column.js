/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { html } from '@polymer/polymer/polymer-element.js';
import '@vaadin/grid/vaadin-grid-column.js';
import { GridSelectionColumn } from '@vaadin/grid/vaadin-grid-selection-column.js';

export class UibuilderGridSelectionColumn extends GridSelectionColumn {

    static get template() {
        return html`
            <template class="header" id="defaultHeaderTemplate">
                <vaadin-checkbox class="vaadin-grid-select-all-checkbox" aria-label="Select All" hidden$="[[_selectAllHidden]]"
                                 on-checked-changed="__onSelectAllCheckedChanged" checked="[[__isChecked(selectAll, __indeterminate)]]"
                                 indeterminate="[[__indeterminate]]"></vaadin-checkbox>
            </template>
            <template id="defaultBodyTemplate">
                <vaadin-checkbox aria-label="Select Row" checked="[[selected]]" on-checked-changed="_onRowSelectionChanged"></vaadin-checkbox>
            </template>
        `;
    }

    static get is() {
        return 'vaadin-uibuilder-grid-selection-column'
    }

    get _grid() {
        if (this._gridValueDisabled) {
            return null;
        }
        if (!this._gridValue) {
            this._gridValue = this._findHostGrid();
        }
        return this._gridValue;
    }

    connectedCallback() {
        this._gridValueDisabled = true;
        // prevent registering listeners to the grid
        super.connectedCallback();
        this._gridValueDisabled = false;

        // set the header checkbox to visible only when the grid is in multi select mode
        this._selectAllHidden = !this._isGridMultiSelect();

        // add selection change listener
        this._grid.addEventListener("changed", e => this._onSelectedItemsChanged(e));
    }

    _onRowSelectionChanged(e) {
        if (e.model.item) {
            if (this._isGridMultiSelect()) {
                this._grid.selectedItems = this._grid._toggleItemSelection(this._grid.selectedItems, e.model.item);
            } else {
                this._grid.selectedItems = e.detail.value ? [e.model.item] : [];
            }
        }
    }

    _onSelectedItemsChanged(e) {
        this._selectAllChangeLock = true;
        if (this._isGridMultiSelect()) {
            if (!this._grid.selectedItems.length) {
                this.selectAll = false;
                this.__indeterminate = false;
            } else if (
                this._grid._currentItems()
                && this._grid._currentItems().length > 0
                && this.__arrayContains(
                [...this._grid.selectedItems].map(item => this._grid._getKeyId(item)),
                [...this._grid._currentItems()].map(item => this._grid._getKeyId(item)))) {
                this.selectAll = true;
                this.__indeterminate = false;
            } else {
                this.selectAll = false;
                this.__indeterminate = true;
            }
        }
        this._selectAllChangeLock = false;
    }

    // override
    // noinspection JSUnusedGlobalSymbols
    __onSelectAllChanged(selectAll) {
        if (selectAll === undefined || !this._grid) {
            return;
        }

        if (this._selectAllChangeLock) {
            return;
        }

        this._grid.selectedItems = selectAll && this._isGridMultiSelect() ? this._grid._currentItems() : [];
    }

    _isGridMultiSelect() {
        return this._grid.selectionMode === 'multi';
    }

}

customElements.define(UibuilderGridSelectionColumn.is, UibuilderGridSelectionColumn);
