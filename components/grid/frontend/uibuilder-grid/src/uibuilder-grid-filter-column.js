/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import '@vaadin/grid/vaadin-grid-column.js';
import { GridFilterColumn } from '@vaadin/grid/vaadin-grid-filter-column.js';

export class UibuilderGridFilterColumn extends GridFilterColumn {

    static get is() {
        return 'vaadin-uibuilder-grid-filter-column'
    }

    _prepareHeaderTemplate() {
        if (!this.$) {
            return null;
        }

        const headerTemplate = this._prepareTemplatizer(this.$.headerTemplate);
        headerTemplate.templatizer.dataHost = this;
        return headerTemplate;
    }

}

customElements.define(UibuilderGridFilterColumn.is, UibuilderGridFilterColumn);
