/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import '@vaadin/flow-frontend/uibuilder/uibuilder-i18n/i18n-base-mixin.js';

export class UIBuilderItemEditable extends ThemableMixin(Uibuilder.I18NBaseMixin('uibuilder-item-editable', PolymerElement)) {

    static get template() {
        return html`
            <template is="dom-if" if="[[item._state.editMode]]">
                <slot name="edit"></slot>
            </template>
            <template is="dom-if" if="[[!item._state.editMode]]">
                <slot></slot>
            </template>
        `;
    }

    static get is() {
        return 'uibuilder-item-editable';
    }

    static get properties() {
        return {
            item: {
                type: Object,
            },
            path: {
                type: String,
            },
            controllerBean: {
                type: String,
            },
            defaultButtons: {
                type: Boolean,
            },
            defaultEditor: {
                type: Boolean,
            },
            disabled: {
                type: Boolean,
                notify: true,
                value: false,
                observer: "_onItemEditableDisabledChange"
            },
            _storeDisabled: {
                type: Array,
                value: []
            }
        };
    }

    _storeDefaultDisabled(disabled, node) {
        if (disabled && node.disabled) {
            this._storeDisabled.push(node);
        }
    }

    _nodeIsNotDefaultDisabled(node) {
        return !this._storeDisabled.includes(node);
    }

    _onItemEditableDisabledChange(disabled) {
        this.querySelectorAll("*").forEach(node => {
            if (node && node.tagName) {
                this._storeDefaultDisabled(disabled, node);

                if (disabled) {
                    node.disabled = disabled;
                } else {
                    if (this._nodeIsNotDefaultDisabled(node)) {
                        node.disabled = disabled;
                    }
                }
            }
        });
        if (!disabled) {
            this._storeDisabled = [];
        }
    }
}

customElements.define(UIBuilderItemEditable.is, UIBuilderItemEditable);
