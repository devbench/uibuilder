/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.detailpanel.interceptor;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.components.detailpanel.UIBuilderDetailPanel;
import io.devbench.uibuilder.components.form.UIBuilderFormRegistry;
import io.devbench.uibuilder.core.utils.ElementCollector;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class DetailPanelInstantiatorParseInterceptorTest {

    private DetailPanelInstantiatorParseInterceptor testObj;

    @BeforeEach
    void setUp() {
        testObj = new DetailPanelInstantiatorParseInterceptor();
    }

    @Test
    @DisplayName("only detail-panel tag should be applicable")
    void test_only_detail_panel_tag_should_be_applicable() {
        Element applicableElement = createElement("detail-panel", "detail-id");
        Element wrongTagElement = createElement("detail-not-panel", "detail-id");

        assertAll(
            () -> assertTrue(testObj.isApplicable(applicableElement)),
            () -> assertFalse(testObj.isApplicable(wrongTagElement))
        );
    }

    @Test
    @DisplayName("should always be an instantiator")
    void test_should_always_be_an_instantiator() {
        Element applicableElement = createElement("detail-panel", "detail-id");

        // always true
        assertAll(
            () -> assertTrue(testObj.isInstantiator(applicableElement)),
            () -> assertTrue(testObj.isInstantiator(null))
        );
    }

    @Test
    @DisplayName("should instantiate proper class")
    void test_should_instantiate_proper_class() {
        UIBuilderFormRegistry.setFormSetProvider(HashSet::new);
        Component component = testObj.instantiateComponent();

        assertAll(
            () -> assertNotNull(component),
            () -> assertTrue(component instanceof UIBuilderDetailPanel)
        );
    }

    @Test
    @DisplayName("should ensure ID during intercept")
    void test_should_ensure_id_during_intercept() {
        UIBuilderFormRegistry.setFormSetProvider(HashSet::new);
        Component component = testObj.instantiateComponent();
        Element element = createElement("detail-panel", null);

        assertFalse(component.getId().isPresent());

        testObj.intercept(component, element);

        assertFalse(component.getId().isPresent());

        element.attr(ElementCollector.ID, "test-id");

        testObj.intercept(component, element);

        assertAll(
            () -> assertTrue(component.getId().isPresent()),
            () -> assertEquals("test-id", component.getId().get())
        );
    }

    @Test
    @DisplayName("should set changedOnlyValidation on detailPanel when attribute is present")
    void test_should_set_changed_only_validation_on_detail_panel_when_attribute_is_present() throws Exception {
        UIBuilderDetailPanel<?> detailPanel = (UIBuilderDetailPanel<?>) testObj.instantiateComponent();
        Element detailPanelElement = createElement("detail-panel", "test-id");
        detailPanelElement.attr("changed-only-validation", true);

        assertFalse(detailPanel.isChangedOnlyValidation());

        testObj.intercept(detailPanel, detailPanelElement);

        assertTrue(detailPanel.isChangedOnlyValidation());
    }

    private Element createElement(String tag, String id) {
        Element element = new Element(tag);
        if (id != null) {
            element.attr(ElementCollector.ID, id);
        }
        return element;
    }

}
