/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.detailpanel;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.ElementData;
import io.devbench.uibuilder.components.detailpanel.event.*;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.core.utils.ElementCollector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class UIBuilderDetailPanelTest {

    private UIBuilderDetailPanel<Serializable> testObj;
    private UIBuilderForm<Serializable> internalForm;

    @BeforeEach
    void setUp() {
        org.jsoup.nodes.Element detailPanelRawElement = new org.jsoup.nodes.Element("detail-panel");
        detailPanelRawElement.attr(ElementCollector.ID, "dp-id");
        detailPanelRawElement.html("<div>field1</div><div>field2</div>");
        internalForm = createMockForm();

        testObj = new UIBuilderDetailPanel<Serializable>() {
            @Override
            protected UIBuilderForm<Serializable> instantiateInternalForm() {
                return internalForm;
            }
        };

        testObj.setRawElement(detailPanelRawElement);
    }

    @Test
    @DisplayName("should create internal form with the original content during attach")
    void test_should_create_internal_form_with_the_original_content_during_attach() {
        testObj.onAttached();

        ArgumentCaptor<org.jsoup.nodes.Element> rawElementCaptor = ArgumentCaptor.forClass(org.jsoup.nodes.Element.class);

        Mockito.verify(internalForm).setRawElement(rawElementCaptor.capture());
        Mockito.verify(internalForm, new Times(1)).onAttached();

        org.jsoup.nodes.Element internalFormRawElement = rawElementCaptor.getValue();

        assertAll(
            () -> assertNotNull(internalFormRawElement),
            () -> assertEquals(UIBuilderForm.TAG_NAME, internalFormRawElement.tagName()),
            () -> assertTrue(internalFormRawElement.hasAttr(ElementCollector.ID)),
            () -> assertEquals("__internalForm", internalFormRawElement.attr(ElementCollector.ID)),
            () -> assertEquals(2, internalFormRawElement.getElementsByTag("div").size()),
            () -> assertEquals("field1", internalFormRawElement.getElementsByTag("div").get(0).text()),
            () -> assertEquals("field2", internalFormRawElement.getElementsByTag("div").get(1).text())
        );
    }

    @Test
    @DisplayName("should delegate internal form's set/get form item")
    void test_should_delegate_internal_form_s_set_get_form_item() {
        testObj.onAttached();

        Serializable testItem = "Test item";
        testObj.setItem(testItem);

        ArgumentCaptor<Serializable> itemCaptor = ArgumentCaptor.forClass(Serializable.class);
        Mockito.verify(internalForm, new Times(1)).setFormItem(itemCaptor.capture());
        Mockito.doReturn(testItem).when(internalForm).getFormItem();

        assertSame(testItem, itemCaptor.getValue());
        assertSame(testItem, testObj.getItem());
    }

    @Test
    @DisplayName("should return proper default property values and be able to set")
    void test_should_return_proper_default_property_values_and_be_able_to_set() {
        assertAll("Default property values",
            () -> assertEquals("", testObj.getLegend()),
            () -> assertTrue(testObj.isBorderHidden()),
            () -> assertFalse(testObj.isFormControlsHidden()),
            () -> assertFalse(testObj.isDefaultSaveHidden()),
            () -> assertFalse(testObj.isDefaultResetHidden()),
            () -> assertFalse(testObj.isReadonly())
        );

        testObj.setLegend("legend");
        testObj.setBorderHidden(false);
        testObj.setFormControlsHidden(true);
        testObj.setDefaultSaveHidden(true);
        testObj.setDefaultResetHidden(true);
        testObj.setReadonly(true);

        assertAll("Modified properties",
            () -> assertEquals("legend", testObj.getElement().getProperty(UIBuilderDetailPanel.PROP_LEGEND)),
            () -> assertFalse(testObj.getElement().getProperty(UIBuilderDetailPanel.PROP_HIDE_BORDER, true)),
            () -> assertTrue(testObj.getElement().getProperty(UIBuilderDetailPanel.PROP_HIDE_FORM_CONTROLS, false)),
            () -> assertTrue(testObj.getElement().getProperty(UIBuilderDetailPanel.PROP_HIDE_DEFAULT_SAVE, false)),
            () -> assertTrue(testObj.getElement().getProperty(UIBuilderDetailPanel.PROP_HIDE_DEFAULT_RESET, false)),
            () -> assertTrue(testObj.getElement().getProperty(UIBuilderDetailPanel.PROP_READONLY, false))
        );
    }

    @Test
    @DisplayName("should add event listeners and receive event")
    void test_should_add_event_listeners_and_receive_events() {
        List<Class<? extends ComponentEvent<?>>> calledEvents = new ArrayList<>();

        testObj.addSaveListener(event -> calledEvents.add(event.getClass()));
        testObj.addResetListener(event -> calledEvents.add(event.getClass()));
        testObj.addCancelListener(event -> calledEvents.add(event.getClass()));
        testObj.addReadyListener(event -> calledEvents.add(event.getClass()));
        testObj.addValueChangedListener(event -> {
            calledEvents.add(event.getClass());
            assertAll("ValueChangeEvent properties",
                () -> assertEquals("propname", event.getPropertyName()),
                () -> assertEquals("oldval", event.getOldValue()),
                () -> assertEquals("newval", event.getNewValue())
            );
        });

        ComponentUtil.fireEvent(testObj, new DetailSaveEvent(testObj, false));
        ComponentUtil.fireEvent(testObj, new DetailResetEvent(testObj, false));
        ComponentUtil.fireEvent(testObj, new DetailCancelEvent(testObj, false));
        ComponentUtil.fireEvent(testObj, new DetailReadyEvent(testObj, false));
        ComponentUtil.fireEvent(testObj, new ValueChangedEvent(testObj, false,
            "propname", "oldval", "newval"));

        assertIterableEquals(Arrays.asList(
            DetailSaveEvent.class, DetailResetEvent.class, DetailCancelEvent.class,
            DetailReadyEvent.class, ValueChangedEvent.class), calledEvents);
    }

    private UIBuilderForm createMockForm() {
        UIBuilderForm mockForm = Mockito.mock(UIBuilderForm.class);
        Element mockFormElement = Mockito.mock(Element.class);
        StateNode mockFormElementStateNode = Mockito.mock(StateNode.class);

        Mockito.doReturn(mockFormElement).when(mockForm).getElement();
        Mockito.doReturn(mockFormElementStateNode).when(mockFormElement).getNode();
        Mockito.doReturn(Mockito.mock(ElementData.class)).when(mockFormElementStateNode).getFeature(Mockito.eq(ElementData.class));

        return mockForm;
    }

    @Test
    @DisplayName("should not throw npe when item set before onAttached")
    void test_should_not_throw_npe_when_item_set_before_on_attached() {
        testObj.setItem("item");
        Mockito.verify(internalForm).setFormItem(Mockito.eq("item"));
    }

    @Test
    @DisplayName("should return internalForm changedOnlyValidation property")
    void test_should_return_internal_form_changed_only_validation_property() throws Exception {
        Mockito.doReturn(true).when(internalForm).isChangedOnlyValidation();
        assertTrue(testObj.isChangedOnlyValidation());
        Mockito.verify(internalForm).isChangedOnlyValidation();
    }

    @Test
    @DisplayName("should set changedOnlyValidation on internalForm")
    void test_should_set_changed_only_validation_on_internal_form() throws Exception {
        testObj.setChangedOnlyValidation(true);
        Mockito.verify(internalForm).setChangedOnlyValidation(true);
    }
}
