/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UibuilderRichTextEditorConfig } from "../uibuilder-rich-text-editor.js";

export class UibuilderRichTextEditorToolbarConfig extends UibuilderRichTextEditorConfig {

    static get is() {
        return "toolbar-config"
    }

    static get properties() {
        return {

            /**
             * possible toolbar modes: "no", "minimal", "default", "extended", "full", "custom"
             */
            mode: {
                type: String,
                value: "default",
                notify: true
            },

            custom: {
                type: String,
                value: "[]",
                notify: true
            }
        }
    }

    constructor() {
        super();

        this.fontDecorators = ["bold", "italic", "underline", "strike"];
        this.codeBlocks = ["blockquote", "code-block"];
        this.mainHeaders = [{"header": 1}, {"header": 2}];
        this.lists = [{"list": "ordered"}, {"list": "bullet"}];
        this.scripts = [{"script": "sub"}, {"script": "super"}];
        this.indents = [{"indent": "-1"}, {"indent": "+1"}];
        this.headers = [{"header": [1, 2, 3, 4, 5, 6, false]}];
        this.colors = [{"color": []}, {"background": []}];
        this.fonts = [{"font": []}];
        this.sizes = [{"size": ["small", false, "large", "huge"]}];
        this.aligns = [{"align": []}];
        this.link = ["link"];
        this.image = ["image"];
        this.linkAndImage = ["link", "image"];
        this.cleanFormat = ["clean"];
    }

    preConfig(config) {
        super.preConfig(config);

        let toolbarOptions = false;

        if (this.mode === "minimal") {
            toolbarOptions = [
                this.fontDecorators, this.codeBlocks, this.aligns, this.lists, this.cleanFormat
            ];
        }

        if (this.mode === "extended") {
            toolbarOptions = [
                this.fontDecorators, this.codeBlocks, this.mainHeaders, this.headers, this.fonts,
                this.sizes, this.colors, this.aligns, this.lists, this.scripts, this.indents, this.cleanFormat
            ];
        }

        if (this.mode === "full") {
            toolbarOptions = [
                this.fontDecorators, this.codeBlocks, this.linkAndImage, this.mainHeaders, this.headers, this.fonts,
                this.sizes, this.colors, this.aligns, this.lists, this.scripts, this.indents, this.cleanFormat
            ];
        }

        if (this.mode === "default") {
            toolbarOptions = [
                this.fontDecorators, this.codeBlocks, this.headers, this.fonts,
                this.sizes, this.colors, this.aligns, this.lists, this.cleanFormat
            ];
        }

        if (this.mode === "custom") {
            toolbarOptions = JSON.parse(this.custom ? this.custom.replaceAll("'", "\"").trim() : "[]");
        }

        Object.assign(config, {
            modules: {
                toolbar: toolbarOptions
            }
        });
    }

    postConfig(quill) {
        super.postConfig(quill);
    }
}

customElements.define(UibuilderRichTextEditorToolbarConfig.is, UibuilderRichTextEditorToolbarConfig);
