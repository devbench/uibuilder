/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.richtext.config;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.dom.DisabledUpdateMode;

@Tag("theme-config")
@JsModule("./uibuilder-rich-text-editor/src/config/theme-config.js")
public class ThemeConfig extends Component {

    private static final PropertyDescriptor<String, String> PROP_THEME = PropertyDescriptors.propertyWithDefault("theme", "snow");

    @Synchronize(property = "theme", value = {"theme-changed"}, allowUpdates = DisabledUpdateMode.ALWAYS)
    public String getTheme() {
        return get(PROP_THEME);
    }

    public void setTheme(String theme) {
        set(PROP_THEME, theme);
    }

}
