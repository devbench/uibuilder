/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.richtext.config;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.dom.DisabledUpdateMode;

@Tag("toolbar-config")
@JsModule("./uibuilder-rich-text-editor/src/config/toolbar-config.js")
public class ToolbarConfig extends Component {

    public enum Mode {
        NO, MINIMAL, DEFAULT, EXTENDED, FULL, CUSTOM
    }

    private static final PropertyDescriptor<String, String> PROP_MODE = PropertyDescriptors.propertyWithDefault("mode", "default");
    private static final PropertyDescriptor<String, String> PROP_CUSTOM = PropertyDescriptors.propertyWithDefault("custom", "[]");

    @Synchronize(property = "mode", value = {"mode-changed"}, allowUpdates = DisabledUpdateMode.ALWAYS)
    public Mode getToolbarMode() {
        return Mode.valueOf(get(PROP_MODE).toUpperCase());
    }

    public void setToolbarMode(Mode toolbarMode) {
        set(PROP_MODE, toolbarMode.name().toLowerCase());
    }

    @Synchronize(property = "custom", value = {"custom-changed"}, allowUpdates = DisabledUpdateMode.ALWAYS)
    public String getCustom() {
        return get(PROP_CUSTOM);
    }

    public void setCustom(String custom) {
        set(PROP_CUSTOM, custom);
    }

}
