/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.richtext;

import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.internal.nodefeature.ElementListenerMap;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import io.devbench.quilldelta.Op;
import io.devbench.quilldelta.Ops;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({BaseUIBuilderTestExtension.class, MockitoExtension.class})
class UIBuilderRichTextEditorTest {

    private UIBuilderRichTextEditor testObj;

    @BeforeEach
    void setUp() {
        testObj = new UIBuilderRichTextEditor();
    }

    @Test
    @DisplayName("Should get default values")
    void test_should_get_default_values() {
        assertEquals(ValueMode.HTML, testObj.getValueMode());
        assertNull(testObj.getValue());
        assertNull(testObj.getValueAsHtmlText());
        assertEquals("\n", testObj.getValueAsPlainText());
        assertEquals(Ops.from(Op.insert("\n")), testObj.getValueAsOps());
    }

    @Test
    @DisplayName("Should get default values with backend html renderer")
    void test_should_get_default_values_with_backend_html_renderer() {
        testObj.setHtmlRenderMode(HtmlRenderMode.BACKEND);

        assertEquals(ValueMode.HTML, testObj.getValueMode());
        assertEquals("", testObj.getValue());
        assertEquals("", testObj.getValueAsHtmlText());
        assertEquals("\n", testObj.getValueAsPlainText());
        assertEquals(Ops.from(Op.insert("\n")), testObj.getValueAsOps());
    }

    @Test
    @DisplayName("Should set value by value mode set to HTML")
    void test_should_set_value_by_value_mode_set_to_html() {
        spyComponentElement();

        testObj.setValueMode(ValueMode.HTML);
        testObj.setValue("test <strong>bold</strong> text");

        verify(testObj.getElement()).executeJs(eq("$0.setValueAsHtmlText($1)"), same(testObj), eq("test <strong>bold</strong> text"));
    }

    @Test
    @DisplayName("Should set value by value mode set to plain text")
    void test_should_set_value_by_value_mode_set_to_plain_text() {
        spyComponentElement();

        testObj.setValueMode(ValueMode.PLAIN);
        testObj.setValue("test bold text");

        verify(testObj.getElement()).executeJs(eq("$0.setValueAsPlainText($1)"), same(testObj), eq("test bold text"));
    }

    @Test
    @DisplayName("Should set value by value mode set to ops")
    void test_should_set_value_by_value_mode_set_to_ops() {
        spyComponentElement();

        testObj.setValueMode(ValueMode.DELTA);
        testObj.setValue("{'ops': [{'insert': 'test '}, {'insert': 'bold', 'attributes': {'bold': true}}, {'insert': ' text'}]}");

        JsonArray jsonOps = Json.createArray();
        JsonObject jsonInsert = Json.createObject();
        jsonInsert.put("insert", Json.create("test "));
        jsonOps.set(0, jsonInsert);
        jsonInsert = Json.createObject();
        jsonInsert.put("insert", Json.create("bold"));
        JsonObject attributesJson = Json.createObject();
        attributesJson.put("bold", Json.create(true));
        jsonInsert.put("attributes", attributesJson);
        jsonOps.set(1, jsonInsert);
        jsonInsert = Json.createObject();
        jsonInsert.put("insert", Json.create(" text"));
        jsonOps.set(2, jsonInsert);
        JsonObject jsonObject = Json.createObject();
        jsonObject.put("ops", jsonOps);

        ArgumentCaptor<JsonObject> jsonObjectCaptor = ArgumentCaptor.forClass(JsonObject.class);
        verify(testObj.getElement()).executeJs(eq("$0.setValueAsOps($1)"), same(testObj), jsonObjectCaptor.capture());
        JsonObject value = jsonObjectCaptor.getValue();
        assertNotNull(value);
        assertTrue(value.hasKey("ops"));
        assertEquals(3, value.getArray("ops").length());
        assertEquals("test ", value.getArray("ops").getObject(0).getString("insert"));
        assertEquals("bold", value.getArray("ops").getObject(1).getString("insert"));
        assertTrue(value.getArray("ops").getObject(1).getObject("attributes").getBoolean("bold"));
        assertEquals(" text", value.getArray("ops").getObject(2).getString("insert"));
    }

    @Test
    @DisplayName("Should get value by value mode")
    void test_should_get_value_by_value_mode() {
        testObj.setHtmlRenderMode(HtmlRenderMode.FRONTEND);
        testObj.onAttached();

        JsonObject eventData = Json.createObject();
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_RESET, true);
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_OPS, createTestJsonOpsObject());
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_HTML, "this is a <u>te<i>st</i></u>");
        testObj.getElement().getNode().getFeature(ElementListenerMap.class)
            .fireEvent(new DomEvent(testObj.getElement(), "delta", eventData));

        testObj.setValueMode(ValueMode.DELTA);
        String order1 = "{\"ops\":[{\"insert\":\"this is a \"},{\"insert\":\"te\",\"attributes\":{\"underline\":true}}," +
            "{\"insert\":\"st\",\"attributes\":{\"underline\":true,\"italic\":true}}]}";
        String order2 = "{\"ops\":[{\"insert\":\"this is a \"},{\"insert\":\"te\",\"attributes\":{\"underline\":true}}," +
            "{\"insert\":\"st\",\"attributes\":{\"italic\":true,\"underline\":true}}]}";
        String deltaOpsString = testObj.getValue();
        assertTrue(deltaOpsString.equals(order1) || deltaOpsString.equals(order2));

        testObj.setValueMode(ValueMode.PLAIN);
        assertEquals("this is a test\n", testObj.getValue());

        testObj.setValueMode(ValueMode.HTML);
        assertEquals("this is a <u>te<i>st</i></u>", testObj.getValue());
    }

    @Test
    @DisplayName("Should modify delta by received ops and render proper html")
    void test_should_modify_delta_by_received_ops_and_render_proper_html() {
        testObj.setValueMode(ValueMode.HTML);
        testObj.setHtmlRenderMode(HtmlRenderMode.BACKEND);
        testObj.onAttached();

        JsonObject eventData = Json.createObject();
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_RESET, true);
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_OPS, createTestJsonOpsObject());
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_HTML, Json.createNull());
        testObj.getElement().getNode().getFeature(ElementListenerMap.class)
            .fireEvent(new DomEvent(testObj.getElement(), "delta", eventData));

        assertEquals("<p>this is a <u>te<em>st</em></u></p>\n", testObj.getValue());

        JsonObject op;
        JsonArray ops = Json.createArray();

        op = Json.createObject();
        op.put("retain", 11);
        ops.set(0, op);

        op = Json.createObject();
        op.put("delete", 2);
        ops.set(1, op);

        JsonObject opsObject = Json.createObject();
        opsObject.put("ops", ops);

        eventData = Json.createObject();
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_RESET, false);
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_OPS, opsObject);
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_HTML, Json.createNull());
        testObj.getElement().getNode().getFeature(ElementListenerMap.class)
            .fireEvent(new DomEvent(testObj.getElement(), "delta", eventData));

        assertEquals("<p>this is a <u>t<em>t</em></u></p>\n", testObj.getValue());

        ops = Json.createArray();

        op = Json.createObject();
        op.put("retain", 5);
        ops.set(0, op);

        op = Json.createObject();
        op.put("retain", 2);
        JsonObject attributes = Json.createObject();
        attributes.put("bold", true);
        op.put("attributes", attributes);
        ops.set(1, op);

        opsObject = Json.createObject();
        opsObject.put("ops", ops);

        eventData = Json.createObject();
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_RESET, false);
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_OPS, opsObject);
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_HTML, Json.createNull());
        testObj.getElement().getNode().getFeature(ElementListenerMap.class)
            .fireEvent(new DomEvent(testObj.getElement(), "delta", eventData));

        assertEquals("<p>this <strong>is</strong> a <u>t<em>t</em></u></p>\n", testObj.getValue());

        ops = Json.createArray();

        op = Json.createObject();
        op.put("retain", 11);
        ops.set(0, op);

        op = Json.createObject();
        op.put("retain", 1);
        attributes = Json.createObject();
        attributes.put("italic", Json.createNull());
        op.put("attributes", attributes);
        ops.set(1, op);

        opsObject = Json.createObject();
        opsObject.put("ops", ops);

        eventData = Json.createObject();
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_RESET, false);
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_OPS, opsObject);
        eventData.put(UIBuilderRichTextEditor.EVENT_DETAIL_HTML, Json.createNull());
        testObj.getElement().getNode().getFeature(ElementListenerMap.class)
            .fireEvent(new DomEvent(testObj.getElement(), "delta", eventData));

        assertEquals("<p>this <strong>is</strong> a <u>tt</u></p>\n", testObj.getValue());

    }

    private JsonObject createTestJsonOpsObject() {
        JsonObject op;
        JsonObject attributes;
        JsonArray ops = Json.createArray();

        op = Json.createObject();
        op.put("insert", "this is a ");
        ops.set(0, op);

        op = Json.createObject();
        op.put("insert", "te");
        attributes = Json.createObject();
        attributes.put("underline", true);
        op.put("attributes", attributes);
        ops.set(1, op);

        op = Json.createObject();
        op.put("insert", "st");
        attributes = Json.createObject();
        attributes.put("underline", true);
        attributes.put("italic", true);
        op.put("attributes", attributes);
        ops.set(2, op);

        JsonObject opsObject = Json.createObject();
        opsObject.put("ops", ops);
        return opsObject;
    }

    private void spyComponentElement() {
        testObj = spy(testObj);
        doReturn(spy(testObj.getElement())).when(testObj).getElement();
    }

}
