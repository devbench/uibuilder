/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html } from '@polymer/polymer/polymer-element.js';
import { templatize } from '@polymer/polymer/lib/utils/templatize.js';
import { ListBox } from '@vaadin/list-box/vaadin-list-box.js';
import { Item } from '@vaadin/item/vaadin-item.js';
import '@vaadin/flow-frontend/uibuilder/data/data-source-mixin.js';
import '@vaadin/flow-frontend/uibuilder-core/uibuilder-ready-listener-mixin.js';

export class UibuilderListBox extends Uibuilder.DataSourceMixin(Uibuilder.ReadyListenerMixin(ListBox)) {

    static get is() {
        return 'uibuilder-listbox'
    }

    static get template() {
        return html`
            <div part="items">
                <slot></slot>
                <slot name="tooltip"></slot>
            </div>
        `;
    }

    static get properties() {
        return Object.assign({}, ListBox.properties, {
            items: {
                type: Array,
                value: [],
                readOnly: true,
                notify: true
            },
            selectionMode: {
                type: String,
                value: "single",
                notify: true,
                reflectToAttribute: true,
                observer: "_onSelectionModeChange"
            },
            dataProvider: {
                type: Function,
                observer: '_onDataProviderChange'
            },
            selected: {
                type: Number,
                notify: true,
                observer: '_onSelectedChange'
            },
            selectedValues: {
                type: Array,
                notify: true,
                value: function () {
                    return [];
                },
                observer: '_onSelectedValuesChange'
            }
        });
    }

    connectedCallback() {
        super.connectedCallback();
        this._setupInstanceFactory();
    }

    _createEmptyItem() {
        let instanceProps = {}
        instanceProps['item'] = {}
        return instanceProps;
    }

    _setupInstanceFactory() {
        const instanceProps = this._createEmptyItem();
        const templateElement = this.querySelector("template:not([slot])");
        if (templateElement) {
            this.templateBindings = this._collectTemplateBindings(templateElement);
            if (!this._instanceFactory) {
                this._instanceFactory = templatize(templateElement, this, {
                    parentModel: true,
                    forwardHostProp(property, value) {
                    },
                    instanceProps: instanceProps,
                    notifyInstanceProp(instance, property, value) {
                    }
                });
            }
        }
    }

    _collectTemplateBindings(templateElement) {
        let innerBindings = [];
        if (templateElement && templateElement._templateInfo && templateElement._templateInfo.nodeInfoList) {
            templateElement._templateInfo.nodeInfoList.forEach(nodeInfo => {
                if (nodeInfo.bindings) {
                    nodeInfo.bindings.forEach(binding => {
                        if (binding.parts) {
                            binding.parts
                                .filter(part => part.source && part.mode === "{")
                                .forEach(part => {
                                    innerBindings.push(part.source);
                                });
                        }
                    })
                }
            });
        }
        return innerBindings;
    }

    _uibuilderReady() {

        this._processDataSourceTag();
    }

    _clearItemElements() {
        while (this.firstChild) {
            this.removeChild(this.lastChild);
        }
    }

    _onDataProviderChange() {
        this._clearItemElements();

        this.dataProvider({}, (receivedItems) => {
            this._clearItemElements();
            receivedItems.forEach(receivedItem => {
                const itemElement = new Item();
                itemElement.value = receivedItem;

                const instance = new this._instanceFactory({item: receivedItem});
                const instanceRoot = instance.root;

                itemElement.appendChild(instanceRoot);
                this.appendChild(itemElement);
            });
        });
    }

    _onSelectedChange(newSelected, oldSelected) {
        if (this.items) {

            const selectedItem = this.items[newSelected];
            const selectedItemValue = selectedItem ? selectedItem.value : null;

            const itemHolderEvent = new UibuilderListBox.ItemHolderEvent(
                "selection",
                selectedItemValue,
                {
                    detail: {
                        newSelectedIndex: newSelected,
                        oldSelectedIndex: oldSelected
                    }
                });

            this.dispatchEvent(itemHolderEvent);
            this.dispatchEvent(new CustomEvent("changed"));
        }
    }

    _onSelectedValuesChange(newSelectedValues, oldSelectedValues) {
        if (this.items) {

            let selectedItemValues = [];
            if (newSelectedValues) {
                for (let i = 0; i < newSelectedValues.length; i++) {
                    const selectedItem = this.items[newSelectedValues[i]];
                    selectedItemValues[i] = selectedItem ? selectedItem.value : null;
                }
            }

            const itemHolderEvent = new UibuilderListBox.ItemHolderEvent(
                "selection",
                selectedItemValues,
                {
                    detail: {
                        newSelectedIndexes: newSelectedValues,
                        oldSelectedIndexes: oldSelectedValues
                    }
                });

            this.dispatchEvent(itemHolderEvent);
            this.dispatchEvent(new CustomEvent("changed"));
        }
    }

    _onSelectionModeChange(newValue) {
        if (newValue === 'single') {
            this.multiple = false;
        } else if (newValue === 'multi') {
            this.multiple = true;
        }
    }

    _getItemKey(item) {
        if (item && item['___ITEM_KEY']) {
            return item['___ITEM_KEY'];
        }
        return null;
    }

    _getIndexByItem(item) {
        const itemKey = this._getItemKey(item);
        if (itemKey) {
            for (let i = 0; i < this.items.length; i++) {
                const itemValue = this.items[i].value;
                if (itemValue) {
                    const key = this._getItemKey(itemValue);
                    if (key === itemKey) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    _onItemsSelected(itemsToSelect) {
        let newSelectedItemIndexes = [];

        [...itemsToSelect].forEach(item => {
            const index = this._getIndexByItem(item);
            if (index !== -1) {
                newSelectedItemIndexes.push(index);
            }
        });

        if (this.multiple) {
            this.selectedValues = newSelectedItemIndexes;
        } else {
            if (newSelectedItemIndexes.length === 1) {
                this.selected = newSelectedItemIndexes[0];
            } else {
                this.selected = null;
            }
        }
    }

}

UibuilderListBox.ItemHolderEvent = class extends CustomEvent {
    constructor(type, item, eventInitDict = {}) {
        super(type, eventInitDict);
        this.model = {item};
    }
};

customElements.define(UibuilderListBox.is, UibuilderListBox);
