/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.listbox;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.shared.Registration;
import elemental.json.JsonArray;
import elemental.json.JsonNull;
import elemental.json.JsonObject;
import elemental.json.JsonValue;
import io.devbench.uibuilder.api.components.form.UIBuilderItemSelectable;
import io.devbench.uibuilder.api.utils.CollectionUtil;
import io.devbench.uibuilder.components.listbox.event.UIBuilderListBoxComponentRawSelectionChangeEvent;
import io.devbench.uibuilder.components.listbox.event.UIBuilderListBoxSelectionChangedEvent;
import io.devbench.uibuilder.components.listbox.exception.UIBuilderListBoxSelectionModeException;
import io.devbench.uibuilder.data.collectionds.datasource.component.AbstractDataSourceComponent;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Slf4j
@Tag(UIBuilderListBox.TAG_NAME)
@JsModule("./uibuilder-listbox/src/uibuilder-listbox.js")
public class UIBuilderListBox<T>
    extends AbstractDataSourceComponent<T>
    implements HasElement, UIBuilderItemSelectable<T>, HasValidation {

    public static final String TAG_NAME = "uibuilder-listbox";

    @Getter
    private List<T> selectedItems = Collections.emptyList();

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public void onAttached() {
        super.onAttached();
        addListener(UIBuilderListBoxComponentRawSelectionChangeEvent.class, (ComponentEventListener) event -> {
            UIBuilderListBoxComponentRawSelectionChangeEvent valueChangeEvent = (UIBuilderListBoxComponentRawSelectionChangeEvent) event;
            List<T> oldItems = selectedItems;
            List<T> newItems = resolveItemsBasedOnEvent(valueChangeEvent);
            selectedItems = newItems;
            if (!CollectionUtil.isCollectionsEqual(oldItems, newItems)) {
                ComponentUtil.fireEvent(this, new UIBuilderListBoxSelectionChangedEvent<>(this, event.isFromClient(), oldItems, newItems));
            }
        });
    }

    private List<T> resolveItemsBasedOnEvent(UIBuilderListBoxComponentRawSelectionChangeEvent valueChangeEvent) {
        JsonValue jsonValue = valueChangeEvent.getValue();
        CommonDataSource<T, ?, ?, ?> dataSource = getDataSource();
        if (dataSource != null && jsonValue != null && !(jsonValue instanceof JsonNull)) {
            if (jsonValue instanceof JsonObject) {
                return dataSource
                    .findItem((JsonObject) jsonValue)
                    .map(Collections::singletonList)
                    .orElse(Collections.emptyList());
            } else if (jsonValue instanceof JsonArray) {
                return dataSource.findItemsByJson((JsonArray) jsonValue);
            }
        }
        return Collections.emptyList();
    }

    private void refreshSelectedItems() {
        if (getDataSource() != null) {
            getElement().callJsFunction("_onItemsSelected", getDataSource().convertToKeysArray(selectedItems));
        }
    }

    @Override
    protected void setSelectedItemsIfItemWasSetPreviously() {
        refreshSelectedItems();
    }

    @Override
    public void setSelectedItems(Collection<T> items) {
        if (!isMultiSelection() && items.size() > 1) {
            throw new UIBuilderListBoxSelectionModeException("Cannot set multiple items when selection mode is single");
        }
        selectedItems = Collections.unmodifiableList(new ArrayList<>(items));
        refreshSelectedItems();
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Registration addSelectionChangedListener(ComponentEventListener<? extends SelectionChangedEvent<T>> listener) {
        return getEventBus().addListener(UIBuilderListBoxSelectionChangedEvent.class, (ComponentEventListener) listener);
    }

    @Synchronize(property = "multiple", value = {"multiple-changed"})
    public boolean isMultiSelection() {
        return getElement().getProperty("multiple", false);
    }

    public void setMultiSelection(boolean multiSelection) {
        getElement().setProperty("multiple", multiSelection);
    }

    @Synchronize(property = "invalid", value = {"invalid-changed"})
    public boolean isInvalid() {
        return getElement().getProperty("invalid", false);
    }

    public void setInvalid(boolean invalid) {
        getElement().setProperty("invalid", invalid);
    }

    public String getErrorMessage() {
        return getElement().getProperty("errorMessage");
    }

    public void setErrorMessage(String errorMessage) {
        getElement().setProperty("errorMessage", errorMessage == null ? "" : errorMessage);
    }
}

