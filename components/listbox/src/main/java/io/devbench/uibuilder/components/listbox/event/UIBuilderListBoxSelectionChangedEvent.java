/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.listbox.event;

import com.vaadin.flow.component.ComponentEvent;
import io.devbench.uibuilder.api.components.form.UIBuilderItemSelectable;
import io.devbench.uibuilder.components.listbox.UIBuilderListBox;
import lombok.Getter;

import java.util.List;

@Getter
public class UIBuilderListBoxSelectionChangedEvent<T> extends ComponentEvent<UIBuilderListBox<T>> implements UIBuilderItemSelectable.SelectionChangedEvent<T> {

    private final List<T> oldItems;
    private final List<T> selectedItems;

    public UIBuilderListBoxSelectionChangedEvent(UIBuilderListBox<T> source, boolean fromClient, List<T> oldItems, List<T> newItems) {
        super(source, fromClient);
        this.oldItems = oldItems;
        this.selectedItems = newItems;
    }

    @Override
    public T getSelectedItem() {
        return selectedItems.size() != 1 ? null : selectedItems.get(0);
    }
}
