/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.listbox.event;

import io.devbench.uibuilder.components.listbox.UIBuilderListBox;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UIBuilderListBoxSelectionChangedEventTest {

    @Mock
    private UIBuilderListBox<Object> component;

    private UIBuilderListBoxSelectionChangedEvent<Object> testObj;

    @Test
    @DisplayName("Should return selected item if selected items count is one")
    void test_should_return_selected_item_if_selected_items_count_is_one() {
        Object item = new Object();
        testObj = new UIBuilderListBoxSelectionChangedEvent<>(component, false,
            Collections.emptyList(),
            Collections.singletonList(item));

        Object selectedItem = testObj.getSelectedItem();

        assertEquals(1, testObj.getSelectedItems().size());
        assertSame(item, selectedItem);
    }

    @Test
    @DisplayName("Should return null if selected items count is 0")
    void test_should_return_null_if_selected_items_count_is_0_or_greater_than_one() {
        testObj = new UIBuilderListBoxSelectionChangedEvent<>(component, false,
            Collections.emptyList(),
            Collections.emptyList());

        Object selectedItem = testObj.getSelectedItem();

        assertEquals(0, testObj.getSelectedItems().size());
        assertNull(selectedItem);
    }

    @Test
    @DisplayName("Should return null if selected items count is greater than 1")
    void test_should_return_null_if_selected_items_count_is_greater_than_1() {
        testObj = new UIBuilderListBoxSelectionChangedEvent<>(component, false,
            Collections.emptyList(),
            Arrays.asList(new Object(), new Object(), new Object()));

        Object selectedItem = testObj.getSelectedItem();

        assertEquals(3, testObj.getSelectedItems().size());
        assertNull(selectedItem);
    }

}
