/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.listbox;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.server.VaadinSession;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import io.devbench.uibuilder.components.listbox.event.UIBuilderListBoxComponentRawSelectionChangeEvent;
import io.devbench.uibuilder.components.listbox.exception.UIBuilderListBoxSelectionModeException;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.data.collectionds.CollectionDataSource;
import io.devbench.uibuilder.data.collectionds.interceptors.ItemDataSourceBindingContext;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProviderEndpointManager;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class UIBuilderListBoxTest {

    @SuppressWarnings("rawtypes")
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(CommonDataSourceContext.class)
    private CommonDataSourceContext commonDataSourceContext;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(DataProviderEndpointManager.class)
    private DataProviderEndpointManager dataProviderEndpointManager;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(UIContext.class)
    private UIContext uiContext;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(DataSourceManager.class)
    private DataSourceManager dataSourceManager;

    @Mock
    @SuppressWarnings("unused")
    private VaadinSession vaadinSession;

    @BeforeEach
    public void setup() {
        reset(commonDataSourceContext, dataProviderEndpointManager, uiContext, dataSourceManager);
        final VaadinSession vaadinSession = VaadinSession.getCurrent();
        doReturn(uiContext).when(vaadinSession).getAttribute("UIBuilderSessionContext");
        doReturn(dataProviderEndpointManager).when(uiContext).computeIfAbsent(eq(DataProviderEndpointManager.class), any());
        doReturn(commonDataSourceContext).when(uiContext).computeIfAbsent(eq(CommonDataSourceContext.class), any());
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should receive selection changed event, resolve item and fire event with the resolved item")
    void test_should_receive_selection_changed_event_resolve_item_and_fire_event_with_the_resolved_item() {
        VaadinSession.getCurrent().setAttribute("UIBuilderSessionContext", uiContext);

        Object oldValue = new Object();
        Object newValue = new Object();

        CollectionDataSource<Object> dataSource = mock(CollectionDataSource.class);
        UIBuilderListBox<Object> testObj = new UIBuilderListBox<>();

        when(dataSourceManager.getDataSourceProvider().getBindingContextForName("ds-name", null))
            .thenReturn(mock(ItemDataSourceBindingContext.class));
        doReturn(commonDataSourceContext).when(uiContext).computeIfAbsent(eq(CommonDataSourceContext.class), any());
        when(commonDataSourceContext.replaceDataSource(any(), any(), any())).thenReturn(dataSource);

        testObj.connectItemDataSource("ds-name");
        testObj.setDataSource(dataSource);
        testObj.setSelectedItem(oldValue);

        JsonObject jsonObject = Json.createObject();
        jsonObject.put("___ITEM_KEY", "new-object-id");

        when(dataSource.findItem(jsonObject)).thenReturn(Optional.of(newValue));

        AtomicBoolean listenerCalled = new AtomicBoolean(false);
        testObj.onAttached();
        testObj.addSelectionChangedListener(event -> {
            listenerCalled.set(true);
            assertTrue(event.isFromClient());
            assertEquals(newValue, event.getSelectedItem());
        });

        ComponentUtil.fireEvent(testObj, new UIBuilderListBoxComponentRawSelectionChangeEvent(testObj, true, jsonObject));

        assertTrue(listenerCalled.get(), "listener should be called");
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should receive selection changed event, resolve items and fire event with the resolved items")
    void test_should_receive_selection_changed_event_resolve_items_and_fire_event_with_the_resolved_items() {
        VaadinSession.getCurrent().setAttribute("UIBuilderSessionContext", uiContext);

        List<Object> oldValue = new ArrayList<>();
        List<Object> newValue = Arrays.asList(new Object(), new Object());

        CollectionDataSource<Object> dataSource = mock(CollectionDataSource.class);
        UIBuilderListBox<Object> testObj = new UIBuilderListBox<>();

        when(dataSourceManager.getDataSourceProvider().getBindingContextForName("ds-name", null))
            .thenReturn(mock(ItemDataSourceBindingContext.class));
        doReturn(commonDataSourceContext).when(uiContext).computeIfAbsent(eq(CommonDataSourceContext.class), any());
        when(commonDataSourceContext.replaceDataSource(any(), any(), any())).thenReturn(dataSource);

        testObj.connectItemDataSource("ds-name");
        testObj.setDataSource(dataSource);
        testObj.setSelectedItems(oldValue);

        JsonObject jsonObject1 = Json.createObject();
        jsonObject1.put("___ITEM_KEY", "new-object-id-1");
        JsonObject jsonObject2 = Json.createObject();
        jsonObject2.put("___ITEM_KEY", "new-object-id-2");
        JsonArray jsonArray = Json.createArray();
        jsonArray.set(0, jsonObject1);
        jsonArray.set(1, jsonObject2);

        when(dataSource.findItemsByJson(jsonArray)).thenReturn(newValue);

        AtomicBoolean listenerCalled = new AtomicBoolean(false);
        testObj.onAttached();
        testObj.addSelectionChangedListener(event -> {
            listenerCalled.set(true);
            assertTrue(event.isFromClient());
            assertEquals(newValue, event.getSelectedItems());
        });

        ComponentUtil.fireEvent(testObj, new UIBuilderListBoxComponentRawSelectionChangeEvent(testObj, true, jsonArray));

        assertTrue(listenerCalled.get(), "listener should be called");
    }

    @Test
    @DisplayName("Should throw exception if setSelectedItems are called with an array and the component is in single select mode")
    void test_should_throw_exception_if_set_selected_items_are_called_with_an_array_and_the_component_is_in_single_select_mode() {
        UIBuilderListBox<Object> listBox = new UIBuilderListBox<>();
        listBox.setMultiSelection(false);

        assertThrows(UIBuilderListBoxSelectionModeException.class,
            () -> listBox.setSelectedItems(Arrays.asList(new Object(), new Object())));
    }

    @Test
    @DisplayName("Should set element attribute multiple")
    void test_should_set_element_attribute_multiple() {
        UIBuilderListBox<Object> listBox = new UIBuilderListBox<>();

        assertFalse(listBox.isMultiSelection());

        listBox.setMultiSelection(true);

        assertTrue(listBox.isMultiSelection());
    }

    @Test
    @DisplayName("Should set element attribute invalid")
    void test_should_set_element_attribute_invalid() {
        UIBuilderListBox<Object> listBox = new UIBuilderListBox<>();

        assertFalse(listBox.isInvalid());

        listBox.setInvalid(true);

        assertTrue(listBox.isInvalid());
    }

    @Test
    @DisplayName("Should set element attribute error message")
    void test_should_set_element_attribute_error_message() {
        UIBuilderListBox<Object> listBox = new UIBuilderListBox<>();

        assertNull(listBox.getErrorMessage());

        listBox.setErrorMessage("Dummy error message");

        assertEquals("Dummy error message", listBox.getErrorMessage());
    }

}
