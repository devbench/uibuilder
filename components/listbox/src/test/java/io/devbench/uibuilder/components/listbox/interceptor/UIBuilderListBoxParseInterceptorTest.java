/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.listbox.interceptor;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.components.listbox.UIBuilderListBox;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Optional;

import static io.devbench.uibuilder.core.utils.ElementCollector.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UIBuilderListBoxParseInterceptorTest {


    @Mock
    private Element element;

    @Mock
    private UIBuilderListBox<?> listBox;

    private UIBuilderListBoxParseInterceptor testObj;

    @BeforeEach
    void setUp() {
        testObj = new UIBuilderListBoxParseInterceptor();
    }

    @Test
    @DisplayName("Should be applicable only for uibuilder listbox")
    void test_should_be_applicable_only_for_uibuilder_listbox() {
        doReturn("other-tag").when(element).tagName();
        assertFalse(testObj.isApplicable(element), "Element with other tag name should not be applicable");

        doReturn(UIBuilderListBox.TAG_NAME).when(element).tagName();
        assertTrue(testObj.isApplicable(element), "Should be applicable element with tag name: " + UIBuilderListBox.TAG_NAME);
    }

    @Test
    @DisplayName("Should always be an instantiator")
    void test_should_always_be_an_instantiator() {
        assertTrue(testObj.isInstantiator(element), "Should be an instantiator");
    }

    @Test
    @DisplayName("Should instantiate an UIBuilderListBox")
    void test_should_instantiate_an_ui_builder_listbox() {
        Component component = testObj.instantiateComponent();

        assertNotNull(component);
        assertTrue(component instanceof UIBuilderListBox, "Instantiated component should be a UIBuilderListBox");
    }

    @Test
    @DisplayName("Should set ID on intercept")
    void test_should_set_id_on_intercept() {
        doReturn(Optional.empty()).when(listBox).getId();
        doReturn(true).when(element).hasAttr(ID);
        doReturn("test-id").when(element).attr(ID);

        testObj.intercept(listBox, element);

        verify(listBox).setId("test-id");
    }

    @Test
    @DisplayName("Should not set existing ID on intercept")
    void test_should_not_set_existing_id_on_intercept() {
        doReturn(Optional.of("test-id")).when(listBox).getId();
        doReturn(true).when(element).hasAttr(ID);
        doReturn("test-id").when(element).attr(ID);

        testObj.intercept(listBox, element);

        verify(listBox, never()).setId("test-id");
    }

    @Test
    @DisplayName("Should generate ID if not found")
    void test_should_generate_id_if_not_found() {
        doReturn(Optional.empty()).when(listBox).getId();
        doReturn(false).when(element).hasAttr(ID);

        testObj.intercept(listBox, element);

        ArgumentCaptor<String> idCaptor = ArgumentCaptor.forClass(String.class);
        verify(element).attr(eq(ID), idCaptor.capture());
        String generatedId = idCaptor.getValue();
        assertNotNull(generatedId);
        verify(listBox).setId(generatedId);
    }

    @Test
    @DisplayName("Should set raw element")
    void test_should_set_raw_element() {
        doReturn(Optional.of("test-id")).when(listBox).getId();
        testObj.intercept(listBox, element);
        verify(listBox).setRawElement(element);
    }

    @Test
    @DisplayName("Should set multiple if there is such attribute present")
    void test_should_set_multiple_if_there_is_such_attribute_present() {
        doReturn(Optional.of("test-id")).when(listBox).getId();
        doReturn(true).when(element).hasAttr("multiple");
        doReturn("").when(element).attr("multiple");

        com.vaadin.flow.dom.Element listBoxElement = mock(com.vaadin.flow.dom.Element.class);
        doReturn(listBoxElement).when(listBox).getElement();
        doAnswer(Answers.CALLS_REAL_METHODS).when(listBox).setMultiSelection(anyBoolean());

        testObj.intercept(listBox, element);

        verify(listBoxElement).setProperty(eq("multiple"), eq(true));
    }

    @Test
    @DisplayName("Should not set multiple if there is such an attribute, but with an illegal value")
    void test_should_not_set_multiple_if_there_is_such_an_attribute_but_with_an_illegal_value() {
        doReturn(Optional.of("test-id")).when(listBox).getId();
        doReturn(true).when(element).hasAttr("multiple");
        doReturn("false").when(element).attr("multiple");

        com.vaadin.flow.dom.Element listBoxElement = mock(com.vaadin.flow.dom.Element.class);
        doReturn(listBoxElement).when(listBox).getElement();
        doAnswer(Answers.CALLS_REAL_METHODS).when(listBox).setMultiSelection(anyBoolean());

        testObj.intercept(listBox, element);

        verify(listBoxElement, never()).setProperty(eq("multiple"), anyBoolean());
    }

    @Test
    @DisplayName("Should set multiple if there is an attribute called selection-mode with multi as value")
    void test_should_set_multiple_if_there_is_an_attribute_called_selection_mode_with_multi_as_value() {
        doReturn(Optional.of("test-id")).when(listBox).getId();
        doReturn(false).when(element).hasAttr("multiple");
        doReturn(true).when(element).hasAttr("selection-mode");
        doReturn("multi").when(element).attr("selection-mode");

        com.vaadin.flow.dom.Element listBoxElement = mock(com.vaadin.flow.dom.Element.class);
        doReturn(listBoxElement).when(listBox).getElement();
        doAnswer(Answers.CALLS_REAL_METHODS).when(listBox).setMultiSelection(anyBoolean());

        testObj.intercept(listBox, element);

        verify(listBoxElement).setProperty(eq("multiple"), eq(true));
    }

    @Test
    @DisplayName("Should set multiple if there is an attribute called selection-mode with single as value")
    void test_should_set_multiple_if_there_is_an_attribute_called_selection_mode_with_single_as_value() {
        doReturn(Optional.of("test-id")).when(listBox).getId();
        doReturn(false).when(element).hasAttr("multiple");
        doReturn(true).when(element).hasAttr("selection-mode");
        doReturn("single").when(element).attr("selection-mode");

        com.vaadin.flow.dom.Element listBoxElement = mock(com.vaadin.flow.dom.Element.class);
        doReturn(listBoxElement).when(listBox).getElement();
        doAnswer(Answers.CALLS_REAL_METHODS).when(listBox).setMultiSelection(anyBoolean());

        testObj.intercept(listBox, element);

        verify(listBoxElement).setProperty(eq("multiple"), eq(false));
    }

    @Test
    @DisplayName("Should not set multiple if there is an attribute called selection-mode with an invalid value")
    void test_should_not_set_multiple_if_there_is_an_attribute_called_selection_mode_with_an_invalid_value() {
        doReturn(Optional.of("test-id")).when(listBox).getId();
        doReturn(false).when(element).hasAttr("multiple");
        doReturn(true).when(element).hasAttr("selection-mode");
        doReturn("none").when(element).attr("selection-mode");

        com.vaadin.flow.dom.Element listBoxElement = mock(com.vaadin.flow.dom.Element.class);
        doReturn(listBoxElement).when(listBox).getElement();
        doAnswer(Answers.CALLS_REAL_METHODS).when(listBox).setMultiSelection(anyBoolean());

        testObj.intercept(listBox, element);

        verify(listBoxElement, never()).setProperty(eq("multiple"), anyBoolean());
    }

}
