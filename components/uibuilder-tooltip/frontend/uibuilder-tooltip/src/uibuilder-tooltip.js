/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ElementMixin } from '@vaadin/component-base/src/element-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';

export class UIBuilderTooltip extends ElementMixin(ThemableMixin(PolymerElement)) {

    static get template() {
        return html`
            <style>
                :host {
                    box-sizing: border-box;
                    padding: 0;
                    display: block;
                    outline: none;
                    z-index: 1000;
                    -moz-user-select: none;
                    -ms-user-select: none;
                    -webkit-user-select: none;
                    user-select: none;
                    position: absolute;
                }

                :host([hidden]) [part="content"] {
                    display: none !important;
                }

                /*noinspection ALL*/
                :host ::slotted(*) {
                    box-sizing: border-box;
                }

                [part="content"] {
                    background-color: black;
                    color: #fff;
                    text-align: center;
                    border-radius: 6px;
                    padding: 5px 10px;
                    box-sizing: border-box;
                    font-style: normal;
                    font-family: -apple-system, BlinkMacSystemFont, "Roboto", "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
                    text-decoration: none;
                    font-weight: normal;
                }

                .tooltip-above {
                    visibility: hidden;
                    opacity: 0;
                    transition: opacity 0.5s;
                }

                .tooltip-above::after {
                    content: " ";
                    position: absolute;
                    top: 100%;
                    left: 50%;
                    margin-left: -5px;
                    border-width: 5px;
                    border-style: solid;
                    border-color: black transparent transparent transparent;
                }

                .tooltip-below {
                    visibility: hidden;
                    opacity: 0;
                    transition: opacity 0.5s;
                }

                .tooltip-below::after {
                    content: " ";
                    position: absolute;
                    bottom: 100%;
                    left: 50%;
                    margin-left: -5px;
                    border-width: 5px;
                    border-style: solid;
                    border-color: transparent transparent black transparent;
                }

                .tooltip-left {
                    visibility: hidden;
                    opacity: 0;
                    transition: opacity 0.5s;
                }

                .tooltip-left::after {
                    content: " ";
                    position: absolute;
                    top: 50%;
                    left: 100%;
                    margin-top: -5px;
                    border-width: 5px;
                    border-style: solid;
                    border-color: transparent transparent transparent black;
                }

                .tooltip-right {
                    visibility: hidden;
                    opacity: 0;
                    transition: opacity 0.5s;
                }

                .tooltip-right::after {
                    content: " ";
                    position: absolute;
                    top: 50%;
                    right: 100%;
                    margin-top: -5px;
                    border-width: 5px;
                    border-style: solid;
                    border-color: transparent black transparent transparent;
                }

                .tooltip-show {
                    visibility: visible;
                    opacity: 0.8;
                }
            </style>

            <div id="contentWrapper" part="content">
                <slot></slot>
            </div>
        `;
    }

    static get is() {
        return "uibuilder-tooltip";
    }

    static get version() {
        return "0.1";
    }

    static get properties() {
        return {
            active: {
                type: Boolean,
                value: false,
                notify: true,
                observer: '_activeChanged'
            },

            /**
             * Position to the target element. (Possible values: above, below, left, right)
             */
            position: {
                type: String,
                value: 'above'
            },

            hidden: {
                type: Boolean,
                value: true,
                reflectToAttribute: true
            },

            elementId: {
                type: String,
                value: '',
            },

            _content: {
                type: String,
                notify: true
            }
        };
    }

    static get observers() {
        return [
            '_positionChanged(position, hidden)'
        ];
    }

    constructor() {
        super();
        this._boundShow = this.show.bind(this);
        this._boundHide = this.hide.bind(this);
    }

    connectedCallback() {
        this._addEventsToParent();
        super.connectedCallback();
    }

    disconnectedCallback() {
        this._removeEventsFromParent();
        super.disconnectedCallback();
    }

    ready() {
        super.ready();
        this._boundSlotChangeHandler = this._setContentToSlotInnerHtml.bind(this);
        this.shadowRoot.querySelector('slot').addEventListener('slotchange', this._boundSlotChangeHandler);
    }

    _setContentToSlotInnerHtml() {
        this._content = this.shadowRoot.querySelector('slot').innerHTML;
    }

    _addEventsToParent() {
        if (this.parentElement) {
            const targetElement = this._findTargetElement();
            targetElement.addEventListener('mouseenter', this._boundShow);
            targetElement.addEventListener('mouseleave', this._boundHide);
            targetElement.addEventListener('tap', this._boundHide);
        }
    }

    _removeEventsFromParent() {
        if (this.parentElement) {
            const targetElement = this._findTargetElement();
            targetElement.removeEventListener('mouseenter', this._boundShow);
            targetElement.removeEventListener('mouseleave', this._boundHide);
            targetElement.removeEventListener('tap', this._boundHide);
        }
    }

    _activeChanged(newValue, oldValue) {
        if (oldValue && !newValue && !this.hidden) {
            this.hidden = true;
            this._removeClassFromContent('tooltip-show');
        }
    }

    _setContentAsText(content) {
        this.shadowRoot.querySelector('slot').innerHTML = content;
        this._positionChanged(this.position, this.hidden);
    }

    _positionChanged(position, hidden) {
        if (!this.parentElement || hidden) {
            return;
        }

        const targetElement = this._findTargetElement();
        const parentElementOffset = {
            left: targetElement.offsetLeft,
            top: targetElement.offsetTop
        };

        const parentRect = targetElement.getBoundingClientRect();
        const thisRect = this.getBoundingClientRect();

        const horizontalOffset = (parentRect.width - thisRect.width) / 2;
        const verticalOffset = (parentRect.height - thisRect.height) / 2;

        const spacingOffset = 5;

        let left;
        let top;

        switch (position) {
            case 'above': {
                top = parentElementOffset.top - thisRect.height - spacingOffset;
                left = parentElementOffset.left + horizontalOffset;
                this._removePositionClassFromContent();
                this._addClassToContent("tooltip-above");
                break;
            }
            case 'below': {
                top = parentElementOffset.top + parentRect.height + spacingOffset;
                left = parentElementOffset.left + horizontalOffset;
                this._removePositionClassFromContent();
                this._addClassToContent("tooltip-below");
                break;
            }
            case 'left': {
                top = parentElementOffset.top + verticalOffset;
                left = parentElementOffset.left - thisRect.width - spacingOffset;
                this._removePositionClassFromContent();
                this._addClassToContent("tooltip-left");
                break;
            }
            case 'right': {
                top = parentElementOffset.top + verticalOffset;
                left = parentElementOffset.left + parentRect.width + spacingOffset;
                this._removePositionClassFromContent();
                this._addClassToContent("tooltip-right");
                break;
            }
            default: {
                this.active = false;
                throw new Error('Invalid position: ' + position);
            }
        }

        this.style.top = Math.max(0, top) + 'px';
        this.style.left = Math.max(0, left) + 'px';
    }

    _removePositionClassFromContent() {
        this._removeClassFromContent('tooltip-above');
        this._removeClassFromContent('tooltip-below');
        this._removeClassFromContent('tooltip-left');
        this._removeClassFromContent('tooltip-right');
    }

    _addClassToContent(className) {
        this.$.contentWrapper.classList.add(className);
    }

    _removeClassFromContent(className) {
        this.$.contentWrapper.classList.remove(className);
    }

    show() {
        if (this.active) {
            this.hidden = false;
            this._addClassToContent('tooltip-show');
        }
    }

    hide() {
        if (this.active) {
            this.hidden = true;
            this._removeClassFromContent('tooltip-show');
        }
    }

    _findTargetElement() {
        const parentElement = this.parentElement;
        const targetElement = this.elementId && this.elementId !== '' ? parentElement.querySelector(`#${this.elementId}`) : null;
        if (targetElement) {
            return targetElement;
        } else {
            return parentElement;
        }
    }


}

customElements.define(UIBuilderTooltip.is, UIBuilderTooltip);
