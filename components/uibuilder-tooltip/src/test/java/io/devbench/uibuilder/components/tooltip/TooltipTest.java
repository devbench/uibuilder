/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.tooltip;

import com.vaadin.flow.dom.Element;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TooltipTest {

    @Mock
    private Element element;

    private Tooltip testObj;

    @BeforeEach
    void setup() {
        testObj = Mockito.spy(new Tooltip());
        when(testObj.getElement()).thenReturn(element);
    }

    @Test
    @DisplayName("Should call _setContentAsText on frontend, when setText is called")
    public void should_call_set_content_as_text_on_frontend_when_set_text_is_called() {
        testObj.setText("test");

        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        verify(element).callJsFunction(eq("_setContentAsText"), textCaptor.capture());
        assertEquals("test", textCaptor.getValue());
        verify(element).setProperty("_content", "test");
    }

    @Test
    @DisplayName("Should throw exception when setText called with null")
    public void should_throw_exception_when_set_text_called_with_null() {
        assertThrows(NullPointerException.class, () -> testObj.setText(null));
    }

    @Test
    @DisplayName("Should get _content property, when getText called")
    public void should_get_content_property_when_get_text_called() {
        when(element.getProperty("_content")).thenReturn("test");

        String text = testObj.getText();

        assertAll(
            () -> verify(element).getProperty("_content"),
            () -> assertEquals("test", text)
        );
    }

    @Test
    @DisplayName("Should set active property when setActive called")
    public void should_set_active_property_when_set_active_called() {
        testObj.setActive(true);

        verify(element).setProperty("active", true);
    }

    @Test
    @DisplayName("Should get active property when getActive called")
    public void should_get_active_property_when_get_active_called() {
        when(element.getProperty("active", false)).thenReturn(true);

        boolean active = testObj.isActive();

        assertAll(
            () -> verify(element).getProperty("active", false),
            () -> assertTrue(active)
        );
    }

}
