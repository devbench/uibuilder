/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.tooltip;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Synchronize;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@Tag("uibuilder-tooltip")
@JsModule("./uibuilder-tooltip/src/uibuilder-tooltip.js")
public class Tooltip extends Component {

    public void setText(@NotNull String text) {
        Objects.requireNonNull(text);
        getElement().setProperty("_content", text);
        getElement().callJsFunction("_setContentAsText", text);
    }

    @Synchronize(value = "_content-changed", property = "_content")
    public String getText() {
        return getElement().getProperty("_content");
    }

    public void setActive(boolean active) {
        getElement().setProperty("active", active);
    }


    @Synchronize("active-changed")
    public boolean isActive() {
        return getElement().getProperty("active", false);
    }
}
