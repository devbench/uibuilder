/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.combobox;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.components.combobox.event.ComboBoxComponentValueChangeEvent;
import io.devbench.uibuilder.components.combobox.interceptor.UIBuilderComboBoxParseInterceptor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

public class UIBuilderComboBoxCustomValueTest {

    private UIBuilderComboBox<?> testComboBox;
    private UIBuilderComboBoxParseInterceptor testParseInterceptor;

    @BeforeEach
    void setUp() {
        testComboBox = new UIBuilderComboBox<>();
        testParseInterceptor = new UIBuilderComboBoxParseInterceptor();
    }

    @Nested
    class InterceptorTests {
        @Test
        @DisplayName("Interceptor should set allow-custom-value property for the combobox instance")
        void test_interceptor_should_set_allow_custom_value_property_for_the_combobox_instance() {
            String comboboxElementHtml = "<uibuilder-combobox id=\"testCb\" allow-custom-value>";
            Element comboboxElement = Jsoup.parse(comboboxElementHtml).getElementById("testCb");

            assertFalse(testComboBox.isAllowCustomValue());
            testParseInterceptor.intercept(testComboBox, comboboxElement);
            assertTrue(testComboBox.isAllowCustomValue());
        }

        @Test
        @DisplayName("Interceptor should set custom-value-function ID for the combobox instance")
        void test_interceptor_should_set_custom_value_function_id_for_the_combobox_instance() {
            String comboboxElementHtml = "<uibuilder-combobox id=\"testCb\" custom-value-function=\"4c28205e-a736-4d14-906d-05cefffcb32c\">";
            Element comboboxElement = Jsoup.parse(comboboxElementHtml).getElementById("testCb");
            UIBuilderComboBox<?> observableTestComboBox = Mockito.spy(testComboBox);

            testParseInterceptor.intercept(observableTestComboBox, comboboxElement);
            Mockito.verify(observableTestComboBox).setCustomValueFunctionId(Mockito.eq("4c28205e-a736-4d14-906d-05cefffcb32c"));
        }
    }

    @Nested
    class ComponentTests {

        @Test
        @DisplayName("Value should be null after custom value, if allow-custom-value is set but custom-value-function is missing")
        void test_value_should_be_null_after_custom_value_if_allow_custom_value_is_set_but_custom_value_function_is_missing() {
            testComboBox.onAttached();
            testComboBox.setAllowCustomValue(true);

            ComboBoxComponentValueChangeEvent event = new ComboBoxComponentValueChangeEvent(testComboBox, true, "customValue");
            ComponentUtil.fireEvent(testComboBox, event);

            assertNull(testComboBox.getValue());
        }

        @Test
        @DisplayName("Should create item from custom value if allow-custom-value and custom-value-function is present")
        void test_should_create_item_from_custom_value_if_allow_custom_value_and_custom_value_function_is_present() {
            Object testItem = new Object();
            VaadinSession.getCurrent().setAttribute("4c28205e-a736-4d14-906d-05cefffcb32c", (Function<String, Object>) customValue -> testItem);

            testComboBox.onAttached();
            testComboBox.setAllowCustomValue(true);
            testComboBox.setCustomValueFunctionId("4c28205e-a736-4d14-906d-05cefffcb32c");

            ComboBoxComponentValueChangeEvent event = new ComboBoxComponentValueChangeEvent(testComboBox, true, "customValue");
            ComponentUtil.fireEvent(testComboBox, event);

            assertSame(testItem, testComboBox.getValue());
        }

    }

}
