/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

const $_documentContainer = html`
<dom-module id="uibuilder-menu-item-style" theme-for="menu-item">
    <template>
        <style>
            :host {
                color: var(--uibuilder-lumo-menu-item-bg-color);
            }

            [part="button"] {
                border-radius: var(--lumo-border-radius);
            }

            .disabled {
                color: var(--lumo-disabled-text-color);
            }

            .btn-selected {
                color: var(--uibuilder-lumo-menu-selected-color);
                background-color: var(--uibuilder-lumo-menu-selected-bg-color);
            }

            .btn-unselected:hover {
                background-color: var(--uibuilder-lumo-menu-hover-bg-color);
                transition: background-color .5s ease;
            }

            .submenuLeftIndicator {
                top: 2px;
            }

            .submenuLeftIndicator::after {
                font-family: 'lumo-icons';
                font-size: var(--lumo-icon-size-m);
                content: var(--lumo-icons-chevron-left);
            }

            .submenuRightIndicator {
                top: 2px;
            }

            .submenuRightIndicator::after {
                font-family: 'lumo-icons';
                font-size: var(--lumo-icon-size-m);
                content: var(--lumo-icons-chevron-right);
            }

            [part="submenu"] {
                background-color: var(--uibuilder-lumo-menu-bg-color);
                border-left: var(--uibuilder-lumo-border);
                border-right: var(--uibuilder-lumo-border);
                border-bottom: var(--uibuilder-lumo-border);
            }

            :host([more]) [part="title"] {
                font-weight: bold;
            }
        </style>
    </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);
