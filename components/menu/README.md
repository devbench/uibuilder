# UIBuilder menu component

* **maven dependency**

  ```xml
  <dependency>
      <groupId>io.devbench.uibuilder.components</groupId>
      <artifactId>uibuilder-menu</artifactId>
      <version>2.0.0</version>
  </dependency>
  ```

* **frontend** (html, css, javascript)

  ```html
  <menu-bar>
      <menu-item name="File">
          <menu-item name="Reset" on-activate="menuItemActivated"></menu-item>
          <menu-item name="Actions">
              <menu-item name="One"></menu-item>
              <menu-item name="Two" on-activate="menuItemActivated"></menu-item>
              <menu-item name="Three"></menu-item>
          </menu-item>
          <ht/>
          <menu-item name="Exit"></menu-item>
      </menu-item>
      <menu-item name="Edit" disabled></menu-item>
      <menu-item name="Help">
          <menu-item name="About" disabled></menu-item>
          <menu-item id="cfu" name="Check for update" on-activate="menuItemActivated"></menu-item>
      </menu-item>
  </menu-bar>
  ```

* **backend** (java)

  ```java
  @ControllerBean(name = "todo", bindTo = "/todo.html")
  public class ControllerBean {
      
      @UIComponent("cfu")
      private UibuilderMenuItem checkForUpdateMenuItem;
      
      @UIEventHandler("menuItemActivated")
      public void menuItemActivated(@Value("detail.menuItem.name") String name) {
          if ("Reset".equals(name)) {
              reset();
          }
      }
      
  }
  ```
