/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.calendar.data;

import com.vaadin.flow.component.JsonSerializable;
import elemental.json.JsonObject;
import lombok.*;

import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TimeSlot extends AbstractCalendarData {
    private Instant start;
    private Instant end;
    private boolean allDay;

    @Override
    public JsonObject toJson() {
        throw new UnsupportedOperationException();
    }

    @Override
    public JsonSerializable readJson(JsonObject value) {
        start = getInstant(value, "start");
        end = getInstant(value, "end");
        allDay = value.getBoolean("allDay");
        return this;
    }
}
