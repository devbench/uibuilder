/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.window;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.shared.Registration;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Tag("uibuilder-window")
@JsModule("./uibuilder-window/src/uibuilder-window.js")
public class UIBuilderWindow extends Component implements HasComponents, HasElement, HasSize, HasStyle {

    public UIBuilderWindow(Component... components) {
        addComponents(components);
    }

    @Synchronize("opened-changed")
    public String getOpened() {
        return getElement().getProperty("opened");
    }

    public void setOpened(Boolean open) {
        getElement().setProperty("opened", open);
        this.fireEvent(new UIBuilderWindow.ChangeEvent(this, false));
    }

    @Synchronize("header-text-changed")
    public String getHeaderText() {
        return getElement().getProperty("headerText");
    }

    public void setHeaderText(String headerText) {
        getElement().setProperty("headerText", headerText);
        this.fireEvent(new UIBuilderWindow.ChangeEvent(this, false));
    }

    @Synchronize("resize-changed")
    public String getResize() {
        return getElement().getProperty("resize");
    }

    public void setResize(String resize) {
        getElement().setProperty("resize", resize);
        this.fireEvent(new UIBuilderWindow.ChangeEvent(this, false));
    }

    @Synchronize("closeable-changed")
    public String getCloseable() {
        return getElement().getProperty("closeable");
    }

    public void setCloseable(Boolean closeable) {
        getElement().setProperty("closeable", closeable);
        this.fireEvent(new UIBuilderWindow.ChangeEvent(this, false));
    }

    protected void addComponents(Component... components) {
        for (Component component : components) {
            component.getElement().setAttribute("slot", "layout");
            getElement().appendChild(component.getElement());
        }
    }

    public Registration addChangeListener(
        ComponentEventListener<ChangeEvent> listener) {
        return addListener(ChangeEvent.class, listener);
    }

    public void close() {
        setOpened(false);
    }

    public void open() {
        setOpened(true);
    }

    @DomEvent("value-changed")
    public static class ChangeEvent extends ComponentEvent<UIBuilderWindow> {
        public ChangeEvent(UIBuilderWindow source, boolean fromClient) {
            super(source, fromClient);
        }
    }
}
