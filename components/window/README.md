## uibuilder-window

A window jelenleg megjeleníti a content-et, bezárható és átméretezhető.

Használata

- Html:

```html
<uibuilder-window id="window">
   content
</uibuilder-window>
```


- Java:

```
    @UIComponent("window")
    private UIBuilderWindow uiBuilderWindow;
...

    @UIEventHandler("showPopup")
    public void showPopup() {
        uiBuilderWindow.setOpened({true|false});
    }

```