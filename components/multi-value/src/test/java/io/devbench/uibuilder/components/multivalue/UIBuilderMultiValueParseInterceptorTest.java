/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.multivalue;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextField;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, JsoupExtension.class})
class UIBuilderMultiValueParseInterceptorTest {

    private UIBuilderMultiValueParseInterceptor testObj;

    @BeforeEach
    void setUp() {
        testObj = new UIBuilderMultiValueParseInterceptor();
    }

    @Test
    @DisplayName("Interceptor should by applied to sub elements")
    void test_interceptor_should_by_applied_to_sub_elements() {
        assertTrue(testObj.appliesToSubElements());
    }

    @Test
    @DisplayName("Should create binding context with template bean name")
    void test_should_create_binding_context_with_template_bean_name() {
        Element element = new Element("test-element");

        UIBuilderMultiValueBindingContext bindingContext = testObj.createBindingContext(element);
        assertNotNull(bindingContext);
        assertEquals("item", bindingContext.getTemplateBeanName());

        element.attr("as", "element");

        bindingContext = testObj.createBindingContext(element);
        assertNotNull(bindingContext);
        assertEquals("element", bindingContext.getTemplateBeanName());
    }

    @Test
    @DisplayName("Should handle binding if template bean name is the binding path root")
    void test_should_handle_binding_if_template_bean_name_is_the_binding_path_root() {
        UIBuilderMultiValueBindingContext bindingContext = new UIBuilderMultiValueBindingContext("element");

        assertFalse(testObj.handleBinding("item.text", bindingContext));
        assertFalse(bindingContext.getBindings().contains("item.text"));

        assertTrue(testObj.handleBinding("element", bindingContext));
        assertTrue(bindingContext.getBindings().contains("element"));

        assertTrue(testObj.handleBinding("element.text", bindingContext));
        assertTrue(bindingContext.getBindings().contains("element.text"));
    }

    @Test
    @DisplayName("Should commit binding context")
    void test_should_commit_binding_context() {
        UIBuilderMultiValueBindingContext bindingContext = new UIBuilderMultiValueBindingContext("element");
        bindingContext.getBindings().add("element.text");

        TextField notCompatibleComponent = new TextField();
        testObj.commitBindingParse(bindingContext, notCompatibleComponent);

        // dumb test for the still modifiable set after a non-compatible component binding parse commit
        bindingContext.getBindings().add("test");
        bindingContext.getBindings().remove("test");

        UIBuilderMultiValue<?, ?> compatibleComponent = mock(UIBuilderMultiValue.class);
        testObj.commitBindingParse(bindingContext, compatibleComponent);
        assertThrows(UnsupportedOperationException.class, () -> bindingContext.getBindings().add("anything"));

        ArgumentCaptor<UIBuilderMultiValueBindingContext> bindingContextArgumentCaptor =
            ArgumentCaptor.forClass(UIBuilderMultiValueBindingContext.class);
        Mockito.verify(compatibleComponent).setBindingContext(bindingContextArgumentCaptor.capture());

        assertSame(bindingContext, bindingContextArgumentCaptor.getValue());
    }

    @Test
    @DisplayName("Should add generated ID if item-bind attribute is present, and id attribute is missing")
    void test_should_add_generated_id_if_item_bind_attribute_is_present_and_id_attribute_is_missing() {
        Element element = new Element("test-element");
        testObj.transform(element);

        assertFalse(element.hasAttr("id"));
        assertFalse(element.hasAttr("item-bind"));

        element.attr("item-bind", "anything");
        testObj.transform(element);

        assertTrue(element.hasAttr("id"));
        assertTrue(element.hasAttr("item-bind"));

        element.attr("id", "custom-id");
        testObj.transform(element);

        assertEquals("custom-id", element.attr("id"));
    }

    @Test
    @DisplayName("Should fix binding when item-bind attribute is present and the binding is for a primitive type and item-value-path is default")
    void test_should_fix_binding_when_item_bind_attribute_is_present_and_the_binding_is_for_a_primitive_type_and_item_value_path_is_default() {
        Element element = new Element("test-element");
        element.attr("item-bind", "somePrimitiveList");

        Element template = new Element("template");
        Element inTemplateComponent = new Element("vaadin-text");
        inTemplateComponent.attr("value", "{{item}}");
        Element inTemplateSpan = new Element("span");
        inTemplateSpan.text("_ {{item}} _");

        template.appendChild(inTemplateComponent);
        template.appendChild(inTemplateSpan);
        element.appendChild(template);

        testObj.transform(element);

        assertEquals("{{item.value}}", element.getElementsByTag("vaadin-text").get(0).attr("value"));
        assertEquals("_ {{item.value}} _", element.getElementsByTag("span").get(0).text());
    }

    @Test
    @DisplayName("Should fix binding when item-bind attribute is present and the binding is for a primitive type and value-item-path is set")
    void test_should_fix_binding_when_item_bind_attribute_is_present_and_the_binding_is_for_a_primitive_type_and_value_item_path_is_set() {
        Element element = new Element("test-element");
        element.attr("item-bind", "somePrimitiveList");
        element.attr("value-item-path", "data");

        Element template = new Element("template");
        Element inTemplateComponent = new Element("vaadin-text");
        inTemplateComponent.attr("value", "{{item}}");
        Element inTemplateSpan = new Element("span");
        inTemplateSpan.text("_ {{item}} _");

        template.appendChild(inTemplateComponent);
        template.appendChild(inTemplateSpan);
        element.appendChild(template);

        testObj.transform(element);

        assertEquals("{{item.data}}", element.getElementsByTag("vaadin-text").get(0).attr("value"));
        assertEquals("_ {{item.data}} _", element.getElementsByTag("span").get(0).text());
    }

    @Test
    @DisplayName("Should fix binding when item-bind attribute is present and the binding is for a primitive type and value-item-path and as are set")
    void test_should_fix_binding_when_item_bind_attribute_is_present_and_the_binding_is_for_a_primitive_type_and_value_item_path_and_as_are_set() {
        Element element = new Element("test-element");
        element.attr("as", "elem");
        element.attr("item-bind", "somePrimitiveList");
        element.attr("value-item-path", "data");

        Element template = new Element("template");
        Element inTemplateComponent = new Element("vaadin-text");
        inTemplateComponent.attr("value", "{{elem}}");
        Element inTemplateSpan = new Element("span");
        inTemplateSpan.text("_ {{elem}} _");

        template.appendChild(inTemplateComponent);
        template.appendChild(inTemplateSpan);
        element.appendChild(template);

        testObj.transform(element);

        assertEquals("{{elem.data}}", element.getElementsByTag("vaadin-text").get(0).attr("value"));
        assertEquals("_ {{elem.data}} _", element.getElementsByTag("span").get(0).text());
    }

    @Test
    @DisplayName("Should not run find any parent crud id nor binding path when no item-bind attribute present")
    void test_should_not_run_find_any_parent_crud_id_nor_binding_path_when_no_item_bind_attribute_present(
        @LoadElement(elemantName = "uibuilder-multi-value", value = "/no-item-bind.html") Element element) {

        UIBuilderMultiValue<?, ?> component = mock(UIBuilderMultiValue.class);

        testObj.intercept(component, element);

        Mockito.verify(component, Mockito.never()).setParentCrudElementId(Mockito.any(String.class));
        Mockito.verify(component, Mockito.never()).setParentCrudElementBindingPropertyPath(Mockito.any(String.class));
    }

    @Test
    @DisplayName("Should not run find any parent crud if nor binding path when item-bind is not an items: prefixed path")
    void test_should_not_run_find_any_parent_crud_if_nor_binding_path_when_item_bind_is_not_an_items_prefixed_path(
        @LoadElement(elemantName = "uibuilder-multi-value", value = "/no-items-prefix.html") Element element) {

        UIBuilderMultiValue<?, ?> component = mock(UIBuilderMultiValue.class);

        testObj.intercept(component, element);

        Mockito.verify(component, Mockito.never()).setParentCrudElementId(Mockito.any(String.class));
        Mockito.verify(component, Mockito.never()).setParentCrudElementBindingPropertyPath(Mockito.any(String.class));
    }

    @Test
    @DisplayName("Should find parent crud property path, but should not find parent crud id, thus there aren't one present")
    void test_should_find_parent_crud_property_path_but_should_not_find_parent_crud_id_thus_there_aren_t_one_present(
        @LoadElement(elemantName = "uibuilder-multi-value", value = "/no-parent-crud.html") Element element) {

        UIBuilderMultiValue<?, ?> component = mock(UIBuilderMultiValue.class);

        testObj.intercept(component, element);

        Mockito.verify(component).setParentCrudElementId(Mockito.isNull());

        ArgumentCaptor<String> propertyPathCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(component).setParentCrudElementBindingPropertyPath(propertyPathCaptor.capture());

        String propertyPath = propertyPathCaptor.getValue();
        assertNotNull(propertyPath);
        assertEquals("testPropPath", propertyPath);
    }

    @Test
    @DisplayName("Should find parent form id and property path")
    void test_should_find_parent_form_id_and_property_path(
        @LoadElement(elemantName = "uibuilder-multi-value", value = "/parent-form.html") Element element) {

        UIBuilderMultiValue<?, ?> component = mock(UIBuilderMultiValue.class);

        testObj.intercept(component, element);

        ArgumentCaptor<String> parentCrudIdCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(component).setParentCrudElementId(parentCrudIdCaptor.capture());

        ArgumentCaptor<String> propertyPathCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(component).setParentCrudElementBindingPropertyPath(propertyPathCaptor.capture());

        String parentCrudId = parentCrudIdCaptor.getValue();
        String propertyPath = propertyPathCaptor.getValue();

        assertNotNull(propertyPath);
        assertEquals("testPropPath", propertyPath);

        assertNotNull(parentCrudId);
        assertEquals("parentForm", parentCrudId);
    }

    @Test
    @DisplayName("Should find parent crud-panel id and property path")
    void test_should_find_parent_crud_panel_id_and_property_path(
        @LoadElement(elemantName = "uibuilder-multi-value", value = "/parent-crud-panel.html") Element element) {

        UIBuilderMultiValue<?, ?> component = mock(UIBuilderMultiValue.class);

        testObj.intercept(component, element);

        ArgumentCaptor<String> parentCrudIdCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(component).setParentCrudElementId(parentCrudIdCaptor.capture());

        ArgumentCaptor<String> propertyPathCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(component).setParentCrudElementBindingPropertyPath(propertyPathCaptor.capture());

        String parentCrudId = parentCrudIdCaptor.getValue();
        String propertyPath = propertyPathCaptor.getValue();

        assertNotNull(propertyPath);
        assertEquals("testPropPath", propertyPath);

        assertNotNull(parentCrudId);
        assertEquals("parentCrudPanel", parentCrudId);
    }

    @Test
    @DisplayName("Should find mdc id and property path when the component is in a detail-panel and bound to an mdc")
    void test_should_find_mdc_id_and_property_path_when_the_component_is_in_a_detail_panel_and_bound_to_an_mdc(
        @LoadElement(elemantName = "uibuilder-multi-value", value = "/parent-mdc-with-detail-panel.html") Element element) {

        UIBuilderMultiValue<?, ?> component = mock(UIBuilderMultiValue.class);

        testObj.intercept(component, element);

        ArgumentCaptor<String> parentCrudIdCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(component).setParentCrudElementId(parentCrudIdCaptor.capture());

        ArgumentCaptor<String> propertyPathCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(component).setParentCrudElementBindingPropertyPath(propertyPathCaptor.capture());

        String parentCrudId = parentCrudIdCaptor.getValue();
        String propertyPath = propertyPathCaptor.getValue();

        assertNotNull(propertyPath);
        assertEquals("testPropPath", propertyPath);

        assertNotNull(parentCrudId);
        assertEquals("mdcId", parentCrudId);
    }

    @Test
    @DisplayName("Should find mdc id and property path when the component is in an editor-window and bound to an mdc")
    void test_should_find_mdc_id_and_property_path_when_the_component_is_in_an_editor_window_and_bound_to_an_mdc(
        @LoadElement(elemantName = "uibuilder-multi-value", value = "/parent-mdc-with-detail-panel.html") Element element) {

        UIBuilderMultiValue<?, ?> component = mock(UIBuilderMultiValue.class);

        testObj.intercept(component, element);

        ArgumentCaptor<String> parentCrudIdCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(component).setParentCrudElementId(parentCrudIdCaptor.capture());

        ArgumentCaptor<String> propertyPathCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(component).setParentCrudElementBindingPropertyPath(propertyPathCaptor.capture());

        String parentCrudId = parentCrudIdCaptor.getValue();
        String propertyPath = propertyPathCaptor.getValue();

        assertNotNull(propertyPath);
        assertEquals("testPropPath", propertyPath);

        assertNotNull(parentCrudId);
        assertEquals("mdcId", parentCrudId);
    }

    @Test
    @DisplayName("Should be applicable if tag name equals")
    void test_should_be_applicable_if_tag_name_equals() {
        Element notApplicableElement1 = new Element("asd-qwe");
        Element notApplicableElement2 = new Element("div");
        Element applicatbleElement = new Element("uibuilder-multi-value");

        assertFalse(testObj.isApplicable(notApplicableElement1));
        assertFalse(testObj.isApplicable(notApplicableElement2));
        assertTrue(testObj.isApplicable(applicatbleElement));
    }

    @Test
    @DisplayName("Should be an instantiator only if the element contains an item-bind attribute")
    void test_should_be_an_instantiator_only_if_the_element_contains_an_item_bind_attribute() {
        Element notInstantiatorElement = new Element("uibuilder-multi-value");
        Element instantiatorElement = new Element("uibuilder-multi-value");
        instantiatorElement.attr("item-bind", "anything");

        assertFalse(testObj.isInstantiator(notInstantiatorElement));
        assertTrue(testObj.isInstantiator(instantiatorElement));
    }

    @Test
    @DisplayName("Should instantiate element")
    void test_should_instantiate_element() {
        Component component = testObj.instantiateComponent();
        assertNotNull(component);
        assertTrue(component instanceof UIBuilderMultiValue);
    }

}
