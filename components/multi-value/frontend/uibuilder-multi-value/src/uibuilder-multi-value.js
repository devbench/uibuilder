/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { templatize } from '@polymer/polymer/lib/utils/templatize.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { ElementMixin } from '@vaadin/component-base/src/element-mixin.js';
import '../../uibuilder-form/src/form-localdate.js';


class UibuilderMultiValueInstanceDescriptor {

    constructor(model, container) {
        this.model = model;
        this.container = container;
        this.locked = false;
        this.removeButton = null;
    }

}

export class UibuilderMultiValue extends ElementMixin(ThemableMixin(PolymerElement)) {

    static get template() {
        return html`
            <div id="multiValueComponent" part="component" class="uibuilder-multi-value-component">

                <div id="multiValueContainer" part="container" class="uibuilder-multi-value-container">
                    <!-- value components -->
                </div>

                <div part="addButtonContainer" hidden$="[[addButtonHidden]]">
                    <vaadin-button id="multiValueAddButton" part="addButton"
                                   class="uibuilder-multi-value-add-button"
                                   on-click="_onAddClicked">
                    </vaadin-button>
                </div>

            </div>

            <slot></slot>
        `;
    }

    static get is() {
        return 'uibuilder-multi-value'
    }

    static get properties() {
        return {
            as: {
                type: String,
                value: "item"
            },

            value: {
                type: Array,
                value: [],
                notify: true,
                observer: '_onValueChange'
            },

            valueString: {
                type: String,
                value: "",
                notify: true
            },

            valueItemPath: {
                type: String,
                value: 'value',
                notify: true
            },

            items: {
                type: Array,
                value: [],
                notify: true,
            },

            itemSupplier: {
                type: String
            },

            addButtonTheme: {
                type: String,
                value: "tertiary success large",
                notify: true
            },

            removeButtonTheme: {
                type: String,
                value: "tertiary error large",
                notify: true
            },

            addButtonHidden: {
                type: Boolean,
                value: false,
                notify: true
            },

            removeButtonHidden: {
                type: Boolean,
                value: false,
                notify: true,
                observer: '_onRemoveButtonHiddenChange'
            },

            minNoValues: {
                type: Number,
                value: undefined,
                notify: true
            },

            maxNoValues: {
                type: Number,
                value: undefined,
                notify: true
            }
        };
    }

    constructor() {
        super();
        this._componentLevelInstanceIndex = 0;
    }

    ready() {
        super.ready();
    }

    connectedCallback() {
        super.connectedCallback();

        this._setupInstanceFactory();

        this._addButton = this.shadowRoot.querySelector("#multiValueAddButton");
        this._setupButton("addButton", this._addButton, "<vaadin-icon icon='vaadin:plus-circle-o'></vaadin-icon>");
        this._addButton.setAttribute("theme", this.addButtonTheme);
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }

    createInstance(props) {
        return new this._instanceFactory(props);
    }

    _setupInstanceFactory() {
        const instanceProps = this._createEmptyItem();
        const templateElement = this.querySelector("template:not([slot])");
        this.templateBindings = this._collectTemplateBindings(templateElement);
        if (!this._instanceFactory) {
            this._instanceFactory = templatize(templateElement, this, {
                parentModel: true,
                forwardHostProp(property, value) {
                },
                instanceProps: instanceProps,
                notifyInstanceProp(instance, property, value) {
                    this._onInstancePropChanged(instance, property, value);
                }
            });
        }
    }

    _setupButton(buttonTemplateSlotName, button, defaultContent) {
        const buttonTemplate = this.querySelector("template[slot='" + buttonTemplateSlotName + "']");
        if (buttonTemplate) {
            const buttonTemplateFactory = templatize(buttonTemplate);
            const buttonInstance = new buttonTemplateFactory({});
            const buttonRoot = buttonInstance.root;
            button.appendChild(buttonRoot);
        } else {
            button.innerHTML = defaultContent;
        }
    }

    _onInstancePropChanged(instance, property, value) {
        if (this._instances) {

            if (property === this.as) {
                Object.keys(value).forEach(key => {
                    const keyProperty = this.as + "." + key;
                    const keyValue = value[key];
                    this._onInstancePropChanged(instance, keyProperty, keyValue);
                });
            } else {
                const instanceDescriptor = this._instances.find(instanceDescriptor => instanceDescriptor.model === instance);
                if (instanceDescriptor) {
                    const instanceIndex = this._instances.indexOf(instanceDescriptor);
                    const propertyElement = this._findPropertyElement(instance, this._getPropertyWithoutAs(property));
                    const propertyElementId = propertyElement && propertyElement.hasAttribute("id") ?
                        propertyElement.getAttribute("id") : "";
                    this.dispatchEvent(new CustomEvent("instance-changed", {
                        detail: {
                            index: instanceIndex,
                            propertyPath: property,
                            propertyValue: value,
                            propertyElementId: propertyElementId
                        }
                    }));
                    this._fireComponentStateChangeEvent();
                }
                if (instanceDescriptor && !instanceDescriptor.locked) {
                    this._collectValues();
                }
            }
        }
    }

    _fireComponentStateChangeEvent() {
        this.dispatchEvent(new CustomEvent("component-state-changed"));
    }

    _onValueChange(newValue, oldValue) {
        this._propagateValues();
        this._updateValueString();
    }

    _createEmptyItem() {
        let instanceProps = {}
        instanceProps[this.as] = {}
        return instanceProps;
    }

    _pushItemValue(index, props) {
        if (this._instances && this._instances.length > index) {
            const instanceDescriptor = this._instances[index];
            if (instanceDescriptor) {
                instanceDescriptor.locked = true;
                try {
                    instanceDescriptor.model.setProperties(props, false);
                } finally {
                    instanceDescriptor.locked = false;
                }
                this._collectValues();
                this._updateMinMaxNoValuesConstraint();
            }
        }
    }

    _addInstance(userAdded, props) {
        const instance = this.createInstance(props);

        const instanceContainer = document.createElement("div");
        instanceContainer.setAttribute("class", "uibuilder-multi-value-instance-container");

        const instanceDescriptor = new UibuilderMultiValueInstanceDescriptor(instance, instanceContainer);

        const removeButton = document.createElement("vaadin-button");
        this._setupButton("removeButton", removeButton, "<vaadin-icon icon='vaadin:minus-circle-o'></vaadin-icon>");
        removeButton.setAttribute("class", "uibuilder-multi-value-remove-button");
        removeButton.setAttribute("theme", this.removeButtonTheme);
        removeButton.hidden = this.removeButtonHidden;
        removeButton.addEventListener("click", ev => {
            this._removeInstance(instanceDescriptor);
            this._updateMinMaxNoValuesConstraint();
        });

        instanceDescriptor.removeButton = removeButton;

        const instanceRoot = instance.root;
        instanceContainer.appendChild(instanceRoot);
        instanceContainer.appendChild(removeButton);

        this.shadowRoot
            .querySelector("#multiValueContainer")
            .appendChild(instanceContainer);

        this._instances = this._instances ? this._instances : [];
        this._instances.push(instanceDescriptor);

        const instanceIndex = this._instances.indexOf(instanceDescriptor);

        this._updateMinMaxNoValuesConstraint();

        let templateHtmlContent = this._ensureComponentIdTemplateHtmlContent(instanceContainer);
        let innerBindings = this._collectInnerBindings(instanceRoot);
        let innerBindingsElementIdMap = this._collectPropertyPathElementIdMap(instance);

        this.dispatchEvent(new CustomEvent("instance-added", {
            detail: {
                userAdded: userAdded,
                index: instanceIndex,
                templateHtmlContent: templateHtmlContent,
                innerBindings: innerBindings,
                innerBindingsElementIdMap: innerBindingsElementIdMap
            }
        }));
        this._fireComponentStateChangeEvent();

        this._collectValues();
    }

    _collectPropertyPathElementIdMap(instanceModel) {
        let innerBindingsElementIdMap = {};

        this.templateBindings.forEach(propertyPath => {
            const element = this._findPropertyElement(instanceModel, this._getPropertyWithoutAs(propertyPath));
            if (element && element.hasAttribute("id")) {
                innerBindingsElementIdMap[propertyPath] = element.getAttribute("id");
            }
        });

        return innerBindingsElementIdMap;
    }

    _getComponentLevelInstanceIndexAndIncrementByOne() {
        return this._componentLevelInstanceIndex++;
    }

    _ensureComponentIdTemplateHtmlContent(instanceContainer) {
        const ID = "id";
        let rootId = this.getAttribute(ID);
        if (!rootId) {
            rootId = "generated-multi-value-instance-id";
        }
        this.elementCounter = this.elementCounter ? this.elementCounter : 1;
        [...instanceContainer.querySelectorAll("*")]
            .filter(element => element.tagName)
            .filter(element => element.tagName.toLowerCase() !== "template")
            .forEach(element => {
                if (!element.hasAttribute(ID)) {
                    element.setAttribute(ID, rootId + "-" + this.elementCounter);
                }
                element.setAttribute(ID, element.getAttribute(ID) + "-index-" +
                    this._getComponentLevelInstanceIndexAndIncrementByOne());
                this.elementCounter++;
            });
        return instanceContainer.innerHTML;
    }

    _removeRegisteredComponentsFromParent(instanceContainer) {
        [...instanceContainer.querySelectorAll("*[id]")]
            .forEach(element => {
                delete this.$[element.getAttribute("id")];
            });
    }

    _collectInnerBindings(instanceRoot) {
        let innerBindings = [];

        const rootNodeList = instanceRoot.nodeList;
        if (rootNodeList) {
            rootNodeList
                .filter(node => node.hasChildNodes())
                .forEach(node => {
                    const template = node.querySelector("template");
                    this._collectTemplateBindings(template)
                        .forEach(innerBinding => innerBindings.push(innerBinding));
                });
        }

        return innerBindings;
    }

    _collectTemplateBindings(templateElement) {
        let innerBindings = [];
        if (templateElement && templateElement._templateInfo && templateElement._templateInfo.nodeInfoList) {
            templateElement._templateInfo.nodeInfoList.forEach(nodeInfo => {
                if (nodeInfo.bindings) {
                    nodeInfo.bindings.forEach(binding => {
                        if (binding.parts) {
                            binding.parts
                                .filter(part => part.source && part.mode === "{")
                                .forEach(part => {
                                    innerBindings.push(part.source);
                                });
                        }
                    })
                }
            });
        }
        return innerBindings;
    }

    _updateMinMaxNoValuesConstraint() {
        if (this._instances) {
            if (this.minNoValues) {
                const disableRemoveButton = this.minNoValues >= this._instances.length;
                this._instances.forEach(instanceDescriptor => instanceDescriptor.removeButton.disabled = disableRemoveButton);
            }
            if (this.maxNoValues) {
                this._addButton.disabled = this.maxNoValues <= this._instances.length;
            }
        }
    }

    _onRemoveButtonHiddenChange() {
        if (this._instances) {
            this._instances.forEach(instanceDescriptor => instanceDescriptor.removeButton.hidden = this.removeButtonHidden);
        }
    }

    _removeInstance(instanceDescriptor, collect = true) {
        let instanceIndex = this._instances.indexOf(instanceDescriptor);

        this.dispatchEvent(new CustomEvent("instance-removed", {
            detail: {
                index: instanceIndex
            }
        }));
        this._fireComponentStateChangeEvent();
        this._removeRegisteredComponentsFromParent(instanceDescriptor.container);
        this.shadowRoot
            .querySelector("#multiValueContainer")
            .removeChild(instanceDescriptor.container);

        this._instances = this._instances.filter(descriptor => descriptor !== instanceDescriptor);
        if (collect) {
            this._collectValues();
        }
    }

    _collectValues() {
        if (this.valueItemPath && this.valueItemPath.trim() !== "" && this._instances) {
            let newValue = this._instances.map(instanceDescriptor => instanceDescriptor.model.get(this.as + "." + this.valueItemPath));
            if (this.value.nodeId) {
                newValue.nodeId = this.value.nodeId;
            }
            this.value = newValue;
            this._updateValueString();
        } else {
            console.warn("No value item path has been set");
        }
    }

    _updateValueString() {
        if (this.value) {
            this.valueString = this.value.join(", ");
        }
    }

    _propagateValues() {
        if (this.value && this.valueItemPath) {

            if (!this._instances) {
                if (this.value.length > 0) {
                    let items = [];
                    this.value.forEach(val => {
                        let item = this._createEmptyItem();
                        item[this.as][this.valueItemPath] = val;
                        items.push(item);
                    });
                    this.setItems(items);
                }
            } else {
                if (this._instances) {

                    if (this.value.length < this._instances.length) {
                        for (let i = this._instances.length - 1; i > this.value.length - 1; i--) {
                            const instanceDescriptor = this._instances[i];
                            this._removeInstance(instanceDescriptor, false);
                        }
                    }

                    for (let i = 0; i < this._instances.length; i++) {
                        const instanceDescriptor = this._instances[i];
                        instanceDescriptor.locked = true;
                        try {
                            instanceDescriptor.model.set(this.as + "." + this.valueItemPath, this.value[i]);
                        } finally {
                            instanceDescriptor.locked = false;
                        }
                    }

                    if (this.value.length > this._instances.length) {
                        for (let valIndex = this._instances.length; valIndex < this.value.length; valIndex++) {
                            this._addInstance(false, this._createEmptyItem());
                            const instanceDescriptor = this._instances[valIndex];
                            instanceDescriptor.locked = true;
                            try {
                                instanceDescriptor.model.set(this.as + "." + this.valueItemPath, this.value[valIndex]);
                            } finally {
                                instanceDescriptor.locked = false;
                            }
                        }
                    }
                }
            }
        }
    }

    _onAddClicked() {
        this._addInstance(true, this._createEmptyItem());
    }

    setItems(items) {
        if (items && items.length >= 0) {
            if (this._instances && this._instances.length > items.length) {
                for (let i = this._instances.length - 1; i >= items.length; i--) {
                    const instanceDescriptor = this._instances[i];
                    this._removeInstance(instanceDescriptor);
                }
            }
            for (let i = 0; i < items.length; i++) {
                const item = items[i];
                if (!this._instances || this._instances.length - 1 < i) {
                    this._addInstance(false, item);
                } else {
                    this._instances[i].model.setProperties(item, false);
                }
            }
        }
        this._collectValues();
    }

    resetInstanceValidity() {
        if (this._instances) {
            this._instances.forEach(instanceDescriptor => {
                instanceDescriptor.model.__templateInfo.nodeList.forEach(node => {
                    node.invalid = false;
                    node.errorMessage = null;
                })
            });
        }
    }

    applyInstanceValidity(propertyPath, errorMessage, instanceIndex) {
        if (this._instances) {
            if (this._instances.length > instanceIndex) {
                let bindingPath = null;
                if (propertyPath) {
                    bindingPath = propertyPath;
                } else if (this.valueItemPath) {
                    bindingPath = this.valueItemPath;
                }

                if (bindingPath) {
                    const instanceDescriptor = this._instances[instanceIndex];
                    const subElementIndex = this._findNodeIndexForProperty(instanceDescriptor.model, bindingPath);
                    if (subElementIndex !== -1) {
                        const subElement = instanceDescriptor.model.__templateInfo.nodeList[subElementIndex];
                        if (subElement) {
                            subElement.invalid = true;
                            subElement.errorMessage = errorMessage;
                        }
                    }
                }
            }
        }
    }

    _findPropertyElement(model, property) {
        const subElementIndex = this._findNodeIndexForProperty(model, property);
        if (subElementIndex !== -1) {
            return model.__templateInfo.nodeList[subElementIndex];
        } else {
            return null;
        }
    }

    _findNodeIndexForProperty(model, property) {
        const propertyBinding = this.as + "." + property;

        if (model && model.__dataHost && model.__dataHost._templateInfo && model.__dataHost._templateInfo.nodeInfoList) {
            const nodeInfoList = model.__dataHost._templateInfo.nodeInfoList;
            for (let index = 0; index < nodeInfoList.length; index++) {
                const nodeInfo = nodeInfoList[index];
                if (nodeInfo.bindings) {
                    for (let i = 0; i < nodeInfo.bindings.length; i++) {
                        const binding = nodeInfo.bindings[i];
                        if (binding.parts) {
                            for (let j = 0; j < binding.parts.length; j++) {
                                const part = binding.parts[j];
                                if (part && part.source && part.source === propertyBinding) {
                                    return index;
                                }
                            }
                        }
                    }
                }
            }
        }

        return -1;
    }

    _getPropertyWithoutAs(propertyWithAs) {
        return propertyWithAs && propertyWithAs.startsWith(this.as + ".") ?
            propertyWithAs.substr((this.as + ".").length) : propertyWithAs;
    }

}

customElements.define(UibuilderMultiValue.is, UibuilderMultiValue);
