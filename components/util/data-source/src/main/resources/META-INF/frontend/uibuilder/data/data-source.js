import { PolymerElement } from '@polymer/polymer/polymer-element.js';

export class DataSource extends PolymerElement {
    static get is() {
        return "data-source";
    }

    static get properties() {
        return {
            name: String,
            defaultQuery: String,
            hierarchical: {
                type: String,
            },
            datasourceId: String,
        };
    }
}

customElements.define(DataSource.is, DataSource);
