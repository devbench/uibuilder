/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.util.datasource;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import com.vaadin.flow.dom.DisabledUpdateMode;

@DomEvent(value = "dataSourceReady", allowUpdates = DisabledUpdateMode.ALWAYS)
public class DataSourceReadyEvent extends ComponentEvent<Component> {

    private final String dataSourceId;
    private final String dataSourceName;
    private final String defaultQuery;

    public DataSourceReadyEvent(Component source, boolean fromClient,
                                @EventData("event.detail.dataSource.id") String dataSourceId,
                                @EventData("event.detail.dataSource.name") String dataSourceName,
                                @EventData("event.detail.dataSource.defaultQuery") String defaultQuery) {
        super(source, fromClient);
        this.dataSourceId = dataSourceId;
        this.dataSourceName = dataSourceName;
        this.defaultQuery = defaultQuery;
    }

    public String getDataSourceId() {
        return dataSourceId;
    }

    public String getDataSourceName() {
        return dataSourceName;
    }

    public String getDefaultQuery() {
        return defaultQuery;
    }
}
