/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.util.datasource;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.data.api.datasource.DataSourceDeposit;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import org.jsoup.nodes.Element;
import java.util.Optional;

public class DataSourceTagBasedParseInterceptor extends BaseTagBasedParseInterceptor<DataSourceBindingContext> {

    static final String DATA_SOURCE_TAG_NAME = "data-source";
    static final String DEFAULT_QUERY_ATTRIBUTE_NAME = "default-query";
    static final String HIERARCHICAL = "hierarchical";

    public boolean appliesToSubElements() {
        return true;
    }

    @Override
    public DataSourceBindingContext createBindingContext(Element element) {
        DataSourceBindingContext context;

        Element dataSourceElement = DataSourceUtils.findOneChildren(element, DATA_SOURCE_TAG_NAME);
        Optional<DataSourceBindingContext> foundBindingContext = tryBindingContextFromDeposit(dataSourceElement);
        context = new DataSourceBindingContext();

        if (foundBindingContext.isPresent()) {

            foundBindingContext.get().copy(context);
            context.setDataSourceId(DataSourceUtils.replaceDataSourceId(dataSourceElement, true));

        } else {

            parseCommonBindingContextElements(dataSourceElement, element, context);
            if (dataSourceElement.hasAttr(DEFAULT_QUERY_ATTRIBUTE_NAME)) {
                context.setDefaultQueryName(dataSourceElement.attr(DEFAULT_QUERY_ATTRIBUTE_NAME));
            }
            setupRepeatingBinding(dataSourceElement, context);
            context.setDataSourceId(DataSourceUtils.replaceDataSourceId(dataSourceElement));

        }

        return context;
    }

    private void setupRepeatingBinding(Element dataSourceElement, DataSourceBindingContext context) {
        if (dataSourceElement.hasAttr(HIERARCHICAL)) {
            mapHierarchicalBinging(dataSourceElement, context);
        }
    }

    private void mapHierarchicalBinging(Element dataSourceElement, DataSourceBindingContext context) {
        final BaseNestedBinding baseNestedBinding = new BaseNestedBinding();
        if (!dataSourceElement.attr(HIERARCHICAL).isEmpty()) {
            baseNestedBinding.setParent(dataSourceElement.attr(HIERARCHICAL));
        }
        context.setNestedBinding(baseNestedBinding);
    }

    @Override
    public void commitBindingParse(DataSourceBindingContext bindingContext, Component component) {
        bindingContext.commit();
        DataSourceManager.getInstance().getDataSourceProvider()
            .registerBindingContextForDataSource(
                bindingContext,
                Optional.ofNullable(bindingContext.getDefaultQueryName())
                    .map(queryName -> new CommonDataSourceSelector(queryName, component))
                    .orElse(null));

        if (component == null) {
            DataSourceDeposit.getInstance().add(bindingContext.getDataSourceName(), bindingContext);
        }
    }

    @Override
    public boolean isApplicable(Element element) {
        return DataSourceUtils.isChildrenExist(element, DATA_SOURCE_TAG_NAME);
    }

    @Override
    public void intercept(Component component, Element element) {
        //ignore
    }
}
