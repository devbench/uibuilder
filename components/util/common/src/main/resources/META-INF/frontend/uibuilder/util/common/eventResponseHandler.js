/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(() => {
    let reqId = 0;
    const reqMap = new Map();
    window.Uibuilder = window.Uibuilder || {};
    window.Uibuilder.EventResponseHandler = window.Uibuilder.EventResponseHandler || {
        nextId() {
            const id = reqId++;
            if (reqId > Number.MAX_SAFE_INTEGER) {
                reqId = 0;
            }
            return id.toString(36);
        },

        resolve(reqId, value) {
            reqMap.get(reqId).resolve(value);
            reqMap.delete(reqId);
        },

        reject(reqId, value) {
            reqMap.get(reqId).reject(value);
            reqMap.delete(reqId);
        }
    };
    window.Uibuilder.ReactingEvent = class extends CustomEvent {
        constructor(eventName, initObject) {
            super(eventName, initObject);
            this._reqId = Uibuilder.EventResponseHandler.nextId();
            // #292 This needs to be here, because Edge and Safari doesn't allow new event prototype functions.
            this.onResponse = () => new Promise((resolve, reject) => reqMap.set(this._reqId, {resolve, reject}));
        }
    };
})();
