/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { unsafeCSS } from 'lit';

window.Uibuilder = window.Uibuilder || {};
window.Uibuilder.applyCurrentThemeModule = window.Uibuilder.applyCurrentThemeModule || function (element) {
    const themeFor = element.getAttribute('theme-for');
    const customElement = customElements.get(themeFor);

    if (customElement) {
        const styleElement = element.querySelector('template').content.querySelector('style');

        const memoizedTemplate = customElement.prototype._template;
        if (memoizedTemplate) {
            memoizedTemplate.content.appendChild(styleElement.cloneNode(true));
            window.Uibuilder.devLog("Theme (id:'" + element.getAttribute("id") + "') applied for custom element: " + customElement.name);
        } else {
            const elementStyles = customElement.elementStyles;
            if (elementStyles) {
                elementStyles.push(unsafeCSS(styleElement.textContent));
                window.Uibuilder.devLog("Theme (id:'" + element.getAttribute("id") + "') applied for custom element: " + customElement.name);
            }
        }
    }
};
window.Uibuilder.createThemeDomModuleElement = window.Uibuilder.createThemeDomModuleElement || function (id, themeFor, style) {
    const templateElement = document.createElement("template");
    templateElement.innerHTML = style;
    const domModule = document.createElement("dom-module");
    domModule.setAttribute("id", id);
    domModule.setAttribute("theme-for", themeFor);
    domModule.appendChild(templateElement);
    return domModule;
};
