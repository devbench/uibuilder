import './z-index-manager.js';

window.Uibuilder = window.Uibuilder || {};

Uibuilder.PopupMixin = superClass => class PopupMixin extends superClass {
    ready() {
        super.ready();
        this.addEventListener("click", () => Uibuilder.zIndexMngr.raise(this));
        this.addEventListener("opened-changed", event => {
            if (event.detail.value)
                setTimeout(() => this.raise());
        });
    }

    raise() {
        Uibuilder.zIndexMngr.raise(this);
    }

    connectedCallback() {
        super.connectedCallback();
        Uibuilder.zIndexMngr.add(this);
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        Uibuilder.zIndexMngr.remove(this);
    }
};
